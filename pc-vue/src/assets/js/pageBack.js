export default {
    init(callback) {
        setTimeout(() => {
            window.history.pushState({
                title: document.title,
                url: location.href
            }, document.title, location.href);
            addEventListener('popstate', (e) => {
                if (!e.state) {
                    callback();
                }
            })
        }, 2e3);
    }
}

function addEventListener(evt, fn) {
    if (typeof window.addEventListener != 'undefined') {
        window.addEventListener(evt, fn, false);
    } else if (typeof window.attachEvent != 'undefined') {
        window.attachEvent(evt, fn);
    }
}