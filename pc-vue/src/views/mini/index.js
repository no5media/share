// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import rootSize from '@/assets/js/rootSize.js'
// require('swiper/dist/css/swiper.css')
rootSize.browerUa();

import Confirm from '@/components/confirm/index'

Vue.use(Confirm);
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
    el: '#app',
    template: '<App/>',
    components: { App }
})