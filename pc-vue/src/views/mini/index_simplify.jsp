<%@page import="com.bruce.geekway.common.enumeration.PartnerEnum"%>
<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ page import="com.bruce.geekway.utils.XSSDefenseUtils" %>
<%@ page import="com.bruce.geekway.model.*" %>
<%@page import="com.bruce.geekway.app.model.*"%>
<%@page import="com.bruce.geekway.partner.model.*"%>
<%@ page import="com.bruce.geekway.constants.*" %>
<%@ page import="com.bruce.geekway.message.util.*" %>
<%@ page import="org.apache.commons.lang3.StringUtils"%>

<%
WyUserInfo wyUserInfo = (WyUserInfo)request.getAttribute(ConstFront.ATTRIBUTE_KEY_WY_USER_INFO);

String channel = (String)session.getAttribute("chn");
channel = XSSDefenseUtils.htmlEncode(channel);
channel = channel==null?"":channel;

SpChannel spChannel = (SpChannel)request.getAttribute("spChannel"); 
WxApp wxApp = (WxApp)request.getAttribute("wxApp");

String title = "1758微游戏";

if(wxApp!=null&&StringUtils.isNotBlank(wxApp.getName())){
	title = wxApp.getName()+"-1758.com";//默认显示的标题
}
%>

<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8" />
	<title>samplify_<%=title%></title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
   
   	<meta name="description" content="1758微游戏是h5手机页游第一平台,提供最热最好玩的h5游戏大全,微社区,开服表,礼包兑换码,手机页游排行榜,微信小游戏,更多不用下载立即玩的手机网页游戏尽在1758微游戏" />
	<meta name="keywords" content="1758微游戏,h5游戏,手机页游,${wxApp.name}" />
   
    <meta name="format-detection" content="telephone=no">    
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="mobile-web-app-capable" content="yes"/>
    <meta name="full-screen" content="true"/>
    <meta name="screen-orientation" content="portrait"/>
    <meta name="x5-fullscreen" content="true"/>
    <meta name="full-screen" content="yes"/>
    <meta name="360-fullscreen" content="true"/>
    <meta http-equiv="pragma" content="no-cache"/>
    <meta http-equiv="cache-control" content="no-cache"/>
    <meta http-equiv="cache" content="no-cache"/>
    <meta http-equiv="expires" content="0"/>
    <link rel="shortcut icon "  href="http://images.1758.com/images/48.ico">
    <link rel="stylesheet" href="http://wx.1758.com/static/common/css/base.css">
    <link rel="stylesheet" type="text/css" href="/static/spf/mini/css/index.00b1d5835bd4c4685e44f30ad0488b29.css"/>
	
    <script type="text/javascript">
		if(top != self){  
		    top.location = self.location;
		}
	</script>
    
    
    <%
    String domain = ConstConfig.MOBILE_DOMAIN;
    int payVersion = 3;//支付版本，默认为3
    boolean forceEnable = false;//是否强制使用某支付版本
    %>
    <script type="text/javascript">
		<%
		//获取游戏对应的aid
		Integer aid = (Integer)request.getAttribute("aid"); 
		if(aid==null) aid = -1;
		%>
		
		/*配置信息*/
		var HLMY_CONFIG = {
			USER_INFO:{
				gid: "${userinfo_gid}",
				nickname:"${userinfo_nickname}",
				headUrl:"${userinfo_headUrl}",
				userType:"${userinfo_userType}",
				sex:"${userinfo_sex}", 
				userStatus:"${userinfo_userStatus}"
			},
			
			GAME_INFO:{
				panelEnable:${panelEnable},
				iconUrl:"${wxApp.iconUrl}",
				appKey:"${wxApp.appKey}",
				aid:${aid},
				name:"${wxApp.name}",
				trialEnable:${trialEnable},
				trialTime:${trialTime},
				hlmy_from:"${hlmy_from}",
				initial:"${initial}",
				cpGameUrl: '${cpGameUrl}'
			},
			BACK_INFO:{
				imgUrl:'http://images.1758.com/article/image/2018/01/01/30631514747930397.jpg',
				time: 40000
			},
			PAY_INFO:{
				version: "${wxApp.versionPay}",
				forceEnable: <%=forceEnable%>,
				forceVersion: <%=payVersion%>
			},
			CHN_INFO:{
				enable:false,
				chn:'<%=channel%>'
			},
			
		};

	</script>
</head>
<body>
	
	<div id="app"></div>
	 	<script src="//cdnjs.cloudflare.com/ajax/libs/vue/2.4.2/vue.min.js"></script>
  		<script src="//cdnjs.cloudflare.com/ajax/libs/axios/0.16.2/axios.min.js"></script>
		<script type="text/javascript" src="/static/spf/mini/js/index.ceb075694ad67cadab19.js"></script>

</body>
</html>
