<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ page import="com.bruce.geekway.model.*" %>
<%@ page import="org.apache.commons.lang.math.*" %>


<%
int v1758 = NumberUtils.toInt(request.getParameter("v1758"), 1);
%>

<!DOCTYPE html>
<html>
<head>
<script type="text/javascript">
	alert(<%=v1758%>)
</script>
<%if(v1758!=3){//支付3.0版本存活在iframe中，支付成功后无需跳出%>
<script>
if(top!=this){
	top.location.href = location.href;
}
</script>
<%}%>

<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<meta name="viewport" content="width=device-width initial-scale=1.0 maximum-scale=1.0 user-scalable=yes" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.js"></script>
<title>购买失败</title>

<style>

body{
	background: #f0eff5;
	padding: 0px;
	margin: 0px;
	font-size: 12px;
}

div.row{
	margin: 0px 0px 16px;
	background: #fff;
}

div.price-container{
	margin-bottom: 16px;
	padding: 5px;
	background:#fff;
	border-bottom: 1px solid #dddddd; 
}

.no-bottom{
	margin-bottom: 0px;
}

img.icon{
	width:26px;
	height:26px;
	vertical-align:middle;
	margin: 0px 6px 0px 10px;
}

.selector-group{
	margin: 4px 0px 2px; 
}

.btn{
	width:40px;
	margin: 0px 6px 6px 0px;
	padding: 8px 16px; 
	text-decoration: none;
	text-align: center;
	display:inline-block;
}

.btn-block{
	color: #fff;
	padding: 12px 20px;
	margin:20px 5px;
	text-decoration: none;
	display:block;
	text-align: center; 
	font-size:14px;
} 

.btn-active{ background-color: #08ba05;color: #fff;border: 1px solid #ffffff;}
.btn-inactive{ background-color: #fff; color:#000; border: 1px solid #dddddd;border-radius: 5px;}

.btn-green{ background-color: #08ba05;color: #fff;border-radius: 5px;}
.btn-orange{ background-color: orange;color: #fff;border-radius: 5px;} 
.btn-white{ background-color: #fff;color: #000;border-radius: 5px;border: 1px solid #dddddd;}
.btn-gray{ background-color: #fff; color:#000;border-radius: 5px;}

.buy-container{margin:40px 0px}   

#loadingText{ 
background:#e7e6ec;
padding: 3px 10px;
border-radius: 3px;
color:#8d8d8d;
}  

.loading-container{ 
margin:10px 0px; 
} 

.service{
text-align:center; 
color:#4584f6 
}
.copyrights{
margin-top:10px;
text-align:center; 
color:#acacac
}

</style> 


 <style>
.user-level{
            	float: right;
    background-color: red;
    color: #fff;
    font-size: 10px;
    padding: 0 2px;
    border-radius: 3px;
    vertical-align: bottom;
    line-height: 17px;
    margin-top: 5px;
    margin-left: 5px;
    height: 15px;
    display: inline-block;
}
            
.vip-tips{
	color:#9c9c9c;
	font-size: 13px;
	padding: 0px 12px;
	margin-bottom:12px;
}

.vip-tips a{
	color:#18a3e3;
	text-decoration:underline;
}

.vip-tips span{
	color:red;
}

.vip-tips img{
width: 18px;
vertical-align: bottom;
}

.color-blue{
	color:#18a3e3;
}

</style>
</head>
<body> 



<div class="row">
	<div style="border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd; padding:10px">
		<img class="icon" src="${wxApp.iconUrl}"/>
		<span style="margin-left:9px">${orderInfo.title}</span>
	</div>
</div> 

<div class="price-container">	 
	
	<div>
		<span style="color:#8d8d8d" >${orderInfo.title}</span> 
	</div>
	 
	<div style="padding:0px 10px; text-align:center;"> 
		
		 <%
         String errorCode = request.getParameter("errorCode");
         if("1".equals(errorCode)){%>
		<div class="loading-container" id="payResult">
			<strong style="color: red; font-size:28px;margin-right:3px">
			支付被拒绝
			</strong>
		</div>
		<div class="loading-container" id="payLoadingText" style="margin-bottom:8px">
			<span id="loadingText">点击<a class="color-blue" href="http://mp.weixin.qq.com/s?__biz=MjM5MjQyOTg3MA==&mid=201371889&idx=6&sn=5de0f56fc19cb770951e0b4190566f6c#rd">【1758微游戏】</a>进入该游戏，即可支付成功！</span>
		</div>
		<%}else{%>
		<div class="loading-container" id="payResult">
			<strong style="color: red; font-size:28px;margin-right:3px">
			支付失败
			</strong>
		</div>
		<div class="loading-container" id="payLoadingText" style="margin-bottom:8px">
			<span id="loadingText">支付失败，您可以重新购买</span>
		</div>
		<% } %>
	</div> 
</div> 

<%
WyVipDic userVipInfo = (WyVipDic)request.getAttribute("userVipInfo");
%>
<a href="javascript: prompt('您的1758微游戏用户ID:','${wyUser.userinfo.externalId}')"  style="color:#18a3e3;">
<div class="row">	
	<div style="color:#18a3e3; border-bottom: 1px solid #dddddd; padding:10px 5px; line-height:26px">
		1758游戏帐号
		<img class="icon" src="${wyUser.userinfo.head}" style="float:right"/>
		
		<%if(userVipInfo!=null){ %>
			<span class="user-level">${userVipInfo.name}</span>
		<%} %> 
		<span style="float:right;color:#8d8d8d">${wyUser.userinfo.nickname}</span>
	</div> 
</div>
</a>

<%if(userVipInfo!=null){%>
	<%if(userVipInfo.getLevel()!=null&&userVipInfo.getLevel()>=4){%>
	<p class="vip-tips">
	您是1758<span>VIP</span>用户，请加<span>VIP专属客服</span><a id="vipQQ"><img src="http://images.1758.com/game/m/qq.png">2622621758</a>，尊享专属服务。
	</p>
	<%} %>
<%}else{%>
	<p class="vip-tips">
	累计充值20元，即可成为<span>VIP</span>用户，尊享专属服务。
	</p>
<%} %>

<div class="buy-container">	
	<div style="padding:0px 10px">   
	<%if(v1758!=3){%>
		<a href="javascript:void(0)" id="rebuyBtn" class="btn-block btn-green">重新购买</a>
	<%}%>
		<a href="javascript:void(0)" id="backGameBtn" class="btn-block btn-white">返回游戏</a>
	</div>
</div>

<div class="footer">
	<%
	String chn = request.getParameter("chn");
	if(!"tcl".equals(chn)){
	%>
	<div class="service">
		<span>客服QQ:2660866818</span>
		<!-- 
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<span><a href="http://mp.weixin.qq.com/s?__biz=MjM5MjQyOTg3MA==&mid=201371889&idx=4&sn=60284684b7c6fdd357ea7c41edbd0ad8#rd">微支付遇到问题？</a></span>
	 	-->
	</div>     
	<%} %>
	<div class="copyrights">Copyright©1758微游戏 wx.1758.com</div> 
	
</div>

</body>

<script>
getVipQQKefu();
function getVipQQKefu(){
	var ua = navigator.userAgent.toLocaleLowerCase();
	if(ua.indexOf('micromessenger') > 0 || ua.indexOf('qq/') > 0 || ua.indexOf('mqqbrowser') > 0){
		$('#vipQQ').attr('href','http://wpa.qq.com/msgrd?v=3&uin=2622621758&site=qq&menu=yes');
	}else{
		$(document).on('click','#vipQQ',function(){
			prompt('VIP专属客服QQ','2622621758');
		})
	}
}
$("#backGameBtn").click(function(e) {
	<%if(v1758!=3){%>
		var url = "${payFailedUrl}";
		location.href=url;
	<%}else{//支付3.0版本存活在iframe中，支付直白后无需跳出%>
		//TODO 关闭iframe
		top.postMessage({hlmy:true,type:'pay',value:{fn:'payCancleBack',args:[]}}, '*');
	<%}%>
});

$("#rebuyBtn").click(function(e) {
	var rebuyUrl = "${rebuyUrl}";
	<%if(v1758!=3){%>
		location.href = rebuyUrl;  
	<%}else{//支付3.0版本存活在iframe中，重新购买操作需跳出iframe%>
		top.location.href = rebuyUrl;  
	<%}%>
	
	
});

</script>


<jsp:include page="../inc/wxOptionSettings.jsp"></jsp:include>
</html>
