<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ page import="com.wenyan.platform.partner.*" %>
<%@ page import="com.wenyan.article.entity.*" %>
<%@ page import="com.wenyan.app.entity.*" %>
<%@ page import="com.bruce.hlmy.rpcdef.message.model.*" %>
<%@ page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="org.apache.commons.lang3.time.DateUtils"%>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>


<%
WxApp wxApp = (WxApp)request.getAttribute("wxApp");
String title = "1758微游戏";
if(wxApp!=null&&StringUtils.isNotBlank(wxApp.getName())){
	title = wxApp.getName()+"-1758.com";
}

String channel = request.getParameter("chn");

String channelLogoImgUrl = null;
if("chiba".equals(channel)){//渠道［吃吧］特定处理
	channelLogoImgUrl = "http://images.1758.com/partner/chiba/logo.png";
}else if("wanmeng".equals(channel)){//渠道［万萌］特定处理
	channelLogoImgUrl = "http://images.1758.com/article/image/2016/05/12/24911463034957464.png"; 
}else if("bahaodianying".equals(channel)){//渠道［8号电影］特定处理
	channelLogoImgUrl = "http://images.1758.com/partner/bhyy/8game.png";
}else if("ycyh".equals(channel)){//渠道［有车以后］特定处理
	channelLogoImgUrl = "http://images.1758.com/article/image/2016/05/27/21181464334782932.png";
}
%>


<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title><%=title%></title>
	
	<script type="text/javascript">
		if (top != this) {
			top.location.href = location.href;
		}
	</script>
	
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="format-detection" content="telephone=no">    
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="mobile-web-app-capable" content="yes"/>
    <meta name="full-screen" content="true"/>
    <meta name="screen-orientation" content="portrait"/>
    <meta name="x5-fullscreen" content="true"/>
    <meta name="360-fullscreen" content="true"/>
    <meta http-equiv="pragma" content="no-cache"/>
    <meta http-equiv="cache-control" content="no-cache"/>
    <meta http-equiv="cache" content="no-cache"/>
    <meta http-equiv="expires" content="0"/>
    <link rel="shortcut icon "  href="http://images.1758.com/images/48.ico">
    <link rel="stylesheet" href="/spf/dist/css/game_4_test-5df50e8aea.css" type="text/css" />
    <link rel="stylesheet" href="/spf/dist/css/hweui_0_1-65bd4a8153.css" type="text/css" />
    <%if(StringUtils.isNotBlank(channelLogoImgUrl)){ %>
    <link rel="stylesheet" type="text/css" href="/spf/dist/css/start_0_1.css"/>
    <%} %>
    <script src="/spf/dist/scripts/metainfo_1_0.min.js"></script>
    <script type="text/javascript">
		var HLMY_GAMEINFO = '${userinfoJson}';
	</script>
	<style type="text/css">
		.modal-flag{
    		right: auto;
    		left: 0;
    		top: 50px;
    	}
    	
    	.loading-en img{
			width:60px;
		}
		.loading-wrapper {
		    bottom: 30px;
		}
		.k-foot{
			padding:8px 0;
		}
		.pay-wrapper{
			position: absolute;
			top: 0;
			left: 0;
		}
		.pay-wrapper iframe{
			vertical-align: bottom;
		}
	</style>
</head>
<body>
	<div class='loading-div' id='loading-div' style="position: absolute;z-index: 1000;top: 0;left: 0;">
		<%
		if(StringUtils.isNotBlank(channelLogoImgUrl)){//特定处理
			%>
			<div class="inner-loading">
		        <div class="loading-wrapper">
			        <div class="loading-des">
			        	<img src="http://images.1758.com/festival/gs9.png"/>
			        </div>
		        </div>
		        <div class="loading-logo">
		        	<img src="<%=channelLogoImgUrl%>" />
		        	<div class="loading-en">
		        		<img src="http://images.1758.com/festival/gs10.gif" />
		        		<p>游戏正在火速加载中</p>
		        	</div>
		        </div>
			</div>
		<%}else{%>
			<div class='inner-loading'>
				<img src='http://images.1758.com/article/image/2015/07/24/27251437716095239.png'>
				<p class="p-loading">游戏载入中...</p>
				<br />
				<p class="text-loading">1758微游戏·即点即玩</p>
			</div>
		<%} %>
		
	</div>
<!--
	<div style="height: 100%;width: 100%; position: absolute;top: 0;">
	<div style="height: 100%;width: 100%; ">
-->
	<div style="height: 100%;width: 100%;">
		<iframe id='gameframe' style="display:block" src="${cpGameUrl}"  class="iframe" scrolling="no" height=100% width=100% name="cpframe" frameborder='0'></iframe>
	</div>
	
	<div id="button" class="modal-flag">
		<img src='http://images.1758.com/ranking/fubiao.png'>
		<span class="point">●</span>
	</div>
	<div id="moban" class="moban">
		<div id="modalframe" class="modalframe"></div>
		<div id="modal-left" class="modal-left">
			<img src="http://wx.1758.com/game/h5/images/modalleft.png">
		</div>
	</div>
	<div id="shareTipInfo" class="share-tip gone">
    	<div class="inner-share-tip">
    		<div class="share-arrow"><img src="http://images.1758.com/festival/sharearrow.png"/></div>
    		<p class="s-title-big">分享有礼</p>
    		<p style="margin-top: 20px;">立即分享您将获到</p>
    		<div id="rewardWrapper" class="reward-wrapper"></div>
    		<p class="s-no-wx">
        		非微信用户请关注<span class="s-c-num2">1758微游戏</span>后再进行分享
    		</p>
			<div class="share-num">
        		<div class="pure-g">
        			<span class="text-l pure-u-1-2">微信号1&nbsp;</span> 
        			<span class="s-c-num1 pure-u-1-2">wx1758com</span>
    			</div>
        		<div class="pure-g">
        			<span class="text-l pure-u-1-2">微信号2&nbsp; </span> 
        			<span class="s-c-num1 pure-u-1-2">com1758</span>
        		</div>
        		<div class="pure-g">
        			<span class="text-l pure-u-1-2">客服QQ&nbsp;</span> 
        			<span class="s-c-num1 pure-u-1-2">2660866818</span>
        		</div>
			</div>
    	</div>
    </div>
	<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
	
	<%if(false){ %>
	<!-- 测试环境未压缩js -->
	<!-- <script type="text/javascript" src="/spf/dist/scripts/require-2.1.20.min.js" data-main="/spf/mweb/js/view/app.js"></script> -->
	<%} %>
	
	<!-- 线上环境压缩js -->
	<script type="text/javascript" src="/spf/dist/scripts/require-2.1.20.min.js" data-main="/spf/dist/scripts/game_4_2_test-5b059c0e4c.js"></script>
	
	<script>
		setTimeout(hide1758Mask, 3000);
		
		function hide1758Mask(){
			document.getElementById("loading-div").style.display="none";
			document.getElementById("gameframe").style.display="block";
		}
	</script>	
	
	<jsp:include page="./favorite/tipJs.jsp"></jsp:include>
</body>
</html>