<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ page import="java.util.*" %>
<%@page import="com.bruce.geekway.utils.CoinUtil"%>
<%@page import="com.bruce.geekway.enumeration.HlmyEnum"%>
<%@ page import="com.bruce.geekway.model.*" %>
<%@ page import="com.bruce.geekway.exception.ErrorCode" %>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<meta name="viewport" content="width=device-width initial-scale=1.0 maximum-scale=1.0 user-scalable=yes" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.js"></script>
<title>1758安全支付</title>
<style>

body{
	background: #f0eff5;
	padding: 0px;
	margin: 0px; 
	font-size: 12px;
}

div.row{
	margin: 0px 0px 16px;
	background: #fff;
}

div.price-container{
	margin: 8px 0px;
}

.no-bottom{
	margin-bottom: 0px;
}

img.icon{
	width:26px;
	height:26px;
	vertical-align:middle;
	margin: 0px 6px 0px 10px;
}

.selector-group{
	margin: 4px 0px 2px; 
}

.btn{
	margin: 0px 6px 6px 0px;
	padding: 8px 16px; 
	text-decoration: none;
	text-align: center;
	display:inline-block;
}

.btn-block{
	color: #fff;
	padding: 12px 20px;
	margin:15px 5px; 
	text-decoration: none;
	display:block;
	text-align: center; 
	font-size:14px;
}

.btn-active{ background-color: #08ba05;color: #fff;border: 1px solid #ffffff;}
.btn-inactive{ background-color: #fff; color:#000; border: 1px solid #dddddd;border-radius: 5px;}

.btn-green{ background-color: #08ba05;color: #fff;border-radius: 5px;}
.btn-orange{ background-color: #ff6d0d;color: #fff;border-radius: 5px;} 
.btn-white{ background-color: #fff;color: #000;border-radius: 5px;border: 1px solid #dddddd;}
.btn-gray{ background-color: #fff; color:#000;border-radius: 5px;}
.btn-1758{ background-color: #ff9103; color:#fff;border-radius: 5px;}
.btn-dxty{ background-color: #2196ff; color:#fff;border-radius: 5px;}
.btn-honey{ background-color: #fb3416; color:#fff;border-radius: 5px;}

.buy-container{margin:20px 0px}  

.service{
text-align:center; 
color:#4584f6
}
.copyrights{
margin-top:10px;
text-align:center; 
color:#acacac
}

.gone{
display:none;
}
.other-gold{
	position: absolute;
	top: 15px;
	right:30px;
	color: #9c9c9c;
}
.other-top:before,
.other-top:after,
.other-bottom:before,
.other-bottom:after{
	border: solid transparent;
	width: 0;
	height: 0;
	position: absolute;
	left: 100%;
	content: ' ';
	margin-left: 4px;
	border-width: 7px;
	top: 0px; 
}
.other-top:before{
	top: -3px;
	border-bottom-color: #000;
}
.other-top:after{
	top: -3px;
	border-bottom-color: #fff;
}
.other-bottom:before{
	top: 4px;
	border-top-color: #000;
}
.other-bottom:after{
	top: 4px;
	border-top-color: #fff;
}

</style>


<style>
.user-level {
	float: right;
	background-color: red;
	color: #fff;
	font-size: 10px;
	padding: 0 2px;
	border-radius: 3px;
	vertical-align: bottom;
	line-height: 17px;
	margin-top: 5px;
	margin-left: 5px;
	height: 15px;
	display: inline-block;
}

.vip-tips {
	color: #9c9c9c;
	font-size: 13px;
	padding: 0px 12px;
	margin-bottom: 12px;
}

.vip-tips a {
	color: #18a3e3;
	text-decoration: underline;
}

.vip-tips span {
	color: red;
}

.vip-tips img {
	width: 18px;
	vertical-align: bottom;
}

.color-blue {
	color: #18a3e3;
}
</style>


<script>
	var _hmt = _hmt || [];
	(function() {
		var hm = document.createElement("script");
		hm.src = "//hm.baidu.com/hm.js?ad8d1c4cd485d1427b0a9b87dad63d83";
		var s = document.getElementsByTagName("script")[0];
		s.parentNode.insertBefore(hm, s);
	})();
</script>

</head>
<body> 

<%
	com.bruce.geekway.interceptor.WyUser wyUser = (com.bruce.geekway.interceptor.WyUser)request.getAttribute("wyUser");

 	List<WxProduct> allAppProducts = (List<WxProduct>) request.getAttribute("allAppProducts");
 	WxProduct currentProduct = (WxProduct) request.getAttribute("product");
 %>
	
<div class="row">
	<div style="border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd; padding:10px">
		<img class="icon" src="${wxApp.iconUrl}"/>
		<span style="margin-left:5px">${wxApp.name}</span> - <span id="titleName">${product.name}</span>
	</div>
	
	<input type="hidden" id="itemCode" name="itemCode" value="${product.itemCode}"/>
	<input type="hidden" id="price" name="price" value="${product.price}"/>
	
	<div style="margin-top:10px;padding-bottom: 10px"><span id="descText" style="color:red">${wxApp.payDescription}</span></div>
</div>


<%
WyVipDic userVipInfo = (WyVipDic)request.getAttribute("userVipInfo");
%>
<a href="javascript: prompt('您的1758微游戏用户ID:','${wyUser.userinfo.externalId}')"  style="color:#18a3e3;">
<div class="row">
        <div style="color:#18a3e3; border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd; padding:10px 5px; line-height:26px">
                1758游戏帐号
                <img class="icon" src="${wyUser.userinfo.head}" style="float:right"/>
                
                <%
                if(userVipInfo!=null){%>
                	<span class="user-level">${userVipInfo.name}</span>
                <%}%>
                <span style="float:right;color:#8d8d8d">${wyUser.userinfo.nickname} </span>
                
        </div>
</div>
</a>


<%if(userVipInfo!=null){%>
	<%if(userVipInfo.getLevel()!=null&&userVipInfo.getLevel()>=4){%>
	<p class="vip-tips">
	您是1758<span>VIP</span>用户，请加<span>VIP专属客服</span><a id="vipQQ"><img src="http://images.1758.com/game/m/qq.png">2622621758</a>，尊享专属服务。
	</p>
	<%} %>
<%}else{%>
	<p class="vip-tips">
	累计充值20元，即可成为<span>VIP</span>用户，尊享专属服务。
	</p>
<%} %>

<div class="price-container">
	<div style="padding:0px 5px; text-align: center;">
	<strong style="color: #fc7b11; font-size:26px;margin-right:3px" id="priceDisplay">${product.price}</strong><span>元</span>
	</div>
</div>

<%
if(allAppProducts!=null&&allAppProducts.size()>0){%>
<div class="buy-container">	
	<div style="padding:0px 10px;">
		<a id="submitWxPay" class="btn-block btn-green pay alipay gone">微信支付</a>
		<a id="submitAlipay" class="btn-block btn-orange pay wxpay">支付宝支付</a>
		<%
			//判断用户积分是否足够兑换(暂时不需要了)
				WxApp app = (WxApp)request.getAttribute("wxApp");
				//if (app.getId()!=107688 && app.getId()!=107695) {//1758充值、亲密付等两个应用不能使用1758支付
				//boolean bonusEnable = CoinUtil.isCoinBonusEnable() && CoinUtil.isSupportBonus(app.getId());
				boolean bonusEnable = CoinUtil.isSupportCoinPay(app.getId());
				if (bonusEnable || app.getId()==107576||app.getId()==107709||app.getId()==107593||app.getId()==107748||app.getId()==107794||app.getId()==107738){
		%>
			<a id="submit1758" class="btn-block btn-1758 pay wxpay">1758币支付</a>
		<%}
		//if (app.getId()!=107688 && app.getId()!=107695) {//1758充值、亲密付等两个应用不能使用1758支付
		if (app.getId()==107694||app.getId()==107631||app.getId()==107718||app.getId()==107576) {%><!-- //1758充值、亲密付等两个应用不能使用1758支付  -->
			<a id="submitDonate" class="btn-block btn-honey pay wxpay">亲密付</a>
		<%}%>
		<%if (app.getId()!=107688 && app.getId()!=107695) { %>
		<a id="backGameBtn" class="btn-block btn-white">返回游戏</a>
		<%} %>
	</div>
</div>
<%}%>

<div class="footer">
	<div class="service"><span>客服QQ:2660866818</span>
	<a target="_blank" href="http://jq.qq.com/?_wv=1027&k=bqsHhO"><img border="0" src="http://pub.idqqimg.com/wpa/images/group.png" alt="1758微游戏粉丝群" title="1758微游戏粉丝群"></a>
	<!-- 
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><a href="http://mp.weixin.qq.com/s?__biz=MjM5MjQyOTg3MA==&mid=201371889&idx=4&sn=60284684b7c6fdd357ea7c41edbd0ad8#rd">支付遇到问题？</a></span> 
	-->
	</div>
	<div class="copyrights">Copyright©1758微游戏 wx.1758.com</div>
</div>

</body>

<%boolean hideWxPay = "caishen".equalsIgnoreCase(wyUser.getUserinfo().getChannelType()) || "qipaiapp".equalsIgnoreCase(wyUser.getUserinfo().getChannelType());%>

<script>
var fromWxClient = false;
var isNativeApp = false;
var isThirdpay = false;

var ResData = {};
if(is_client() && getVersion()){
	isNativeApp = true;
	<%if(!hideWxPay){//真对财神渠道的特殊处理，android客户端下无法唤起微信支付%>
		$("#submitWxPay").removeClass("gone");
	<%}%>
}

if(is_phone() == 'android' || is_phone() == 'ios'){
	isThirdpay=true;
	if(is_phone() == 'android' && isTa()){
	}else{
		<%if(!hideWxPay){//真对财神渠道的特殊处理，android客户端下无法唤起微信支付%>
			$("#submitWxPay").removeClass("gone");
		<%}%>
	}
	
}


if(is_weixin()){
	fromWxClient = true;
	isThirdpay=false;
	<%if(!hideWxPay){//真对财神渠道的特殊处理，android客户端下无法唤起微信支付%>
		$("#submitWxPay").removeClass("gone");
	<%}%>
}

function is_phone(){
	var usa=navigator.userAgent;
	var p = '';
	if(usa.indexOf("Android")>-1){
	    p = "android";
	} else if(usa.indexOf("iPhone")>-1 || usa.indexOf("iPad")>-1 || usa.indexOf("iPod")>-1){
	     p = "ios";
	}else{
	    p="pc";
	}
	return p;
}

function isTa(){
	var ua = navigator.userAgent;
	var pre = /v\//ig;
	return pre.test(ua);
}
getVipQQKefu();
function getVipQQKefu(){
	var ua = navigator.userAgent.toLocaleLowerCase();
	if(ua.indexOf('micromessenger') > 0 || ua.indexOf('qq/') > 0 || ua.indexOf('mqqbrowser') > 0){
		$('#vipQQ').attr('href','http://wpa.qq.com/msgrd?v=3&uin=2622621758&site=qq&menu=yes');
	}else{
		$(document).on('click','#vipQQ',function(){
			prompt('VIP专属客服QQ','2622621758');
		})
	}
}

$("#backGameBtn").click(function(e) {
	//location.href="${wxApp.backGameUrl}";
	top.postMessage({hlmy:true,type:'pay',value:{fn:'payCancleBack',args:[]}}, '*');
}); 

//当微信内置浏览器完成内部初始化后会触发WeixinJSBridgeReady事件。
	$(".pay").click(function(e) {
		var thisBtn = $(this);
		thisBtn.text("正在生成订单...");
		thisBtn.unbind();
		var eleId = thisBtn.attr("id");
		var payType =  <%=HlmyEnum.WPayTypeEnum.WPAY.getType()%>;
		if("submitAlipay"==eleId){//支付宝支付
			payType =  <%=HlmyEnum.WPayTypeEnum.ALIPAY.getType()%>;
		}else if("submitDxtykj"==eleId){//电信天翼空间支付
			payType =  <%=HlmyEnum.WPayTypeEnum.DXTYKJ.getType()%>;
		}else if("submit1758"==eleId){//1758币支付
			payType = <%=HlmyEnum.WPayTypeEnum.HLMY1758.getType()%>;
		}else if("submitDonate"==eleId){//亲密付
			payType =  <%=HlmyEnum.WPayTypeEnum.DONATE.getType()%>;
		}
		$(this).attr("src","javascript:void(0)");
		var itemCode = $("#itemCode").val();
		var price = $("#price").val();
		var paramData = {'isNativeApp':isNativeApp,'fromWxClient':fromWxClient, 'isThirdpay':isThirdpay, 'appKey':"${appKey}",  'payType':payType, 'itemCode':itemCode, 'price': price, 'buyAmount':"${buyAmount}", 'txId':"${txId}", 'state':"${state}",'v1758':"3"};
		$.post('${pageContext.request.contextPath}/submitOrder.json', paramData, function(responseData) {
			console.log("ajax成功响应");
			var result = responseData.result;
			console.log("ajax responseData.result："+responseData.result);
			if(result==1){
				
				var tradeNo = responseData.data.tradeNo;
				//alert(tradeNo);
				thisBtn.text("订单创建成功，正在跳转支付页...");
				if("submitAlipay"==eleId){//支付宝支付
					if(false){//如果是安卓客户端
						android_tw_system.callAlipayApp(jsonObj);
					}else{
						var alipayUrl = responseData.data.alipayUrl;
						window.location.href= alipayUrl;
					}
				}else if("submitDxtykj"==eleId){//电信天翼空间用户支付
					thisBtn.text("正在等待支付结果...");
					var urlTianyi = responseData.data.urlTianyi;
					window.location.href= urlTianyi;
				}else if("submit1758"==eleId){//1758支付
					var url1758 = responseData.data.url1758;
					top.window.location.href= url1758;
				}else if("submitDonate"==eleId){//亲密付
					var url1758 = responseData.data.url1758;
					top.window.location.href= 'http://h5.x1758.com/honeypay/honeypay.html?sharerTradeNo='+responseData.data.sharerTradeNo+'&scene='+responseData.data.scene;
				}else if("submitWxPay"==eleId){//微信支付
					thisBtn.text("订单创建成功，正在唤起微信支付...");
					if(isNativeApp){
						//客户端微信支付
						ResData.responseData = responseData;
						//if(is_ios()){
						//	clientWxPayIos(responseData);
						//}else{
						//	clientWxPay(responseData);	
						//}
						
						//iframe下选择客户端支付时，需要跳出iframe进行支付
						var wxPaypayId = responseData.data.wxPayJsObj.packageValue;
						top.location.href= "http://wx.1758.com/pay/wxpayH5Simple?appKey=${appKey}&wpayType=1&wxPaypayId="+wxPaypayId+"&orderId="+tradeNo;
						
					}else if(fromWxClient){
						//微信支付
						//weiXinPay(responseData);
						
						var packageValue = responseData.data.wxPayJsObj.packageValue;
						var wxPaypayId = packageValue.substring(10);// "prepay_id=".length=10
						
						//iframe下选择微信支付时，需要跳出iframe进行支付
						top.location.href= "http://wx.1758.com/pay/wxpayH5Simple?appKey=${appKey}&wpayType=0&wxPaypayId="+wxPaypayId+"&orderId="+tradeNo;
					}else{//nowpay
						top.window.location.href= responseData.data.nowpayUrl;
					}
				}
			}else{
				if(responseData.errorcode==<%=ErrorCode.WX_PRODUCT_ORDER_REPEAT%>){
					thisBtn.text("订单重复，请返回游戏重新购买");
				}else{
					thisBtn.text("订单创建失败...");
				}
				//ajax记录订单失败log
				var logParam = {'actionName':"wpayFailed", 'appKey':"${appKey}", 'gid':"${wyUser.userinfo.externalId}", 'openId':"${wyUser.wxBinding.openId}", 'orderId':'', 'message':'createOrderFailed'};
				$.get("http://api.1758.com/log", logParam, function(responseData) {});
			}
		});
	});


function is_weixin(){
    var ua = navigator.userAgent.toLowerCase();
    if(ua.match(/MicroMessenger/i)=="micromessenger" && (!(ua.match(/dwjia/i)=="dwjia"))) {
        return true;
     } else {
        return false;
    }
}

//判断是否客户端，是的话为true，否为false
function is_client(){
	var ua = navigator.userAgent.toLowerCase();
	if(ua.match(/MicroMessenger/i)=="micromessenger" && (ua.match(/dwjia/i) == "dwjia")){
		return true;
	}else{
		return false;
	}
}
function is_ios(){
	var ua = navigator.userAgent.toLowerCase();
	if(ua.match(/ios/i) == 'ios'){
		return true;
	}else{
		return false;
	}
}

//微信h5里面的微信支付
function weiXinPay(responseData){
	
	var tradeNo = responseData.data.tradeNo;
	var appId = responseData.data.wxPayJsObj.appId;
	var timeStamp = responseData.data.wxPayJsObj.timeStamp;
	var nonceStr = responseData.data.wxPayJsObj.nonceStr;
	var packageValue = responseData.data.wxPayJsObj.packageValue;
	var signType = responseData.data.wxPayJsObj.signType;
	var paySign = responseData.data.wxPayJsObj.paySign;
	
	//ajax记录唤起微支付log
	var jspayLogParam = {'actionName':"wpayJs", 'appKey':"${appKey}", 'gid':"${wyUser.userinfo.externalId}", 'openId':"${wyUser.wxBinding.openId}", 'orderId':tradeNo, 'message':'wpayJs'};
	$.get("http://api.1758.com/log", jspayLogParam, function(responseData) {});
	
	
	WeixinJSBridge.invoke('getBrandWCPayRequest', {
		"appId" :  appId, //公众号名称，由商户传入
		"timeStamp" : timeStamp, //时间戳
		"nonceStr" : nonceStr, //随机串
		"package" : packageValue,//扩展包
		"signType" : signType, //微信签名方式:1.sha1
		"paySign" : paySign
	//微信签名
	}, function(res) {
		if (res.err_msg == "get_brand_wcpay_request:ok") {
			//支付成功
			
			//ajax记录log
			var logParam = {'actionName':"wpaySuccees", 'appKey':"${appKey}", 'gid':"${wyUser.userinfo.externalId}", 'openId':"${wyUser.wxBinding.openId}", 'orderId':tradeNo, 'message':'wpaySuccess'};
			$.get("http://api.1758.com/log", logParam, function(responseData) {});
			//页面跳转
			var successUrl = '${pageContext.request.contextPath}/paySuccess?appKey=${appKey}&orderId='+tradeNo;
			setTimeout(function(){location.href=successUrl;}, 1000);
		}else{
			//js支付有错误，提交日志记录
			var currentUrl = window.location.href;
			
			//ajax记录log
			var logParam = {'actionName':"wpayFailed", 'appKey':"${appKey}", 'gid':"${wyUser.userinfo.externalId}", 'openId':"${wyUser.wxBinding.openId}", 'orderId':tradeNo, 'message':res.err_msg, 'currentUrl':currentUrl};
			$.get("http://api.1758.com/log", logParam, function(responseData) {});
			
			var failUrl = "${pageContext.request.contextPath}/payFailed?appKey=${appKey}&orderId="+tradeNo;
			
			if(res.err_msg=="get_brand_wcpay_request:cancel"){//用户主动取消
				failUrl = "${pageContext.request.contextPath}/payFailed?errorCode=1&appKey=${appKey}&orderId="+tradeNo;
				setTimeout(function(){location.href=failUrl;}, 1000);
			}else{//可能是微信问题，改用扫码支付
				failUrl = "${pageContext.request.contextPath}/payQrcode?appKey=${appKey}&orderId="+tradeNo;
				location.href=failUrl;
			}
			
			if(res.err_msg=="system:access_denied"||res.err_msg=="get_brand_wcpay_request:fail_invalid appid"||res.err_msg=="getBrandWCPayRequest:fail_invalid appid"){
				//支付失败
			}
			
		}
		//使用以上方式判断前端返回,微信团队郑重提示：res.err_msg将在用户支付成功后返回ok，但并不保证它绝对可靠。
		//因此微信团队建议，当收到ok返回时，向商户后台询问是否收到交易成功的通知，若收到通知，前端展示交易成功的界面；若此时未收到通知，商户后台主动调用查询订单接口，查询订单的当前状态，并反馈给前端展示相应的界面。
	});
}

function clientWxPay(responseData){
	var jsonParameters = responseData.data.wxPayJsObj;
	var gid = "${wyUser.userinfo.externalId}";
	var logParam = {'actionName':"wpayClientAndroid", 'appKey':"${appKey}", 'gid':"${wyUser.userinfo.externalId}", 'openId':"${wyUser.wxBinding.openId}", 'orderId':'', 'message':'wpayClientAndroid'};
	$.get("http://api.1758.com/log", logParam, function(responseData) {});
	
	jsonParameters = JSON.stringify(jsonParameters);
	weixinBridge.invokeWxPay(jsonParameters,gid);
}
function clientWxPayIos(responseData){
	var logParam = {'actionName':"wpayClientIos", 'appKey':"${appKey}", 'gid':"${wyUser.userinfo.externalId}", 'openId':"${wyUser.wxBinding.openId}", 'orderId':'', 'message':'wpayClientIos'};
	$.get("http://api.1758.com/log", logParam, function(responseData) {});
	
	var jsonParameters = responseData.data.wxPayJsObj;
	jsonParameters = JSON.stringify(jsonParameters);

	connectWebViewJavascriptBridge(function(bridge) {
		bridge.init(function(message, responseCallback) {
			if (responseCallback) {
				responseCallback("")
			}
		})
		window.ios = bridge;
		ios.registerHandler('clientPayInfo', function(data, responseCallback) {
		    clientPayInfo(data);
		});
		ios.callHandler('iosWxPay', jsonParameters);
	});
}
//客户端微信支付完成后调用的方法，参数flagInfo为boolean类型，true代表支付成功，false代表支付失败
function clientPayInfo(flagInfo){
	var tradeNo,responseData;
	responseData = ResData.responseData;
	tradeNo = responseData.data.tradeNo;
	if(typeof(flagInfo) == 'string'){
		if(flagInfo == '1' || flagInfo == 'true'){
			flagInfo = true;
		}else{
			flagInfo = false;
		}
	}
	if(flagInfo){
		//ajax记录log
		var logParam = {'actionName':"wpaySuccees", 'appKey':"${appKey}", 'gid':"${wyUser.userinfo.externalId}", 'openId':"${wyUser.wxBinding.openId}", 'orderId':tradeNo, 'message':'wpaySuccessByClient'};
		$.get("http://api.1758.com/log", logParam, function(responseData) {});
		//TODO 父页面跳转
		var successUrl = '${pageContext.request.contextPath}/paySuccess?appKey=${appKey}&orderId='+tradeNo;
		setTimeout(function(){location.href=successUrl;}, 1000);
	}else{
		//js支付有错误，提交日志记录
		var currentUrl = window.location.href;
		//ajax记录log
		var logParam = {'actionName':"wpayFailed", 'appKey':"${appKey}", 'gid':"${wyUser.userinfo.externalId}", 'openId':"${wyUser.wxBinding.openId}", 'orderId':tradeNo, 'message':'wpayFailedByClient', 'currentUrl':currentUrl};
		$.get("http://api.1758.com/log", logParam, function(responseData) {});
		
		//TODO 父页面跳转
		var failUrl = "${pageContext.request.contextPath}/payFailed?appKey=${appKey}&orderId="+tradeNo;
		failUrl = "${pageContext.request.contextPath}/payFailed?errorCode=1&appKey=${appKey}&orderId="+tradeNo;
		setTimeout(function(){location.href=failUrl;}, 1000);
	}
}
function getParameters(url,name){
	//正则验证
	var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
	var r = url.substr(url.indexOf('?')).substr(1).match(reg);
	if(r!=null){
	//return  unescape(r[2]);
	return  decodeURIComponent(r[2]);
	} 
	return '';
}
function getVersion(){
	var ua = navigator.userAgent.toLowerCase();
	var reg = /dwjia\+\/([\d\.]+)/i;
	var defaultVersion = '3.0';
	var ver = ua.match(reg);
	//如何比较版本号大小
	var flag = false;
	if(!!ver){
		flag = compareVersion(ver[1],defaultVersion);
	}
	return flag;
}
function compareVersion(v1,v2){
	var t1 = v1.split('.');
	var t2 = v2.split('.');
	var len,len1,len2,num;
	len1 = t1.length;
	len2 = t2.length;
	len = len1;

	if(len1 >= len2){
		len = len2;
	}
	for(var i = 0 ; i<len;i++){
		num = t1[i] - t2[i];
		if(num > 0){
			return true;
		}else if(num < 0){
			return false;
		}
	}
	if(len1 > len2){
		return true;
	}else if(len1 == len2){
		return 'equals';
	}else if(len1 < len2){
		return false;
	}
}
function connectWebViewJavascriptBridge(callback) {
    if (window.WebViewJavascriptBridge) {
        callback(WebViewJavascriptBridge)
    } else {
        document.addEventListener('WebViewJavascriptBridgeReady', function() {
            callback(WebViewJavascriptBridge)
        }, false)
    }
}


</script>

<jsp:include page="../inc/wxOptionSettings.jsp"></jsp:include>

</html>
