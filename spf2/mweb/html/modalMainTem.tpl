<template tpl-id="modalMainTem">
	<div id="content" class="modal-content">
		<section id="base-footer-game" class="base-footer gone">
			<div id="base-menu" class="pure-g base-menu">
			    <div id="t-t" class="pure-u-1-3 t" data-value="un" data-type="talk">
			    	<a id='tt'>
			    		<img src="http://images.1758.com/images/talkw.png">	
			    	</a>
			    </div>
			    <div id="t-g" class="pure-u-1-3 t" data-value="un" data-type="gift">
			    	<a id='gg'>
			    		<img src="http://images.1758.com/images/giftw.png">	
			    	</a>
			    </div>
			    <div id="t-c" class="pure-u-1-3 t" data-value="un" data-type="chat">
			    	<a id='cc'>
			    		<img src="http://images.1758.com/images/talkw.png">	
			    	</a>
			    </div>
			</div>
		</section>
		<section id="gift" class="gift panelitem gone">
			<header class="head bg">
				<span>
					<img src="http://wx.1758.com/game/h5/images/head-icon.png">
				</span>
				<div id="hgift" class="h-info h-gift">
					<img />
				</div>
			</header>
			<div class="gift-content-wrapper">
				<div class="listshow bg">
					<ul class="pure-g ">
						<li class="pure-u-1-3 ls-li">
							<a id="collect">
								<span class="ls-img">
									<img id="game-icon" src="http://wx.1758.com/game/h5/images/qq1.png" />
									<img class="ls-flag" src="http://images.1758.com/38/38-modalgame.png" />
								</span>
								<div>关注有礼</div>
							</a>
						</li>
						<li class="pure-u-1-3 ls-li">
							<a id="goHome">
								<span class="ls-img">
									<img src="http://wx.1758.com/game/h5/images/modallogo.png" />
									<img class="ls-flag" src="http://wx.1758.com/game/h5/images/modalhome.png" />
								</span>
								<div>官方首页</div>
							</a>
						</li>
						<li class="pure-u-1-3 ls-li">
							<a id="goKefu">
								<span class="ls-img">
									<img src="http://wx.1758.com/game/h5/images/qq1.png" />
									<img class="ls-flag" src="http://wx.1758.com/game/h5/images/modalman.png" />
								</span>
								<div>在线客服</div>
							</a>
						</li>
					</ul>
				</div>
				<div id="QFOpenTimeList" class="klist gone">
					<div class="inner-klist bg">
						<header class="g-head g-k-h"><span>开服计划</span></header>
						
						<div id="qfWrapper" class="klist-item-wrapper">
							
						</div>
						<footer id="showQF" class="k-foot gone">
		            		<span><img src="http://images.1758.com/game/down.png" alt="向下"></span>
		            	</footer>
	            	</div>
				</div>
				<div id="giftList" class="list bg gone">
					<div class="inner-klist bg">
						<header class="g-head g-l-h"><span>游戏礼包</span></header>
						
						<div id="giftWrapper" class="klist-item-wrapper">
							
						</div>
						<footer id="showHG" class="k-foot gone">
		            		<span><img src="http://images.1758.com/game/down.png" alt="向下"></span>
		            	</footer>
	            	</div>
				</div>
			</div>
		</section>
		<section id="talk" class="talk panelitem gone">
			<header class="head bg">
				<span>聊吧</span>
				<!--<div id="htalk" class="h-info">
					<img src="http://images.1758.com/ranking/write.png" />
				</div>-->
			</header>
			<div class="list">
				<div class="more-info" data-num="1" data-track='talk' data-value="un"><a>点击加载更多</a></div>
			</div>
			<div class="comment-logo">
				<div class="inner-comment-logo">
					<img id="commentLogo" src="http://images.1758.com/fubiao/fubiao_pinglun.png">
				</div>
			</div>
		</section>
		<section id="chat" class="chat gone panelitem">
			<div class="chat-panel">
				<header class="head bg panel-header">
					<div id="chatClose" class="h-close">关闭聊天</div>
					<span id="chatRoomName">1758聊天室</span>
					<div class="h-info"></div>
				</header>
				<div class="panel-body-wrapper">
					<div id="panelChatContainer" class="panel-chat-container">
						<!--<div class="chat-time">
							<span>14:26</span>
						</div>-->
					</div>
				</div>
				<footer id="panel-footer" class="panel-footer">
					<div class="chat-toobar">
						<div class="chat-textarea" id="chatTextarea" >
							<!--<textarea contenteditable="plaintext-only" editable="faslse"></textarea>-->
							<input id="chatCon" class="chat-container" />
						</div>
						<div id="sendChatBtn" class="chat-send-btn">
							<span id="sendMessage" class="btn-text">发送</span>
						</div>
					</div>
				</footer>
			</div>
		</section>
		<div class="tip gone">
			<div class="tip-dialog">
				<div class="tip-wrapper">
					<div class="tip-content tip-exclusive">
						<div class="tip-body">
							<p class="ex-con"><span style="color: #71A540;">专属礼包</span>需通过1758客户端领取</p>
							<span class="ex-inner">
							<span class="ex-info">您还没有安装1758客户端？</span>
							<a class="ex-load">立即下载</a>
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="weui_dialog_confirm gone" id="weDialog">
	        <div class="weui_mask"></div>
	        <div class="weui_dialog">
				<div class="weui_dialog_hd">
					<strong class="weui_dialog_title">发表评论</strong>
					<!--<strong class="weui_add_img" id="wxAddImg"></strong>-->
				</div>
				<div class="weui_dialog_bd webd">
					<textarea id="weText" contenteditable="false" placeholder="大侠留个名吧~"></textarea>
					<div class="img-wrapper pure-g" id="wxImgWrapper">
						<div id="wxAddImg" class="pure-u-1-4">
							<div class="imgitem">
								<img src="http://images.1758.com/daren/upload.png" />
							</div>
						</div>
					</div>
				</div>
				<div class="weui_dialog_ft">
					<a id="weCancle" class="weui_btn_dialog default">取消</a>
					<a id="wePublic" class="weui_btn_dialog primary">发布</a>
				</div>
			</div>
	    </div>
	    <div id="toast" class="gone">
	        <div class="weui_mask_transparent"></div>
	        <div class="weui_toast">
	            <i class="weui_icon_toast"></i>
	            <p class="weui_toast_content">已发送</p>
	        </div>
	    </div>
	    <div id="toastTip" class="gone">
	        <div class="weui_mask_transparent"></div>
	        <div class="weui_toast weui_toast_text">
	            <p class="weui_toast_content weui_toast_content_text">最多四张图片</p>
	        </div>
	    </div>
	    <div id="loadingToast" class="weui_loading_toast gone">
	        <div class="weui_mask_transparent"></div>
	        <div class="weui_toast">
	            <div class="weui_loading">
	                <div class="weui_loading_leaf weui_loading_leaf_0"></div>
	                <div class="weui_loading_leaf weui_loading_leaf_1"></div>
	                <div class="weui_loading_leaf weui_loading_leaf_2"></div>
	                <div class="weui_loading_leaf weui_loading_leaf_3"></div>
	                <div class="weui_loading_leaf weui_loading_leaf_4"></div>
	                <div class="weui_loading_leaf weui_loading_leaf_5"></div>
	                <div class="weui_loading_leaf weui_loading_leaf_6"></div>
	                <div class="weui_loading_leaf weui_loading_leaf_7"></div>
	                <div class="weui_loading_leaf weui_loading_leaf_8"></div>
	                <div class="weui_loading_leaf weui_loading_leaf_9"></div>
	                <div class="weui_loading_leaf weui_loading_leaf_10"></div>
	                <div class="weui_loading_leaf weui_loading_leaf_11"></div>
	            </div>
	            <p class="weui_toast_content">相册打开中</p>
	        </div>
	    </div>
	</div>
</template>