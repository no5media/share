/**
 * 游戏返回按钮监听事件
 * @param  {String} $){	var appKey        [description]
 * @return {[type]}          [description]
 */
define(['jquery'],function($){
	var appKey = '';
	var aid = '';
	var gameArr = '';
	var chn = '';
	if(HLMY_CONFIG && HLMY_CONFIG.GAME_INFO){
		appKey = HLMY_CONFIG.GAME_INFO.appKey;
		aid = HLMY_CONFIG.GAME_INFO.aid;
	}
	if(HLMY_CONFIG.CHN_INFO){
		chn = HLMY_CONFIG.CHN_INFO.chn;
	}

	var pageBack = {
		init: init
	}
	/**
	 * 初始化内容
	 * @return {[type]} [description]
	 * ?aid=113292&chn=chiba
	 */
	function init(){
		var url = '/play/game/moreRecommendGames.json';
		var info = {
			aid:aid,
			chn:chn
		}
		$.post(url,info,handleInitData,'json');
	}
	/**
	 * 处理初始化返回的数据
	 * @param  {[type]} resData [description]
	 * @return {[type]}         [description]
	 */
	function handleInitData(data){
		if(data.result == 1){
			if(data.data &&　data.data.appInfoList){
				gameArr = data.data.appInfoList;
				if(gameArr.length > 0){
					pageBackInit();
				}
			}
		}
	}
	/**
	 * 返回信息初始化
	 * @return {[type]} [description]
	 */
	function pageBackInit(){
		//组装dom节点
		spliceDom(gameArr);
		//初始化相关事件
		eventInit();
	}
	/**
	 * 拼接返回信息的Dom节点
	 */
	function spliceDom(gameArr){
		var str = '';
		for(var i = 0,len=gameArr.length;i<len;i++){
			str += '<div class="hbi-flex-item"><a onclick="_czc.push([\'_trackEvent\',\'进入游戏\', \''+gameArr[i].appName+'\',\''+aid+'\']);" href="'+gameArr[i].gameUrl+'"><img src="'+gameArr[i].iconUrl+'"></a><p>'+gameArr[i].appName+'</p></div>';
		}
		$('#hbiGameContainer').html(str);
		return str;
	}
	/**
	 * 监听返回事件
	 * 以及相关事件的初始化
	 * @return {[type]} [description]
	 */
	function eventInit(){
		if(!redLocalStorageBackInfo()){
			$(document).on('click','#hbiExit',handleExitEvent);
			$(document).on('click','#hbiExitModal',handleExitModalEvent);
			$(document).on('click','#hbiCancelButton',handleCancelButtonEvent);
			$(document).on('click','#exitDialog',handleExitDialogEvent);
			$(document).on('click','#hbiFollow',handleFollowEvent);
			$(document).on('click','#hbiEWMWrapper',handleEWMWrapperEvent);
			$(document).on('click','#avoidBackConfirm',handleNoEvent);

        	$(function(){		
        		//在部分游戏上pushState方法无效
        		// 导致刚进去游戏弹出返回游戏提示
        		// 加上ready方法后 会生效，无法确定产生问题的原因
		        setTimeout(function() {
			        	window.history.pushState({
			                title: document.title,
			                url: location.href
			            }, document.title, location.href);
			            window.addEventListener && window.addEventListener("popstate", function(e) {
			                if (!e.state) {
			                    try{
									_czc.push(["_trackEvent",'打开弹窗','打开弹窗',aid]);
			                    }catch(err){}
			                    openModal();
			                }
			            });
		        }, 2e3);
        	})
		}
	}

	function handleExitEvent(e){
		//仍要离开
		try{
			_czc.push(["_trackEvent",'离开游戏','离开游戏',aid]);
		}catch(err){}
	    var isSelect = document.getElementById('avoidBackConfirm').checked;
	    if(isSelect){
	    	try{
	    		_czc.push(['_trackEvent','不再提示','不再提示',aid]);
	    	}catch(err){}
	    	//存储localstorage
	    	setLocalStorageBackInfo();
	    }
		if("" === document.referrer && getUA() === 'weixin'){
			window.wx.closeWindow();
		}else{
			window.history.back();
		}
		e.stopPropagation();
	};

	function handleExitModalEvent(e){
		//点击背景取消
		try{
			_czc.push(['_trackEvent','取消弹窗','点击背景取消',aid]);
		}catch(err){}
		e.stopPropagation();
	    openModal('close');
	    window.history.pushState({
	        title: document.title,
	        url: location.href
	    }, document.title, location.href);
	};

	function handleCancelButtonEvent(e){
		//点击x号关闭
		try{
			_czc.push(['_trackEvent','取消弹窗','点击按钮取消',aid]);
		}catch(err){}
	    e.stopPropagation();
	    openModal('close');
	    window.history.pushState({
	        title: document.title,
	        url: location.href
	    }, document.title, location.href);
	};

	function handleFollowEvent(e){
		//关注我们按钮
		try{
			_czc.push(['_trackEvent','关注我们','关注我们',aid]);
		}catch(err){}
		openModal('ewm');
		e.stopPropagation();
	};

	function handleEWMWrapperEvent(){
		//二维码
		openModal('ewmClose');
	};

	function handleExitDialogEvent(e){
		//点击游戏主体 禁止事件冒泡
		e.stopPropagation();
	};
	function handleNoEvent(e){
		//不再提示点击
		e.stopPropagation();
	}


	function openModal(str){
		switch(str){
			case 'ewm':
				// 关注我们
				$('#hbiEWMWrapper').removeClass('gone');
			break;
			case 'ewmClose':
				// 二维码关注取消
				$('#hbiEWMWrapper').addClass('gone');
			break;
			case 'close':
				$('#hbiBackInfo').addClass('gone');
			break;
			default:
				$('#hbiEWMWrapper').addClass('gone');
				$('#hbiExitModal').removeClass('gone');
				$('#hbiBackInfo').removeClass('gone');
		}
	}
	//存储localstorage
	function setLocalStorageBackInfo(){
		if(!window.localStorage){
			return false;
		}
		var backinfo = window.localStorage.backinfo;
		var mark = false;
		if(!!backinfo){
			//backinfo存在
			backinfo = JSON.parse(backinfo);
			if(backinfo.length > 0){
				//有内容
				for(var i=0,len=backinfo.length;i<len;i++){
					if(backinfo[i].appKey == appKey){
						mark = true;
						backinfo[i].back = 1;
						backinfo[i].t = new Date().getTime();
						break;
					}
				}
			}
		}else{
			backinfo = [];
		}
		if(!mark){
			var item = {'appKey':appKey,'back':1};
			item.t = new Date().getTime();
			backinfo.push(item);
		}
		backinfo = JSON.stringify(backinfo);
		window.localStorage.setItem('backinfo',backinfo);
	}
	/**
	 * 读取localstorage信息  查看是否已经存储过
	 * 返回弹出游戏信息
	 * 如果存储过则返回true
	 * 其余返回false
	 * @return {[type]} [description]
	 */
	function redLocalStorageBackInfo(){
		if(!window.localStorage){
			return false;
		}else{
			var backinfo = window.localStorage.backinfo;
			if(!!backinfo){
	    		backinfo = JSON.parse(backinfo);
	    		for(var i = 0,len = backinfo.length; i<len;i++ ){
					if(backinfo[i].appKey == appKey && backinfo[i].back == 1){
						if(!!backinfo[i].t){
							return compareTime(backinfo[i].t);
						}else{
							return false;
						}
						break;
					}
				}
				return false;
	    	}
	    	return false;
		}
	}

	/**
	 * 比较local存储的时间和当前时间
	 * 是否为一天
	 * @param  {[type]} t [description]
	 * @return {[type]}   [description]
	 */
	function compareTime(t){
		var oneDay = 1000*60*60*24;	
		var oldTime = new Date(t);
		var nowTime = new Date();
		var cha = nowTime.getTime() - oldTime.getTime();
		if(cha > oneDay){
			return false;
		}else if(nowTime.getDate() == oldTime.getDate()){
			return true;
		}else{
			return false;
		}

	}
	/**
	 * 获取ua
	 * @return {[type]} [description]
	 */
	function getUA(){
		var usa = window.navigator.userAgent.toLocaleLowerCase();
		if(usa.indexOf('micromessager') > 0 && usa.indexOf('dwjia').indexOf == -1){
			return 'weixin';
		}else{
			return 'other';
		}
	}
	
	return pageBack;
});