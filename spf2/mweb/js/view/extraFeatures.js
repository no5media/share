/**
 * 该js主要是为了处理付费游戏里面
 * 除了弹框主要功能外的额外功能（辅助功能）
 * 包括但不限于：第三方渠道的各种临时需求、弹框提示功能
 */
define(['jquery','common/eventBase'],function($,EventBase){
	var exportModal = {
		//添加到桌面功能
		addToDesktopInit:deskInit
	};
	var UA = window.navigator.userAgent.toLocaleLowerCase();
	/**
	 * 内部对象，里面是各种内部需要的方法
	 * @type {Object}
	 */
	var _Tool = {
		isAndroid: UA.indexOf('android') > 0,
		isIos: /(iphone|ipad|ipod|ios)/i.test(UA),
		isClient: UA.indexOf('dwjia') > 0,
		isWeChat: (UA.indexOf('micromessenger') > 0) && (UA.indexOf('dwjia') < 0)
	};
	/**
	 * 添加到桌面初始化函数
	 * 开始游戏X分钟后弹框提示用户下载或者添加至桌面
	 * @return {[type]} [description]
	 */
	function deskInit(){
		if(!!HLMY_CONFIG.ADD_TO_DESKTOP && HLMY_CONFIG.ADD_TO_DESKTOP.enable){
			//添加到桌面功能
			addToDesktop();
		}
	};
	//添加到桌面功能
	function addToDesktop(){
		//背景点击取消
		// $(document).on('click','#deskContainer',function(){
		// 	// $(this).addClass('gone');
		// })
		// $(document).on('click','#huiWrapper',function(e){
		// 	e.stopPropagation();
		// })
		//点击添加到桌面功能
		$(document).on('click','#addToDesktop',function(e){
			if(_Tool.isIos){
				location.href = HLMY_CONFIG.ADD_TO_DESKTOP.url;
			}else{
				location.href = 'http://dl.1758.com/1758_3.3.6_caiyinad.apk';
			}
			// e.stopPropagation();
		})
		//修改icon
		$('#huiGameLogo').attr('src',HLMY_CONFIG.GAME_INFO.iconUrl)
		setTimeout(function(){
			$('#deskContainer').removeClass('gone');
		},HLMY_CONFIG.ADD_TO_DESKTOP.time);
	};
	
	return exportModal;
});
