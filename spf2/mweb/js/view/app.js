require.config({
	baseUrl:'http://h5.g1758.cn/spf/mweb/js',
	paths:{
		libs:'libs',
		data:'data',
		common:'common',
		view:'view',
		
		jquery:'jquery',
		fastclick:'libs/fastclick',
		'jquery.modal':'plugin/weui/js/modal',
	},
	shim:{
		fastclick:{
			exports: 'FastClick'
		},
		'jquery.cookie': {
        	deps: ['jquery'],
        	exports: 'jQuery.fn.cookie'
       },
       'jquery.modal':{
			deps:['jquery'],
			exports: '_'
		},
	}
});
// plugin/weui/js/modal
require(['libs/fastclick','jquery','common/eventBase','data/getInfoData','view/modalBtn','view/extraFeatures','view/pageBack','plugin/weui/js/modal'],function(FastClick,$,EventBase,InfoData,ModalBtn,ExtraFeatures,PageBack){
	FastClick.attach(document.body);
	if(typeof HLMY_GAMEINFO === 'string'){
		HLMY_GAMEINFO = JSON.parse(HLMY_GAMEINFO);
	}
	var dataPara = {
		verLogin		: 	'',
		deffConfig 		: 	$.Deferred(),	//配置请求
		//deffUser 		: 	$.Deferred(),	//用户gid请求
		deffLoad		:   $.Deferred(),		//游戏页面load完
		deffIosBridge	:	$.Deferred(),		//ios bridge注入对象
		locationUrl		: 	encodeURIComponent(location.href),
		appkey			:	'',	//游戏
		title			:	'1758微游戏',	//游戏title	
		wxjsconfig		:  	{},		//微信分享的配置信息
		trialEnable		:	HLMY_GAMEINFO.trialEnable ?	true : false,
		trialTime		:   HLMY_GAMEINFO.trialTime ? HLMY_GAMEINFO.trialTime : '30',		//（暂不启用）实现自动登录的时候用到的时间间隔
		sharePageCpurl	:	'',		//游戏的分享地址
		shareText		:   [],		//分享语的数组
		customizeText	:	[],		//自定义参数使用的默认分享语数组(无效)
		gid				:	HLMY_GAMEINFO.gid,		//用户的gid
		userState		: 	HLMY_GAMEINFO.userStatus,			//用户类型（0则为试玩用户）
		sid				:	'',		//当前分享语的id
		userTerminal	:	'',	//用户终端类型
		src				:	'http://wx.1758.com/game/platform/v2.0/user/flogin?appKey=',
		chn				:	'',
		timeoutId		: 	'',
		taTip 			:   false,
		panelEnable		:	HLMY_GAMEINFO.panelEnable ?	true : false,
		chatroomEnable	:	false,		//是否需要聊天室
		isCpShareInfo	:	false,		//控制是否有cp传递过来的信息
		messageFromCp	:	{				//是否是从cp传递过来的内容
				isCp:false,				//从cp地方获取到内容后 置为true
				urlPara:'',
				title:'',
				desc:'',
				imgUrl:''
			},
		defaultText		:	{
				sharedesc:"1758微游戏",
				shareimg:"http://images.1758.com/images/48.ico",
				sharelink:"http://wx.1758.com",
				sharesumary:"1758微游戏",
				sharetitle:"1758微游戏，即点即玩",
				sid:""
			}
	}
	$(document).on('touchmove',function(e){
		e.preventDefault();
	});
	$('body').on('touchmove', '#moban,#payWrapper', function(e) {
		e.stopPropagation();
	});
	init();
	//初始化函数
	function init(){
		dataPara.userTerminal = EventBase.userTerminal();		//得到用户使用的是客户端还是微信
		dataPara.appkey = EventBase.getParameters('appKey');	//获取微信分享config用
		dataPara.chn = EventBase.getParameters('chn');
		// 获取到登录的版本号
		getVerLogin();
		var ux = dataPara.userTerminal;
		//初始化一些全局对象
		initWindow(ux);
		//iframe页面绑定loadend事件
		loadEndPage();
		//检测是否一键登录过
		hasOneKeyLogin();
		//loadPage((dataPara.src+dataPara.appkey+'&chn='+chn));
		//获取微信配置信息
		getWxConfigAndText();
		
		/*获取gid信息 该功能从jsp直接获取，不再需要跨域获取了*/
		//getUserInfo();
		$.when(dataPara.deffLoad).done(function(){
			//浮标初始化信息
			ModalBtn.init(dataPara.panelEnable);
			//在第二个版本中暂时不开启websocket通讯
//			ModalBtn.initWschat();
		});
		setShareInfo();
		//接受cp传递info
		cpInfoPostInit();
		//她社区提示
		taSheQuHandleTip();
		//检查关注信息		
		checkFollowInfo();
		//特殊渠道添加到桌面
		ExtraFeatures.addToDesktopInit();
		//页面返回事件初始化
		PageBack.init();
	}
	//config完成之后分享语注入
	function setShareInfo(){
		var ux = dataPara.userTerminal;
		$.when(dataPara.deffConfig).done(function(){
			//分享语注入信息
			if(ux == 'iphoneclient' || ux == 'androidclient'){
				if(ux == 'iphoneclient'){
					$.when(dataPara.deffIosBridge).done(function(){
						pareShareText();
					})
				}else{
					//客户端分享语的注入
					pareShareText();
				}
			}else{
				//进行微信分享语的注入	
				changeWxShareInfo();
			}
		}).fail(function(){
			
		});
	}
	//获取游戏的wx配置和分享语
	function getWxConfigAndText(){
		var url = 'http://wx.1758.com/game/platform/getShareInfoAndWxJsconfig';
		var infos = {
			appKey:dataPara.appkey,
			pageUrl:encodeURIComponent(location.href),
			'wyOpenAppId':HLMY_CHNINFO.wyOpenAppId
		};
		InfoData.getShareInfo(url,infos,handleWxConfig);
	}
	//处理得到的微信配置信息和分享语
	function handleWxConfig(data){
		if(data.code == 0){
			if(data.appshares.length == 0){
				dataPara.shareText.push(dataPara.defaultText);
			}else{
				dataPara.shareText = data.appshares;
			}
			dataPara.sharePageCpurl = data.sharePageCpurl;
			dataPara.title = data.title;
			dataPara.wxjsconfig = data.wxjsconfig;
			
			//设置title名字
			if(!!data.title){
				//document.title = data.title;
			}
			//设置是否有聊天室
			if(data.chatroomEnable != undefined){
				dataPara.chatroomEnable = data.chatroomEnable;
				ModalBtn.chatroomEnable = data.chatroomEnable;
			}
			//微信的分享配置信息
			window.wx.config(data.wxjsconfig);
			dataPara.deffConfig.resolve();
		}
	}
	//获取用户的gid信息并且添加到分享link上面
	function getUserInfo(){
		var userInfo;
		//数据页面地址，主要是通过改地址获取gid
		var dataurl = 'http://wx.1758.com/game/platform/user/shareIframeNotify';
		//通过window.name属性进行跨域的数据读取
		var state = 0, 
	    giframe = document.createElement('iframe'),
	    loadfn = function() {
	        if (state === 1) {
	            dataPara.gid = giframe.contentWindow.name;    // 读取数据
	            //changeLink(userinfo);
	            dataPara.deffUser.resolve();
	            document.body.removeChild(giframe);
	        } else if (state === 0) {
	            state = 1;
	            giframe.contentWindow.location = "http://h5.g1758.cn/spf/blank.html";    // 设置的代理文件
	        }  
	    };
	    giframe.src = dataurl;
	    giframe.style.display = 'none';
	    if (giframe.attachEvent) {
	        giframe.attachEvent('onload', loadfn);
	    } else {
	        giframe.onload  = loadfn;
	    }
	    document.body.appendChild(giframe);
	}
	//微信的注入信息
	function changeWxShareInfo(){
		var info = randomGetShareContent();
		var wx = window.wx;
		wx.ready(function(){
        	//分享给朋友
	        wx.onMenuShareAppMessage({
	            title: info.title, // 分享标题
	            desc: info.desc, // 分享描述
	            link:  info.link + '&stype=appmessage&superlongId='+Math.ceil(Math.random()*10000000000),
	            imgUrl: info.imgUrl,
	            type: 'link',
	            dataUrl: '',
	            success: function(){
	            	exec_iframe('appmessage',dataPara.sharePageCpurl);
	            	changeWxShareInfo();
	            	sendMessageServer('appmessage',0);
	            },
	            cancel: function(){
	            	changeWxShareInfo();
	            	sendMessageServer('appmessage',1);
	            }
	        });

	        //分享到朋友圈
	        wx.onMenuShareTimeline({
	            title: info.title, // 分享标题
	            link:  info.link + '&stype=timeline&superlongId='+Math.ceil(Math.random()*10000000000),
	            imgUrl: info.imgUrl,
	            success: function(){
	            	exec_iframe('timeline',dataPara.sharePageCpurl);
	            	changeWxShareInfo();
	            	sendMessageServer('timeline',0);
	            },
	            cancel: function(){
	            	changeWxShareInfo();
	            	sendMessageServer('timeline',1);
	            }
	        });
	        //分享到qq好友
	        wx.onMenuShareQQ({
			    title: info.title, // 分享标题
			    desc: info.desc, // 分享描述
			    link: info.link + '&stype=qq&superlongId='+Math.ceil(Math.random()*10000000000), // 分享链接
			    imgUrl: info.imgUrl, // 分享图标
			    success: function(){
			    	exec_iframe('qq',dataPara.sharePageCpurl);
			    	changeWxShareInfo();
			    	sendMessageServer('qq',0);
			    },
			    cancel: function(){
			    	changeWxShareInfo();
			    	sendMessageServer('qq',1);
			    }
			});
			//分享到qq空间
			wx.onMenuShareQZone({
			    title: info.title, // 分享标题
			    desc: info.desc, // 分享描述
			    link: info.link + '&stype=qzone&superlongId='+Math.ceil(Math.random()*10000000000), // 分享链接
			    imgUrl: info.imgUrl, // 分享图标
			    success: function () { 
			       // 用户确认分享后执行的回调函数
			    	exec_iframe('qzone',dataPara.sharePageCpurl);
			    	changeWxShareInfo();
			    	sendMessageServer('qzone',0);
			    },
			    cancel: function () { 
			        // 用户取消分享后执行的回调函数
			        changeWxShareInfo();
			    	sendMessageServer('qzone',1);
			    }
			});
        });
	}
	//得到随机分享语
	function randomGetShareContent(){
		//先判断是否是cp传递过来的内容
		var kk;
		kk = EventBase.randomShareContent(dataPara.shareText,dataPara.gid);
		//根据特殊渠道的特殊需求，替换chn的值
		if(!!kk && !!kk.link){
			kk.link = changeShareChn(kk.link);
		}
		//如果cp自定义分享语则添加cp的分享语
		if(dataPara.isCpShareInfo){
			var cptitle = dataPara.messageFromCp.title,
				cpdesc = dataPara.messageFromCp.desc,
				imgUrl = dataPara.messageFromCp.imgUrl,
				state = dataPara.messageFromCp.state;
			if(!!kk){
				(!!cptitle && typeof(cptitle) == 'string' ) ? (kk.title = cptitle) : '';
				(!!cpdesc && typeof(cpdesc) == 'string' ) ? (kk.desc = cpdesc) : '';
				(!!imgUrl && typeof(imgUrl) == 'string' ) ? (kk.imgUrl = imgUrl) : '';
				if(!!state && typeof(state) == 'string'){
					if(kk.link.indexOf('?') > 0){
						kk.link += '&state='+state;
					}else{
						kk.link += '?state='+state;
					}
				}
				kk.sid = '';
			}
			dataPara.isCpShareInfo = false;
		}
		if(!kk){
			dataPara.sid = '';			
		}else{
			dataPara.sid = kk.sid;
		}
		return kk;
	}
	//向cp发送分享成功的请求
	function exec_iframe(shareType,cpShareUrl){
		if(dataPara.verLogin == '2'){
			EventBase.exec_iframe(shareType,cpShareUrl);
		}else{
			//新的通知cp方法
			setInfoForSend(shareType);
		}
		//隐藏信息提示页面
		hideShareTipInfo();
	}
	//每次分享成功之后执行一次发送给服务器  0成功  1取消
	function sendMessageServer(name,flag){
		$.get('http://wx.1758.com/game/platform/user/logShareAction',{
			'gid': dataPara.gid,
			'appKey': dataPara.appkey,
			'type': name,
			'code': flag,
			'sid': dataPara.sid
			},function(data){
				if(data.msg == 'ok'){
					return;
				};
			},'jsonp');
	}
	//格式化json对象，注入客户端分享内容
	function pareShareText(){
		var shareAppmessageInfo,shareTimelineInfo,temp;
		var data = randomGetShareContent();
		shareTimelineInfo = JSON.stringify(data);
		temp = data.title;
		data.title = data.desc;
		data.desc = temp;
		shareAppmessageInfo = JSON.stringify(data);
		if(typeof(window.weixinBridge) != 'undefined'){
			if(window.weixinBridge.invokeWx){
				window.weixinBridge.invokeWx(shareAppmessageInfo, shareTimelineInfo);
	        }
		}else{
			if(window.ios){
				window.ios.callHandler('shareTimeline', shareTimelineInfo);
				window.ios.callHandler('shareMessage', shareAppmessageInfo);
			}
		}
	}
	//初始化一些全局对象
	function initWindow(ux){
		var win = window;
		if(ux == 'iphoneclient' || ux == 'androidclient'){
			win.clientSuccessInfo = function(type){
				var _type='';
			    type = type.toLowerCase();
			    switch(type){
			        case 'sendappmessage':
			            _type = 'appmessage';
			        break;
			        case 'sharetimeline':
			            _type = 'timeline';
			        break;
			        case 'shareqq':
			            _type = 'qq';
			        break;
			        case 'shareweiboapp':
			        break;
			        case 'shareqzone':
			        	_type = 'qzone';
			        break;
			    }
			    //发送给cp，执行cp的回调函数
			    exec_iframe(_type,dataPara.sharePageCpurl);
			    //随机分享语
			    pareShareText();
			    //发送给服务器记录信息
			    sendMessageServer(_type,0);
			};
			//假如没有注册分享信息，客户端调用该方法进行注册信息
			win.clientGetShareInfo = function(){
				pareShareText();
			}
			//ios 客户端分享对象
			win.connectWebViewJavascriptBridge = function(callback) {
			    if (window.WebViewJavascriptBridge) {
			        callback(WebViewJavascriptBridge)
			    } else {
			        document.addEventListener('WebViewJavascriptBridgeReady', function() {
			            callback(WebViewJavascriptBridge)
			        }, false)
			    }
			}
			//安卓客户端添加大桌面方法，假如没有自动执行 则客户端调用该方法
			win.clientdesktopInfo = function(){
				androidPutDesktop();
			}
			if(ux == 'iphoneclient'){
				//ios 客户端注册对象
				handleIosClient();
			}else if(ux == 'androidclient'){
				//Android 添加到桌面
				androidPutDesktop();
			}
		}
		$(document).on('click','#shareTipInfo',function(){
			$(this).addClass('gone');
		})
	}
	//ios客户端注册对象
	function handleIosClient(){
		window.connectWebViewJavascriptBridge(function(bridge) {
			bridge.init(function(message, responseCallback) {
				if (responseCallback) {
					responseCallback("")
				}
			})
			window.ios = bridge;
			ios.registerHandler('clientSuccessInfo', function(data, responseCallback) {
			    window.clientSuccessInfo(data);
			});
			ios.registerHandler('clientGetShareInfo', function(data, responseCallback) {
			    window.clientGetShareInfo();
			});
			dataPara.deffIosBridge.resolve();
		});
	}
	//androlid 放到桌面
	function androidPutDesktop(){
		$.get('http://wx.1758.com/game/api/app/getAppInfoJsonP',{
			'appKey': dataPara.appkey
		},function(data){
			if(data.code == 0){
				var jskk = {
					'gameName':data.wxApp.name,
					'gameUrl':data.wxApp.backGameUrl,
					'imgUrl':data.wxApp.iconUrl,
					'tp':''
				};
				jskk = JSON.stringify(jskk);
				try{
					android_tw_system.addDeskIconNew(jskk);
				}catch(e){
					//TODO handle the exception
				}
			}
		},'jsonp');
	}
	
	/**
	 * 动态jsp页面用到（iframe加载完成则消失load）
	 */
	function loadEndPage(){
		var gframe = document.getElementById('gameframe');
		if (gframe.attachEvent) {
	        gframe.attachEvent('onload', hideLoading);
	    } else {
	        gframe.onload  = hideLoading;
	    }
	    setTimeout(function(){
	    	dataPara.deffLoad.resolve();
	    },8000)
	}
	//gafarme游戏加载完成之后 
	function hideLoading(){
		$('#loading-div').css('display','none');
		$('#gameframe').css('display','block');
		dataPara.deffLoad.resolve();
	}
	
    /**
     * 试玩状态绑定点击登录按钮
     */
    function hasOneKeyLogin(){
    	if(dataPara.trialEnable){
    		//一键登录了
    		$.when(dataPara.deffLoad).done(function(){
				//添加登录提示框
				$('body').prepend(dyCreateLoginTip());
				dataPara.timeoutId = setTimeout(function(){
					$('#logTip').remove();
//					$('body').append(dyCreateLoginHtml());
					getWeuiModalForTrial();
//					loginEvent();
				},dataPara.trialTime*1000);
		    	bindLogTipClick();
			})
    	}
    }
    //试玩时间到，弹出提示登录框
    function getWeuiModalForTrial(){
    	$.modal({
		  wrapperCssName:'modal-trial',
	      text: '<p>本游戏由1758提供</p><p>为保存进度,请登录后在操作</p>',
	      title: '<img src="http://wx.1758.com/game/h5/images/head-icon.png"/>',
	      buttons: [
//		  {
//	        text: '退出游戏',
//	        className: "default",
//	        onClick: function(){
//	        	window.opener = null;
//	        	window.open(" ","_self");
//	        	window.close();
//	        }
//	      },
		  {
	        text: '前去登录',
	        className: "primary",
	        onClick: function(){
	        	top.window.document.location.href = 'http://wx.1758.com/game/platform/v3.0/user/flogin?loginNow=true&appKey='+dataPara.appkey+'&chn='+dataPara.chn;
	        }
	      }]
	    });
    }
    //动态生成登录页面
    function dyCreateLoginHtml(){
    	var str = '<div id="login-bc" class="login-bc" style="display:block">'+
			'<div id="login-main" class="login-main">'+
				'<p class="headimg">'+
					'<img src="http://images.1758.com/images/login_1758_1.png" style="width:35%;">'+
				'</p>'+
				'<hr style="border:1px rgb(247,247,247) solid;">'+
				'<p id="tip" class="tip-1">该游戏由1758.com提供，登录后即可继续操作</p>'+
				'<p class="tip-2">一键登录</p>'+
				'<div class="l-img" style="text-align:center;margin-bottom:10px;">'+
					'<span>'+
						'<a id="qqLogin">'+
						'<img src="http://images.1758.com/images/login_QQ1.png" style="width:20%;max-height:60px;margin-right:20px;border-color:#fff">'+
						'</a>'+
					'</span>'+
					'<span id="wxLogin" class="wxlogin">'+
						'<a>'+
						'<img src="http://images.1758.com/images/login_WX1.png" style="width:20%;max-height:60px;">'+
						'</a>'+
					'</span>'+
					'<span>'+
						'<a id="weiboLogin">'+
						'<img src="http://images.1758.com/images/login_XL1.png" style="width:20%;max-height:60px;margin-left:20px;">'+
						'</a>'+
					'</span>'+
				'</div>'+
			'</div>'+
		'</div>'
		return str;
    };
    function dyCreateLoginTip(){
    	var str = '<div id="logTip" class="logtip" style="display:block">'+
				'<img src="http://wx.1758.com/game/h5/images/head-icon.png">'+
				'<div class="wrap">'+
					'<div id="tip-close" class="close"><img src="http://images.1758.com/images/closeLogin.png"></div>'+
					'<span>登录后可继续进行上次游戏进度</span>'+
					'<div id="tip-log" class="tip-log">'+
						'<span>'+
							'<span class="login-btn">登录</span>'+
							'<span id="countdown"></span>'+
						'</span>'+
					'</div>'+
				'</div>'+
			'</div>'
		return str;
    }
    /**
     * 用户试玩状态则绑定可删除和登录的按钮
     */
    function bindLogTipClick(){
    	$(document).on('click','#tip-close',function(){
			$('#logTip').remove();
		});
		$(document).on('click','#tip-log',function(){
			$('#logTip').remove();
//			$('body').append(dyCreateLoginHtml());
			clearTimeout(dataPara.timeoutId);
			//跳转登录链接
			top.window.document.location.href = 'http://wx.1758.com/game/platform/v3.0/user/flogin?loginNow=true&appKey='+dataPara.appkey+'&chn='+dataPara.chn;
			//登录事件
//  		loginEvent();
		});
    }
    /**
     * 登录按钮点击事件（试玩状态下用户点击登录）（暂停使用）
     */
    function loginEvent(){
    	var ua = dataPara.userTerminal;
    	if(ua == 'weixin' || ua == 'iphoneclient' || ua == 'androidclient'){
    		$('#wxLogin').css('display','inline');
    		$(document).on('click','#wxLogin',function(){
    			try{
			      	android_wxgame_auth.weixinLogin();
					android_tw_system.toast("通过微信登录，可看到好友的得分排名哦",1);
			    }catch (exp){
			        try{
			  			window.ios.callHandler('weixinLogin',{},function(res){});
			  		}catch (exp2){
			   		 	location.href="http://wx.1758.com/game/platform/v3.0/user/login?appKey="+dataPara.appkey+"&state=&chn=onekey&share=";
			  		}
			    }
    		})
    	}
    	$(document).on('click','#qqLogin',function(){
    		location.href = 'http://wx.1758.com/game/platform/v3.0/qq/login?appKey='+dataPara.appkey+'&state=&chn=&share='
    	})
    	$(document).on('click','#weiboLogin',function(){
    		location.href="http://wx.1758.com/game/platform/v3.0/weibo/login?appKey="+dataPara.appkey+"&state=&chn=&share=";
    	})
    }
    
    //初始化与cp之间的信息交流
    function cpInfoPostInit(){
    	if(typeof window.addEventListener != 'undefined') {
		    window.addEventListener('message', receiveCpInfo, false);
		} else if (typeof window.attachEvent != 'undefined') {
		    window.attachEvent('onmessage', receiveCpInfo);
		}
    }
    //隐藏信息提示页
    function hideShareTipInfo(){
    	var shareTip = $('#shareTipInfo');
    	if(shareTip){
    		shareTip.addClass('gone');
    	}
    }
    //接受cp信息
    function receiveCpInfo(e){
    	var dataInfo = e.data,
    		test = {},
    		func;
    	func = {
    		setShareReward: function(arg){
    			if(Object.prototype.toString.call(arg) === '[object Array]'){
    				var str = '';
    				for(var i = 0,len = arg.length;i<len;i++){
    					str += '<p>'+arg[i]+'</p>'
    				}
    				$('#rewardWrapper').html(str);
    			}
    			$('#shareTipInfo').removeClass('gone');
    		},
    		showShareTipInfo:function(){
    			$('#shareTipInfo').removeClass('gone');
    		}
    	}
    	if(dataInfo.hlmy){
    		//接受cp信息
    		if(dataInfo.type == 'share'){
    			if(dataInfo.value.share == 'shareInfo'){
	    			var cpinfo = dataInfo.value.shareInfo;
					if(typeof(cpinfo.tipInfo)=='boolean' && cpinfo.tipInfo){
						//true 显示信息
						func.setShareReward(cpinfo.reward);
					}
					dataPara.messageFromCp = cpinfo;
					dataPara.isCpShareInfo = true;
	    			setShareInfo();
    			}
    		}else if(dataInfo.type == 'pay' && dataInfo.value.fn == 'payInfo'){
    			var para = dataInfo.value.args;
    			showPayPageInfo(para);
    		}else if(dataInfo.type == 'pay' && dataInfo.value.fn == 'payCancleBack'){
    			createDelPayPage('del');
    		}else if(dataInfo.type == 'follow'){
    			followWxNum();
    		}
//  		else if(dataInfo.type == 'fn'){
//  			func[dataInfo.value.fn].apply(window,dataInfo.value.args);
//  		}
    	}
    }
    //根据cp的调用 显示 支付页面
    function showPayPageInfo(para){
    	//支付
		var src = 'http://wx.1758.com/pay/buy?v1758=3&appKey='+dataPara.appkey;
		// var src="http://wx.1758.com/pay/buy?v1758=3&appKey=e40f4b393f5ff5082eac4f3ba1b61035"
		for(var p in para){
			if(para.hasOwnProperty(p)){
				if(p == 'appKey'){
					continue;
				}else{
					src += '&'+p+'='+para[p];
				}
			}
		}
		//她社区和tclyy用户在iframe下拿不到cookie 所以她社区的支付 走2.0支付
		if(HLMY_CHNINFO.payVersion != 3){
			src = src.replace('v1758=3','v1758=');
			top.window.location.href = src;
		}else{
			createDelPayPage('show',src);
		}
    }
    function createDelPayPage(flag,src){
    	var bl = '';
    	var str = '';
    	flag === 'del' ? bl = false : bl = true;
    	if(bl){
    		//true 为添加payiframe
    		// str += '<div id="payWrapper" class="pay-wrapper" style="height: 100%;width: 100%;"><div class="inner-pay-wrapper"><div id="payClose" class="pay-close"><img src="http://images.1758.com/image/20160831/open_1_6f57ffea4e579ea726c90e8b53cc1ebb.png"></div><iframe id="payFrame" src="'+src+'" height=100% width=100% name="payframe" scrolling="yes" frameborder="0"></iframe></div></div>'
    		str += '<div id="payWrapper" class="pay-wrapper" style="height: 100%;width: 100%;"><iframe id="payFrame" src="'+src+'" height=100% width=100% name="payframe" scrolling="yes" frameborder="0"></iframe></div>'
    		$(document.body).append(str);
    	}else{
    		//false 删除payiframe
    		$('#payWrapper').remove();
    	}
    }
    //根据cp传回来的信息，进行注入
    //组装发送给cp的信息
    function setInfoForSend(shareType){
    	var st = '';
		if(shareType == "appmessage" || shareType == "qq"){
			st = 'onShareFriend';
		}else if(shareType == 'timeline' || shareType == 'qzone'){
			st = 'onShareTimeline';
		}
		var info = {
			hlmy:true,
			from:'1758',
			type:'fn',
			value:{
				'fn':st,
				args:[]
			}
		}
		sendCpInfo(info);
    }
    //发送给cpinfo
    function sendCpInfo(info){
    	window.frames['cpframe'].postMessage(info,'*');
    }
    //关注微信方法
    function followWxNum(){
    	// var str = '<div id="ewmCode" class="ewm-contain"><div class="ewm-wripper"><img src="http://images.1758.com/game/m/ewm.png"/></div></div>';
    	// $(document.body).append(str);
    	$('#ewmCode').removeClass('gone');
    }
    //判断渠道号为tashequ712则显示提示页面
    function taSheQuHandleTip(){
    	if(HLMY_CHNINFO.enable && !taSheQuLocalStorage()){
    		// 显示信息
    		showTaSheQuTip();
    	}
    }
    function showTaSheQuTip(){
    	// var domHtml = '';
    	// domHtml = '<div id="taSheQu" class="tashequ"><img src=""/></div>';
    	// $(document.body).append(domHtml);
    	$('#taSheQu').removeClass('gone');
    }
    // 
    /**
     * 处理她社区的localstorage信息
     * @return {[type]} [description]
     * false 表示没有显示过，
     * true 则表示已经显示过 无需再次显示
     */
    function taSheQuLocalStorage(){
    	if(!window.localStorage){
    		return false;
    	}
    	var chninfo = window.localStorage.chninfo;
    	if(!!chninfo){
    		chninfo = JSON.parse(chninfo);
    		for(var i = 0,len = chninfo.length; i<len;i++ ){
				if(chninfo[i].appkey == dataPara.appkey && chninfo[i].ta == 1){
					return true;
					break;
				}
			}
			return false;
    	}
    	return false;
    }
    /**
     * 更新localstorage 把她社区的标志chninfo写入
     * @return {[type]} [description]
     */
    function updataTaSheQuLocalStorage(){
    	if(!window.localStorage){
    		return false;
    	}
    	var chninfo = window.localStorage.chninfo;
    	var mark = false;
    	if(!!chninfo){
    		//chninfo存在
    		chninfo = JSON.parse(chninfo);
    		if(chninfo.length > 0){
    			//有内容
    			for(var i=0,len=chninfo.length;i<len;i++){
    				if(chninfo[i].appkey == dataPara.appkey){
						mark = true;
						chninfo[i].ta = 1;
    					break;
    				}
    			}
    		}
    	}else{
    		chninfo = [];
    	}
    	if(!mark){
    		var item = {'appkey':dataPara.appkey,'ta':1};
    		chninfo.push(item);
    	}
    	chninfo = JSON.stringify(chninfo);
    	window.localStorage.setItem('chninfo',chninfo);
    }
    
    $(document).on('click','#talk .head',function(){
    	// $('#talk .list').animate({scrollTop:0},500);
    	$('#talk .list').scrollTop(0);
    })
    $(document).on('click','#gift .head',function(){
    	// $('#talk .list').animate({scrollTop:0},500);
    	$('#gift .gift-content-wrapper').scrollTop(0);
    })
    //关闭二维码
    $(document).on('click','#ewmCode',function(){
    	$('#ewmCode').addClass('gone');
    })
    // 她社区点击我知道了提示
    $(document).on('click','#IKnow',function(){
    	//默认是false 表示不勾选
    	if(dataPara.taTip){
    		//已经是true了  点击表示关闭
    		$('#selectImg').attr('src','http://images.1758.com/partner/ta/noselect.png');
    		dataPara.taTip = false;
    	}else{
    		//false了  点击表示确定
    		$('#selectImg').attr('src','http://images.1758.com/partner/ta/selected.png');
    		dataPara.taTip = true;
    	}
    })
    //她社区关闭提示按钮
    $(document).on('click','#taBackBtn',function(){
    	$('#taSheQu').addClass('gone');
    	if(dataPara.taTip){
    		// 写入localStorage，下次无需再次显示
    		updataTaSheQuLocalStorage();
    	}
    })
    /**
     * 根据特殊需求替换分享链接中的chn的值
     * @param  {[type]} link 分享的link地址
     * @return {[type]}      返回修改后的link地址
     */
    function changeShareChn(link){
    	link = $.trim(link);
    	var shareChn = $.trim(HLMY_CHNINFO.shareChn),
    		chn = '';
    	if(!!shareChn){
    		//不为空的情况下，分享的chn替换为当前的chn
    		chn = EventBase.getParameters(link,'chn');
    		if(!!chn){
    			link = link.replace('chn='+chn,'chn='+shareChn);
    		}else if(!chn && link.indexOf('chn')>0){
    			link = link.replace('chn=','chn='+shareChn);
    		}else{
    			if(link.indexOf('?')>0){
    				link = link + '?chn='+shareChn;
    			}else{
    				link = link + '&chn='+shareChn;
    			}
    		}
    	}
    	return link;
    }
    /**
     * 根据HLMY_GAMEINFO提供的消息
     * 是否进行ajax 查询
     * 如果查询返回的内容为true，则弹出二维码让用户关注
     * @return {[type]} [description]
     */
    function checkFollowInfo(){
    	if(HLMY_GAMEINFO.promptSubscribeEnable){
    		setTimeout(function(){
				$.post('/play/subscribe/getStrategy.json',{'appKey':dataPara.appkey},function(responseData){
					if(responseData.result == 1){
						var src = responseData.data.subscribeImgUrl;
						$('#followEwmCode img').attr('src',src);
						$('#followEwmCode').removeClass('gone');
					}
				})
    		},HLMY_GAMEINFO.promptSubscribeDelay * 1000);
    	}
    }
    //关闭关注的二维码
    $(document).on('click','#followEwmCode',function(){
    	$('#followEwmCode').addClass('gone');
    })
    /**
     * 获取登录版本号，区别在分享回调函数上面
     * 3.0以上 使用的是h5的postmessage 方法
     * 2.0 使用的是iframe的share文件方法
     * @return {[type]} [description]
     */
    function getVerLogin(){
    	var verlogin = EventBase.getParameters('verLogin');
    	var vlogin = EventBase.getParameters('v');
    	if(!!verlogin){
    		//3.0版本
    		dataPara.verLogin = '3';
    		console.log('vlogin:3.0');
    	}else if(vlogin == '3'){
    		//3.0版本
    		dataPara.verLogin = '3';
    		console.log('vlogin:3_old');
    	}else{
    		//2.0版本
    		dataPara.verLogin = '2';
    		console.log('vlogin:2.0');
    	}
    }
    // 点击关闭支付弹，支付弹出从全屏变为中间部分
    // 暂时无用
    $(document).on('click','#payWrapper,#payClose',function(){
    	$('#payWrapper').remove();
    })
});