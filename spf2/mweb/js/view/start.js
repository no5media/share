/**
 * 无用
 * 游戏启动页面的图片旋转动画
 * 该js暂时无效
 */
!function(window) {
    function isParent() {
        try {
            return window.top == window ? !1 : !0
        } catch (a) {
            return !0
        }
    }
    function getGrid(degree, wid, hei) {
        return {
            x: wid * Math.cos(degree),
            y: hei * Math.sin(degree)
        }
    }
    //返回角度
    function degree(ang) {
        return Math.PI / 180 * ang
    }
    function setPos(a) {
        function b(imgItem) {
            var scale, pos = {};
            imgItem.ang = imgItem.ang >= 360 ? 0 : imgItem.ang + imgItem.step,
            pos = getGrid(degree(imgItem.ang), imgItem.wid, imgItem.hei),
            scale = imgItem.ang > 90 ? Math.abs(270 - imgItem.ang) / 180 : (imgItem.ang + 90) / 180,
            scale = .66 + scale / 3,
            document.getElementById(imgItem.mid).style.webkitTransform = "translate(" + pos.x + "px, " + pos.y + "px) scale(" + scale + ")"
        }
        for (var imgList = [{
            wid: byId("l0").offsetWidth / 2,
            hei: byId("l0").offsetHeight / 2,
            time: 35,
            ang: 90,
            mid: "roate_1",
            step: 1
        }, {
            wid: byId("l0").offsetWidth / 2,
            hei: byId("l0").offsetHeight / 2,
            time: 35,
            ang: 280,
            mid: "roate_2",
            step: 1
        }, {
            wid: byId("l1").offsetWidth / 2,
            hei: byId("l1").offsetHeight / 2,
            time: 35,
            ang: 30,
            mid: "roate_3",
            step: 1
        }, {
            wid: byId("l1").offsetWidth / 2,
            hei: byId("l1").offsetHeight / 2,
            time: 35,
            ang: 230,
            mid: "roate_4",
            step: 1
        }], i = 0; i < imgList.length; i++){
            !function() {
                var imgItem = imgList[i];
                window.clearInterval(l[imgItem.mid]),
                l[imgItem.mid] = window.setInterval(function() {
                    b(imgItem)
                }, imgItem.time)
            }()
        }
    }
    function f(boo) {
        var c = boo ? Math.min(window.innerWidth / p, window.innerHeight / q) : window.innerWidth / p, 
        	d = parseInt(1e4 * c * r) / 1e4;
        return d
    }
    isParent() && (parent.window.location.href = location.href);
    var docObj, h, i, j, 
    byId = function(a) {
        return document.getElementById(a)
    }, 
    l = {}, 
    m = 0, 
    docElement = document.documentElement, 
    docBody = document.body, 
    p = 1080, 
    q = 1920, 
    r = 67.5;
    docBody ? docBody.style.display = "none" : !1,
    !function(boo, setPos) {
        if (docObj = docElement.getBoundingClientRect().width,j = j ? j : arguments.callee,docObj !== window.innerWidth && 30 > m){
            window.setTimeout(function() {
                m++,
                j(boo, setPos)
            }, 0);
        }else {
            if (h = f(boo),h + "px" !== getComputedStyle(docElement)["font-size"]){
                return docElement.style.fontSize = h + "px",docBody ? docBody.style.display = "block" : !1,setPos && setPos(h);
            }
            docBody ? docBody.style.display = "block" : !1
        }
    }(!0, setPos),
    window.addEventListener("resize", function() {
        clearTimeout(i),
        i = setTimeout(function() {
            j(!0, setPos)
        }, 100)
    }, !1);
//  stopInterval();
    function stopInterval(){
	    var gframe = document.getElementById('gameframe');
		if (gframe.attachEvent) {
	        gframe.attachEvent('onload', handleStopInterval);
	    }else { 
	        gframe.onload  = handleStopInterval;
	    }
    }
    //8秒之后，不管怎样绿屏界面都消失
    setTimeout(function(){
    	handleStopInterval();
    	var gframe = document.getElementById('gameframe');
    	gframe.style.display = 'block';
	},1000*8);
    function handleStopInterval(){
    	window.clearInterval(l['roate_1']);
    	window.clearInterval(l['roate_2']);
    	window.clearInterval(l['roate_3']);
    	window.clearInterval(l['roate_4']);
    }
}(window)