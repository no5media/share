define(['jquery'],function($){
	var obj = {
		//获取分享详细
		getShareInfo:function(url,infos,callback){
			//url为当前页面地址，用于获取微信配置信息。
			$.get(url,infos,callback,'jsonp');
		},
		getData:function(url,infos,callback){
			$.post(url,infos,function(data){
				callback(data);
			},'json')
		}
	};
	return obj;
});