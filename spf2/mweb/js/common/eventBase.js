/**
 * 定义常用事件
 */
define(['jquery','libs/doT'],function($,doT){
	var obj = {
		getParameters:function(url,name){
			if(arguments.length == 1){
				name = url;
				url = window.location.href;
			}
			//正则验证
			 var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
		     var r = url.substr(url.indexOf('?')).substr(1).match(reg);
		     if(r!=null){
		     	return  decodeURIComponent(r[2]);
		     } 
		     return '';
		},
		//在最大最小值之间获取个随机数
		getRandomNun : function (Min,Max){
	        var range = Max-Min;
	        var rand = Math.random();
	        return (Min+Math.floor(rand * range))
    	},
    	//加载游戏地址
		loadPage:function (gameurl){
			var gframe = window.document.getElementById('gameframe');
			gframe.src= gameurl;
			if (gframe.attachEvent) {
		        gframe.attachEvent('onload', hideLoading);
		    } else {
		        gframe.onload  = hideLoading;
		    }
		},
		//设置title
		setTitle:function (htmlTitle){
			document.title= htmlTitle;
		},
		//得到随机分享语
		randomShareContent: function (shareText,gid){
			var textarray,
				len,
				num;
			textarray = shareText;
			len = textarray.length;
			var conApp = {};
			if(len > 0){
				num = this.getRandomNun(0,len);
				conApp.desc = textarray[num].sharedesc;
				conApp.title = textarray[num].sharetitle;
				conApp.imgUrl = textarray[num].shareimg;
				if(textarray[num].sharelink.indexOf('?') >-1){
					conApp.link = textarray[num].sharelink +'&share='+gid;
				}else{
					conApp.link = textarray[num].sharelink+'?share='+gid;
				}
				conApp.sid = textarray[num].sid;
				return conApp;
			}else{
				return;
			}
		},
		getRandomNun:function(Min,Max){
			var k = Max-Min;
			var rand = Math.random();
			return (Min+Math.floor(rand * k))
		},
		//分享成功向cp发送请求
		//该方法调用方法通知cp
	    exec_iframe: function (shareType,cpShareUrl){  
	        if(typeof(exec_obj)=='undefined'){  
	            exec_obj = document.createElement('iframe');  
	            exec_obj.name = 'tmp_frame';  
	            exec_obj.src = cpShareUrl+'?shareType='+shareType;  
	            exec_obj.style.display = 'none';  
	            document.body.appendChild(exec_obj);  
	        }else{  
	            exec_obj.src = cpShareUrl+'?shareType='+shareType+'&' + Math.random();  
	        }  
	    },
	    //判断用户客户端信息
	    userTerminal:function(){
	    	var ua = window.navigator.userAgent.toLowerCase();
			if(ua.match(/MicroMessenger/i) == 'micromessenger' && !(ua.match(/dwjia/i) == 'dwjia')){
				return 'weixin';
			}else if(ua.match(/dwjia/i) == 'dwjia'){
				if(ua.match(/iPhone/i) == 'iphone' || ua.match(/ios/i) == 'ios'){
					return 'iphoneclient';
				}else if(ua.match(/Android/i) == 'android'){
					return 'androidclient';
				}
			}else{
				var fork = this.getParameters('fork');
				if(fork == 4){
					return 'androidclient';
				}else if(fork == 5){
					return 'iphoneclient';
				}
				return 'others';
			}
	   },
	   //load 模板
		loadTem:function(url){
			var domTem = '<div id="dot-template" style="display:none;"></div>'
			var htmlTem;
			var str;
			$.ajax({
				type:"get",
				datatype:'html',
				url:url,
				async:false,
				success:function(data){
					domTem = $(domTem).append(data);
					$(document.body).append(domTem);
					htmlTem = $('#dot-template');
					$('#dot-template').remove();
				}
			});
			return htmlTem;
		},
		replaceTem:function(str){
			str = str.replace(/\&lt;/g,'<').replace(/\&gt;/g,'>').replace(/\&amp;\&amp;/g,'&&');
			return str;
		},
		/**
		 * 加载template 利用正则去匹配
		 */
		loadTemplate: function(url){
			var that = this,
				tpl;
			$.ajax({
				type:'get',
				datatype:'html',
				url:url,
				async:false,
				success:function(data){
					tpl = that.parseTemplate(data);
				}
			});
			return tpl;
		},
		parseTemplate: function(tplStr){
			var tplArr = tplStr.replace(/\r\n\s*/g,'').split('</template>'),
				tplData = [],	//模板数组
				tplList = {},	//模板集合（id 和 数组）
				templStart = '',
				templId = '';
				for(var i = 0,len = tplArr.length; i<len-1;i++){
					if(tplArr[i].length < 5) continue;
					
					templStart = tplArr[i].indexOf('>') + 1;
					tplData.push(tplArr[i].slice(templStart));
					templId = tplArr[i].slice(0,templStart).match(/tpl-id="(.*)"/);
					if(!!templId){
						tplList[templId[1]] = tplArr[i].slice(templStart);
					}
				}
				return tplList;
		},
		/**
		 * 编译模板，转换成dom节点
		 * @param {Object} tem 模板
		 * @param {Object} data 数据
		 */
		compileTem:function(tem,data){
			var ss = doT.template(tem);
			var kk = ss(data);
			return kk;
			//return doT.template(tem)(data);
		},
		/**
		 * 日期格式转换，
		 * @param {Object} date
		 * @param {Object} fmt 默认格式yyyy-MM-dd hh:mm:ss
		 */
		dateFormat:function(date,fmt){
			if(typeof(fmt) == 'undefined'){
				fmt = 'yyyy-MM-dd hh:mm:ss' 
			}
			return (Format((new Date(date)),fmt));
		},
		setLocalStorage:function(name,value){
			if(window.localStorage){
				var losto = window.localStorage;
				losto.setItem(name,value)
			}
		},
		/**
		 * 洗牌算法
		 * @param  {[type]} arr [description]
		 * @return {[type]}     [description]
		 */
		shuffle:function(arr){
			var m = arr.length,
		        t, i;
		    // 如果还剩有元素…
		    while (m) {
		        // 随机选取一个元素…
		        i = Math.floor(Math.random() * m--);
		        // 与当前元素进行交换
		        t = arr[m];
		        arr[m] = arr[i];
		        arr[i] = t;
		    }
		    return arr;
		}
	};
	
	function Format(date,fmt){
	    var o = {
	        "M+": date.getMonth() + 1, //月份 
	        "d+": date.getDate(), //日 
	        "h+": date.getHours(), //小时 
	        "m+": date.getMinutes(), //分 
	        "s+": date.getSeconds(), //秒 
	        "q+": Math.floor((date.getMonth() + 3) / 3), //季度 
	        "S": date.getMilliseconds() //毫秒 
	    };
	    if (/(y+)/.test(fmt)) {
	    	fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
	    }
	    for (var k in o){
	    	if (new RegExp("(" + k + ")").test(fmt)) {
	    		fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
	    	}
	    }
	    return fmt;
	}
	return obj;
})