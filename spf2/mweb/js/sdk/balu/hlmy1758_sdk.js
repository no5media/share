(function(window){
	var hlmy = function(){
		this.version = '1.0.7';
		console.log(this.version);
	};
	hlmy.prototype = {
		postData:function(data){
			data.hlmy = true;
			top.postMessage(data, '*');
		},
		//设置分享信息
		setShareInfo: function(para){
			this.postData({
				type:'share',
				value:{
					share:'shareInfo',
					shareInfo: para
				}
			});
		},
		//支付调用接口
		pay:function(para){
			this.postData({
				type:'pay',
				value:{
					fn:'payInfo',
					args:para
				}
			});
		},
		follow:function(){
			this.postData({
				type:'follow',
				value:{
					fn:'followWx',
					args:[]
				}
			})
		}
	}
	window.hlmy = new hlmy();
	window.addEventListener('message',function(evt){
		var b = {
			onShareTimeline:function(args){
				"function" === typeof(onShareTimeline) && onShareTimeline(args);
			},
			onShareFriend:function(args){
				"function" === typeof(onShareFriend) && onShareFriend(args);
			}
		}
		if(evt.data.hlmy){
			switch(evt.data.type){
				case 'fn':
					b[evt.data.value.fn].apply(window,evt.data.value.args);
					break;
				default :
					console.log('');
			}
		}
	},!1)
}(this))
