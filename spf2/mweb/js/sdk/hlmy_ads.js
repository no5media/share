(function(window){
	var _hlmy = function(){
		this.ads_version = '1.0.0';
		console.log(this.ads_version);
		// 事件回调
		this.onCheckAd = undefined;
		this.onPlayAd = undefined;
	}
	_hlmy.prototype = {
		postData:function(data){
			data.hlmy = true;
			window.parent.postMessage(data, '*');
		},
		checkAds:function(callback,flag){
			this.onCheckAds = callback;
			this.postData({
				type:'fn',
				value:{
					fn:'checkAds',
					args:[],
				}
			});
		},
		playAds:function(callback){
			this.onPlayAd = callback;
			this.postData({
				type:'fn',
				value:{
					fn:'playAds',
					args:[],
				}
			});
		},
	}
	window.HLMY_ADS = new _hlmy();
	window.addEventListener('message',function(evt){
		var b = {
			hlmyAds:function(args){
				"function" === typeof(hlmyAds) && hlmyAds(args);
			},
			playResult:function(args){
				"function" === typeof(onShareFriend) && onShareFriend(args);
			}
		}
		if(evt.data.hlmy){
			switch(evt.data.type){
				case 'fn':
					b[evt.data.value.fn].apply(window,evt.data.value.args);
					break;
				default :
					console.log('');
			}
		}
	},!1)
}(this))