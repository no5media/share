(function(){
	var _hlmy = function() {
		this.appKey = '';
		this.gid = '';
		this.hlmy_gw = '';
		this.pf = ''; 
	};
	var _url={
		pay:'http://wtest.1758.com/pay/partner/payInit.jsonp',
		share:'',
		init:'http://wtest.1758.com/play/partner/partnerInit.jsonp'
	};
	_hlmy.prototype = {
		'postData':function(data){
			data.hlmy = true;
			top.postMessage(data, '*');
		},
		/**
		 * 支付
		 * @param  {[type]} safeCode [description]
		 * @return {[type]}          [description]
		 */
		'pay':function(safeCode) {
			var infos = {};
			if(!!this.appKey && !!this.gid && !!this.hlmy_gw){
				infos.paySafecode = safeCode;
				if(this.pf == '1758'){
					this.pay1758(infos);
				}else{
					infos.appKey = this.appKey,
					infos.gid = this.gid;
					infos.hlmy_gw = this.hlmy_gw;
					this.dynamicScript(_url.pay,infos);
				}
			}else{
				console.log('参数不足');
			}
		},
		'pay1758':function(data){
			this.postData({
					type:'pay',
					value:{
						fn:'payInfo',
						args:data
					}
				});
		},
		/**
		 * 关注
		 * @return {[type]} [description]
		 */
		'follow': function(){
			if(this.pf == '1758'){
				this.postData({
					type:'follow',
					value:{
						fn:'followWx',
						args:[]
					}
				});
			}else{
				console.log('this is not 1758 platform');
			}
		},
		//综合设置分享信息
		'setShareInfo': function(data){
			if(this.pf == '1758'){
				this.postData({
					type:'share',
					value:{
						share:'shareInfo',
						shareInfo: data
					}
				});
			}else{
				this.dynamicScript(_url.share,{
					appKey:this.appKey,
					gid:this.gid,
					hlmy_gw:this.hlmy_gw,
					data:data
				},'',true);
			}
		},
		onShareTimeline:function(args){
			"function" === typeof(onShareTimeline) && onShareTimeline(args);
		},
		onShareFriend:function(args){
			"function" === typeof(onShareFriend) && onShareFriend(args);
		},
		dynamicScript:function(url,data,success,bl){
			var dataString = '';
			//处理calback
			var jsonpcallback = "jsonpcallback" + (Math.random() + "").substring(2);
			if(typeof success !== 'function'){success=function(){}};
			window[jsonpcallback] = function(){
				success();
			}
			if (typeof data == "object" && data != null) {
				data['callback'] = jsonpcallback;
				for (var p in data) {
					dataString = dataString + "&" + p + "=" + escape(data[p]);
				}
			}
			if (url.indexOf("?") > 0) {
				url = url + "&" + dataString;
			} else {
				url = url + "?" + dataString;
			}
			var script = document.createElement('script');
			script.type = "text/javascript";
			script.src = url;
			script.onload = function(){
				if(bl){
					head.removeChild(script);
				}
			}
			var head = document.getElementsByTagName('head').item(0);
			head.appendChild(script);
		},
		'kefu': function(callback) {
			
		},
		'reload' : function() {
			// location.reload();
			top.location = top.location;
		}
	};
	window.HLMY_SDK = new _hlmy();
	window.HLMY_SDK.init =  function(appKey,gid,hlmy_gw){
		console.log(this);
		this.appKey = appKey || '';
		this.gid = gid || '';
		this.hlmy_gw = hlmy_gw || '';
		if(typeof hlmy_gw === 'undefined'){
			this.pf = ''; 
		}else{
			this.pf = hlmy_gw.split('_')[0];
		}
		var datainfo = {
			appKey: this.appKey,
			gid:this.gid,
			hlmy_gw:this.hlmy_gw
		}
		this.dynamicScript(_url.init,datainfo,'',true);
	}
	/*监听*/
	window.addEventListener('message',function(evt){
		var b = {
			onShareTimeline:function(args){
				"function" === typeof(onShareTimeline) && onShareTimeline(args);
			},
			onShareFriend:function(args){
				"function" === typeof(onShareFriend) && onShareFriend(args);
			}
		}
		if(evt.data.hlmy){
			switch(evt.data.type){
				case 'fn':
					if('function' === typeof b[evt.data.value.fn]){
						b[evt.data.value.fn].apply(window,evt.data.value.args);
					}
					break;
				default :
					console.log(evt.data.type);
			}
		}
	},!1)	
})();







