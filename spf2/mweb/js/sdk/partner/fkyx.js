var res = function(){
	window.hlmyUtils.init();
	var theHead = document.getElementsByTagName("head")[0] || document.documentElement;
	var scriptControll = document.createElement("script");
	scriptControll.charset = "utf-8";
	theHead.insertBefore(scriptControll, theHead.lastChild);
	scriptControll.src = '';
	scriptControll.onload = function(){
		initconfig();
	}
	function initconfig (){
		var sdk = window.HORTOR_AGENT;
		sdk.init();
		sdk.config({
			gameId: "",
			share: {
				timeline: {
					title: "",
					imgUrl: "",
					success: function(){
						hlmyUtils.timeline.success();
					},
					cancel: function(){
						hlmyUtils.timeline.cancel();
					}
				},
				friend: {
					title: "",
					desc: "",
					imgUrl: "",
					success: function(){
						hlmyUtils.friend.success();
					},
					cancel: function(){
						hlmyUtils.friend.cancel();
					}
				}
			}
		});
	}
}