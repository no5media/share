(function(window){
	var hlmy = function(){
		this.version = '1.1.2';
		console.log(this.version);
		_hlmy1758.init();
	};
	hlmy.prototype = {
		postData:function(data){
			data.hlmy = true;
			top.postMessage(data, '*');
		},
		/**
		 *{
		 * 	title:'',
		 * 	desc
		 * 	imgUrl
		 * 	state
		 * 	tipInfo
		 * 	reward
		 * }
		 * 
		 */
		//综合设置分享信息
		setShareInfo: function(para){
			if(_hlmy1758.chnType == 'hlmy'){
				this.postData({
					type:'share',
					value:{
						share:'shareInfo',
						shareInfo: para
					}
				});
			}else if(_hlmy1758.chnType == 'sinaGame'){
				var gid = para.gid || '';
				var obj = new _hlmy1758.jsonp({
					url:'http://wx.1758.com/play/game/share/share2Weibo.jsonp',
					data:{
						appKey:para.appKey,
						gid:gid
					},
					success:function(res){
						if(res.result == '1'){
							"function" === typeof(onShareTimeline) && onShareTimeline();
						}else{
							console.log('sina share fail');
						}
					}
				});
				obj.request();
			}else{
				this.postData({
					type:'share',
					value:{
						share:'shareInfo',
						shareInfo: para
					}
				});
			}
		},
		//支付调用接口
		pay:function(para){
			var appkey = '';
			var str = '';
			if(_hlmy1758.chnType == 'hlmy'){
				this.postData({
					type:'pay',
					value:{
						fn:'payInfo',
						args:para
					}
				});
			}else{
				for(var i in para){
					if(i == 'appKey'){
						appkey = para[i]
					}else if(i == 'itemCode'){
						str += '&itemCode='+para[i];
					}else if(i == 'txId'){
						str += '&txId='+para[i];
					}else if(i == 'state'){
						str += '&state='+para[i];
					}
				}
				location.href = _hlmy1758.partner[_hlmy1758.chnType].payUrl+appkey+str;
			}
		},
		follow:function(){
			this.postData({
				type:'follow',
				value:{
					fn:'followWx',
					args:[]
				}
			})
		},
		isAndroid: function() {
			return /android/i.test(navigator.userAgent);
		},
		isIos: function() {
			return /iphone|ipod|ios|ipad/i.test(navigator.userAgent);
		},
		isHlmyAndroid: function() { 
			return (/dwjia/i.test(navigator.userAgent) && /android/i.test(navigator.userAgent));
		},
		isHlmyIos: function(){
			return (/dwjia/i.test(navigator.userAgent) && /iphone|ipod|ios|ipad/i.test(navigator.userAgent));
		},
		userTerminal: function(){
			var ua = window.navigator.userAgent.toLowerCase();
			if(ua.match(/dwjia/i) == 'dwjia' && this.isIos()){
				return 'hlmyIos'; 
			}else if(ua.match(/dwjia/i) == 'dwjia' && this.isAndroid()){
				return 'hlmyAndroid';
			}else if(ua.match(/MicroMessenger/i) == 'micromessenger' && !(ua.match(/dwjia/i) == 'dwjia')){
				return 'wx';
			}
			
		}
	}
	//内部使用对象方法
	var _hlmy1758 = {
		chnType:'hlmy',		//标记该游戏在那个渠道
		appkey: '',
		//初始化获取游戏所在的渠道
		init:function(){
			var gw = this.getParameters('hlmy_gw');
			var ct = gw.split('_')[0];
			switch(ct){
				case '57':
				case '1':
					//美图1为兼容处理
					this.chnType = 'meitu';
				break;
				case '58':
					//梦三国
					this.chnType = 'mengsg';
				break;
				case '59':
					//新浪游戏
					this.chnType = 'sinaGame';
				break;
				case '60':
					// 9G
					this.chnType = 'game9g';
				break;
				case '61':
					//qq阅读
					this.chnType = 'qqRead';
				break;
				case '62':
					//起点阅读
					this.chnType = 'qdRead';
				break;
				case '63':
					//疯狂游乐场
					this.chnType = 'fkyx';
				break;
				case '64':
					//群黑游戏
					this.chnType = 'qhyx';
				break;
				case '65':
					//pla68平台
					this.chnType = 'play68';
				break;
			}
		},
		partner:{
			"game9g":{
				payUrl:'http://9g.wan-youxi.cn/pay/buy?appKey='
			},
			"meitu":{
				payUrl:'http://wx.1758.com/pay/buy?appKey='
			},
			"sinaGame":{
				payUrl:'http://wx.1758.com/pay/buy?appKey='
			},
			"mengsg":{
				payUrl:'http://wx.1758.com/pay/buy?appKey='
			},
			"qqRead":{
				payUrl:'http://yuewenqq.wan-youxi.cn/pay/buy?appKey='
			},
			"qdRead":{
				payUrl:'http://yuewenqd.wan-youxi.cn/pay/buy?appKey='
			},
			"fkyx":{
				payUrl:'http://fkyx.wan-youxi.cn/pay/buy?appKey='
			},
			"qhyx":{
				payUrl:'http://qhyx.wan-youxi.cn/pay/buy?appKey='	
			},
			"play68":{
				payUrl:'http://68.wan-youxi.cn/pay/buy?appKey='
			}

		},
		getParameters:function(url,name){
			if(arguments.length == 0){
				return '';
			}else if(arguments.length == 1){
				name = url;
				url = window.location.href;
			}
			//正则验证
			 var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
		     var r = url.substr(url.indexOf('?')).substr(1).match(reg);
		     if(r!=null){
		     	return  decodeURIComponent(r[2]);
		     } 
		     return '';
		},
		jsonp:function(param){
			var url = param.url || '';
			var data = param.data || {};
			var cbname = param.cbname || 'callback';
			var success = param.success || function(){};

			var finish = false;
			var theHead = document.getElementsByTagName("head")[0] || document.documentElement;
			var scriptControll = document.createElement("script");
			var jsonpcallback = "jsonpcallback" + (Math.random() + "").substring(2);
			var collect = function() {
				if (theHead != null) {
					theHead.removeChild(scriptControll);
					try {
						delete window[jsonpcallback];
					} catch (ex) { }
					theHead = null;
				}
			};
			var init = function() {
				scriptControll.charset = "utf-8";
				theHead.insertBefore(scriptControll, theHead.firstChild);
				window[jsonpcallback] = function(responseData) {
					finish = true;
					success(responseData);
					timer();
				};
				cbname = cbname || "callback";
				if (url.indexOf("?") > 0) {
					url = url + "&" + cbname + "=" + jsonpcallback;
				} else {
					url = url + "?" + cbname + "=" + jsonpcallback;
				}
				if (typeof data == "object" && data != null) {
					for (var p in data) {
						url = url + "&" + p + "=" + escape(data[p]);
					}
				}
			};
			var timer = function() {
				if (typeof window[jsonpcallback] == "function") {
					collect();
				}
			};
			this.request = function() {
				init();
				scriptControll.src = url;
			};
		}
	}
	window.hlmy = new hlmy();
	window.addEventListener('message',function(evt){
		var b = {
			onShareTimeline:function(args){
				"function" === typeof(onShareTimeline) && onShareTimeline(args);
			},
			onShareFriend:function(args){
				"function" === typeof(onShareFriend) && onShareFriend(args);
			}
		}
		if(evt.data.hlmy){
			switch(evt.data.type){
				case 'fn':
					b[evt.data.value.fn].apply(window,evt.data.value.args);
					break;
				default :
					console.log('');
			}
		}
	},!1)
}(this))
