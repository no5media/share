(function(window){
	var hlmy = function(){
		this.version = '1.0.7';
		console.log(this.version);
		_hlmy1758.init();
	};
	hlmy.prototype = {
		postData:function(data){
			data.hlmy = true;
			top.postMessage(data, '*');
		},
		/**
		 *{
		 * 	title:'',
		 * 	desc
		 * 	imgUrl
		 * 	state
		 * 	tipInfo
		 * 	reward
		 * }
		 * 
		 */
		//综合设置分享信息
		setShareInfo: function(para){
			this.postData({
				type:'share',
				value:{
					share:'shareInfo',
					shareInfo: para
				}
			});
		},
		//设置分享url的state参数(暂时无用)
		setShareUrlPara:function(state){
			this.postData({
				type:'share',
				value:{
					share:'sharePara',
					state:state
				}
			});
		},
		//设置分享内容(暂时无用)
		setShareText: function(title,desc){
			this.postData({
				type:'share',
				value:{
					share:'shareText',
					title:title,
					desc:desc
				}
			});
		},
		//设置分享图片(暂时无用)
		setShareImg: function(imgUrl){
			this.postData({
				type:'share',
				value:{
					share:'shareImg',
					imgUrl:imgUrl
				}
			});
		},
		//调用分享提示页面(暂时无用)
		showShareTipInfo: function(){
			this.postData({
				type:'fn',
				value:{
					fn:'showShareTipInfo',
					args:[]
				}
			});
		},
		//设置reward奖励：数组形式(暂时无用)
		setShareReward: function(reward){
			this.postData({
				type:'fn',
				value:{
					fn:'setShareReward',
					args:[reward]
				}
			});
		},
		//支付调用接口
		pay:function(para){
			var appkey = '';
			var str = '';
			if(_hlmy1758.chnType == 'hlmy'){
				this.postData({
					type:'pay',
					value:{
						fn:'payInfo',
						args:para
					}
				});
			}else{
				for(var i in para){
					if(i == 'appKey'){
						appkey = para[i]
					}else if(i == 'itemCode'){
						str += '&itemCode='+para[i];
					}else if(i == 'txId'){
						str += '&txId='+para[i];
					}else if(i == 'state'){
						str += '&state='+para[i];
					}
				}
				location.href = _hlmy1758.partner[_hlmy1758.chnType].payUrl+appkey+str;
			}
		},
		follow:function(){
			this.postData({
				type:'follow',
				value:{
					fn:'followWx',
					args:[]
				}
			})
		},
		isAndroid: function() {
			return /android/i.test(navigator.userAgent);
		},
		isIos: function() {
			return /iphone|ipod|ios|ipad/i.test(navigator.userAgent);
		},
		isHlmyAndroid: function() { 
			return (/dwjia/i.test(navigator.userAgent) && /android/i.test(navigator.userAgent));
		},
		isHlmyIos: function(){
			return (/dwjia/i.test(navigator.userAgent) && /iphone|ipod|ios|ipad/i.test(navigator.userAgent));
		},
		userTerminal: function(){
			var ua = window.navigator.userAgent.toLowerCase();
			if(ua.match(/dwjia/i) == 'dwjia' && this.isIos()){
				return 'hlmyIos'; 
			}else if(ua.match(/dwjia/i) == 'dwjia' && this.isAndroid()){
				return 'hlmyAndroid';
			}else if(ua.match(/MicroMessenger/i) == 'micromessenger' && !(ua.match(/dwjia/i) == 'dwjia')){
				return 'wx';
			}
			
		},
		getParameters:function(url,name){
			if(arguments.length == 0){
				return '';
			}else if(arguments.length == 1){
				name = url;
				url = window.location.href;
			}
			//正则验证
			 var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
		     var r = url.substr(url.indexOf('?')).substr(1).match(reg);
		     if(r!=null){
		     	return  decodeURIComponent(r[2]);
		     } 
		     return '';
		},
	}
	//内部使用对象方法
	var _hlmy1758 = {
		chnType:'hlmy',		//标记该游戏在那个渠道
		appkey: '',
		//初始化获取游戏所在的渠道
		init:function(){
			var gw = this.getParameters('hlmy_gw');
			var ct = gw.split('_')[0];
			if(ct == '60'){
				// 9G
				this.chnType = 'game9g';
			}else if(ct == '57' || ct == '1'){
				//美图1为兼容处理
				this.chnType = 'meitu';
			}else if(ct == '59'){
				//新浪游戏
				this.chnType = 'sinaGame';
			}else if(ct == '58'){
				//梦三国
				this.chnType = 'mengsg';
			}
		},
		partner:{
			"game9g":{
				payUrl:'http://9g.wan-youxi.cn/pay/buy?appKey='
			},
			"meitu":{
				payUrl:'http://wx.1758.com/pay/buy?appKey='
			},
			"sinaGame":{
				payUrl:'http://wx.1758.com/pay/buy?appKey='
			},
			"mengsg":{
				payUrl:'http://wx.1758.com/pay/buy?appKey='
			}
		},
		getParameters:function(url,name){
			if(arguments.length == 0){
				return '';
			}else if(arguments.length == 1){
				name = url;
				url = window.location.href;
			}
			//正则验证
			 var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
		     var r = url.substr(url.indexOf('?')).substr(1).match(reg);
		     if(r!=null){
		     	return  decodeURIComponent(r[2]);
		     } 
		     return '';
		}
	}
	window.hlmy = new hlmy();
	window.addEventListener('message',function(evt){
		var b = {
			onShareTimeline:function(args){
				"function" === typeof(onShareTimeline) && onShareTimeline(args);
			},
			onShareFriend:function(args){
				"function" === typeof(onShareFriend) && onShareFriend(args);
			}
		}
		if(evt.data.hlmy){
			switch(evt.data.type){
				case 'fn':
					b[evt.data.value.fn].apply(window,evt.data.value.args);
					break;
				default :
					console.log('');
			}
		}
	},!1)
}(this))
