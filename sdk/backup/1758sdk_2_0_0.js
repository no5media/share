/**
 * 1758sdk 2.0 版本
 * @return {[type]} [description]
 */
(function(){
	var _hlmy = function() {
		this.version = '2.0.0';
		console.log('version:',this.version);
	};
	var _hlmyUtil = {
		appKey:'',
		gid:'',
		hlmy_gw:'',
		pf:''
	};
	var _url={
		pay:'http://wx.1758.com/pay/v4/jsonp/payInit.jsonp',
		share:'http://wx.1758.com/play/v4/jsonp/share.jsonp',
		init:'http://wx.1758.com/play/v4/jsonp/init.jsonp'
	};
	_hlmy.prototype = {
		'postData':function(data){
			data.hlmy = true;
			top.postMessage(data, '*');
		},
		/**
		 * 支付
		 * @param  {[type]} safeCode [description]
		 * @return {[type]}          [description]
		 */
		'pay':function(obj) {
			var infos = {};
			if(!!_hlmyUtil.appKey && !!_hlmyUtil.gid && !!_hlmyUtil.hlmy_gw){
				infos.appKey = _hlmyUtil.appKey ;
				infos.gid = _hlmyUtil.gid;
				infos.hlmy_gw = _hlmyUtil.hlmy_gw;
				infos.paySafecode = obj.paySafecode;
				if(_hlmyUtil.pf == '0' || _hlmyUtil.pf == ''){
					this.pay1758(infos);
				}else{
					this.dynamicScript(_url.pay,infos);
				}
			}else{
				if(_hlmyUtil.appKey == ''){console.log('缺少appKey参数');};
				if(_hlmyUtil.gid == ''){console.log('缺少gid参数');};
				if(_hlmyUtil.hlmy_gw == ''){console.log('缺少hlmy_gw参数');};
			}
		},
		'pay1758':function(data){
			this.postData({
				type:'pay',
				value:{
					fn:'payInfo',
					args:data
				}
			});
		},
		/**
		 * 关注
		 * @return {[type]} [description]
		 */
		'follow': function(){
			if(_hlmyUtil.pf == '0' || _hlmyUtil.pf == ''){
				this.postData({
					type:'follow',
					value:{
						fn:'followWx',
						args:[]
					}
				});
			}else{
				console.log('this is not 1758 platform');
			}
		},
		//综合设置分享信息
		'setShareInfo': function(data){
			if(_hlmyUtil.pf == '0' || _hlmyUtil.pf == ''){
				this.postData({
					type:'share',
					value:{
						share:'shareInfo',
						shareInfo: data
					}
				});
			}else{
				this.dynamicScript(_url.share,{
					appKey:_hlmyUtil.appKey,
					gid:_hlmyUtil.gid,
					hlmy_gw:_hlmyUtil.hlmy_gw,
					data:data
				},'',true);
			}
		},
		onShareTimeline:function(args){
			if("function" === typeof(onShareTimeline)){
				onShareTimeline(args);
			}else{
				console.log('sorry,i can not find function onShareTimeline');
			}
		},
		onShareFriend:function(args){
			if("function" === typeof(onShareFriend)){
				onShareFriend(args);
			}else{
				console.log('sorry,i can not find function onShareFriend');
			}
		},
		dynamicScript:function(url,data,success,bl){
			var dataString = '';
			//处理calback
			var jsonpcallback = "jsonpcallback" + (Math.random() + "").substring(2);
			if (typeof data == "object" && data != null) {
				if(typeof success == 'function'){
					window[jsonpcallback] = function(){
						success();
					}
					data['callback'] = jsonpcallback;
				};
				for (var p in data) {
					dataString = dataString + "&" + p + "=" + escape(data[p]);
				}
			}
			if (url.indexOf("?") > 0) {
				url = url + "&" + dataString;
			} else {
				url = url + "?" + dataString;
			}
			var script = document.createElement('script');
			script.type = "text/javascript";
			script.src = url;
			script.onload = function(){
				if(bl){
					head.removeChild(script);
				}
			}
			var head = document.getElementsByTagName('head').item(0);
			head.appendChild(script);
		},
		'kefu': function(callback) {
			
		},
		'reload' : function() {
			top.location = top.location;
		}
	};
	window.HLMY_SDK = new _hlmy();
	window.HLMY_SDK.init =  function(obj){
		_hlmyUtil.appKey = obj.appKey || '';
		_hlmyUtil.gid = obj.gid || '';
		_hlmyUtil.hlmy_gw = obj.hlmy_gw || '';
		if(typeof obj.hlmy_gw !== 'undefined'){
			_hlmyUtil.pf = obj.hlmy_gw.split('_')[0];
		}
		var datainfo = {
			appKey: _hlmyUtil.appKey,
			gid:_hlmyUtil.gid,
			hlmy_gw:_hlmyUtil.hlmy_gw
		}
		this.dynamicScript(_url.init,datainfo,'',true);
	}
	/*监听*/
	window.addEventListener('message',function(evt){
		var b = {
			onShareTimeline:function(args){
				"function" === typeof(onShareTimeline) && onShareTimeline(args);
			},
			onShareFriend:function(args){
				"function" === typeof(onShareFriend) && onShareFriend(args);
			}
		}
		if(evt.data.hlmy){
			switch(evt.data.type){
				case 'fn':
					if('function' === typeof b[evt.data.value.fn]){
						b[evt.data.value.fn].apply(window,evt.data.value.args);
					}
					break;
				default :
					console.log(evt.data.type);
			}
		}
	},!1)	
})();







