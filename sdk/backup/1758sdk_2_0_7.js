/**
 * 1758sdk 2.0.7 版本
 * @return {[type]} [description]
 * 
 * console 输出格式修改 
 */
(function(){
	var _hlmy = function() {
		this.version = '2.0.7';
		console.log('[1758SDK %cversion','color:blue;',']:',this.version);
	};
	var _hlmyUtil = {
		appKey:'',
		gid:'',
		hlmy_gw:'',
		pf:''
	};
	var _url={
		pay:'//wx.1758.com/pay/v4/jsonp/payInit.jsonp',
		share:'//wx.1758.com/play/v4/jsonp/share.jsonp',
		init:'//wx.1758.com/play/v4/jsonp/init.jsonp',
		role:'//wx.1758.com/play/v4/jsonp/roleInfo.jsonp',
		follow:'//wx.1758.com/play/v4/jsonp/follow.jsonp'
	};
	_hlmy.prototype = {
		postData:function(data){
			data.hlmy = true;
			top.postMessage(data, '*');
		},
		/**
		 * 支付
		 * @param  {[type]} safeCode [description]
		 * @return {[type]}          [description]
		 */
		pay:function(obj) {
			var infos = {};
			if(!!_hlmyUtil.appKey && !!_hlmyUtil.gid && !!_hlmyUtil.hlmy_gw){
				infos.appKey = _hlmyUtil.appKey ;
				infos.gid = _hlmyUtil.gid;
				infos.hlmy_gw = _hlmyUtil.hlmy_gw;
				infos.paySafecode = obj.paySafecode;
				if(_hlmyUtil.pf == '0' || _hlmyUtil.pf == ''){
					this.pay1758(infos);
				}else{
					this.dynamicScript(_url.pay,infos);
				}
			}else{
				if(_hlmyUtil.appKey == ''){console.log('[1758SDK %cpay%c]\n%c初始化时缺少appKey参数','color:blue','color:black','color:red');};
				if(_hlmyUtil.gid == ''){console.log('[1758SDK %cpay%c]\n%c初始化时缺少gid参数','color:blue','color:black','color:red');};
				if(_hlmyUtil.hlmy_gw == ''){console.log('[1758SDK %cpay%c]\n%c初始化时缺少hlmy_gw参数','color:blue','color:black','color:red');};
			}
		},
		pay1758:function(data){
			this.postData({
				type:'pay',
				value:{
					fn:'payInfo',
					args:data
				}
			});
		},
		/**
		 * 关注
		 * @return {[type]} [description]
		 */
		follow: function(){
			if(_hlmyUtil.pf == '0' || _hlmyUtil.pf == ''){
				this.postData({
					type:'follow',
					value:{
						fn:'followWx',
						args:[]
					}
				});
			}else{
				this.dynamicScript(_url.follow,{
					appKey:_hlmyUtil.appKey,
					gid:_hlmyUtil.gid,
					hlmy_gw:_hlmyUtil.hlmy_gw
				},'',true);
				console.log('this is not 1758 platform');
			}
		},
		
		//综合设置分享信息
		setShareInfo: function(data){
			if(_hlmyUtil.pf == '0' || _hlmyUtil.pf == ''){
				this.postData({
					type:'share',
					value:{
						share:'shareInfo',
						shareInfo: data
					}
				});
			}else{
				this.dynamicScript(_url.share,{
					appKey:_hlmyUtil.appKey,
					gid:_hlmyUtil.gid,
					hlmy_gw:_hlmyUtil.hlmy_gw,
					data:data
				},'',true);
			}
		},
		onShareTimeline:function(args){
			if("function" === typeof(onShareTimeline)){
				onShareTimeline(args);
			}else{
				console.log('[1758SDK %conShareTimeline%c]\n%csorry,not find function onShareTimeline','color:blue','color:black','color:red');
			}
		},
		onShareFriend:function(args){
			if("function" === typeof(onShareFriend)){
				onShareFriend(args);
			}else{
				console.log('[1758SDK %conShareFriend%c]\n%csorry,not find function onShareFriend','color:blue','color:black','color:red');
			}
		},
		/**
		 * 动态创建script
		 * @param  {[string]} url     [请求地址]
		 * @param  {[object]} data    [请求数据]
		 * @param  {[function]} success [成功之后的方法]
		 * @param  {[boolean]} bl      [为true则成后销毁该script节点]
		 * @return {[type]}         [description]
		 */
		dynamicScript:function(url,data,success,bl){
			var dataString = '';
			//处理calback
			var jsonpcallback = "jsonpcallback" + (Math.random() + "").substring(2);
			if (typeof data == "object" && data != null) {
				if(typeof success == 'function'){
					window[jsonpcallback] = function(){
						success();
					}
					data['callback'] = jsonpcallback;
				};
				for (var p in data) {
					dataString = dataString + "&" + p + "=" + data[p];
				}
			}
			if (url.indexOf("?") > 0) {
				url = url + "&" + dataString;
			} else {
				url = url + "?" + dataString;
			}
			var script = document.createElement('script');
			script.type = "text/javascript";
			script.src = url;
			script.onload = function(){
				if(bl){
					head.removeChild(script);
				}
			}
			var head = document.getElementsByTagName('head').item(0);
			head.appendChild(script);
		},
		kefu: function(callback) {
			
		},
		reload : function() {
			var url = "//wx.1758.com/play/login/floginV4?appKey="+_hlmyUtil.appKey+"&hlmy_gw="+_hlmyUtil.hlmy_gw
			top.location = url;
		},
		roleInfo:function(obj){
			var infos = {
				appKey:_hlmyUtil.appKey,
				gid:_hlmyUtil.gid,
				hlmy_gw:_hlmyUtil.hlmy_gw
			};
			// 游戏服务器id
			infos.serverId = obj.serverId || '';
			// 游戏服务器名称
			infos.serverName = obj.serverName || '';
			// 游戏角色id
			infos.roleId = obj.roleId || '';
			// 游戏角色姓名
			infos.roleName = obj.roleName || '';
			// 游戏角色等级
			infos.roleLevel = obj.roleLevel || '';
			// 玩家游戏金币
			infos.roleCoins = obj.roleCoins || '';
			// 是否新创建角色
			infos.isNewRole = obj.isNewRole || false;
			this.dynamicScript(_url.role,infos,'',true);
		},
		shareGuide:function(){
			var str = '<style>.share-square{position:fixed;background:rgba(0,0,0,.4);width:100%;height:100%;left:0;top:0;font-size:14px;color:#fff;z-index:110000;font-weight:700}.share-square .share-box{position:relative;top:5px}.share-square .share-box>img{position:absolute;-moz-animation:icon-bounce .2s ease-in-out infinite alternate;-webkit-animation:icon-bounce .2s ease-in-out infinite alternate;animation:icon-bounce .2s ease-in-out infinite alternate;right:10px}.share-square span{position:absolute;right:5px;top:50px}.share-square span img{position:relative;top:5px;vertical-align:baseline}@-moz-keyframes icon-bounce{0%{top:5px}50%{top:0}100%{top:-5px}}@-webkit-keyframes icon-bounce{0%{top:5px}50%{top:0}100%{top:-5px}}@keyframes icon-bounce{0%{top:5px}50%{top:0}100%{top:-5px}}</style><div id="share-square" class="share-square"><div class="share-box"><img src="http://images.1758.com/image/20161124/open_1_50b53ad143c7d193e61bb9733ceabe5c.png"><span><img class="z" src="http://images.1758.com/image/20161124/open_1_8a3082c3ea9b04e38a8c76d301f5518a.png" alt="">发送微信群或朋友</span></div></div>';
			var shareContainer = document.createElement("div");
		    shareContainer.id = 'shareContainer';
			shareContainer.innerHTML = str;
		    document.body.appendChild(shareContainer);
			var shareId = document.getElementById('shareContainer');
			shareId.addEventListener("touchstart", function(){
			    shareId.remove();
			});
			shareId.addEventListener("click", function(){
			    shareId.remove();
			});
		}
	};
	window.HLMY_SDK = new _hlmy();
	window.HLMY_SDK.init =  function(obj){
		_hlmyUtil.appKey = obj.appKey || '';
		_hlmyUtil.gid = obj.gid || '';
		_hlmyUtil.hlmy_gw = obj.hlmy_gw || '';
		if(typeof obj.hlmy_gw !== 'undefined'){
			_hlmyUtil.pf = obj.hlmy_gw.split('_')[0];
		}
		var datainfo = {
			appKey: _hlmyUtil.appKey,
			gid:_hlmyUtil.gid,
			hlmy_gw:_hlmyUtil.hlmy_gw
		}
		this.dynamicScript(_url.init,datainfo,'',true);
	}
	/*监听*/
	window.addEventListener('message',function(evt){
		var b = {
			onShareTimeline:function(args){
				"function" === typeof(onShareTimeline) && onShareTimeline(args);
			},
			onShareFriend:function(args){
				"function" === typeof(onShareFriend) && onShareFriend(args);
			}
		}
		if(evt.data.hlmy){
			switch(evt.data.type){
				case 'fn':
					if('function' === typeof b[evt.data.value.fn]){
						b[evt.data.value.fn].apply(window,evt.data.value.args);
					}
					break;
				default :
					console.log(evt.data.type);
			}
		}
	},!1)	
})();







