/**
 * 1758sdk 2.1.6 版本
 * @return {[type]} [description]
 *
 * console 输出格式修改 
 * 添加checkFollow方法，检查关注状态
 * 
 * 修改reload方法，适配第三方渠道
 * 游戏适配参数 添加前端查询方法
 *
 * 添加广告功能
 *
 * 修正checkAd广告的处理bug，
 * 广告引入3.0js 修正checkad的时候广告js没有加载下来的bug
 *
 * 添加 execute 方法，对应一些特殊渠道的特殊需求
 *
 * 添加pay方法的callback回调
 */
(function(){
	var _hlmy = function() {
		this.version = '2.1.6';
		console.log('[1758SDK %cversion','color:blue;',']:',this.version);
	};
	var _hlmyUtil = {
		appKey:'',
		gid:'',
		hlmy_gw:'',
		pf:'',			//0或''为1758平台
		isAds: true,		//是否允许广告，默认是true 初始化的时候可以设置一次
		ads:{
			playUrl: '',		//广告播放地址
			time:0,			//倒计时显示时间
			cache: false,		//每次check后，adCache为true，使用一次后设置为false
			obj: {},			//比如推啊的对象
			type:'',			//广告主 类型 比如推啊
			closeLock: false,		//点击锁，true为打开false为禁止关闭
			cpParam: '',
		},
		state:{
			//如果check的时候js还没有加载完成，那么标记为true在js加载完之后回去执行对应的广告初始化
			isCheck:false,			
			tuiaInitInfo:null,
			tuiaInitCallback:null
		}
	};
	var _url={
		pay:'//wx.1758.com/pay/v4/jsonp/payInit.jsonp',
		share:'//wx.1758.com/play/v4/jsonp/share.jsonp',
		init:'//wx.1758.com/play/v4/jsonp/init.jsonp',
		role:'//wx.1758.com/play/v4/jsonp/roleInfo.jsonp',
		follow:'//wx.1758.com/play/v4/jsonp/follow.jsonp',
		checkFollow:'//wx.1758.com/play/v4/jsonp/checkFollow.jsonp',
		initAd:'//wx.1758.com/play/v4/jsonp/initAd.jsonp',
		checkAd:'//wx.1758.com/play/v4/jsonp/checkAd.jsonp',
		showAd:'//wx.1758.com/play/v4/jsonp/reportAd.jsonp',
		playAd:'//wx.1758.com/play/v4/jsonp/playAd.jsonp',
		finishAd:'//wx.1758.com/play/v4/jsonp/finishAd.jsonp',
		execute:'//wx.1758.com/play/v4/jsonp/execute.jsonp'
	};
	_hlmy.prototype = {
		postData:function(data){
			data.hlmy = true;
			top.postMessage(data, '*');
		},
		/**
		 * 支付
		 * @param  {[type]} safeCode [description]
		 * @return {[type]}          [description]
		 */
		pay:function(obj) {
			var infos = {};
			var callback;
			if(!!_hlmyUtil.appKey && !!_hlmyUtil.gid && !!_hlmyUtil.hlmy_gw){
				infos.appKey = _hlmyUtil.appKey ;
				infos.gid = _hlmyUtil.gid;
				infos.hlmy_gw = _hlmyUtil.hlmy_gw;
				infos.paySafecode = obj.paySafecode;
				callback = obj.callback || function(){};
				if(_hlmyUtil.pf == '0' || _hlmyUtil.pf == ''){
					this.pay1758(infos);
				}else{
					this.dynamicScript(_url.pay,infos,callback);
				}
			}else{
				if(_hlmyUtil.appKey == ''){console.log('[1758SDK %cpay%c]\n%c初始化时缺少appKey参数','color:blue','color:black','color:red');};
				if(_hlmyUtil.gid == ''){console.log('[1758SDK %cpay%c]\n%c初始化时缺少gid参数','color:blue','color:black','color:red');};
				if(_hlmyUtil.hlmy_gw == ''){console.log('[1758SDK %cpay%c]\n%c初始化时缺少hlmy_gw参数','color:blue','color:black','color:red');};
			}
		},
		pay1758:function(data){
			this.postData({
				type:'pay',
				value:{
					fn:'payInfo',
					args:data
				}
			});
		},
		/**
		 * 游戏加载前的loading显示
		 * @param  {[type]} time 毫秒数
		 * @return {[type]}      [description]
		 */
		payLoading:function(time,flag){
			time = time || 1000 * 2;
			flag = flag || true;
			var divContainer = document.createElement('div');
			divContainer.setAttribute('style','position: fixed;width: 100%;height:100%;left:0;top:0;');
			var divWrapper = document.createElement('div');
			divWrapper.setAttribute('style','position: absolute;width: 150px;height: 70px;background-color:rgba(0,0,0,.7);margin: 0 auto;left: 0;right: 0;top: 50%;margin-top: -35px;border-radius: 8px;text-align: center;color:#fff;');
			var divText = document.createElement('div');
			divText.setAttribute('style','margin-top:19px;');
			divText.innerText='支付加载中';
			var imgW = document.createElement('img');
			imgW.setAttribute('style','width: 30px;');
			divWrapper.appendChild(divText);
			divWrapper.appendChild(imgW);
			divContainer.appendChild(divWrapper);
			document.body.appendChild(divContainer);
			if(flag){
				setTimeout(function(){
					divContainer.remove();
				},time);
			}
		},
		/**
		 * [subscribe description]
		 * @param  {Function} fn [description]
		 * @return {[type]}      [description]
		 */
		checkFollow:function(fn){
			var hlmyfn = function(obj){
				if(typeof fn !== 'function'){fn=function(){}};
				fn(obj);
			}
			if(!!_hlmyUtil.appKey && !!_hlmyUtil.gid && !!_hlmyUtil.hlmy_gw){
				this.dynamicScript(_url.checkFollow,{
					appKey:_hlmyUtil.appKey,
					gid:_hlmyUtil.gid,
					hlmy_gw:_hlmyUtil.hlmy_gw
				},hlmyfn,true);
			}else{
				if(_hlmyUtil.appKey == ''){console.log('[1758SDK %csubscribe%c]\n%c初始化时缺少appKey参数','color:blue','color:black','color:red');};
				if(_hlmyUtil.gid == ''){console.log('[1758SDK %csubscribe%c]\n%c初始化时缺少gid参数','color:blue','color:black','color:red');};
				if(_hlmyUtil.hlmy_gw == ''){console.log('[1758SDK %csubscribe%c]\n%c初始化时缺少hlmy_gw参数','color:blue','color:black','color:red');};
			}
		},
		/**
		 * 关注
		 * @return {[type]} [description]
		 */
		follow: function(){
			if(_hlmyUtil.pf == '0' || _hlmyUtil.pf == ''){
				this.postData({
					type:'follow',
					value:{
						fn:'followWx',
						args:[]
					}
				});
			}else{
				this.dynamicScript(_url.follow,{
					appKey:_hlmyUtil.appKey,
					gid:_hlmyUtil.gid,
					hlmy_gw:_hlmyUtil.hlmy_gw
				},'',true);
				console.log('this is not 1758 platform');
			}
		},
		
		
		//综合设置分享信息
		setShareInfo: function(data){
			if(_hlmyUtil.pf == '0' || _hlmyUtil.pf == ''){
				this.postData({
					type:'share',
					value:{
						share:'shareInfo',
						shareInfo: data
					}
				});
			}else{
				if(typeof data === 'object'){
					data.appKey = _hlmyUtil.appKey;
					data.gid = _hlmyUtil.gid;
					data.hlmy_gw = _hlmyUtil.hlmy_gw;
					this.dynamicScript(_url.share,data);
				}else{
					this.dynamicScript(_url.share,{
						appKey:_hlmyUtil.appKey,
						gid:_hlmyUtil.gid,
						hlmy_gw:_hlmyUtil.hlmy_gw,
						data:data
					},'',true);
				}
			}
		},
		setBaseState:function(state){
			var isState;
			if(state === undefined){
				isState = false;
				state = '';	
			}else{
				isState = true;
			}
			this.postData({
				type:'baseState',
				value:{
					state:state,
					isState:isState
				}
			});
		},
		onShareTimeline:function(args){
			if("function" === typeof(onShareTimeline)){
				onShareTimeline(args);
			}else{
				console.log('[1758SDK %conShareTimeline%c]\n%csorry,not find function onShareTimeline','color:blue','color:black','color:red');
			}
		},
		onShareFriend:function(args){
			if("function" === typeof(onShareFriend)){
				onShareFriend(args);
			}else{
				console.log('[1758SDK %conShareFriend%c]\n%csorry,not find function onShareFriend','color:blue','color:black','color:red');
			}
		},
		/**
		 * 动态创建script
		 * @param  {[string]} url     [请求地址]
		 * @param  {[object]} data    [请求数据]
		 * @param  {[function]} success [成功之后的方法]
		 * @param  {[boolean]} bl      [为true则成后销毁该script节点]
		 * @return {[type]}         [description]
		 */
		dynamicScript:function(url,data,success,bl){
			var _obj = {};
			var dataString = '';
			if(arguments.length === 1 &&　typeof arguments[0] === 'object'){
				_obj = arguments[0];
				url = _obj.url;
				data = _obj.data;
				success = _obj.success;
				bl = _obj.bl;
			}
			//处理calback
			var jsonpcallback = "jsonpcallback" + (Math.random() + "").substring(2);
			if (typeof data == "object" && data != null) {
				if(typeof success == 'function'){
					window[jsonpcallback] = success;
					data['callback'] = jsonpcallback;
				};
				for (var p in data) {
					dataString = dataString + "&" + p + "=" + data[p];
				}
			}
			if (url.indexOf("?") > 0) {
				url = url + "&" + dataString;
			} else {
				url = url + "?" + dataString;
			}
			var script = document.createElement('script');
			script.type = "text/javascript";
			script.src = url;
			script.onload = function(){
				if(typeof _obj.loadEndFn === 'function'){
					_obj.loadEndFn(_obj.fnData);
				}
				if(bl){
					head.removeChild(script);
				}
			}
			var head = document.getElementsByTagName('head').item(0);
			head.appendChild(script);
		},
		kefu: function(callback) {
			
		},
		reload : function(callback) {
			var url = "//wx.1758.com/play/login/floginV4?appKey="+_hlmyUtil.appKey+"&hlmy_gw="+_hlmyUtil.hlmy_gw
			if(_hlmyUtil.pf == '0' || _hlmyUtil.pf == ''){
				top.location = url;
			}else{
				var datainfo = {
					appKey: _hlmyUtil.appKey,
					gid:_hlmyUtil.gid,
					hlmy_gw:_hlmyUtil.hlmy_gw
				};
				this.dynamicScript('//wx.1758.com/play/v4/jsonp/reload.jsonp',datainfo,function(resdata){
					callback(resdata);
				},true)
			}
		},
		adaptParams:function(callback){
			var url = '//wx.1758.com/play/v4/jsonp/adaptParams.jsonp';
			var dataInfo = {
				appKey: _hlmyUtil.appKey,
				gid: _hlmyUtil.gid,
				hlmy_gw: _hlmyUtil.hlmy_gw
			};
			this.dynamicScript(url,dataInfo,function(resdata){
				callback(resdata);
			},true)
		},
		roleInfo:function(obj){
			var infos = {
				appKey:_hlmyUtil.appKey,
				gid:_hlmyUtil.gid,
				hlmy_gw:_hlmyUtil.hlmy_gw
			};
			// 游戏服务器id
			infos.serverId = obj.serverId || '';
			// 游戏服务器名称
			infos.serverName = obj.serverName || '';
			// 游戏角色id
			infos.roleId = obj.roleId || '';
			// 游戏角色姓名
			infos.roleName = obj.roleName || '';
			// 游戏角色等级
			infos.roleLevel = obj.roleLevel || '';
			// 玩家游戏金币
			infos.roleCoins = obj.roleCoins || '';
			// 是否新创建角色
			infos.isNewRole = obj.isNewRole || false;
			this.dynamicScript(_url.role,infos,'',true);
		},
		/**
		 * 动态创建一个分享提示页面
		 * @return {[type]} [description]
		 */
		shareGuide:function(){
			var str = '<style>.share-square{position:fixed;background:rgba(0,0,0,.4);width:100%;height:100%;left:0;top:0;font-size:14px;color:#fff;z-index:110000;font-weight:700}.share-square .share-box{position:relative;top:5px}.share-square .share-box>img{position:absolute;-moz-animation:icon-bounce .2s ease-in-out infinite alternate;-webkit-animation:icon-bounce .2s ease-in-out infinite alternate;animation:icon-bounce .2s ease-in-out infinite alternate;right:10px}.share-square span{position:absolute;right:5px;top:50px}.share-square span img{position:relative;top:5px;vertical-align:baseline}@-moz-keyframes icon-bounce{0%{top:5px}50%{top:0}100%{top:-5px}}@-webkit-keyframes icon-bounce{0%{top:5px}50%{top:0}100%{top:-5px}}@keyframes icon-bounce{0%{top:5px}50%{top:0}100%{top:-5px}}</style><div id="share-square" class="share-square"><div class="share-box"><img src="http://images.1758.com/image/20161124/open_1_50b53ad143c7d193e61bb9733ceabe5c.png"><span><img class="z" src="http://images.1758.com/image/20161124/open_1_8a3082c3ea9b04e38a8c76d301f5518a.png" alt="">发送微信群或朋友</span></div></div>';
			var shareContainer = document.createElement("div");
		    shareContainer.id = 'shareContainer';
			shareContainer.innerHTML = str;
		    document.body.appendChild(shareContainer);
			var shareId = document.getElementById('shareContainer');
			shareId.addEventListener("touchstart", function(){
			    shareId.remove();
			});
			shareId.addEventListener("click", function(){
			    shareId.remove();
			});
		},

		/**
		 * 特殊渠道的一些对应的特殊需求
		 * 
		 */
		execute: function(infos){
			var callback = function(){};
			if(typeof infos !== 'object' || infos === null){
				infos = {}
			}
			if(!infos.name){return;}
			infos.appKey = _hlmyUtil.appKey;
			infos.gid = _hlmyUtil.gid;
			infos.hlmy_gw = _hlmyUtil.hlmy_gw;
			if(!!infos.callback){callback = infos.callback}
			this.dynamicScript(_url.execute,infos,callback,true);
		},
		/**
		 * 检查广告情况
		 * callback cp接受结果的回调函数
		 * 
		 * ``` 返回
		 * 没有广告
		 * {
		 * 		result: 0,
		 * 		msg: '暂无广告内容'
		 * }
		 * 有内容
		 * {
		 * 		result:1,
		 * 		title:'200元话费'，
		 * 		imgUrl:'',
		 * 		imgWidth:'',
		 * 		imgHeight:''
		 * }
		 * ```
		 * @return {[type]} [description]
		 */
		checkAd: function(callback){
			var infos;
			if(!_hlmyUtil.isAds){
				infos = {result:0,msg:'暂无广告内容'};
				callback(infos);
			}else{
				infos ={
					url: _url.checkAd,
					data: {
						appKey: _hlmyUtil.appKey,
						gid:_hlmyUtil.gid,
						hlmy_gw:_hlmyUtil.hlmy_gw
					},
					success: function(resData){
						if(resData.result === 1 && resData.data.adEnable){
							_hlmyUtil.ads.time = resData.data.adTimeout;
							if(resData.data.adType == 'tuia'){
								_hlmyUtil.ads.type = 'tuia';
								_hlmySdk.adsTuiInit(resData.data,callback);
							}
						}else{
							// false 则表示没有广告
							infos = {result:0,msg:'暂无广告内容'};
							callback(infos);
						}
					}
				} 
				this.dynamicScript(infos)
			}
		},
		/**
		 * cp显示广告内容的时候调用
		 * @return {[type]} [description]
		 */
		showAd:function(obj){
			var cpPosId = 1;
			if(!!obj && typeof obj.cpPosId !== 'undefined'&& obj.cpPosId !== ''){
				cpPosId = obj.cpPosId;
			} 
			var infos = {
				url:_url.showAd,
				data:{
					appKey: _hlmyUtil.appKey,
					gid:_hlmyUtil.gid,
					hlmy_gw:_hlmyUtil.hlmy_gw,
					cpPosId: cpPosId
				}
			}
			this.dynamicScript(infos);
		},
		playAd: function(infos){
			var container,ifr,loading,time,closeBtn,interId,cpPosId;
			var callback = infos.callback || function(){};
			if(!_hlmyUtil.ads.cache){callback({result:0,msg:'请先使用checkAd方法进行查询'});return;}
			_hlmyUtil.ads.cpParam = infos.cpParam;
			var cpPosId = 1;
			if(typeof infos.cpPosId !== 'undefined'){
				cpPosId = infos.cpPosId;
			} 
			if(!!_hlmyUtil.ads.playUrl){
				container = document.querySelector('#hlmyAdsContainer');
				ifr = document.querySelector('#hlmyAdsIframe');
				loading = document.querySelector('#hlmyAdsLoading');
				closeBtn = document.querySelector('#hlmyAdsClose');
				closeBtn.innerHTML = '<span id="hlmyAdsTime">'+_hlmyUtil.ads.time+'</span> 可关闭';
				time = document.querySelector('#hlmyAdsTime');

				ifr.setAttribute('src',_hlmyUtil.ads.playUrl);
				ifr.style.display = 'none';
				loading.style.display = 'block';
				setTimeout(function(){
					loading.style.display = 'none';
					ifr.style.display = 'block';
					countDown(callback);
				},1000* 2);
				container.style.display = 'block';
				// 关闭按钮点击事件
				closeBtn.addEventListener('touchstart',function(evt){
					if(_hlmyUtil.ads.closeLock){
						ifr.removeAttribute('src');
						container.style.display = 'none';
						_hlmyUtil.ads.closeLock = false;
						evt.stopPropagation();
					}
				})
				closeBtn.addEventListener('click',function(evt){
					if(_hlmyUtil.ads.closeLock){
						ifr.removeAttribute('src');
						container.style.display = 'none';
						_hlmyUtil.ads.closeLock = false;
						evt.stopPropagation();
					}
				})
				// 推啊统计（不需要了）
				// if(_hlmyUtil.ads.type === 'tuia'){
				// 	_hlmyUtil.ads.obj.exposure(1,function(){})
				// }
				// 模拟点击 触发推啊的点击icon的行为
				document.getElementById('hlmyCustomer').click();
				// 服务器统计
				this.dynamicScript({
					url:_url.playAd,
					data:{
						appKey: _hlmyUtil.appKey,
						gid:_hlmyUtil.gid,
						hlmy_gw:_hlmyUtil.hlmy_gw,
						cpParam: _hlmyUtil.ads.cpParam,
						cpPosId: cpPosId
					}
				});
				// 使用后置为false
				_hlmyUtil.ads.cache = false;
				function countDown(callback){
					var outTime = _hlmyUtil.ads.time;
					interId = setInterval(function(){
						if( outTime === 0){
							clearInterval(interId);
							closeBtn.innerText = '点击关闭';
							_hlmyUtil.ads.closeLock = true;
							// 通知cp
							callback({result:1,finished:true,cpParam:_hlmyUtil.ads.cpParam,cpPosId:cpPosId});
							// 通知服务器
							_hlmySdk.dynamicScript({
								url:_url.finishAd,
								data:{
									appKey: _hlmyUtil.appKey,
									gid:_hlmyUtil.gid,
									hlmy_gw:_hlmyUtil.hlmy_gw,
									cpParam:_hlmyUtil.ads.cpParam,
									cpPosId:cpPosId
								}
							})
						}else{
							time.innerText = --outTime;
						}
					},1000);
				}
			}
		},
		/**
		 * 广告初始化(内部)
		 * @return {[type]} [description]
		 */
		adsInit: function(){
			var infos = {
				url: _url.initAd,
				data: {
					appKey: _hlmyUtil.appKey,
					gid:_hlmyUtil.gid,
					hlmy_gw:_hlmyUtil.hlmy_gw
				},
				success: function(resData){
					if(resData.result === 1 && resData.data.adEnable){
						// 成功
						_hlmySdk.loadAdsScript();
						_hlmySdk.createAdsDom();
					}else{
						_hlmyUtil.isAds = false;
					}
				}
			};
			this.dynamicScript(infos)
		},
		/**
		 * (内部)
		 * 初始化成功之后进行广告相关的dom建设
		 * @return {[type]} [description]
		 */
		createAdsDom: function(){
			var str='<div id="hlmyAdsContainer" style="position: fixed;width: 100%;left: 0;right:0;top:35px;bottom: 0;display:none;"><div style="height: 35px;line-height: 35px;background-color: #3d3d3d;position: absolute;top: -35px;left: 0;right: 0;width: 100%;color: #fff;text-align: right;box-sizing:border-box;padding:0 5px;"><div style="float:left;color:#ffe21b;">参与活动后得奖励</div><div id="hlmyAdsClose" style="display: inline-block;padding: 0px 10px;background-color:#1e1e1e;border-radius:10px;line-height: initial;vertical-align: baseline;padding:1px 12px 2px;"><span id="hlmyAdsTime">30</span> 可关闭</div></div><div id="hlmyAdsIcon" style="display:none;"></div><div id="hlmyAdsLoading" style="position: absolute;display: inline-block;padding:12px 25px;background-color: rgba(0,0,0,.4);color:#fff;margin:0 auto;left:50%;top:50%;transform: translate(-50%,-50%);-ms-transform:translate(-50%,-50%);-moz-transform:translate(-50%,-50%);-webkit-transform:translate(-50%,-50%);-o-transform:translate(-50%,-50%);border-radius: 4px;display:none;">加载中</div><iframe id="hlmyAdsIframe" style="margin: 0;padding: 0;display: block;width: 100%;width: 100px;min-width: 100%;height: 100px;min-height: 100%;border: 0;"></iframe></div>'
			var divContainer = document.createElement('div');
			divContainer.innerHTML = str;
			document.body.appendChild(divContainer);
		},
		/**
		 * (内部)
		 * 加载广告相关的js文件
		 * @return {[type]} [description]
		 */
		loadAdsScript: function(){
			var infos = {
				url:'//yun.tuia.cn/h5-tuia/media/media-3.0.1.min.js',
				loadEndFn:function(){
					if(_hlmyUtil.state.isCheck){
						_hlmySdk.adsTuiInit(_hlmyUtil.state.tuiaInitInfo,_hlmyUtil.state.tuiaInitCallback);
					}
				}
			}
			this.dynamicScript(infos);
		},
		/**
		 * 广告主推啊 广告的初始化
		 * @return {[type]} [description]
		 */
		adsTuiInit: function(infos,callback){
			if(typeof(TuiaMedia) === 'function'){
				// 重置check对应的状态
				_hlmyUtil.state.isCheck = false;
				_hlmyUtil.state.tuiaInitInfo = null;
				_hlmyUtil.state.tuiaInitCallback = null;

				_hlmyUtil.ads.obj = new TuiaMedia({
				  container:'#hlmyAdsIcon',//需要将广告放入的位置,写法和JQuery一样
				  appKey:infos.adVendorKey,//媒体app_key
				  adslotId:infos.adVendorSn,//广告位ID
				  clickTag:true,
				  success:function(resData){
				  	var str = '<img id="hlmyCustomer"  src="'+ resData.img_url +'"/> ';
      				document.querySelector('#hlmyAdsIcon').innerHTML = str;
				    //广告成功加载后的回调方法
				    _hlmyUtil.ads.playUrl = resData.clickurl
				    _hlmyUtil.ads.cache = true;
				    callback({
				    	result: 1,
				    	msg: '',
				    	title: resData.ad_title,
				    	imgUrl:resData.img_url,
				    	imgWidth: resData.img_width,
				    	imgHeight:resData.img_height
				    })
				  }
				})
			}else{
				// 标记check为true，表示loadjs 成功后需要自动进行check方法
				_hlmyUtil.state.isCheck = true;
				_hlmyUtil.state.tuiaInitInfo = infos;
				_hlmyUtil.state.tuiaInitCallback = callback;

				this.loadAdsScript();
			}
		}
	};
	var _hlmySdk = window.HLMY_SDK = new _hlmy();
	window.HLMY_SDK.init =  function(obj){
		_hlmyUtil.appKey = obj.appKey || '';
		_hlmyUtil.gid = obj.gid || '';
		_hlmyUtil.hlmy_gw = obj.hlmy_gw || '';
		if(typeof obj.hlmy_gw !== 'undefined'){
			_hlmyUtil.pf = obj.hlmy_gw.split('_')[0];
		}
		var datainfo = {
			appKey: _hlmyUtil.appKey,
			gid:_hlmyUtil.gid,
			hlmy_gw:_hlmyUtil.hlmy_gw
		}
		this.dynamicScript(_url.init,datainfo,'',true);
		this.adsInit();
	}
	/*监听*/
	window.addEventListener('message',function(evt){
		var b = {
			onShareTimeline:function(args){
				"function" === typeof(onShareTimeline) && onShareTimeline(args);
			},
			onShareFriend:function(args){
				"function" === typeof(onShareFriend) && onShareFriend(args);
			}
		}
		if(evt.data.hlmy){
			switch(evt.data.type){
				case 'fn':
					if('function' === typeof b[evt.data.value.fn]){
						b[evt.data.value.fn].apply(window,evt.data.value.args);
					}
					break;
				default :
					console.log(evt.data.type);
			}
		}
	},!1)	
})();







