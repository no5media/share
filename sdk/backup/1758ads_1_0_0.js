(function(){
	var _hlmy = function(){
		this.version = '1.0.0';
		console.log('[1758ADS %cversion','color:blue;',']:',this.version);
	}
	var _hlmyUtil = {
		appKey:'',
		gid:'',
		isAds: true,		//是否允许广告，默认是true 初始化的时候可以设置一次
		ads:{
			playUrl: '',		//广告播放地址
			time:0,			//倒计时显示时间
			cache: false,		//每次check后，adCache为true，使用一次后设置为false
			obj: {},			//比如推啊的对象
			type:'',			//广告主 类型 比如推啊
			closeLock: false,		//点击锁，true为打开false为禁止关闭
			cpParam: '',
		},
		state:{
			//如果check的时候js还没有加载完成，那么标记为true在js加载完之后回去执行对应的广告初始化
			isCheck:false,			
			tuiaInitInfo:null,
			tuiaInitCallback:null
		}
	};
	var _url={
		initAd:'//wx.1758.com//play/v4/jsonp/ad/thirdparty/initAd.jsonp',
		checkAd:'//wx.1758.com/play/v4/jsonp/ad/thirdparty/checkAd.jsonp',
		showAd:'//wx.1758.com/play/v4/jsonp/ad/thirdparty/reportAd.jsonp',
		playAd:'//wx.1758.com/play/v4/jsonp/ad/thirdparty/playAd.jsonp',
		finishAd:'//wx.1758.com/play/v4/jsonp/ad/thirdparty/finishAd.jsonp'
	};
	_hlmy.prototype = {
		/**
		 * 检查广告情况
		 * callback cp接受结果的回调函数
		 * 
		 * ``` 返回
		 * 没有广告
		 * {
		 * 		result: 0,
		 * 		msg: '暂无广告内容'
		 * }
		 * 有内容
		 * {
		 * 		result:1,
		 * 		title:'200元话费'，
		 * 		imgUrl:'',
		 * 		imgWidth:'',
		 * 		imgHeight:''
		 * }
		 * ```
		 * @return {[type]} [description]
		 */
		checkAd: function(callback){
			var infos;
			if(!_hlmyUtil.isAds){
				infos = {result:0,msg:'暂无广告内容'};
				callback(infos);
			}else{
				infos ={
					url: _url.checkAd,
					data: {
						appKey: _hlmyUtil.appKey,
						gid:_hlmyUtil.gid,
					},
					success: function(resData){
						if(resData.result === 1 && resData.data.adEnable){
							_hlmyUtil.ads.time = resData.data.adTimeout;
							if(resData.data.adType == 'tuia'){
								_hlmyUtil.ads.type = 'tuia';
								_hlmySdk.adsTuiInit(resData.data,callback);
							}
						}else{
							// false 则表示没有广告
							infos = {result:0,msg:'暂无广告内容'};
							callback(infos);
						}
					}
				}
				this.dynamicScript(infos)
			}
		},
		/**
		 * cp显示广告内容的时候调用
		 * @return {[type]} [description]
		 */
		showAd:function(obj){
			var cpPosId = 1;
			if(!!obj && typeof (obj.cpPosId) !== 'undefined' && obj.cpPosId !== ''){
				cpPosId = obj.cpPosId;
			} 
			var infos = {
				url:_url.showAd,
				data:{
					appKey: _hlmyUtil.appKey,
					gid:_hlmyUtil.gid,
					cpPosId: cpPosId
				}
			}
			this.dynamicScript(infos);
		},
		playAd: function(infos){
			var container,ifr,loading,time,closeBtn,interId,cpPosId;
			var callback = infos.callback || function(){};
			if(!_hlmyUtil.ads.cache){callback({result:0,msg:'请先使用checkAd方法进行查询'});return;}
			_hlmyUtil.ads.cpParam = infos.cpParam;
			var cpPosId = 1;
			if(typeof infos.cpPosId !== 'undefined'){
				cpPosId = infos.cpPosId;
			} 
			if(!!_hlmyUtil.ads.playUrl){
				container = document.querySelector('#hlmyAdsContainer');
				ifr = document.querySelector('#hlmyAdsIframe');
				loading = document.querySelector('#hlmyAdsLoading');
				closeBtn = document.querySelector('#hlmyAdsClose');
				closeBtn.innerHTML = '<span id="hlmyAdsTime">'+_hlmyUtil.ads.time+'</span> 可关闭';
				time = document.querySelector('#hlmyAdsTime');

				ifr.setAttribute('src',_hlmyUtil.ads.playUrl);
				ifr.style.display = 'none';
				loading.style.display = 'block';
				setTimeout(function(){
					loading.style.display = 'none';
					ifr.style.display = 'block';
					countDown(callback);
				},1000* 2);
				container.style.display = 'block';
				// 关闭按钮点击事件
				closeBtn.addEventListener('touchstart',function(evt){
					if(_hlmyUtil.ads.closeLock){
						ifr.removeAttribute('src');
						container.style.display = 'none';
						_hlmyUtil.ads.closeLock = false;
						evt.stopPropagation();
					}
				})
				closeBtn.addEventListener('click',function(evt){
					if(_hlmyUtil.ads.closeLock){
						ifr.removeAttribute('src');
						container.style.display = 'none';
						_hlmyUtil.ads.closeLock = false;
						evt.stopPropagation();
					}
				})
				// 模拟点击 触发推啊的点击icon的行为
				document.getElementById('hlmyCustomer').click();
				// 服务器统计
				this.dynamicScript({
					url:_url.playAd,
					data:{
						appKey: _hlmyUtil.appKey,
						gid:_hlmyUtil.gid,
						cpParam: _hlmyUtil.ads.cpParam,
						cpPosId: cpPosId
					}
				});
				// 使用后置为false
				_hlmyUtil.ads.cache = false;
				function countDown(callback){
					var outTime = _hlmyUtil.ads.time;
					interId = setInterval(function(){
						if( outTime === 0){
							clearInterval(interId);
							closeBtn.innerText = '点击关闭';
							_hlmyUtil.ads.closeLock = true;
							// 通知cp
							callback({result:1,finished:true,cpParam:_hlmyUtil.ads.cpParam,cpPosId:cpPosId});
							// 通知服务器
							_hlmySdk.dynamicScript({
								url:_url.finishAd,
								data:{
									appKey: _hlmyUtil.appKey,
									gid:_hlmyUtil.gid,
									cpParam:_hlmyUtil.ads.cpParam,
									cpPosId:cpPosId
								}
							})
						}else{
							time.innerText = --outTime;
						}
					},1000);
				}
			}
		},
		/**
		 * 广告初始化(内部)
		 * @return {[type]} [description]
		 */
		adsInit: function(){
			var infos = {
				url: _url.initAd,
				data: {
					appKey: _hlmyUtil.appKey,
					gid:_hlmyUtil.gid,
				},
				success: function(resData){
					if(resData.result === 1 && resData.data.adEnable){
						// 成功
						_hlmySdk.loadAdsScript();
						_hlmySdk.createAdsDom();
					}else{
						_hlmyUtil.isAds = false;
					}
				}
			};
			this.dynamicScript(infos)
		},
		/**
		 * (内部)
		 * 初始化成功之后进行广告相关的dom建设
		 * @return {[type]} [description]
		 */
		createAdsDom: function(){
			var str='<div id="hlmyAdsContainer" style="position: fixed;width: 100%;left: 0;right:0;top:35px;bottom: 0;display:none;z-index:6000;"><div style="height: 35px;line-height: 35px;background-color: #3d3d3d;position: absolute;top: -35px;left: 0;right: 0;width: 100%;color: #fff;text-align: right;box-sizing:border-box;padding:0 5px;"><div style="float:left;color:#ffe21b;">参与活动后得奖励</div><div id="hlmyAdsClose" style="display: inline-block;padding: 0px 10px;background-color:#1e1e1e;border-radius:10px;line-height: initial;vertical-align: baseline;padding:1px 12px 2px;"><span id="hlmyAdsTime">30</span> 可关闭</div></div><div id="hlmyAdsIcon" style="display:none;"></div><div id="hlmyAdsLoading" style="position: absolute;display: inline-block;padding:12px 25px;background-color: rgba(0,0,0,.4);color:#fff;margin:0 auto;left:50%;top:50%;transform: translate(-50%,-50%);-ms-transform:translate(-50%,-50%);-moz-transform:translate(-50%,-50%);-webkit-transform:translate(-50%,-50%);-o-transform:translate(-50%,-50%);border-radius: 4px;display:none;">加载中</div><iframe id="hlmyAdsIframe" style="margin: 0;padding: 0;display: block;width: 100%;width: 100px;min-width: 100%;height: 100px;min-height: 100%;border: 0;"></iframe></div>'
			var divContainer = document.createElement('div');
			divContainer.innerHTML = str;
			document.body.appendChild(divContainer);
		},
		/**
		 * (内部)
		 * 加载广告相关的js文件
		 * @return {[type]} [description]
		 */
		loadAdsScript: function(){
			var infos = {
				url:'//yun.tuia.cn/h5-tuia/media/media-3.0.1.min.js',
				loadEndFn:function(){
					if(_hlmyUtil.state.isCheck){
						_hlmySdk.adsTuiInit(_hlmyUtil.state.tuiaInitInfo,_hlmyUtil.state.tuiaInitCallback);
					}
				}
			}
			
			this.dynamicScript(infos);
		},
		/**
		 * 广告主推啊 广告的初始化
		 * @return {[type]} [description]
		 */
		adsTuiInit: function(infos,callback){
			if(typeof(TuiaMedia) === 'function'){
				// 重置check对应的状态
				_hlmyUtil.state.isCheck = false;
				_hlmyUtil.state.tuiaInitInfo = null;
				_hlmyUtil.state.tuiaInitCallback = null;

				_hlmyUtil.ads.obj = new TuiaMedia({
				  container:'#hlmyAdsIcon',//需要将广告放入的位置,写法和JQuery一样
				  appKey:infos.adVendorKey,//媒体app_key
				  adslotId:infos.adVendorSn,//广告位ID
				  clickTag: true,
				  success:function(resData){
				  	var str = '<img id="hlmyCustomer"  src="'+ resData.img_url +'"/> ';
      				document.querySelector('#hlmyAdsIcon').innerHTML = str;
				    //广告成功加载后的回调方法
				    _hlmyUtil.ads.playUrl = resData.clickurl;
				    _hlmyUtil.ads.cache = true;
				    callback({
				    	result: 1,
				    	msg: '',
				    	title: resData.ad_title,
				    	imgUrl:resData.img_url,
				    	imgWidth: resData.img_width,
				    	imgHeight:resData.img_height
				    })
				  }
				})
			}else{
				// 标记check为true，表示loadjs 成功后需要自动进行check方法
				_hlmyUtil.state.isCheck = true;
				_hlmyUtil.state.tuiaInitInfo = infos;
				_hlmyUtil.state.tuiaInitCallback = callback;
			}
		},
		/**
		 * 动态创建script
		 * @param  {[string]} url     [请求地址]
		 * @param  {[object]} data    [请求数据]
		 * @param  {[function]} success [成功之后的方法]
		 * @param  {[boolean]} bl      [为true则成后销毁该script节点]
		 * @return {[type]}         [description]
		 */
		dynamicScript:function(url,data,success,bl){
			var _obj = {};
			var dataString = '';
			if(arguments.length === 1 &&　typeof arguments[0] === 'object'){
				_obj = arguments[0];
				url = _obj.url;
				data = _obj.data;
				success = _obj.success;
				bl = _obj.bl;
			}
			//处理calback
			var jsonpcallback = "jsonpcallback" + (Math.random() + "").substring(2);
			if (typeof data == "object" && data != null) {
				if(typeof success == 'function'){
					window[jsonpcallback] = success;
					data['callback'] = jsonpcallback;
				};
				for (var p in data) {
					dataString = dataString + "&" + p + "=" + data[p];
				}
			}
			if (url.indexOf("?") > 0) {
				url = url + "&" + dataString;
			} else {
				url = url + "?" + dataString;
			}
			var script = document.createElement('script');
			script.type = "text/javascript";
			script.src = url;
			script.onload = function(){
				if(typeof _obj.loadEndFn === 'function'){
					_obj.loadEndFn(_obj.fnData);
				}
				if(bl){
					head.removeChild(script);
				}
			}
			var head = document.getElementsByTagName('head').item(0);
			head.appendChild(script);
		},
	}

	var _hlmySdk = window.HLMY_ADS = new _hlmy();
	window.HLMY_ADS.init =  function(obj){
		_hlmyUtil.appKey = obj.appKey || '';
		_hlmyUtil.gid = obj.gid || '';
		var datainfo = {
			appKey: _hlmyUtil.appKey,
			gid:_hlmyUtil.gid,
		}
		this.adsInit();
	}
}())