# spf-vue

> 该项目是游戏框架的vue版本重构

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

jsp 代码查看功能
+ loading
+ 浮标
+ 分享Tip
+ 二维码
    + sdk关注
    + VIP二维码（客服页面我的专属客服）
    + 关注二维码（ajax主动查询关注订阅）
+ 活动功能（展示图片）
+ 充值活动（展示gif）
+ 返回功能
+ 一键登录（暂时废弃）
+ 模拟cp弹框关注
+ VIP客服消息弹框
+ 推送提醒
+ 图片分享（一种是后台id，一种是图片url）
+ 娃娃机浮标以及对应的功能，排行等
+ 签到功能
+ 支付页面

## 非浮标功能
 + [ ] 二个关注二维码
    + [ ] [sdk关注调用](#sdkfollow)
    + [ ] 初始化的时候 查阅是否订阅过（关注）
 + [ ] 关于活动，如果有则弹框提示关注
 + [x] 分享提示小手指引
 + [x] 返回功能推荐游戏提示
 + [x] 试玩结束登录提示
 + [x] 手机号登录填写
 + [x] 平台模拟游戏分享弹框
 + [ ] vip客服消息弹框
 + [x] 推送消息设置弹框
    + [ ] 在pushSet组件内完成
 + [ ] 自定义分享图片 预定义分享图片展示
 + [ ] 公告提示框
 + [x] loading和info提示框
 + [ ] 聊天室和客服的websocket通讯功能
    + [ ] 通过chatInit完成

 
#### 返回功能推荐游戏提示

+ 初始化的时候请求url链接
+ 如果返回内容有游戏，并且localstorage没有记录“今日不再提示的时间”则初始化监听事件
+ 当监听到back事件，则打开弹框


#### 试玩功能
+ 首先确认是试玩状态
+ 在一定的时间之后弹出登录框（微信，qq，手机，微博）
+ 如果点击手机登录 则弹框手机号登录框，点击其他进行跳转登录


#### 平台模拟游戏分享弹框功能
+ 首先是微信或者客户端状态下
+ `promptEnable`参数为true
+ 经过 `promptDelay` 时间后，向服务器发起请求
+ 如果结果为1 并且 `enable` 为true，则打开弹框
+ 在弹框打开的30秒内，设置 `reasonType` 为1 正常为0 服务器记录分享日志的时候用到

#### 分享提示小手指引
+ 平台接受到sdk的分享设置，并且`tipInfo`为 `true`时，显示指引
+ 模拟游戏的分享弹框的 `去分享`按钮点击的时候显示


#### 推送消息设置弹框
+ 延迟五秒执行
+ 查看gameInfo的 `initial`参数值是否为 `performance`
+ 如果是的话，则展示信息，ajax请求获取用户资料 vip等级和经验等级

## 浮标里面的功能

## 流程


## 目录结构

+ [ ] src
    + [ ] `assets`  资源文件夹
        + [ ] `css`
        + [ ] `js`
            + [ ] `template`  嵌入到app主组件中的方法
            + [ ] `xxx.js`    全局用到的一些js方法
    + [ ] `components`   组件文件夹
        + [ ] `base`  全局组件
        + [ ] `buoy`    浮标中各个tab选项组件
        + [ ] `common`  浮标中各个tab选项使用到的小组件
        + [ ] `infos`   浮标按钮和浮标中的head内容
        + [ ] `main`    非浮标功能组件
    + [ ] `store`             `store` 存储地方
        + [ ] `index.js`
        + [ ] `mutations.js`
        + [ ] `state.js`
    + [ ] `App.vue`     入口主组件
    + [ ] `main.js`   入口文件







var k = {
        USER_INFO:{
            gid: "${userinfo_gid}",
            nickname:"${userinfo_nickname}",
            headUrl:"${userinfo_headUrl}",
            userType:"${userinfo_userType}",
            sex:"${userinfo_sex}", 
            userStatus:"${userinfo_userStatus}"
        },
        GAME_INFO: {
        iconUrl:"${wxApp.iconUrl}",
        appKey:"${wxApp.appKey}",
        aid:${aid},
        name:"${wxApp.name}",
        hlmy_from: "${hlmy_from}", //支付的时候拼接参数hlmy_from
        },
        PROMOTION: {
        enable: <%=promotion%>,     运营活动是否拉取数据
        },
        PAY_INFO:{
            version: "${wxApp.versionPay}",
            forceEnable: <%=forceEnable%>,
            forceVersion: <%=payVersion%>
        },
        CHN_INFO: {
            enable: false,
            chn: '<%=channel%>'
        },
        SHARE_INFO:{
            enable: <%=chnShareEnable%>,
            version: '${wxApp.versionLogin}',
            shareChn:'<%=shareChn%>',
            wyOpenAppId:${openAppId},
            shareAbility:${wxApp.shareAbility},
            hideMenu:[]
        },
        SUBSCRIBE_INFO:{
            enable:<%=negativeSubscribePromptEnable%>,
            positiveSubscribeEnable:<%=positiveSubscribeEnable%>,   //暂时无用
            negativeSubscribeEnable:<%=negativeSubscribePromptEnable%>,         //暂时无用
            promptDelay:${subscribePromptDelay},
        },
        CHAT_INFO:{
            scene:"game_${wxApp.appKey}",
            kfDialogKey:"<%=ChatUtil.genKefuChatDialogKey(wyUserInfo.getId())%>"
        },
        TAB_INFO:{
            <%
            //乐视的游戏，取消热门游戏的tab
            if("third1758_letvapp".equals(channel)
                ||"third1758_letvapp_jssg".equals(channel)
                ||"third1758_letvapp_ygbkm".equals(channel)
                ||"third1758_letvapp_wdzz".equals(channel)
            ){
            initTab = 2;
            %>
            item:['tabLB','tabGG','tabLT'],
            <%}else{%>
            item:['tabLB','tabGG','tabLT','tabHot'],
            <%}%>
            initTab:<%=initTab%>,
        },
        ADD_TO_DESKTOP: {
            enable: <%=addDesktop%>,
            time: <%=addDesktopTimeDelay%>, //ms
            url: "<%=addDesktopUrl%>"
        },
        SIMULATE_INFO:{
            promptEnable:<%=chnShareEnable && promptEnable%>,
            promptDelay:<%=promptDelay%>,
        },
        BUOY_INFO: {
            // panelEnable:${panelEnable},  是否使用浮标
        downloadApp: <%=downloadAppDisplay%>,
        },
        PUSHSET_INFO: {
            initial: "${initial}" //设置提醒参数
        },
        LOGIN_INFO: {
            // trialEnable:${trialEnable},  //是否是试玩状态
            // trialTime:${trialTime},      试玩状态下多久弹出登录框
        },
        }
