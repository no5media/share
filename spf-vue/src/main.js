// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import rootSize from '@/assets/js/rootSize.js'
import utils from '@/assets/js/utils.js'
import store from '@/store/index'
import com from '@/components/base/index.js'
import axios from 'axios'

Vue.prototype.axios = axios;
Vue.use(com);

import '@/assets/css/base.styl'


Vue.config.productionTip = false

// 设置font-size
rootSize.browerUa();

function getWxConfig() {
    var jssdkEle = document.createElement("script");
    jssdkEle.type = "text/javascript";
    var currentUrl = encodeURIComponent(location.href.split('#')[0]);
    //开发、测试阶段如需开启debug模式，请在wxJssdkSrc中增加debug=true的参数，见下行
    jssdkEle.src = "http://h5.wx1758.com/openapi/wx/wxJsConfig?debug=true&wyOpenAppId=107686&pageUrl=" + currentUrl;
    var allJsEles = document.getElementsByTagName("script")[0];
    allJsEles.parentNode.insertBefore(jssdkEle, allJsEles);
}
// if (utils.userAgent.client === 'wx') {
//     // getWxConfig();
// }

// 防止被嵌套劫持
if (top != self) {
    top.location = self.location;
}


/* eslint-disable no-new */
new Vue({
    el: '#app',
    store,
    template: '<app/>',
    components: {
        App
    }
})

// 禁止页面下拉回弹效果
document.body.addEventListener('touchmove', function(evt) {
    if (!evt._isScroller) {
        evt.preventDefault();
    }
});