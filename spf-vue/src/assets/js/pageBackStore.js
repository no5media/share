export default {
    getTime,
    set,
    remove,
}

/**
 * 获取
 * 
 */
function getBackInfo() {
    var store = window.localStorage;
    if (!store) {
        return null;
    }
    // 查看是否有backinfo内容
    var backinfo = store.getItem('backinfo');
    if (backinfo === null) {
        return null;
    } else {
        try {
            return JSON.parse(backinfo);
        } catch (err) {
            return null;
        }
    }
}

/**
 * 获取localstorage存储的时间
 * 如果没有则返回''
 * @param {any} appKey 
 * @returns 
 */
function getTime(appKey) {
    var time = '';
    if (!appKey) { return ''; }
    var backinfo = getBackInfo();
    if (!backinfo) {
        return '';
    }
    backinfo.forEach(function(element) {
        if (element.appKey === appKey) {
            time = element.t;
        }
    }, this);
    return time;
}

/**
 * localStorage 写入 backinfo
 * 
 * @param {any} data 
 * @returns 
 */
function setBackInfo(data) {
    if (!window.localStorage) {
        return;
    }
    data = JSON.stringify(data);
    return window.localStorage.setItem('backinfo', data);
}

/**
 * 删除某个key值
 * 
 * @param {any} appKey 
 */
function remove(appKey) {
    var backinfo = getBackInfo();
    if (!!backinfo) {
        backinfo.forEach(function(element, index, arr) {
            if (element.appKey === appKey) {
                arr.splice(index, 1)
            }
        }, this)
        setBackInfo(backinfo);
    }
}

// 设置appkey的localstorage信息
function set(appKey) {
    var backinfo = getBackInfo();
    var t = new Date().getTime();
    var is = false;
    var item = {
        appKey: appKey,
        t: t,
        back: 1
    };
    if (!!backinfo) {
        // 存在 替换
        backinfo.forEach(function(element) {
            if (element.appKey === appKey) {
                element.time = t;
                is = true;
            }
        }, this);
        // 不存在
        if (!is) {
            backinfo.push(item)
        }
    } else {
        backinfo = [];
        backinfo.push(item);
    }
    setBackInfo(backinfo);
}