/**
 * 发布评论内容
 * /play/ugc/postFeed.json
参数{
	aid:'',   游戏aid
	content: '',
	img: '', 	//逗号隔开，url或者mediaid  可选
	isWxImg: true/false,	  默认false     可选
	openAppId: ''   公众号id  默认1758微游戏   	可选的
}
回复
/play/ugc/postReply.json
参数{
	ugcId:'',   主贴id
	content: '',
	referUid: '',		用户uid
	img: '', 	//逗号隔开，url或者mediaid  可选
	isWxImg: true/false,	  默认false     可选
	openAppId: ''   公众号id  默认1758微游戏   	可选的
}
 */
import axios from 'axios'
import qs from 'qs'
export default {
    post(aid, img) {

    },
    /**
     * 回复评论
     * @param {} ugcId 
     * @param {*} content 
     * @param {*} referUid 
     */
    reply(ugcId, content, referUid) {
        var params = {
            ugcId: ugcId,
            content: content,
            referUid: referUid
        }
        var url = '/play/ugc/postReply.json'
        return axios.post(url, qs.stringify(params));
    },
    /**
     * 评论 点赞 
     * @param {Number} ugcPostId 
     */
    like(ugcPostId) {
        var url = '/play/ugc/likeUgcPost.json';
        return axios.post(url, qs.stringify({
            ugcPostId: ugcPostId
        }))
    }
}