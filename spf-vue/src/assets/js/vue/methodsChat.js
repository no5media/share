import axios from 'axios';
import qs from 'qs';
import utils from '../utils'
var _config = {
    appKey: "",
    // 聊天室消息得到后的处理
    chatFn: function() {},
    // 客服消息 得到后的处理
    kfFn: function() {},
    //是否使用浮标
    panelEnable: false,
    // 轮训id
    intervalId: '',
    ws: {
        kickoff: false, //是否被踢下线，true则表示是后台服务器踢下线 不需要重连
        state: false, //websocket通讯状态是否被关闭
        kickoffMessage: '', //被服务器踢掉之后的提示语
        fromToken: '', // 用户身份，在每次发送消息的时候带着
        kfDialogKey: '', //	客服dialogKey
        chatDialogKey: '', // 聊天室dialogkey
        connectNum: 0, //重连次数
        connectMax: 5, //重连最多次数
        imPingInterval: 60 * 1000 //心跳频率，单位毫秒 每隔多少毫秒像后端发送
    },
}
var _wsConfig = {
    // im服务器 地址
    url: 'ws://imtest.1758.com/',
    open: wsOpen,
    close: wsClose,
    message: wsMessage
}

var _self = "";

export default {
    // 初始化websocket
    chatInit,
    // 客服tab的打开与关闭
    kfTabSwitch,
    // 聊天室tab的打开语关闭
    chatTabSwitch,
    // 注册聊天室和客服 中处理消息的事件
    registerEvent
}

/**
 * 初始化websocket通讯信息
 * 
 */
function chatInit() {
    _self = this;
    // 获取websocket 通信信息
    getIMServerInfo();
    // 获取一些基础变量信息
    getBaseVar();
    // 如果开启浮标功能才去查看 客服聊天和聊天室 相关的信息
    if (_config.panelEnable) {
        // 获取客服聊天会话id
        getIMInfo('KF');
        // 获取聊天室聊天会话id
        getIMInfo('R');
    }
}

/**
 * 获取一些基础变量值
 */
function getBaseVar() {
    // 是否有浮标
    _config.panelEnable = _self.$store.state.StoreBuoyInfo.enable;
    _config.appKey = _self.$store.state.StoreGameInfo.appKey;
    // 客服dialogkey
    _config.ws.kfDialogKey = _self.$store.state.StoreChatInfo.kfDialogKey;
}
/**
 * ajax 获取IM服务器的信息 以及token
 */
function getIMServerInfo() {
    var url = '/play/im/config.json';
    axios.get(url)
        .then(response => {
            var resData = response.data;
            if (resData.result == 1 && !!resData.data) {
                _wsConfig.url = resData.data.imServer;
                // _wsConfig.url = 'wss://im.1758.com';
                _config.ws.imPingInterval = resData.data.imPingInterval;
                _config.ws.fromToken = resData.data.fromToken;
                _self.$store.commit('StoreChatInfo', {
                    'fromToken': resData.data.fromToken
                });
                // ws通讯 初始化
                if (typeof HLMY_WS === 'object') {
                    HLMY_WS.init(_wsConfig);
                }
            }

        })
        .catch(err => { console.log(err) })
}

/**
 * websocket open对应方法
 * @return {[type]} [description]
 */
function wsOpen(evt) {
    console.log('与服务器建立链接');
    // 进入场景 每个游戏算是一个场景
    enterScene();
    _config.ws.state = true;
    //间隔60s发心跳包，便于后端服务器检测用户在线
    _config.intervalId = setInterval(heartBeat, _config.ws.imPingInterval);
}
/**
 * websocket 对应close方法
 * @return {[type]} [description]
 */
function wsClose(evt) {
    console.log('关闭链接');
    clearInterval(_config.intervalId);
    _config.ws.state = false;
    _config.ws.kickoffMessage = "链接已关闭"
        // 进行重连
    if (!_config.ws.kickoff) {
        console.log('connect', _config.ws.kickoff);
        if (_config.ws.connectNum < _config.ws.connectMax) {
            _config.ws.connectNum++;
            _self.chatInit();
        }
    } else {
        // 被踢下线了不处理
        console.log('kickoff', _config.ws.kickoff);
    }
}
/**
 * websocket 对应的message方法
 * @return {[type]} [description]
 */
function wsMessage(evt) {
    var data, arr = [];
    if (typeof evt.data === 'string') {
        data = JSON.parse(evt.data);
    }
    if (data.command === 'CHAT') {
        // 如果没有command 则认为是用户发送的消息
        // chat 说明是在获取聊天室或者客服tab项的信息
        arr.push(data);
        // 处理数据格式
        handleResponeChatInfo(arr);
    } else if (data.command === 'KICKOFF') {
        arr.push(data)
        _config.ws.kickoff = true;
        _config.ws.kickoffMessage = data.message;
        _config.kfFn(arr);
        _config.chatFn(arr);
    }
}
/**
 * 进入场景
 * @return {[type]} [description]
 */
function enterScene() {
    HLMY_WS.send({
        "fromToken": _config.ws.fromToken,
        "command": "SCENE_ENTER",
        "scene": 'game_' + _config.appKey
    });
}


/**
 * 发送心跳包
 * @return {[type]} [description]
 */
function heartBeat() {
    HLMY_WS.send({
        "fromToken": _config.ws.fromToken,
        "command": "PING",
        "scene": 'game_' + _config.appKey
    });
}




/**
 * 获取IM聊天信息
 * type 分为R 和KF
 * 聊天室消息和客服消息
 * @param  {[type]} type [description]
 * @return {[type]}      [description]
 */
function getIMInfo(type) {
    var url = '/play/im/getDialogSummary.json',
        infos = {
            dialogType: type,
            appKey: _config.appKey
        },
        dialogSummary = '';
    axios.post(url, qs.stringify(infos))
        .then(responses => {
            var resData = responses.data;
            if (resData.result == 1 && !!resData.data && !!resData.data.dialogSummary) {
                if (type === 'R') {
                    _config.ws.chatDialogKey = resData.data.dialogSummary.dialogKey;
                    _self.$store.commit('StoreChatInfo', {
                        'chatDialogKey': resData.data.dialogSummary.dialogKey,
                        "chatEnable": resData.data.dialogSummary.enable
                    });
                    // 是否有聊天室tab项
                    if (resData.data.dialogSummary.enable) {
                        _self.$store.commit('setTabInfoItem', 'tabChat');
                    }
                } else if (type === 'KF') {
                    _config.ws.kfDialogKey = resData.data.dialogSummary.dialogKey;
                    _self.$store.commit('StoreChatInfo', {
                        'kfDialogKey': resData.data.dialogSummary.dialogKey,
                        'kfEnable': resData.data.dialogSummary.enable
                    });
                    // 是否有客服tab项
                    if (resData.data.dialogSummary.enable) {
                        _self.$store.commit('setTabInfoItem', 'tabKeFu');
                    }
                }
            }
        })
        .catch(err => console.log(err));
}

/**
 * websocket 切换会话
 * 客服tab打开与关闭，进入会话和关闭会话
 * @param {*} bl 
 */
function kfTabSwitch(bl) {
    if (_config.ws.kickoff || !_config.ws.state) {
        // 如果主动被踢下线 或者 websock链接关闭
        var arr = [{
            command: "KICKOFF",
            message: _config.ws.kickoffMessage
        }]
        _config.kfFn(arr);
        return;
    }
    if (bl) {
        // 打开
        HLMY_WS.send({
            "fromToken": _self.$store.state.StoreChatInfo.fromToken,
            "command": "DIALOG_OPEN",
            "dialogKey": _config.ws.kfDialogKey,
            "scene": 'game_' + _config.appKey
        });
    } else {
        // 关闭
        HLMY_WS.send({
            "fromToken": _self.$store.state.StoreChatInfo.fromToken,
            "command": "DIALOG_CLOSE",
            "dialogKey": _config.ws.kfDialogKey,
            "scene": 'game_' + _config.appKey
        });
    }
}

/**
 * websocket 切换会话
 * 聊天室tab打开与关闭，进入会话和关闭会话
 * @param {*} bl 
 */
function chatTabSwitch(bl) {
    if (_config.ws.kickoff || !_config.ws.state) {
        // 如果主动被踢下线 或者 websock链接关闭
        var arr = [{
            command: "KICKOFF",
            message: _config.ws.kickoffMessage
        }]
        _config.chatFn(arr);
        return;
    }
    if (bl) {
        HLMY_WS.send({
            "fromToken": _self.$store.state.StoreChatInfo.fromToken,
            "command": 'DIALOG_OPEN',
            "dialogKey": _config.ws.chatDialogKey,
            "scene": 'game_' + _config.appKey
        });
    } else {
        HLMY_WS.send({
            "fromToken": _self.$store.state.StoreChatInfo.fromToken,
            "command": 'DIALOG_CLOSE',
            "dialogKey": _config.ws.chatDialogKey,
            "scene": 'game_' + _config.appKey
        });
    }
}


/**
 * 处理返回的聊天消息
 * @return {[type]} [description]
 */
function handleResponeChatInfo(dataList) {
    handleChatMessage(dataList);
    // 颠倒顺序排序,每个dataList 为一个元素
    if (dataList[0].dialogKey == _config.ws.kfDialogKey) {
        // 客服tab模块
        _config.kfFn(dataList);
    } else if (dataList[0].dialogKey == _config.ws.chatDialogKey) {
        // 聊天室 模块
        _config.chatFn(dataList);
    }
}
/**
 * 处理获取到的聊天数据
 * @return {[type]} [description]
 */
function handleChatMessage(dataList) {
    if (dataList.length == 0) { return; }
    for (var i = 0, len = dataList.length; i < len; i++) {
        dataList[i].showTime = utils.handleTime(dataList[i].sendTime, dataList[i].currentTime);
    }
}

/**
 * 注册websocket处理函数
 * @param {*} type 
 * @param {*} fn 
 */
function registerEvent(type, fn) {
    type === 'chat' ? _config.chatFn = fn : '';
    type === 'kf' ? _config.kfFn = fn : '';
}