// 游戏分享用到的一些方法
// 该文件 export出的方法 都mixin混合进 app.vue文件的methods中去了

import utils from '../utils'
import jsonp from 'jsonp'

const ua = utils.userAgent;
var that;

export default {
    // 分享语的初始化对象
    shareInit,
    // 分享语的注入
    invokeShareInfo,
    // 分享成功通知cp
    execFrame,
    // 改变微信下的分享语
    changeWxShareInfo,
    // 改变客户端注入分享语
    changeClientShareInfo,
    // 发送给服务器进行日志记录
    sendMessageServer,
}

/***********************************************************************
 * **********************************************************************
 * **************************  mixin混合入 App.vue *******************
 * ********************************************************************
 ************************************************************************/

/**
 * 
 * @param {Object} _self vue组件实例 
 * 
 * 设置客户端分享功能
 * 设置客户端添加到桌面功能
 */
function shareInit() {
    that = this;
    // 如果是在客户端环境下
    if (ua.client == 'client') {
        // 客户端分享成功之后调用的方法
        window.clientSuccessInfo = clientSuccessInfo.bind(this);
        //假如没有注册分享信息，客户端调用该方法进行注册信息
        window.clientGetShareInfo = clientGetShareInfo.bind(this);
        //ios 客户端分享对象
        window.connectWebViewJavascriptBridge = connectWebViewJavascriptBridge.bind(this);
        //安卓客户端添加到桌面方法，假如没有自动执行 则客户端调用该方法
        window.clientdesktopInfo = clientdesktopInfo.bind(this);

        if (ua.plat == 'ios') {
            //ios 客户端注册对象
            handleIosClient();
            // ios客户端下设置不需要显示浮标按钮
            this.StoreBuoyInfo({ enable: false });
        } else if (ua.plat == 'android') {
            // 安卓客户端下默认也是不显示，使用自带浮标
            this.StoreBuoyInfo({ enable: false });
            //Android 添加到桌面
            androidPutDesktop(this);
            // 新版本安卓客户端换成页面浮标
            androidBuoyShow();
        }
    }

    // 获取微信配置信息以及分享语
    getWxConfigAndText();
}
/**
 * 注入分享信息
 * 共三处调用，
 * 1、ios bridge 之后
 * 2、微信config之后
 * 3、初始化 init的时候
 */
function invokeShareInfo() {
    //分享语注入信息
    if (utils.userAgent.client == 'client') {
        this.changeClientShareInfo();
    } else {
        //进行微信分享语的注入	
        this.changeWxShareInfo();
    }
}
/**
 * 分享成功后通知 cp
 * 
 * @param {String} 分享类型 
 * @param {String} 通知地址 
 * @param {String} 分享版本
 */
function execFrame(shareType) {
    var version = this.$store.state.StoreShareInfo.version;
    var cpShareUrl = this.$store.state.StoreShareInfo.sharePageUrl;
    if (version == '2') {
        // 如果版本为2 兼容老版本  通过iframe 页面进行传递
        exec_iframe(shareType, cpShareUrl)
    } else {
        // 新版本 通过postMessage进行消息传递
        setInfoForSend(shareType)
    }
}

/**
 * 微信的分享语的注入
 * 
 */
function changeWxShareInfo() {
    that = this;
    // 随机获取一个分享语信息
    var info = handleShareContent();
    wxshare(info);
}
/**
 * 客户端分享语的注入
 */
function changeClientShareInfo() {
    // 随机获取一个分享语信息
    var info = handleShareContent();
    var shareTimelineInfo = JSON.stringify(info);
    var shareAppmessageInfo = JSON.stringify(info);

    if (typeof(window.weixinBridge) != 'undefined') {
        if (window.weixinBridge.invokeWx) {
            window.weixinBridge.invokeWx(shareAppmessageInfo, shareTimelineInfo);
        }
    } else {
        if (window.ios) {
            window.ios.callHandler('shareTimeline', shareTimelineInfo);
            window.ios.callHandler('shareMessage', shareAppmessageInfo);
        }
    }
}

/**
 * 分享成功后发送给后台进行记录信息，
 * 
 * @param {String} name    分享的位置 
 * @param {Number} flag    分享成功或取消 0 成功 1取消
 */
function sendMessageServer(name, flag) {
    var state = this.$store.state;
    // 	微信好友  	1
    // 	朋友圈		2
    // 	微信收藏    4
    // 	QQ空间 		8
    // 	qq好友		16
    // 	微博分享	32
    // 	未知		-1
    var shareType = {
        'appmessage': 1,
        'timeline': 2,
        'qq': 16,
        'qzone': 8
    }[name];
    var url = `//wx.1758.com/play/share/shareResult.jsonp?appKey=${state.StoreGameInfo.appkey}&gid=${state.StoreUserInfo.gid}&shareType=${shareType}&reasonType=${state.StoreShareInfo.reasonType}&shareId=${state.StoreShareInfo.id}&code=${flag}`;
    jsonp(url, function(err, data) {
        console.log(data);
    })
}

//获取游戏的wx配置和分享语
function getWxConfigAndText() {
    // var url = `//wx.1758.com/play/game/getShareInfoAndWxJsconfig?appKey=${this.$store.state.StoreGameInfo.appKey}&pageUrl=${encodeURIComponent(window.location.href)}&wyOpenAppId=${this.$store.state.StoreShareInfo.wyOpenAppId}`;
    var url = 'http://wx.1758.com/play/game/getShareInfoAndWxJsconfig?appKey=ac0f51ce5d76e0c86c71a6b79860a270&pageUrl=http%253A%252F%252Fh5.starapy.cn%252Fplay%252Flogin%252FstartGameV4%253F%2526appKey%253Dac0f51ce5d76e0c86c71a6b79860a270%2526state%253D%2526chn%253D%2526loginToken%253DCkevX0U0S7csuQk5FaP-ww%2526wyOpenAppId%253D107686%2526rcontentimeId%253D1507529530745%2526HLMY_THEME%253D1%2526o%253D1507529530745&wyOpenAppId=107689&_=1507529484953';
    jsonp(url, handelWxConfigData);
}


/**
 * 处理拿到的微信配置信息
 * 
 * 关于图片分享功能目前有两种：
 * 一、是服务器配置好url，sdk分享接口传入适当id
 * 条件：1、服务器enable为true
 * 2、sdk的分享类型是img
 * 3、非客户端情况下
 * 
 * 二、直接传入图片url
 * 条件、1、服务器enable为true
 * 2、sdk分享类型是pic
 * 3、非客户端下
 * 
 * @param {any} response 
 */
function handelWxConfigData(err, resData) {
    if (!err && resData.result == 1) {
        that.StoreShareInfo({
            // store配置分享回调通知页面url,  最早版本中会用到，新版本中用不到
            sharePageUrl: resData.sharePageCpurl
        });
        that.StoreShareInfoImg({
            // store存储是否可以分享图片
            preShareEnable: resData.shareImageEnable,
            // store是否可以自定义图片进行分享
            customizeShareEnable: resData.customizeShareEnable,
        });
        // 配置分享语
        if (resData.shareItems.length) {
            that.StoreShareInfo({
                text: handleWxShareInfoUrl(resData.shareItems)
            });
        }
        // 微信分享config
        if (window.wx) {
            window.wx.config(resData.wxJsConfig)
        }
        // 注入分享语
        that.invokeShareInfo();
    }
}
/**
 * 处理从数据库拿到的分享语信息的url
 * 由于受微信的分享机制的影响，
 * 分享的url必须和当前的url域名一致
 * 
 * @param {any} data 
 */
function handleWxShareInfoUrl(data) {
    var sharelink;
    var urlObj;
    for (var i = 0, len = data.length; i < len; i++) {
        sharelink = data[i].shareLink;
        if (sharelink.indexOf('http') == 0 || sharelink.indexOf('https') == 0) {
            urlObj = new URL(sharelink);
            data[i].shareLink = sharelink.replace(urlObj.origin, document.location.origin)
        }
    }
    return data;
}


/***********************************************************************
 * **********************************************************************
 * **************************  内部方法 *******************
 * ********************************************************************
 ************************************************************************/
/**
 * 组装消息 然后通知cp 分享成功
 * 
 * @param {any} shareType 
 */
function setInfoForSend(shareType) {
    var st = '';
    if (shareType == "appmessage" || shareType == "qq") {
        // st = 'onShareFriend';
        // 为了防止微信被封，根据策略调整，发送到哪里都触发 onShareTimeline 
        // 诱导用户分享给朋友和朋友圈，并提供奖励，
        st = 'onShareTimeline';
    } else if (shareType == 'timeline' || shareType == 'qzone') {
        st = 'onShareTimeline';
    }
    var info = {
        hlmy: true,
        from: '1758',
        type: 'fn',
        value: {
            'fn': st,
            args: []
        }
    }
    sendCpInfo(info);
}
/**
 * 
 * 2.0及以前的版本发送给cp方法
 * @param {any} shareType 
 * @param {any} cpShareUrl 
 */
function exec_iframe(shareType, cpShareUrl) {
    var exec_obj = window.exec_obj;
    if (typeof(exec_obj) == 'undefined') {
        exec_obj = document.createElement('iframe');
        exec_obj.name = 'tmp_frame';
        exec_obj.src = cpShareUrl + '?shareType=' + shareType;
        exec_obj.style.display = 'none';
        document.body.appendChild(exec_obj);
    } else {
        exec_obj.src = cpShareUrl + '?shareType=' + shareType + '&' + Math.random();
    }
}

//发送给cpinfo
function sendCpInfo(info) {
    window.frames['cpframe'].postMessage(info, '*');
}




/**
 * 随机得到一条分享语
 * 并且处理这条分享语
 * 
 */
function handleShareContent() {
    //item 是一个对象包含title desc link sid imgUrl
    var item = randomShareContent();
    if (!item) { return; }
    // link上添加gid
    if (item.link.indexOf('?') > -1) {
        item.link = item.link + '&share=' + that.$store.state.StoreUserInfo.gid;
    } else {
        item.link = item.link + '?share=' + that.$store.state.StoreUserInfo.gid;
    }
    //根据特殊渠道的特殊需求，替换chn的值
    item.link = changeShareChn(item.link);
    // 检查是否有通用state,如果有则进行state配置
    if (that.$store.state.StoreStateInfo.isBase) {
        item.link = changeLinkState(item.link, that.$store.state.StoreStateInfo.baseState);
    }
    // 设置cp分享语
    if (that.$store.state.StoreShareInfoCp.isCp) {
        //如果cp自定义分享语则添加cp的分享语
        var cptitle = that.$store.state.StoreShareInfoCp.title,
            cpdesc = that.$store.state.StoreShareInfoCp.desc,
            imgUrl = that.$store.state.StoreShareInfoCp.imgUrl,
            state = that.$store.state.StoreStateInfo.state;

        (!!cptitle && typeof(cptitle) == 'string') ? (item.title = cptitle) : '';
        (!!cpdesc && typeof(cpdesc) == 'string') ? (item.desc = cpdesc) : '';
        (!!imgUrl && typeof(imgUrl) == 'string') ? (item.imgUrl = imgUrl) : '';
        if (!!state && typeof(state) == 'string') {
            item.link = changeLinkState(item.link, state);
        }
        // 设置store中是否cp分享的信息
        this.$store.commit('StoreShareInfoCp', {
            isCp: false
        })
    }
    return item;
}
/**
 * 
 * 从数据源中随机获取一条分享语
 * @returns 
 */
function randomShareContent() {
    var textarray, len, num;
    textarray = that.$store.state.StoreShareInfo.text;
    len = textarray.length;
    var info = {};
    if (len) {
        num = utils.getRandomNum(0, len);
        info.desc = textarray[num].shareDesc;
        info.title = textarray[num].shareTitle;
        info.imgUrl = textarray[num].shareIconUrl;
        info.link = textarray[num].shareLink;
        //设置store中分享语的id
        that.StoreShareInfo({ id: textarray[num].id })
        return info;
    } else {
        return null;
    }
}

/**
 * 有特殊需求则进行替换chn
 * 如果 HLMY_CONFIG 中有shareChn 则分享链接进行替换
 * 
 * @param {any} link 
 * @param {any} shareChn 
 * @returns 
 */
function changeShareChn(link) {
    var chn = '';
    var shareChn = that.$store.state.StoreShareInfo.shareChn;
    link = link.trim();
    shareChn = shareChn.trim();
    if (!!shareChn) {
        //不为空的情况下，分享的chn替换为当前的chn
        chn = utils.getUrlParams(link, 'chn');
        if (!!chn) {
            link = link.replace('chn=' + chn, 'chn=' + shareChn);
        } else if (!chn && link.indexOf('chn') > 0) {
            link = link.replace('chn=', 'chn=' + shareChn);
        } else {
            if (link.indexOf('?') > 0) {
                link = link + '&chn=' + shareChn;
            } else {
                link = link + '?chn=' + shareChn;
            }
        }
    }
    return link;
}

/**
 * 设置link连接上的state 参数
 * @param  {[type]} link  [description]
 * @param  {[type]} state [description]
 * @return {[type]}       [description]
 */
function changeLinkState(link, state) {
    state = state.trim();
    var oldState = utils.getUrlParams(link, 'state');
    if (state !== '') {
        if (!!oldState) {
            link = link.replace('state=' + oldState, 'state=' + state);
        } else if (!oldState && link.indexOf('state') > 0) {
            link = link.replace('state=', 'state=' + state);
        } else {
            if (link.indexOf('?') > 0) {
                link = link + '&state=' + state;
            } else {
                link = link + '?state=' + state;
            }
        }
    }
    return link;
}


/**
 * 注入微信分享语
 * 
 * @param {any} info 
 * @returns 
 */
function wxshare(info) {
    if (!window.wx) { return; }
    window.wx.ready(function() {
        //关闭微信功能
        hideWxMenu();
        //分享给朋友
        wx.onMenuShareAppMessage({
            title: info.title, // 分享标题
            desc: info.desc, // 分享描述
            link: info.link + '&stype=appmessage&superlongId=' + Math.ceil(Math.random() * 10000000000),
            imgUrl: info.imgUrl,
            type: 'link',
            dataUrl: '',
            success: function() {
                wxShareCallBackFn(true, 'appmessage');
            },
            cancel: function() {
                wxShareCallBackFn(false, 'appmessage');
            }
        });

        //分享到朋友圈
        wx.onMenuShareTimeline({
            title: info.title, // 分享标题
            link: info.link + '&stype=timeline&superlongId=' + Math.ceil(Math.random() * 10000000000),
            imgUrl: info.imgUrl,
            success: function() {
                wxShareCallBackFn(true, 'timeline');
            },
            cancel: function() {
                wxShareCallBackFn(false, 'timeline');
            }
        });
        //分享到qq好友
        wx.onMenuShareQQ({
            title: info.title, // 分享标题
            desc: info.desc, // 分享描述
            link: info.link + '&stype=qq&superlongId=' + Math.ceil(Math.random() * 10000000000), // 分享链接
            imgUrl: info.imgUrl, // 分享图标
            success: function() {
                wxShareCallBackFn(true, 'qq');
            },
            cancel: function() {
                wxShareCallBackFn(false, 'qq');
            }
        });
        //分享到qq空间
        wx.onMenuShareQZone({
            title: info.title, // 分享标题
            desc: info.desc, // 分享描述
            link: info.link + '&stype=qzone&superlongId=' + Math.ceil(Math.random() * 10000000000), // 分享链接
            imgUrl: info.imgUrl, // 分享图标
            success: function() {
                // 用户确认分享后执行的回调函数
                wxShareCallBackFn(true, 'qzone');
            },
            cancel: function() {
                // 用户取消分享后执行的回调函数
                wxShareCallBackFn(false, 'qzone');
            }
        });
    });
}
/**
 * 关闭微信部分功能
 * @return {[type]} [description]
 */
function hideWxMenu() {
    var hideMenu = that.$store.state.StoreShareInfo.hideMenu;
    if (hideMenu.length) {
        try {
            window.wx.hideMenuItems({
                menuList: hideMenu
            })
        } catch (err) {}
    }
}

/**
 * 微信分享后的处理函数
 * 
 * @param {any} bl 分享成功和取消
 * @param {any} type 分享类型
 */
function wxShareCallBackFn(bl, type) {
    // 注入分享语
    that.invokeShareInfo();
    if (bl) {
        // 分享成功
        that.execFrame(type);
        that.sendMessageServer(type, 0);
    } else {
        // 分享取消
        that.sendMessageServer(type, 1);
    }
}

/********************************************************************
 * *********************    客户端处理（****************************
 ***********************************************************************/
/**
 * 客户端分享成功之后调用的方法
 * 该方法中的this已经绑定app.vue中的this
 * 
 * @param {any} type 
 */
function clientSuccessInfo(type) {
    var _type = '';
    type = type.toLowerCase();
    switch (type) {
        case 'sendappmessage':
            _type = 'appmessage';
            break;
        case 'sharetimeline':
            _type = 'timeline';
            break;
        case 'shareqq':
            _type = 'qq';
            break;
        case 'shareweiboapp':
            break;
        case 'shareqzone':
            _type = 'qzone';
            break;
    }
    wxShareCallBackFn(true, _type);
}

/**
 * 客户端在没有拿到注册信息的时候回主动调用该函数
 * 该函数中的this已经绑定了app.vue中的this了
 */
function clientGetShareInfo() {
    that.invokeShareInfo();;
}
/**
 * ios 注入方法
 * @param {} callback 
 */
function connectWebViewJavascriptBridge(callback) {
    if (window.WebViewJavascriptBridge) {
        callback(WebViewJavascriptBridge)
    } else {
        document.addEventListener('WebViewJavascriptBridgeReady', function() {
            callback(WebViewJavascriptBridge)
        }, false)
    }
}
//ios客户端注册对象
function handleIosClient() {
    window.connectWebViewJavascriptBridge(function(bridge) {
        bridge.init(function(message, responseCallback) {
            if (responseCallback) {
                responseCallback("")
            }
        })
        bridge.registerHandler('clientSuccessInfo', function(data, responseCallback) {
            window.clientSuccessInfo(data);
        });
        bridge.registerHandler('clientGetShareInfo', function(data, responseCallback) {
            window.clientGetShareInfo();
        });
        // ios客户端bridge完成后注入分享语
        that.invokeShareInfo();
    });
}

/**
 * 安卓客户端添加到桌面
 * 
 */
function clientdesktopInfo() {
    androidPutDesktop();
}

//androlid 客户端添加到桌面功能
function androidPutDesktop() {
    var jskk = {
        'gameName': that.$store.state.StoreGameInfo.name,
        'gameUrl': that.$store.state.StoreGameInfo.entryUrl,
        'imgUrl': that.$store.state.StoreGameInfo.iconUrl,
        'tp': ''
    };
    jskk = JSON.stringify(jskk);
    try {
        android_tw_system.addDeskIconNew(jskk);
    } catch (e) {
        //TODO handle the exception
    }
}

// 新版案桌客户端根据返回的数据
// 是否显示浮标
function androidBuoyShow() {
    try {
        // 判断是否需要浮标
        // 返回{enable:true, option:[true, true, true, true, true]}
        // 分别代表：退出、刷新、分享、添加到桌面、切换账号
        var clientRes = android_panelAssistant.getH5PanelOption();
        if (typeof clientRes === 'string') {
            clientRes = JSON.parse(clientRes)
        }
        if (clientRes.enable) {
            // 浮标展开后head显示哪几个tab选项
            // 新版本下客户端显示页面浮标按钮
            this.StoreBuoyInfo({ enable: true });
            //TODO: 在安卓客户端上添加页面浮标
        }
    } catch (e) {}
}