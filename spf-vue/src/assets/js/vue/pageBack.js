import qs from 'qs';
import pStore from '../pageBackStore';
import utils from '../utils'
export default {
    pageBackInit
}

/**
 * 初始化返回按钮信息
 * 
 */
function pageBackInit() {
    var gameArr;
    var url = '/play/game/moreRecommendedContent.json';
    var params = {
        aid: this.$store.state.StoreGameInfo.aid,
        chn: this.$store.state.StoreGameInfo.chn
    }
    this.axios.post(url, qs.stringify(params))
        .then((response) => {
            var resData = response.data;
            if (resData.result == 1) {
                if (resData.data && resData.data.recommendedContentList) {
                    gameArr = resData.data.recommendedContentList;
                    if (gameArr.length) {
                        // 设置dom
                        this.StoreBackInfo({
                            url: gameArr[0].imgeURL,
                            link: gameArr[0].link
                        });
                        // 初始化事件
                        _init.apply(this);
                    }
                }
            }
        })
        .catch((err) => { console.log(err) })
}

/**
 * 监听事件初始化
 * 
 */
function _init() {
    if (!isToday(this)) {
        setTimeout(() => {
            window.history.pushState({
                title: document.title,
                url: location.href
            }, document.title, location.href);
            addEventListener('popstate', (e) => {
                // 是否勾选今日不再提示
                if (!isToday(this)) {
                    if (!this.$store.state.StoreBackInfo.isShow) {
                        try {
                            _czc.push(["_trackEvent", '打开弹窗', '打开弹窗', aid]);
                        } catch (err) {}
                    }
                    // 打开弹框
                    this.$store.commit('StoreBackInfo', {
                        isShow: true
                    });
                    // if (!e.state) {}
                }
            })
        }, 2e3);
    }
}

function addEventListener(evt, fn) {
    if (typeof window.addEventListener != 'undefined') {
        window.addEventListener(evt, fn, false);
    } else if (typeof window.attachEvent != 'undefined') {
        window.attachEvent(evt, fn);
    }
}

function isToday(self) {
    var time = pStore.getTime(self.$store.state.StoreGameInfo.appKey);
    return utils.isToday(time);
}