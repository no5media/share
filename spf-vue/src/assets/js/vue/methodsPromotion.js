// 活动入口
export default {
    // 活动广告初始化
    promotionInit() {
        var url = '/play/promotion/load.json';
        var infos = {
            chn: this.$store.state.StoreGameInfo.chn
        }
        this.axios.get(url, { params: infos })
            .then(response => {
                var resData = response.data;
                if (resData.result == 1) {
                    if (!!resData.data.promotion) {
                        this.$store.commit('StorePromotionInfo', {
                            imgUrl: resData.data.promotion.adImgUrl,
                            linkUrl: resData.data.promotion.link,
                            isShow: true
                        })
                    }
                }
            })
            .catch(err => console.log(err));
    },
    // 弹框关闭
    promotionClose() {
        this.$store.commit('StorePromotionInfo', {
            isShow: false
        })
    }

}