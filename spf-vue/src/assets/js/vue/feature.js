/**
 * 一些第三方的小的功能集合
 * 
 */
import utils from '../utils'
import qs from 'qs'

export default {
    // cp之间postmessage事件绑定
    postMessageInit,
    // 程序根据参数自动查询是否关注
    checkFollowInfoInit,
    // 模拟cp游戏弹框引导关注
    simulateGameShareInit
}

/**
 * 跟cp之間 postmessage初始化
 * 
 */
function postMessageInit() {
    if (typeof window.addEventListener != 'undefined') {
        window.addEventListener('message', receiveCpInfo.bind(this), false);
    } else if (typeof window.attachEvent != 'undefined') {
        window.attachEvent('onmessage', receiveCpInfo.bind(this));
    }
}

/**
 * 1、通过ajax获取主动关注和被动关注用到的二维码图片
 * 2、检查被动关注情况，根据配置参数的时间延迟显示
 * 
 * @return {[type]} [description]
 */
function checkFollowInfoInit() {
    var url = '/play/subscribe/loadStrategy.json';
    var params = {
        'appKey': this.appKey,
        'chn': this.chn
    }
    this.axios.post(url, qs.stringify(params))
        .then(response => {
            var resData = response.data;
            if (resData.result == 1 && resData.data.dataList) {
                for (var i = 0, len = resData.data.dataList.length; i < len; i++) {
                    if (resData.data.dataList[i].sceneType1758 === 0) {
                        // 主动关注  sdk调用
                        this.$store.commit('StoreSdkFollowInfo', resData.data.dataList[i]);
                    } else if (resData.data.dataList[i].sceneType1758 === 10) {
                        // 被动订阅关注
                        this.$store.commit('StoreSubscribeInfo', resData.data.dataList[i]);
                    }
                }
                //检查关注信息		
                subscribeEwmShow.apply(this);
            }
        })
        .catch((err) => { console.log(err) })

}

// 被动关注检查关注信息
function subscribeEwmShow() {
    var ua = utils.userAgent;
    if (ua.client == 'wx' && this.$store.state.StoreSubscribeInfo.enable) { //微信客户端内才弹出关注图片
        setTimeout(() => {
            if (this.$store.state.StoreSubscribeInfo.subscribeImgUrl) {
                this.$store.commit('StoreSubscribeInfo', { isShow: true });
                reportEwmShow({
                    appKey: this.appKey,
                    chn: this.chn,
                    sceneType1758: this.$store.state.StoreSubscribeInfo.sceneType1758,
                    openAppId: this.$store.state.StoreSubscribeInfo.openAppId,
                    sceneId: this.$store.state.StoreSubscribeInfo.sceneId,
                    sceneType: this.$store.state.StoreSubscribeInfo.sceneType
                });
            }
        }, this.$store.state.StoreSubscribeInfo.promptDelay);
    }
}




/**
 * 模拟cp游戏关注功能初始化
 * 1、根据客户的和StoreSimulateInfo.enable的情况 确定是否查询
 * 2、经过一定时间后 去查询是否具备弹框条件
 * 
 */
function simulateGameShareInit() {
    var ua = utils.userAgent;
    var time;
    // (ua.client == 'wx' || ua == 'client') && 
    if (this.$store.state.StoreSimulateInfo.enable) {
        //经过固定时间后查询url 是否弹出模拟游戏分享内容
        time = this.$store.state.StoreSimulateInfo.promptDelay;
        setTimeout(() => {
            getSimulateShareInfo.apply(this);
        }, 1000 * time);
    }
}

/**
 * 请求服务器 查看是否有模拟弹框信息
 * 如果有 则弹框
 */
function getSimulateShareInfo() {
    var url = '/play/share/getStrategy.json';
    var params = {
        appKey: this.appKey,
        chn: this.chn
    }
    this.axios.post(url, qs.stringify(params))
        .then((response) => {
            var resData = response.data;
            if (resData.result == 1 && !!resData.data && resData.data.enable) {
                //打开弹框
                this.$store.commit('StoreSimulateInfo', { promptImgUrl: resData.data.promptImgUrl, isShow: true });
                //分享弹框30秒内  设置为1
                this.$store.commit('StoreShareInfo', { 'reasonType': 1 });
                setTimeout(() => {
                    this.$store.commit('StoreShareInfo', { 'reasonType': 0 });
                }, 1000 * 30)
            }
        })
        .catch(function(err) {
            console.log(err);
        })
}
/**
 * 接受sdk传递进来的数据
 * 
 */
function receiveCpInfo(evt) {
    var dataInfo = evt.data,
        test = {};
    if (dataInfo.hlmy) {
        //接受cp信息
        switch (dataInfo.type) {
            case 'share':
                this.handleSDKShare(dataInfo.value);
                break;
            case 'baseState':
                this.handleSDKBaseState(dataInfo.value);
                break;
            case 'pay':
                this.handleSDKPay(dataInfo.value);
                break;
            case 'follow':
                this.handleSDKFollowWx();
                break;
        }
    }
}

// 上报关注二维码显示 
// 有sdk调用上报  和  被动订阅上报
function reportEwmShow(params) {
    var url = '/play/subscribe/reportPrompt.json';
    this.axios.post(url, qs.stringify(params))
}

// 充值活动弹框初始化
function rechargeInit() {
    var url = '/play/activity/game/rechargePrompt.json'
    this.axios.post(url, qs.stringify({
            appKey: this.appKey,
            gid: this.gid,
            chn: this.chn
        }))
        .then(response => {
            var resData = response.data;
            if (resData.result == 1) {
                this.$store.commit('StoreRechargeInfo', {
                    imgUrl: resData.data.promptImgUrl,
                    isShow: true
                })
            }
        })
}