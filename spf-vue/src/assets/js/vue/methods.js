/**
 * app.vue的methods集合
 */
import jsonp from 'jsonp'

import { mapMutations } from 'vuex';
import utils from '../utils.js'
import mutations from '../../../store/mutations'
export default {
    // vuex中的mutations进行映射
    ...mapMutations(Object.keys(mutations)),
    // 一些初始化操作，刚进入游戏的时候需要进行的操作
    init() {
        // 分享初始化  添加到桌面初始化
        this.shareInit();
        // 游戏iframe绑定loading事件
        this.loadEndPage();
        // 跟cp之间 postmessage 进行初始化操作
        this.postMessageInit();
        // 1、获取二维码图片（sdk关注、主动关注） 
        // 2、检查是否关注
        this.checkFollowInfoInit();
        // 游戏后退/返回 事件初始化
        this.pageBackInit();
        // 模拟游戏 弹出分享提示框 初始化
        this.simulateGameShareInit();
        // vip消息弹框初始化,延迟执行
        setTimeout(() => {
            this.getVipNotifyData();
        }, 5000);
        // websocket 初始化
        this.chatInit();
        // 设置提醒初始化
        // 活动展示初始化
        if (this.$store.state.StorePromotionInfo.enable) {
            setTimeout(this.promotionInit, this.$store.state.StorePromotionInfo.time)
        }
        // 充值弹框活动
        if (this.$store.state.StoreRechargeInfo.enable) {
            setTimeout(this.rechargeInit, this.$store.state.StoreRechargeInfo.promptDelay)
        }
    },
    // 给游戏iframe进行绑定，游戏加载完loading消失
    loadEndPage() {
        var gframe = document.getElementById('gameframe');
        if (gframe.attachEvent) {
            gframe.attachEvent('onload', () => { this.loadingShow = false });
        } else {
            gframe.onload = () => {
                this.loadingShow = false;
            };
        }
        // 定时消失，防止游戏内有部分资源长时间不加载一直loading
        setTimeout(() => {
            this.loadingShow = false;
        }, this.loadingEndTime);
    },
    // 显示浮标内容
    showBuoy() {
        this.mainShow = true;
    },
    // 隐藏浮标内容
    hideBuoy() {
        this.mainShow = false;
    },
    // 获取vip推送给用户的信息
    getVipNotifyData() {
        var url = '/play/message/chat/getPromoteInfo.json';
        this.axios.get(url)
            .then(response => {
                var resData = response.data;
                if (resData.result === 1 && !!resData.data && !!resData.data.promoteMessageList && resData.data.promoteMessageList.length > 0) {
                    this.handleVipNotifyData(resData.data.promoteMessageList);
                }
            })
    },
    // 处理获取到的vip通知信息
    handleVipNotifyData(promoteMessageList) {
        // 处理数据中的时间
        for (var i = 0, len = promoteMessageList.length; i < len; i++) {
            promoteMessageList[i].showTime = utils.handleTime(promoteMessageList[i].sendTime, promoteMessageList[i].currentTime);
        }
        promoteMessageList = promoteMessageList.reverse();

        this.vipNotifyData = promoteMessageList;
        this.vipNotifyInfoShow = true;
    },
    // 关闭vip消息通知的弹框
    closeVipNotifyInfo() {
        this.vipNotifyInfoShow = false;
    }
}