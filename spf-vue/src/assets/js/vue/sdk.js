/**
 * 处理sdk传递过来的信息
 */
import utils from '../utils'
export default {
    handleSDKShare,
    handleSDKPay,
    handleSDKBaseState,
    handleSDKFollowWx
}


/***********************************************************************
 * **********************************************************************
 * **************************  mixin混合入 App.vue *******************
 * ********************************************************************
 ************************************************************************/

/**
 * 处理分享
 * 
 */
function handleSDKShare(value) {
    var shareInfo = this.$store.state.StoreShareInfo;
    if (value.share == 'shareInfo') {
        var cpinfo = value.shareInfo;
        // 设置store中的cpShareInfo信息
        this.setStoreCpShare(cpinfo);
        // 重置分享语
        this.invokeShareInfo();
        if (typeof(cpinfo.tipInfo) == 'boolean' && cpinfo.tipInfo) {
            //true 显示信息
            var ua = utils.userAgent;
            if (shareInfo.shareImageEnable && ua.client !== 'client' && cpinfo.type == 'img') {
                // 服务器明确告诉可以进行图片分享，并且是
                // 在非客户端状态下，并且type是图片分享，则进行图片弹出
                // 其他情况进行小手提示分享
                // 指定分享的图片id 只有在图片分享的时候该参数有用

                var params = {
                    appKey: this.$store.state.StoreGameInfo.appKey,
                    gid: this.$store.state.StoreUserInfo.gid,
                    state: this.$store.state.StoreStateInfo.state,
                    shareId: cpinfo.id || ''
                }
                showPreShareImg.apply(this, [params]);
            } else if (shareInfo.customizeShareEnable && ua.client !== 'client' && cpinfo.type == 'pic') {
                // 1，服务器允许，
                // 2，非客户端
                // 3，类型为pic
                showCustomSharePicture.apply(this, [cpinfo.picUrl]);
            } else {
                //先尝试调用客户端的分享功能，弹出分享按钮
                //其他情况下则调用h5页面的分享提示
                try {
                    android_panelAssistant.share();
                } catch (err) {
                    this.$store.commit('StoreShareGuideInfo', { shareGuideShow: true });
                }
            }

        }
    }
}
/**
 * 处理接受到的支付相关的信息
 * 
 */
function handleSDKPay(value) {
    if (value.fn === 'payInfo') {
        // 打开支付
        var data = value.args;
        var url = buildPayUrl(data, this.$store.state.StorePayInfo, this.$store.state.StoreGameInfo);
        this.setStorePay({
            show: true,
            pageUrl: url
        })
    } else if (value.fn === 'payCancleBack') {
        // 隐藏支付
        this.setStorePay({
            show: false,
            pageUrl: ''
        })
    }
}

/**
 * 处理接受到的basestate的信息
 * 
 */
function handleSDKBaseState(value) {
    if (value.isState) {
        this.setStoreBaseState({
            isBase: value.isState,
            baseState: value.state
        })
        this.invokeShareInfo();
    }
}

/**
 * 关注sdk，调用该方法，则直接显示关注图片
 * 
 */
function handleSDKFollowWx() {
    this.storeShowFollow(true);
}



/***********************************************************************
 * **********************************************************************
 * **************************  内部函数 *******************
 * ********************************************************************
 ************************************************************************/
/**
 * 组建支付url
 * data是sdk cp传递进来的数据结构
 * 
 * @param {any} data
 * @return {String} url 
 */
function buildPayUrl(data, payInfo, gameInfo) {
    var url;
    var urlPara = '';
    var version = payInfo.version;
    if (payInfo.forceEnable) {
        version = payInfo.forceVersion
    }
    // 随机数
    var rcontentime = '&rcontentimeId=' + Math.ceil(Math.random() * 1000000);
    // 参数拼接
    for (var p in data) {
        if (data.hasOwnProperty(p)) {
            if (p == 'appKey') {
                continue;
            } else {
                urlPara += '&' + p + '=' + data[p];
            }
        }
    }

    if (version == 3) {
        url = `//wx.1758.com/pay/buy?hlmy_from= ${gameInfo.hlmy_from}&v1758=3&appKey=${gameInfo.appKey}${urlPara}${rcontentime}`;
    } else if (version == 2) {
        url = `//wx.1758.com/pay/buy?hlmy_from= ${gameInfo.hlmy_from}&appKey=${gameInfo.appKey}${urlPara}${rcontentime}`;
        top.window.location.href = src;
    } else {
        url = `//wx.1758.com/pay/payChoice?hlmy_from= ${gameInfo.hlmy_from}${urlPara}${rcontentime}`;
    }
    return url;
}

/**
 * 显示自定义图片分享
 * 分享以图片展示方式进行分享
 * 弹出图片后 用户自动下载或识别
 * 
 */
function showPreShareImg(params) {
    var url = ''
    this.axios.post(url, stringify(params))
        .then((response) => {
            var resData = response.data;
            if (resData.result == 1) {
                this.setStorePreShareImg({ show: true, url: resData.data.shareImgUrl })
            }
        })
        .catch(function(err) { console.log(err) })
}

/**
 * 分享以图片的展示方式，
 * 直接传递进来图片url
 * 
 * @param {any} url 
 */
function showCustomSharePicture(url) {
    this.setStoreCustomShareImg({
        show: true,
        url: url
    })
}