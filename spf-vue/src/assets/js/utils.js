var ua = window.navigator.userAgent.toLowerCase();
var userAgent = getUserAgent();
if (!String.prototype.trim) {
    String.prototype.trim = function() {
        return this.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
    };
}
export default {
    userAgent,
    // 获取userAgent
    getUserAgent,
    // 获取url参数
    getUrlParams,
    // 时间格式化
    dateFormat,
    // 经验值转换为图片icon
    expToIcon,
    //   读取浮标的位置
    readPosition,
    // 更新记录浮标位置
    setPosition,
    // 随机数获取
    getRandomNum,
    // 是否是今天的时间（自然天）
    isToday,
    // 日期形式显示 今日 明日 后天
    dateShow,
    // 局部地方可以下拉回弹
    overscroll,
    // 处理vip弹框消息
    handleTime
}

/**
 * 获取用户的ua信息
 * return 
 * ```
 * {
 *      plat: 'android | ios | pc',
 *      client: 'client | wx | qq | others'
 * }
 * ```
 */
function getUserAgent() {
    var plat = ''; //安卓 ios pc
    var client = ''; //微信 qq 客户端 其他
    if (ua.indexOf("android") > -1) {
        plat = "android";
    } else if (ua.indexOf("iphone") > -1 || ua.indexOf("ipad") > -1 || ua.indexOf("ipod") > -1) {
        plat = "ios";
    } else {
        plat = "pc";
    }

    if (/dwjia/i.test(ua)) {
        client = 'client';
    } else if (/MicroMessenger/i.test(ua)) {
        client = 'wx';
    } else if (/QQ\//i.test(ua)) {
        client = 'qq';
    } else {
        client = 'others'
    }
    return {
        plat: plat,
        client: client
    }
}

/**
 * 
 * 获取url上的参数
 * 
 * @param {String} name     要获取的参数
 * @param {String} url      要使用的url 如果不穿参数 则会使用当前url
 * @returns 
 */
function getUrlParams(name, url) {
    if (name === undefined) {
        return;
    }
    if (url === undefined) {
        url = window.location.href;
    }
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = url.substr(url.indexOf('?')).substr(1).match(reg);
    if (r != null) {
        return decodeURIComponent(r[2]);
    }
    return '';
}

/**
 * 时间格式转换
 * @param {String|Date} date 
 * @param {String} fmt 
 */
function dateFormat(date, fmt) {
    if (typeof(fmt) == 'undefined') {
        fmt = 'yyyy-MM-dd hh:mm:ss'
    }
    return (Format((new Date(date)), fmt));
}

// 日期格式转换
function dateShow(date, fmt) {
    if (fmt === undefined) {
        fmt = 'MM-dd hh:mm'
    }
    var n = new Date(date).getTime();
    var t = 24 * 60 * 60 * 1000;
    var t1, t2, t3, t4;
    t1 = new Date().setHours(0, 0, 0, 0);
    t2 = new Date(t1 + t).setHours(0, 0, 0, 0);
    t3 = new Date(t2 + t).setHours(0, 0, 0, 0);
    t4 = new Date(t3 + t).setHours(0, 0, 0, 0);
    if (n > t4) {
        return dateFormat(date, fmt);
    } else if (n > t3) {
        return '后天 ' + dateFormat(date, 'hh:mm');
    } else if (n > t2) {
        return '明日 ' + dateFormat(date, 'hh:mm');
    } else if (n > t1) {
        return '今天 ' + dateFormat(date, 'hh:mm');
    } else {
        return dateFormat(date, fmt);
    }
}

/**
 * 格式化日期
 * @param {*} date 
 * @param {*} fmt 
 */
function Format(date, fmt) {
    var o = {
        "M+": date.getMonth() + 1, //月份 
        "d+": date.getDate(), //日 
        "h+": date.getHours(), //小时 
        "m+": date.getMinutes(), //分 
        "s+": date.getSeconds(), //秒 
        "q+": Math.floor((date.getMonth() + 3) / 3), //季度 
        "S": date.getMilliseconds() //毫秒 
    };
    if (/(y+)/.test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
    }
    for (var k in o) {
        if (new RegExp("(" + k + ")").test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        }
    }
    return fmt;
}

/**
 * 经验数值转换为icon图片
 * @param {Number} level 
 */
function expToIcon(level) {
    var lev, levToFour, levArr,
        i, len, n,
        testHtml, strHtml = '',
        imgurl;
    lev = parseInt(level);
    if (!lev) {
        return '';
    }
    levToFour = lev.toString(4);
    levArr = levToFour.split('').reverse();
    for (i = 0, len = levArr.length; i < len; i++) {
        switch (i) {
            case 0:
                //星星
                imgurl = '<img src="http://images.1758.com/game/m/hdt.png" alt="星星">';
                break;
            case 1:
                //月亮
                imgurl = '<img src="http://images.1758.com/game/m/hds.png" alt="月亮">';
                break;
            case 2:
                //太阳
                imgurl = '<img src="http://images.1758.com/game/m/hdu.png" alt="太阳">';
                break;
            case 3:
                //皇冠
                imgurl = '<img src="http://images.1758.com/game/m/hdr.png" alt="皇冠">';
                break;
        }
        testHtml = '';
        n = parseInt(levArr[i]);
        while (n > 0) {
            testHtml += imgurl;
            n--;
        }
        if (strHtml == '') {
            strHtml = testHtml;
        } else {
            strHtml = testHtml + strHtml;
        }
    }
    return strHtml;
}

// 读取浮标的位置
function readPosition(appKey) {
    var gp = window.localStorage.gameposition;
    if (!!gp) {
        gp = JSON.parse(gp);
        for (var i = 0, len = gp.length; i < len; i++) {
            if (gp[i].appkey == appKey) {
                return {
                    x: gp[i].x,
                    y: gp[i].y
                }
            }
        }
    }
    return {}
}

/**
 * 记录浮标的位置
 * {
 * 	appKey:'',
 * 	x:'',
 * 	y:''
 * }
 * @param {} obj 
 */
function setPosition(item) {
    var gpo = window.localStorage.gameposition;
    if (!!gpo) {
        gpo = JSON.parse(gpo);
    } else {
        gpo = [];
    }
    updateStorageArray(gpo, item);
}
// 在localStorage中更新记录
//处理localstorage的数组,有该内容则替换，没有则添加
function updateStorageArray(gpo, item) {
    // mark是为了标记是否已经存在
    var mark = false;
    if (gpo.length != 0) {
        for (var i = 0, len = gpo.length; i < len; i++) {
            if (gpo[i].appkey == item.appkey) {
                gpo[i].x = item.x;
                gpo[i].y = item.y;
                mark = true;
                break;
            }
        }
    }
    if (!mark) {
        gpo.push(item);
    }
    gpo = JSON.stringify(gpo);
    window.localStorage.setItem('gameposition', gpo);
}


/**
 * 获取随机数
 * 
 * @param {any} Min 
 * @param {any} Max 
 * @returns 
 */
function getRandomNum(Min, Max) {
    var num = Max - Min;
    var rand = Math.random();
    return (Min + Math.floor(rand * num))
}

/**
 * 检测传入的时间是否是今天的时间
 * 
 */
function isToday(time) {
    var d = new Date(time);
    var todaysDate = new Date();
    if (d.setHours(0, 0, 0, 0) == todaysDate.setHours(0, 0, 0, 0)) {
        return true;
    } else {
        return false;
    }
}

function overscroll(el) {
    el.addEventListener('touchstart', function() {
        var top = el.scrollTop,
            totalScroll = el.scrollHeight,
            currentScroll = top + el.offsetHeight;
        if (top === 0) {
            el.scrollTop = 1;
        } else if (currentScroll === totalScroll) {
            el.scrollTop = top - 1;
        }
    });

    el.addEventListener('touchmove', function(evt) {
        if (el.offsetHeight < el.scrollHeight) {
            evt._isScroller = true;
        } else {
            evt._isScroller = false;
        }
    });
}

/**
 * 拉取vip信息之后的时间出来
 * 页面如果有vip消息则会在页面上弹框提示用户
 * @return {[type]} [description]
 * sendTime: 发送时间
 * currentTime 从服务器拿到的当前时间
 */
function handleTime(sendTime, currentTime) {
    var fmt = 'yyyy-MM-dd hh:mm:ss';
    if (!currentTime) { currentTime = new Date().getTime() }
    var minute = 1000 * 60;
    var hour = minute * 60;
    var day = hour * 24;
    var date = new Date(sendTime);
    // 今天凌晨时间
    var ling = new Date(currentTime).setHours(0, 0, 0, 0)
        // 昨天凌晨
    var lingYes = ling - 86400 * 1000;
    // 前天凌晨
    var lingQian = lingYes - 86400 * 1000;
    var result;
    // 时间差
    var value = currentTime - sendTime;
    if (value < 1000 * 60) {
        result = '刚刚';
    } else if (value < hour) {
        result = parseInt(value / minute) + '分钟前';
    } else if (value < day) {
        result = parseInt(value / hour) + '小时前';
    } else if (lingYes < sendTime && sendTime < ling) {
        result = '昨天 ' + date.getHours() + ':' + date.getMinutes();
    } else if (lingQian < sendTime && sendTime < lingYes) {
        result = '前天 ' + date.getHours() + ':' + date.getMinutes();
    } else {
        result = Format(date, fmt);
    }
    return result;
}