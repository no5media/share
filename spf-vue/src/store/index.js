import Vuex from 'vuex'
import Vue from 'vue'
import mutations from './mutations'
import state from './state'
import actions from './action'
Vue.use(Vuex)
const store = new Vuex.Store({
    state,
    mutations,
    actions
});


init();

/**
 * 
 * 初始化 store状态
 * 主要是从jsp页面上复制一些全局变量到这里
 * 
 */
function init() {
    console.log('初始化 store信息');
    var hlmy = window.HLMY_CONFIG;
    if (!hlmy) { return; }
    // 用户信息
    if (hlmy.USER_INFO) {
        store.commit('StoreUserInfo', hlmy.USER_INFO);
        hlmy.USER_INFO = null;
    }
    // 游戏信息
    if (hlmy.GAME_INFO) {
        store.commit('StoreGameInfo', hlmy.GAME_INFO);
        hlmy.GAME_INFO = null;
    }
    // 支付信息
    if (hlmy.PAY_INFO) {
        store.commit('StorePayInfo', hlmy.PAY_INFO);
        hlmy.PAY_INFO = null;
    }
    // 分享信息
    if (hlmy.SHARE_INFO) {
        store.commit('StoreShareInfo', hlmy.SHARE_INFO);
        hlmy.SHARE_INFO = null;
    }
    // 聊天室信息
    if (hlmy.CHAT_INFO) {
        store.commit('StoreChatInfo', hlmy.CHAT_INFO);
        hlmy.CHAT_INFO = null;
    }
    // 设置tab
    if (hlmy.TAB_INFO) {
        store.commit('StoreTabInfo', hlmy.TAB_INFO);
        hlmy.TAB_INFO = null;
    }
    // 模拟cp弹框像是奖励引导分享
    if (hlmy.SIMULATE_INFO) {
        store.commit('StoreSimulateInfo', hlmy.SIMULATE_INFO);
        hlmy.SIMULATE_INFO = null;
    }
    // 浮标的一些信息
    if (hlmy.BUOY_INFO) {
        store.commit('StoreBuoyInfo', hlmy.BUOY_INFO);
        hlmy.BUOY_INFO = null;
    }
    // 推送消息设置 弹框
    if (hlmy.PUSHSET_INFO) {
        store.commit('StorePushSetInfo', hlmy.PUSHSET_INFO);
        hlmy.PUSHSET_INFO = null;
    }
    // 设置用户的登录态信息 ， 主要是是否试玩状态
    if (hlmy.LOGIN_INFO) {
        store.commit('StoreLoginInfo', hlmy.LOGIN_INFO)
        hlmy.LOGIN_INFO = null;
    }
    // 订阅信息（如果没有订阅  展示二维码）
    if (hlmy.SUBSCRIBE_INFO) {
        store.commit('StoreSubscribeInfo', hlmy.SUBSCRIBE_INFO);
        hlmy.SUBSCRIBE_INFO = null;
    }
    // 运营活动信息（弹框）
    if (hlmy.PROMOTION) {
        store.commit('StorePromotionInfo', hlmy.PROMOTION);
        hlmy.PROMOTION = null;
    }
    // 充值活动弹框
    if (hlmy.RECHARGE_INFO) {
        store.commit('StoreRechargeInfo', hlmy.RECHARGE_INFO);
        hlmy.RECHARGE_INFO = null;
    }
}
export default store;