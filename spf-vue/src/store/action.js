export default {
    triableAction(context) {
        setTimeout(() => {
            // 显示登录框
            context.commit('StoreLoginInfo', { isShow: true })
        }, context.state.StoreLoginInfo.trialTime * 1000)
    }
}