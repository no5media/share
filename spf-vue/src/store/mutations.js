import store from './state'
var mutations = {};
Object.keys(store).forEach((item) => {
    mutations[item] = function(state, arg) {
        state[item] = {...state[item], ...arg };
    }
});

mutations['setTabInfoItem'] = function(state, arg) {
    if (state['StoreTabInfo'].item.indexOf(arg) < 0) {
        state['StoreTabInfo'].item.push(arg);
    }
}
export default mutations;