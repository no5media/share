/**
 * store的存储对象
 */
export default {

    /*********************** 常用基础信息 ********************* */
    // 用户信息
    StoreUserInfo: {
        // 用户gid
        gid: "",
        // 用户昵称
        nickname: "",
        // 用户头像
        headUrl: "",
        // 用户类型
        userType: "",
        // 用户性别
        sex: "",
        // 用户状态
        userStatus: ""
    },
    // 游戏信息
    StoreGameInfo: {
        // 游戏icon 
        iconUrl: "",
        // 游戏的appkey
        appKey: "",
        // 游戏的aid
        aid: 0,
        // 游戏名称
        name: "",
        // 在跳转支付链接的时候用到，拼接到链接后面
        hlmy_from: "",
        // 安卓添加到桌面入口文件
        entryUrl: "",
        // 渠道号信息
        chn: '',
        // loading形式
        loadingType: 'normal',
        // 第三方渠道首屏页面
        loadingLogoUrl: ''
    },
    // 支付信息
    StorePayInfo: {
        //支付版本号
        version: "4",
        //是否使用强制支付版本
        forceEnable: false,
        //强制使用的支付版本号
        forceVersion: 3,
        /*********************************************************/
        /***********        分界线               ******************/
        /*********************************************************/
        // 是否显示支付页面
        isShow: false,
        // 支付url
        pageUrl: ''
    },
    /**
     * 
     * 运营活动信息
     */
    StorePromotionInfo: {
        // 是否拉取活动信息
        enable: false,
        // 等候多长时间之后发起请求，防止初始化进来  请求过多
        time: 0,
        // 是否展示活动组件
        isShow: false,
        // 图片地址
        imgUrl: '',
        // 跳转地址url
        linkUrl: ''
    },
    /**
     * 订阅二维码
     * 在微信中根据enable是否为true进行ajax查询
     * 如果查询结果为true 则弹框让用户关注
     */
    StoreSubscribeInfo: {
        //是否查询
        enable: true,
        //单位是毫秒 
        promptDelay: 60 * 1000,
        // 是否显示二维码
        isShow: false,
        url: 'http://images.1758.com/image/20161019/open_1_e07bcd50b8ea140797cf9f776daa1274.png',
        // 一下内容统计的时候用到
        "openAppId": 0,
        "subscribeImgUrl": "",
        "sceneId": 0,
        "sceneType": 1,
        "sceneType1758": 10
    },
    /**
     * 关注二维码
     * sdk中follow方法调用的时候用到，显示二维码让用户关注
     */
    StoreSdkFollowInfo: {
        isShow: false,
        "url": "http://images.1758.com/image/20161019/open_1_e07bcd50b8ea140797cf9f776daa1274.png",
        // 以下内容 统计的时候用到
        "openAppId": 0,
        "sceneId": 0,
        "sceneType": 1,
        "sceneType1758": 0
    },
    // 充值活动 弹框
    StoreRechargeInfo: {
        enable: false,
        promptDelay: 240 * 1000,
        imgUrl: '',
        isShow: false,
        // 传递给cp使用
        itemCode: 0
    },
    /**
     * im 通讯聊天信息
     * 暂时无用
     */
    StoreChatInfo: {
        // 通讯聊天的场景
        scene: "game_1563005bd578e0801ec36ed2cfc498ed",
        // 会话框key值
        kfDialogKey: "KF_6_1186493"
    },
    /**
     * 浮标中tab项信息
     */
    StoreTabInfo: {
        // 打开浮标显示的tab项 根据数组的顺序来排序
        item: ['tabLB', 'tabGG', 'tabLT', 'tabHot'],
        // 浮标打开的时候默认选中的第几个tab，从0开始
        initTab: 2,

        /*********************************************************/
        /***********        分界线               ******************/
        /*********************************************************/

        list: [{
            // 礼包
            img: 'http://images.1758.com/image/20161228/open_1_6b9c3527e45d9dd7d07a26523a39cf45.png',
            active: 'http://images.1758.com/image/20161228/open_1_caca91653aab832fdd9fb60d7110bca1.png',
            name: 'tabLB'
        }, {
            // 公告
            img: 'http://images.1758.com/image/20170301/open_1_3d3d860094ee886af72127c0a43aac57.png',
            active: 'http://images.1758.com/image/20170301/open_1_d954a13bbc5c81beebbdd9987e4a050b.png',
            name: 'tabGG'
        }, {
            // 聊吧
            img: 'http://images.1758.com/image/20161228/open_1_92e53d38804abb16ae4065c24aa9c88e.png',
            active: 'http://images.1758.com/image/20161228/open_1_6c5dcc3dcb69d2b99cacee971ffaabdb.png',
            name: 'tabLT'
        }, {
            // 热门游戏
            img: 'http://images.1758.com/image/20161228/open_1_0534d0f397f5b8827b15ce5c3505b222.png',
            active: 'http://images.1758.com/image/20161228/open_1_719b8d71baf112eeef0ce7cf6ee6685c.png',
            name: 'tabHot'
        }, {
            // 聊天室
            img: 'http://images.1758.com/image/20170518/open_1_7a7842b66cd387bc1c0420f1aea47563.png',
            active: 'http://images.1758.com/image/20170518/open_1_31dc3455c0eac9c7f960ea543d9686ec.png',
            name: 'tabChat'
        }, {
            // 客服
            img: 'http://images.1758.com/image/20161228/open_1_9b83f2de2a72f39adedb89a0dc19e667.png',
            active: 'http://images.1758.com/image/20161228/open_1_0837f2e2a8a9de4dddb6287402fc1c2a.png',
            name: 'tabKeFu'
        }]
    },

    /**
     * 平台模拟cp分享弹框信息
     */
    StoreSimulateInfo: {
        //游戏中是否有分享弹框（平台自己模拟cp诱导用户分享）
        enable: true,
        // 是否显示弹框
        isShow: false,
        //多久之后进行弹出弹框 默认是360s 注意单位是毫秒
        promptDelay: 360 * 1000,
        /*********************************************************/
        /***********        分界线               ******************/
        /*********************************************************/
        // 图片地址
        promptImgUrl: '',
    },
    // 浮标
    StoreBuoyInfo: {
        //是否需要浮标按钮 
        enable: false
    },
    // 消息推送弹框
    StorePushSetInfo: {
        //如果值为 perference  则设置提醒初始化用到
        initial: "",
        // 是否显示弹框
        isShow: false,
    },
    // 处理在试玩状态下的登录框显示的相关信息
    StoreLoginInfo: {
        //现在是否是试玩状态
        enable: false,
        //试玩多久后弹出登录按钮
        trialTime: 0,
        // 登录框是否显示
        isShow: false,
        // 手机号登录框是否显示
        phoneShow: false
    },
    /*********************************************************/
    /***********        分界线               ******************/
    /*********************************************************/
    // 分享信息
    StoreShareInfo: {
        // 暂时无用
        enable: true,
        //分享通知版本，如果是2 则使用pageUrl进行通知cp进行分享回调，其他的则使用postmessage进行分享回调通知
        version: '4',
        //分享的时候替换链接中的chn
        shareChn: '',
        //请求微信config用到的微信appId
        wyOpenAppId: '',
        //暂时无用
        shareAbility: 0,
        //关闭微信分享菜单里面几个按钮功能，数组项需要跟微信同步
        hideMenu: [],

        //每次随机分享语的id，分享完成后，发送给后台进行日志记录的时候需要传递给后台
        id: '',
        //默认的分享语
        text: [{
            shareDesc: "1758微游戏",
            shareIconUrl: "http://images.1758.com/images/48.ico",
            shareLink: "http://wx.1758.com",
            shareSummary: "1758微游戏",
            shareTitle: "1758微游戏，即点即玩",
            id: ''
        }],
        // 2.0方式分享后通知cp需要用到的地址
        sharePageUrl: "",
        // 记录的分享的原因，正常分享为0  如果是模拟分享弹框30秒内的分享为1
        reasonType: 0,
    },
    // 图片分享
    StoreShareInfoImg: {
        // 是否允许预定义分享
        preShareEnable: false,
        // 预定义分享
        isPreShow: false,
        // 预定义分享的图片url
        preUrl: '',
        // 是否允许自定义分享
        customizeShareEnable: false,
        // 自定义图片分享
        isCustomShow: false,
        // 自定义图片的url
        customUrl: ''
    },
    // 接受从cp发送过来的分享语信息
    StoreShareInfoCp: {
        // 是否使用cp的分享语
        isCp: false,
        title: '',
        desc: '',
        imgUrl: '',
    },
    // state 自定义状态info信息
    StoreStateInfo: {
        // 是否使用通用state
        isBase: false,
        //通用state 通过sdk传入，
        baseState: '',
        //state 每次cp调用sdk的时候传入
        state: ''
    },
    // 弹框信息
    StoreShareGuideInfo: {
        // 分享指引弹框
        shareGuideShow: false,
    },
    // 返回提示信息
    StoreBackInfo: {
        //返回功能中是否使用chn
        enable: false,
        // 是否显示返回提示狂
        isShow: false,
        // 图片显示地址
        url: '',
        // 图片点击链接
        link: ''
    },

    // 浮标中礼包列表信息
    StoreBuoyGiftInfo: {
        // 初步加载后为true 浮标显示隐藏的之间切换的时候 不再加载数据
        isInit: false,
        list: []
    },
    // 浮标中公告的列表信息
    StoreBuoyBroadcastInfo: {

    },
    // 浮标中热门游戏的列表消息
    StoreBuoyGameInfo: {
        isInit: false,
        list: [],
        pageNo: 1
    },
    // 浮标中微社区的列表消息
    StoreBuoyFeedsInfo: {
        isInit: false,
        list: [],
        pageNo: 1,
        pageSize: 10
    }

}