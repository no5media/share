import Vue from 'vue'
import BigImgComponent from './src/index.vue'

let instance;
let BigImgConstructor = Vue.extend(BigImgComponent);

// 初始化组件对象 创建组件实例
let initInstance = () => {
    // 实例化 MessageConstructor的组件
    instance = new BigImgConstructor({
        el: document.createElement('div')
    });
    // 添加到document上面去
    document.body.appendChild(instance.$el);
}

let BigImg = (src) => {
    var options = {};
    options.src = src;
    // 初始化 组件
    initInstance();
    // 合并参数
    Object.assign(instance.$data, options);
    instance.show = true;
    return instance.close;
};

export default BigImg;