import Vue from 'vue'
import CommentComponent from './src/index.vue'

let instance;
let CommentConstructor = Vue.extend(CommentComponent);

// 初始化组件对象 创建组件实例
let initInstance = () => {
    // 实例化 MessageConstructor的组件
    instance = new CommentConstructor({
        el: document.createElement('div')
    });
    // 添加到document上面去
    document.body.appendChild(instance.$el);
}

let Comment = (options = {}) => {
    // 初始化 组件
    initInstance();
    // 合并参数
    Object.assign(instance.$data, options);
    options.success = options.success || instance.success;
    options.cancel = options.cancel || instance.cancel;
    instance.show = true;
    instance.success = () => {
        options.success(instance);
    }
    instance.cancel = () => {
        options.cancel(instance);
    }
    return instance;
};
export default Comment;