import Vue from 'vue'
import MessageComponent from './src/index.vue'

let instance;
let MessageConstructor = Vue.extend(MessageComponent);

// 初始化组件对象 创建组件实例
let initInstance = () => {
    // 实例化 MessageConstructor的组件
    instance = new MessageConstructor({
        el: document.createElement('div')
    });
    // 添加到document上面去
    document.body.appendChild(instance.$el);
}

let Message = (options = {}) => {
    // 初始化 组件
    initInstance();
    // 合并参数
    Object.assign(instance.$data, options);
    instance.show = true;
    return instance.close;
};

// 普通的info方法
Message.info = function(options) {
    var opt = {
        type: 'info',
        duration: 1800,
        iconName: 'icon-information-circled'
    };
    if (typeof options === 'string') {
        options = {
            content: options
        }
    }
    opt = Object.assign(opt, options);
    return Message(opt);
}

// loading 方法
Message.loading = function(options) {
    var opt = {
        type: 'loading',
        iconName: 'icon-load-c icon-load-loop'
    };
    if (typeof options === 'string') {
        options = {
            content: options
        }
    }
    opt = Object.assign(opt, options);
    return Message(opt);
}

export default Message;