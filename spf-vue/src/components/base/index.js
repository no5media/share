import Message from './message/index.js'
import Comment from './comment/index.js'
import Modal from './modal/index.js'
import BigImg from './bigImg/index.js'
const install = function(Vue) {
    //注册全局组件
    //添加全局API
    Vue.prototype.$Comment = Comment;
    Vue.prototype.$Message = Message;
    Vue.prototype.$Modal = Modal;
    Vue.prototype.$BigImg = BigImg;
}
export default install