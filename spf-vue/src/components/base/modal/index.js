import Vue from 'vue'
import ModalComponent from './src/index.vue'
//合并对象函数，这个方法是会改变，第一个参数的值的
let instance;
//extend 是构造一个组件的语法器.传入参数，返回一个组件
let ModalConstructor = Vue.extend(ModalComponent);

let initInstance = () => {
    //实例化ModalConstructor组件
    instance = new ModalConstructor({
        el: document.createElement('div')
    });
    //添加到boby
    document.body.appendChild(instance.$el);
}

let Modal = (options = {}) => {
    //初始化
    initInstance();
    // 将单个 Modal instance 的配置合并到默认值（instance.$data，就是main.vue里面的data）中
    Object.assign(instance.$data, options);
    //返回Promise
    return new Promise((resolve, reject) => {
        instance.show = true;
        let success = instance.success;
        let cancel = instance.cancel;
        instance.success = () => {
            //再执行自定义函数
            success();
            resolve(instance);
        }
        instance.cancel = () => {
            cancel();
            reject(instance)
        }
    });

}
export default Modal;