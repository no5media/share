var gulp = require('gulp');
var concat = require('gulp-concat'); //- 多个文件合并为一个；
var minifyCss = require('gulp-minify-css'); //- 压缩CSS为一行；
var rev = require('gulp-rev'); //- 对文件名加MD5后缀

var taskName = {
    concatcss: 'concatcss', //
    outCssName: 'game_1_1_21.css',
    outWeuiCssName: 'hweui_0_1.css'
}

//合并*压缩*md5
//命令 
// gulp concatcss
//
gulp.task(taskName.concatcss, function() { //- 创建一个名为 concat 的 task
    var postcss = require('gulp-postcss');
    var sourcemaps = require('gulp-sourcemaps');

    return gulp.src(['./mweb/style/external/pure-min.css',
            './mweb/style/common.css',
            './mweb/style/modal/baseinfo.css',
            './mweb/style/share_index.css',
            './mweb/style/modal/gamemodal.css',
            './mweb/style/modal/weself.css',
            './mweb/style/modal/shareinfo.css',
            './mweb/style/modal/destTop.css',
            './mweb/style/modal/pageBack.css',
            './mweb/style/modal/onkey.css',
            './mweb/style/modal/mobileLogin.css',
            './mweb/style/modal/chat.css',
            './mweb/style/modal/remindSet.css',
            './mweb/style/modal/signup.css',
            './mweb/style/modal/warank.css',
            './mweb/style/modal/invite.css',
            './mweb/style/modal/real.css'
        ]) //- 需要处理的css文件，放到一个字符串数组里
        .pipe(concat(taskName.outCssName)) //- 合并后的文件名
        .pipe(minifyCss()) //- 压缩处理成一行
        // .pipe(rev())                                            //- 文件名加MD5后缀
        // .pipe(sourcemaps.init())
        // .pipe(postcss([require('precss'), require('autoprefixer')]))
        // .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./dist/css')) //- 输出文件本地
        //      .pipe(rev.manifest())                                   //- 生成一个rev-manifest.json
        //      .pipe(gulp.dest('./dist/rev'));                              //- 将 rev-manifest.json 保存到 rev 目录内
});

//weui 样式
gulp.task('hlmyweui', function() {
    return gulp.src(['./mweb/js/plugin/weui/css/weui.css',
            './mweb/js/plugin/weui/css/hlmy-weui.css'
        ])
        .pipe(concat(taskName.outWeuiCssName))
        .pipe(minifyCss())
        .pipe(rev())
        .pipe(gulp.dest('./dist/css'))
});