<template tpl-id="details">
	{{for(var i in it){ }}
	<div class="n-item">
		<div class="info-item">
			<span class="cover">
				<a>
					<img src="{{=it[i].userProfile.profile.headUrl}}" />	
				</a>
			</span>
			<div class="meta u-c-info">
				<div class="title">{{=it[i].userProfile.profile.nickname}}</div>
				<div class="expLevel">{{=it[i].userProfile.expInfo.expShowing}}</div>
				<div class="desc">{{=it[i].content}}</div>
				{{?it[i].img[0]}}
				<div class="u-img">
					{{~it[i].img:value:index}}
					<img src="{{=value}}?imageView2/2/w/120"/>
					{{~}}
				</div>
				{{?}}
				<div class="g-replay">
					<div class="r-time">
						<span>{{=it[i].createTime}}</span>
						<div class="t-info">
							{{? it[i].hasLike}}
								<span class="like-p liked" data-like="{{=it[i].id}}" data-type="hasLike">
								<img id='like-img-{{=it[i].id}}'  src="http://images.1758.com/game/n-cxihuan-1-tw.png"/>
								<span id="like-count-{{=it[i].id}}">{{=it[i].likeCount}}</span>
								</span>
							{{??}}
								<span class="like-p" data-like="{{=it[i].id}}" data-type="unlike">
									<img id='like-img-{{=it[i].id}}'  src="http://images.1758.com/game/n-cxihuan-2-tw.png"/>
									<span id="like-count-{{=it[i].id}}"  >{{=it[i].likeCount}}</span>
								</span>
							{{?}}
							<span class="replay" data-message="zhanji,{{=it[i].id}},{{=it[i].resourceId}}">
								<img src="http://wx.1758.com/game/h5/images/n-cpinglun-2-tw.png">
								<span>{{=it[i].replyCount}}</span>
							</span>
						</div>
					</div>
					{{? it[i].replies || it[i].likeList}}
					<div class="r-com">
						<div class="mask">
							<div class="pladebox"> </div>
						</div>
						{{? it[i].likeList && it[i].likeList.length > 0}}
						{{var lListObj = it[i].likeList;var likeListLen = lListObj.length;}}
						<div class="like-com-inner">
							<div class="flag-wrapper">
								<img src="http://images.1758.com/game/n-cxihuan-2-tw.png">
                        	</div>
							<div class="like-com-container">
								{{for(var y=0;y < likeListLen ;y++){ }}
								<div class="like-wrapper" data-uid="{{=lListObj[y].profile.userId}}">
									<a href="{{=lListObj[y].profile.homeLink}}">
									<img src="{{=lListObj[y].profile.headUrl}}">
									</a>
                            	</div>
                            	{{}}}
                        	</div>
						</div>
						{{?}}
						{{? it[i].replies && it[i].replies.length != 0}}
						<div class="com-inner">
							<div class="flag-wrapper">
								<img src="http://images.1758.com/game/n-cpinglun-2-tw.png">
                        	</div>
							{{for(var o in it[i].replies){ }}
							<div>
								{{? it[i].replies[o].referUserProfile != undefined}}
									<a class="c-name" href="http://wx.1758.com/game/h5/user.htm?sid={{=it[i].replies[o].userProfile.profile.gid}}&tp=full&ex1758=1">{{=it[i].replies[o].userProfile.profile.nickname}}</a> 回复 
									<a class="c-name" href="http://wx.1758.com/game/h5/user.htm?sid={{=it[i].replies[o].referUserProfile.profile.gid}}&tp=full&ex1758=1">{{=it[i].replies[o].referUserProfile.profile.nickname}}</a>： 
								{{??}}
									<a class="c-name" href="http://wx.1758.com/game/h5/user.htm?sid={{=it[i].replies[o].userProfile.profile.gid}}&tp=full&ex1758=1">{{=it[i].replies[o].userProfile.profile.nickname}}</a>：
								{{?}}
								<a class="replay" data-message="zhanji_all,{{=it[i].id}},{{=it[i].replies[o].userProfile.profile.userId}},{{=it[i].replies[o].userProfile.profile.gid}},{{=it[i].replies[o].userProfile.profile.nickname}}">{{= it[i].replies[o].content}}</a>
							</div>
							{{ } }}
						</div>
						{{?}}
					</div>
					{{?}}
				</div>
			</div>
		</div>
	</div>
	{{ } }}
</template>

<template tpl-id="gift">
	{{var t = new Date().getTime();var len = it.length;var i = 0;}}
	{{ for(i=0;i<len;i++){ }}

	<div class="item{{?i>2}} gone{{?}}">
		<a class="i-info" href="http://wx.1758.com/play/gift/{{=it[i].gift.id}}?{{? it[i].status == 1}}&exclusive=1{{?}}&tp=full&ex1758=1&title=领取礼包">
			<div class="meta">
				{{? it[i].gift.beginTime > t}}
				<h3 class="title">{{=it[i].gift.name}}</h3>{{? it[i].status == 1}}<span class="exclusive-flag"></span>{{?}}
				<span class="g-fail">开放时间</span>&nbsp;<span class="g-fail-day">{{=it[i].gift.bt}}</span> 
				{{?? it[i].gift.beginTime < t && t< it[i].gift.endTime}}
					{{? it[i].gift.leftNum > 0}}
					<h3 class="title">{{=it[i].gift.name}}</h3>{{? it[i].status == 1}}<span class="exclusive-flag"></span>{{?}}
					<div class="progress-bar g-progress">
						<div class="progress-active" style="width: {{=Math.ceil(it[i].gift.leftNum*100/it[i].gift.num)}}%;"></div>
					</div>
					<span class="g-rest">剩余{{=Math.ceil(it[i].gift.leftNum*100/it[i].gift.num)}}%</span>
					{{?? it[i].gift.updateTime && it[i].gift.leftNum < 1 && (t-it[i].gift.updateTime)< 60*60*1000}}
						<h3 class="title">{{=it[i].gift.name}}</h3>{{? it[i].status == 1}}<span class="exclusive-flag"></span>{{?}}
						<span class="g-fail">即将开启淘号</span>
					{{??}}
						<h3 class="title">{{=it[i].gift.name}}</h3>{{? it[i].status == 1}}<span class="exclusive-flag"></span>{{?}}
						<span class="g-fail">已淘{{=it[i].gift.taoNum}}次</span>
					{{?}}
				{{?? it[i].gift.endTime < t }}
					<h3 class="title">{{=it[i].gift.name}}</h3>{{? it[i].status == 1}}<span class="exclusive-flag"></span>{{?}}
					<span class="g-fail">敬请期待下次活动</span>
				{{?}}
			</div>
		</a>
		
		{{? it[i].gift.beginTime > t}}
		<a class="fail-btn" data-num='{{=it[i].gift.id}}' data-value='{{=it[i].status}}'>
			未开放
		</a>
		{{?? it[i].gift.beginTime < t && t< it[i].gift.endTime}}
			{{? it[i].gift.leftNum > 0}}
			<a class="play-btn linghao" data-num='{{=it[i].gift.id}}' data-value='{{=it[i].status}}'>
				</i>领号
			</a>
			{{?? it[i].gift.updateTime && it[i].gift.leftNum < 1 && (t-it[i].gift.updateTime)< 60*60*1000}}
			<a class="fail-btn" data-num='{{=it[i].gift.id}}' data-value='{{=it[i].status}}'>
				已领完
			</a>
			{{??}}
			<a class="tao-btn taohao" data-num='{{=it[i].gift.id}}' data-value='{{=it[i].status}}'>
				</i>淘号
			</a>
			{{?}}
		{{?? it[i].gift.endTime < t }}
			<a class="fail-btn" data-num='{{=it[i].gift.id}}' data-value='{{=it[i].status}}'>
				已结束
			</a>
		{{?}}
	</div>
	{{ } }}
</template>
<template tpl-id="gameQFOpen">
	{{var len = it.length;var i = 0;}}
	{{ for(i=0;i<len;i++){ }}
	<div class='item{{?i>2}} gone{{?}}'>
		<div class="inner-item">
			<div class="k-qf">{{=it[i].name}}</div>
			<div class="k-time">{{=it[i].bt}}</div>
		</div>
	</div>
	{{ } }}
</template>
<template tpl-id="broadCast">
	{{var len = it.length;var i = 0;}}
	{{ for(i=0;i<len;i++){ }}
	<div class='item{{?i>9}} gone{{?}}' data-id={{=it[i].id}}>
		<div class="inner-item">
			<div class="bc-name">{{=it[i].title}}</div>
			<div class="bc-time">{{=it[i].bctime}}</div>
		</div>
	</div>
	{{ } }}
</template>
<template tpl-id="hotGames">
	{{ for(var i in it){ }}
	<div class="item">
		<a class="i-info" href="/play/game/{{=it[i].appArticleId}}?title={{=it[i].appName}}&tp=full&ex1758=1">
			<figure class="cover">
				<img src="http://wx.1758.com/game/h5/images/head.jpg" data-echo="{{=it[i].appIconUrl}}" />
			</figure>
			<div class="meta">
				<h3 class="title">{{=it[i].appName}}
					<span class="identification">
					{{? it[i].publotteryArticleId == 1 }}
					<img src="http://images.1758.com/game/m/lb1.png">
					{{?}}
					</span>
				</h3>
				<div class="desc">{{=it[i].appRemark}}</div>
			</div>
		</a>
		<a href="{{=it[i].appVideoUrl}}?tp=fs&ex1758=1&title={{=it[i].appName}}&hlmy_ent=platform_fubiaoremen_{{=window.htj_fbhot}}">
			<div class="play-btn">
				<i class="icon-right"></i>
				开始	
			</div>
		</a>
		{{window.htj_fbhot ++;}}
	</div>
	{{ } }}
</template>