<template tpl-id="modalMainTem">
	<div id="content" class="modal-content">
		<style type="text/css" media="screen">
			.base-head{line-height:46px;height:46px;background-color:#fff;position: relative;}.head-menu{padding:0;position:relative;text-align:center;position:relative;}.head-menu .head-index{position:absolute;left:6px;padding:0 10px}.head-index img{width:40px;vertical-align:middle;border-radius:100px;}.head-menu .head-icon img{width:100px;vertical-align:middle}.head-menu .head-dowload{position:absolute;right:0;line-height:initial;margin-top:15px;box-sizing: border-box;margin-right: 10px;}.head-dowload a{background-color:#FCA218;color:#fff;text-decoration:none;display:inline-block;height:18px;line-height:18px;padding:0 5px;border-radius:50px;font-size:12px;cursor:pointer}
		</style>
    	<header id="headContent" class="base-head" style="display: none;">
			<div class="head-menu">
				<span class="head-index" id="modalUserHead"><a href="//wx.1758.com/play/user/home"><img src=""></a></span>
				<span class="head-icon"><a href="//wx.1758.com/play/index"><img src="http://wx.1758.com/game/h5/images/head-icon.png"></a></span>
				<span class="head-dowload">
				<a href="/static/m/follow/follow.html?from=gfb">
					安装APP
				</a></span>
			</div>
		</header>
		<style type="text/css" media="screen">
			.modal-content{background-color:initial}.client-share-container{height:68px;display:none;padding-top:9px;box-sizing:border-box}.client-share-wrapper{width:240px;background-color:rgba(0,0,0,.4);border-radius:34px;margin:0 auto;text-align:center;line-height:40px;height:50px;position:relative;padding:0 20px;box-sizing:border-box}.client-share-flex{box-sizing:border-box;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;width:100%;position:relative}.client-btnwrapper{display:inline-block;box-sizing:border-box;text-align:center;height:50px;line-height:50px;-webkit-box-flex:1;-webkit-flex:1;-ms-flex:1;flex:1;overflow:hidden;box-sizing:border-box}.client-btnwrapper>img{width:100%;max-width:45px;box-sizing:border-box;vertical-align:middle}.client-btn{position:absolute;right:-15px;top:0}.client-btn .clientHeadImg{border-radius:50px;border:1px solid #fff;box-sizing:border-box;height:36px;width:36px;margin-top:8px}.client-switch-account{position:absolute;right:-2px;width:15px;top:30px}.base-footer{margin-bottom:0}.main-container{background-color:#fff}
		</style>
		<div id="clientHeader" class="client-share-container">
    		<div class="client-share-wrapper">
    			<div class="client-share-flex">
	    			<div class="client-btnwrapper clientExit gone">
						<img src="http://images.1758.com/image/20170414/open_1_13165744f381c21711109141febbd0e4.png" alt="退出">
	    			</div>
	    			<div class="client-btnwrapper clientDeskTop gone">
						<img src="http://images.1758.com/image/20170414/open_1_745d65bf8c7c9b61a9c48d56de713bf4.png" alt="添加到桌面">
	    			</div>
	    			<div class="client-btnwrapper clientRefresh gone">
						<img src="http://images.1758.com/image/20170414/open_1_8df38e74a4e60d9e32d9df4dda31f025.png" alt="刷新">
	    			</div>
	    			<div class="client-btnwrapper clientShare gone">
						<img src="http://images.1758.com/image/20170414/open_1_f1bf5303c807a67895bebcc79d0a6a0f.png" alt="分享">
	    			</div>
    			</div>
    			<div class="client-btn">
        			<img class="clientHeadImg" src="http://q.qlogo.cn/qqapp/101026391/783165BE37D28F1E569809395ABB68CA/100" alt="">
        			<img class="client-switch-account gone clientSwitch" src="http://images.1758.com/image/20170414/open_1_0203290f0c09bd9119930797f0da19e8.png" alt="">
        		</div>
    		</div>
    	</div>
		<section id="base-footer-game" class="base-footer">
			<div id="base-menu" class="tab-g base-menu">
			    <div class="tab-i t tabLB gone">
                    <img src="http://wtest.1758.com/static/test/test/images/gift_none.png">
                </div>
                <div class="tab-i t tabGG gone">
                    <img src="http://images.1758.com/image/20170301/open_1_3d3d860094ee886af72127c0a43aac57.png">
                </div>
                <div class="tab-i t tabLT gone">
                    <img src="http://wtest.1758.com/static/test/test/images/talk_none.png"> 
                </div>
                <div class="tab-i t tabHot gone">
		    		<img src="http://wtest.1758.com/static/test/test/images/hot_none.png">	
			    </div>
			    <div class="tab-i t tabChat gone">
		    		<img src="http://images.1758.com/image/20170518/open_1_7a7842b66cd387bc1c0420f1aea47563.png">	
			    </div>
			    <div class="tab-i t tabKeFu gone">
			    		<img src="http://wtest.1758.com/static/test/test/images/kefu_none.png">	
			    </div>
			</div>
		</section>
		<section id="modalMainContainer" class="main-container">
			<section id="gift" class="gift panelitem gone">
				<div class="gift-content-wrapper">
					<div id="giftList" class="list bg gone">
						<div class="inner-klist bg">
							<!-- <header class="g-head g-l-h"><span>游戏礼包</span></header> -->
							<div id="giftWrapper" class="klist-item-wrapper">
							</div>
							<footer id="showHG" class="k-foot gone">
			            		<span>••• 查看更多礼包</span>
			            	</footer>
		            	</div>
					</div>
					<div class="more-info gone">
						<a>暂无更多数据</a>
					</div>
				</div>
			</section>
			<section id="broadcast" class="panelitem gone">
				<div id="QFOpenTimeList" class="klist gone">
					<div class="inner-klist bg">
						<!-- <header class="g-head g-k-h"><span>开服计划</span></header> -->
						<div id="qfWrapper" class="klist-item-wrapper">
						</div>
						<footer id="showQF" class="k-foot gone">
		            		<span>••• 更多开服计划</span>
		            	</footer>
	            	</div>
				</div>
				<div id="broadList" class="list gone">
					<div class="inner-klist bg">
						<div id="bcWrapper" class="klist-item-wrapper">
						</div>
						<footer id="showBC" class="k-foot gone">
		            		<span>••• 查看更多公告</span>
		            	</footer>
	            	</div>
				</div>
				<div class="more-info gone">
					<a>暂无更多数据</a>
				</div>

				<div id="broadDetail" class="gone broadcast-content">
					<div class="inner-broaddetail">
						<div class="bdetail-head">
							<div class="bdetail-title">
							</div>
							<div class="bdetail-time">
							</div>
						</div>
						<div class="bdetail-main">
							<div class="inner-bdetail-main gone">
							</div>
							<div class="bdetail-loading gone">
								正在加载内容...								
							</div>
						</div>
						<div class="bdetail-foot gone">
							<div class="bdetail-foot-inner">
								<div id="bdetailBtn" class="bdetail-btn">
									返回
								</div>
							</div>
						</div>
					</div>
				</div>
				
			</section>
			<section id="talk" class="talk panelitem gone">
				<div class="list">
					<div class="more-info" data-num="1" data-track='talk' data-value="un"><a>点击加载更多</a></div>
				</div>
				<div class="comment-logo">
					<div class="inner-comment-logo">
						<img id="commentLogo" src="http://images.1758.com/fubiao/fubiao_pinglun.png">
					</div>
				</div>
			</section>
			<section id="hotGame" class="hot-game panelitem gone">
				<div class="list"></div>
				<div class="more-info">
					<a></a>
				</div>
			</section>
			<section id="kefu" class="kefu panelitem gone">
				<div class="kefu-container kefuContainer">
					<div class="kefu-back" id="kefuClose">
						返回
					</div>
					<div class="inner-kefu-container">
						<div id="chatContainer" class="kflist">
							<div class="kfitem">
								<div class="kfleft">
									<img src="http://q.qlogo.cn/qqapp/101026391/08F712AFECA9B2805863CFA337AC4143/100" alt="用户头像">
								</div>
								<div class="kfright">
									<div class="kfinfo">
										<span class="kfname">女王大人</span>
										<span class="kftime">今天</span>
									</div>
									<div class="kfcontent other-content" style="padding:1px;max-width:96%">
										<a href="http://wpa.qq.com/msgrd?v=3&uin=2781758974&site=qq&menu=yes">
											<img style="max-width: 100%;" src="http://images.1758.com/image/20170428/open_1_96e60dcf15530041e73f6b812926b7d6.jpg">
										</a>
									</div>
								</div>
							</div>
							<div class="kfitem">
								<div class="kfleft">
									<img src="http://q.qlogo.cn/qqapp/101026391/08F712AFECA9B2805863CFA337AC4143/100" alt="用户头像">
								</div>
								<div class="kfright">
									<div class="kfinfo">
										<span class="kfname">女王大人</span>
										<span class="kftime">今天</span>
									</div>
									<div class="kfcontent other-content" style="padding:1px;max-width: 96%;">
										<a href="http://wpa.qq.com/msgrd?v=3&uin=2660866818&site=qq&menu=yes">
											<img style="max-width: 100%;" src="http://images.1758.com/image/20170428/open_1_a136132780930e6ce7efc17aa9127bf4.jpg">
										</a>
									</div>
								</div>
							</div>
							<div class="kfitem">
								<div class="kfleft">
									<img src="http://q.qlogo.cn/qqapp/101026391/08F712AFECA9B2805863CFA337AC4143/100" alt="用户头像">
								</div>
								<div class="kfright">
									<div class="kfinfo">
										<span class="kfname">女人大人</span>
										<span class="kftime">今天</span>
									</div>
									<div class="kfcontent other-content" style="padding:1px;">
										<img style="width: 150px;" src="http://images.1758.com/image/20170428/open_1_3ec3af4fa84804ccc53b56e8f7e3752c.jpg">
									</div>
								</div>
							</div>
							<div class="historyline"></div>
						</div>
					</div>
				</div>
				<div class="input-container">
					<div class="chat-wrapper">
						<input id="chatText" placeholder="聊天内容不能超过40个字" type="text">
						<a id="chatBtn">发送</a>
					</div>
				</div>
			</section>
			<section id="kefuInfo" class="kefu panelitem">
				<div class="kefu-container">
					<div class="inner-kefuinfo inner-kefu-container">
						<div class="kefuinfo-square">
							<div class="kefuinfo-header">
								<span class="point"></span>
								客服专区
							</div>
							<div class="kefuinfo-wrapper">
								<div class="kefuinfo-left">
									<img src="http://images.1758.com/image/20171213/open_104818_1de62515de2c4639fb47fabbd06a50bc.png" alt="">
								</div>
								<div class="kefuinfo-right">
									<div class="kefuinfo-text">客服专区-24小时在线</div>
									<div class="kefuinfo-qq">QQ:2660866818</div>
									<div class="kefuinfo-btn">
										<a href="http://wpa.qq.com/msgrd?v=3&uin=2660866818&site=qq&menu=yes">点击联系官方客服QQ</a>
									</div>
								</div>
							</div>
						</div>
						<div class="infos-square">
							<div id="kefuOpen" class="infosquare-container">
								我的消息盒子
							</div>
						</div>
						<div class="vipinfo-square">
							<div class="kefuinfo-header">
								<span class="point"></span>
								VIP特权专区
							</div>
							<div class="vipsquare-list">
								<div class="vipsquare-item">
									<div class="vipsquare-left">
										<div class="vipsquare-inner">
											<div class="vipsquare-title">贵族礼包</div>
											<div class="vipsquare-text">贵族专享 身份红利</div>
										</div>
									</div>
									<div class="vipsquare-right">
										<img src="http://images.1758.com/image/20171213/open_104818_e71e111d02248aba522c856237e7f5ac.png" alt="">
									</div>
								</div>
								<div class="vipsquare-item">
									<div class="vipsquare-left">
										<div class="vipsquare-inner">
											<div class="vipsquare-title">专属折扣</div>
											<div class="vipsquare-text">相同消费，更为优惠，更享尊贵</div>
										</div>
									</div>
									<div class="vipsquare-right">
										<img src="http://images.1758.com/image/20171213/open_104818_748a592ee18f235a7c45298f5a4b249b.png" alt="">
									</div>
								</div>
								<div class="vipsquare-item">
									<div class="vipsquare-left">
										<div class="vipsquare-inner">
											<div class="vipsquare-title">贴心客服</div>
											<div class="vipsquare-text">专属客服妹子一对一服务,优先解答您的问题及需求</div>
										</div>
									</div>
									<div class="vipsquare-right">
										<img src="http://images.1758.com/image/20171213/open_104818_6831f7b976438dc807774710b7d07a31.png" alt="">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="kefuvip-btn-container input-container">
					<div class="btn" id="kefuInfoEwm">我的专属客服</div>
					<div class="text">VIP4以上点击了解更多福利</div>
				</div>
			</section>
			<section id="chat" class="kefu panelitem gone">
				<div class="kefu-container kefuContainer">
					<div class="inner-kefu-container">
						<div id="chatContainer" class="kflist">
							<div class="historyline"></div>
						</div>
					</div>
				</div>
				<div class="input-container">
					<div class="chat-wrapper">
						<input id="chatText" placeholder="聊天内容不能超过40个字" type="text">
						<a id="chatBtn">发送</a>
					</div>
				</div>
			</section>
		</section>
		<div class="tip gone">
			<div class="tip-dialog">
				<div class="tip-wrapper">
					<div class="tip-content tip-exclusive">
						<div class="tip-body">
							<p class="ex-con"><span style="color: #71A540;">专属礼包</span>需通过1758客户端领取</p>
							<span class="ex-inner">
							<span class="ex-info">您还没有安装1758客户端？</span>
							<a class="ex-load">立即下载</a>
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="weui_dialog_confirm gone" id="weDialog">
	        <div class="weui_mask"></div>
	        <div class="weui_dialog">
				<div class="weui_dialog_hd">
					<strong class="weui_dialog_title">发表评论</strong>
					<!--<strong class="weui_add_img" id="wxAddImg"></strong>-->
				</div>
				<div class="weui_dialog_bd webd">
					<textarea id="weText" contenteditable="false" placeholder="大侠留个名吧~"></textarea>
					<div class="img-wrapper pure-g" id="wxImgWrapper">
						<div id="wxAddImg" class="pure-u-1-4">
							<div class="imgitem">
								<img src="http://images.1758.com/daren/upload.png" />
							</div>
						</div>
					</div>
				</div>
				<div class="weui_dialog_ft">
					<a id="weCancle" class="weui_btn_dialog default">取消</a>
					<a id="wePublic" class="weui_btn_dialog primary">发布</a>
				</div>
			</div>
	    </div>
	    <div id="toast" class="gone">
	        <div class="weui_mask_transparent"></div>
	        <div class="weui_toast">
	            <i class="weui_icon_toast"></i>
	            <p class="weui_toast_content">已发送</p>
	        </div>
	    </div>
	    <div id="toastTip" class="gone">
	        <div class="weui_mask_transparent"></div>
	        <div class="weui_toast weui_toast_text">
	            <p class="weui_toast_content weui_toast_content_text">最多四张图片</p>
	        </div>
	    </div>
	    <div id="loadingToast" class="weui_loading_toast gone">
	        <div class="weui_mask_transparent"></div>
	        <div class="weui_toast">
	            <div class="weui_loading">
	                <div class="weui_loading_leaf weui_loading_leaf_0"></div>
	                <div class="weui_loading_leaf weui_loading_leaf_1"></div>
	                <div class="weui_loading_leaf weui_loading_leaf_2"></div>
	                <div class="weui_loading_leaf weui_loading_leaf_3"></div>
	                <div class="weui_loading_leaf weui_loading_leaf_4"></div>
	                <div class="weui_loading_leaf weui_loading_leaf_5"></div>
	                <div class="weui_loading_leaf weui_loading_leaf_6"></div>
	                <div class="weui_loading_leaf weui_loading_leaf_7"></div>
	                <div class="weui_loading_leaf weui_loading_leaf_8"></div>
	                <div class="weui_loading_leaf weui_loading_leaf_9"></div>
	                <div class="weui_loading_leaf weui_loading_leaf_10"></div>
	                <div class="weui_loading_leaf weui_loading_leaf_11"></div>
	            </div>
	            <p class="weui_toast_content">相册打开中</p>
	        </div>
	    </div>
	</div>
</template>