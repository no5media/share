<template tpl-id="modalMainTem">
	<div id="content" class="modal-content">
		<style type="text/css" media="screen">
			.base-head{line-height:46px;height:46px;background-color:#fff;position: relative;}.head-menu{padding:0;position:relative;text-align:center;position:relative;}.head-menu .head-index{position:absolute;left:6px;padding:0 10px}.head-index img{width:40px;vertical-align:middle;border-radius:100px;}.head-menu .head-icon img{width:100px;vertical-align:middle}.head-menu .head-dowload{position:absolute;right:0;line-height:initial;margin-top:15px;box-sizing: border-box;margin-right: 10px;}.head-dowload a{background-color:#FCA218;color:#fff;text-decoration:none;display:inline-block;height:18px;line-height:18px;padding:0 5px;border-radius:50px;font-size:12px;cursor:pointer}
		</style>
    	<header id="headContent" class="base-head" style="display: block;">
			<div class="head-menu">
				<span class="head-index" id="modalUserHead"><a href="//wx.1758.com/play/user/home"><img src=""></a></span>
				<span class="head-icon"><a href="//wx.1758.com/play/index"><img src="http://wx.1758.com/game/h5/images/head-icon.png"></a></span>
				<span class="head-dowload">
				<a href="/static/m/follow/follow.html?from=gfb">
					安装APP
				</a></span>
			</div>
		</header>
		<section id="base-footer-game" class="base-footer">
			<div id="base-menu" class="pure-g base-menu">
			    <div id="t-t" class="pure-u-1-4 t">
                    <img src="http://wtest.1758.com/static/test/test/images/gift_none.png">
                </div>
                <div id="t-g" class="pure-u-1-4 t">
                    <img src="http://wtest.1758.com/static/test/test/images/talk_none.png"> 
                </div>
                <div id="t-k" class="pure-u-1-4 t">
			    		<img src="http://wtest.1758.com/static/test/test/images/hot_none.png">	
			    </div>
			    <div id="t-k" class="pure-u-1-4 t">
			    		<img src="http://wtest.1758.com/static/test/test/images/kefu_none.png">	
			    </div>
			</div>
		</section>
		<section id="modalMainContainer" class="main-container">
			<section id="gift" class="gift panelitem gone">
				<div class="gift-content-wrapper">
					<div id="QFOpenTimeList" class="klist gone">
						<div class="inner-klist bg">
							<header class="g-head g-k-h"><span>开服计划</span></header>
							<div id="qfWrapper" class="klist-item-wrapper">
							</div>
							<footer id="showQF" class="k-foot gone">
			            		<span><img src="http://images.1758.com/game/down.png" alt="向下"></span>
			            	</footer>
		            	</div>
					</div>
					<div id="giftList" class="list bg gone">
						<div class="inner-klist bg">
							<header class="g-head g-l-h"><span>游戏礼包</span></header>
							<div id="giftWrapper" class="klist-item-wrapper">
							</div>
							<footer id="showHG" class="k-foot gone">
			            		<span><img src="http://images.1758.com/game/down.png" alt="向下"></span>
			            	</footer>
		            	</div>
					</div>
					<div class="more-info gone">
						暂无更多数据
					</div>
				</div>
			</section>
			<section id="talk" class="talk panelitem gone">
				<div class="list">
					<div class="more-info" data-num="1" data-track='talk' data-value="un"><a>点击加载更多</a></div>
				</div>
				<div class="comment-logo">
					<div class="inner-comment-logo">
						<img id="commentLogo" src="http://images.1758.com/fubiao/fubiao_pinglun.png">
					</div>
				</div>
			</section>
			<section id="hotGame" class="hot-game panelitem gone">
				<div class="list">
					
				</div>
				<div class="more-info">
					<a></a>
				</div>
			</section>
			<section id="kefu" class="kefu panelitem gone">
				<div class="kefu-container">
					<div class="kefu-header-wrapper">
						<div class="kefu-title">
							关注公众号
						</div>
						<div class="kefu-desc">
							专业客服7X24小时为您悉心解答游戏中的问题
						</div>
					</div>
					<div class="kefu-wx">
						<img src="http://images.1758.com/image/20170216/open_1_36e41cdad68dedf2da0702875e129a35.png">
						<div>
							长按识别二维码关注
						</div>
					</div>
				</div>
				<div class="kefu-container kefu-qq-con">
					<div class="kefu-header-wrapper">
						<div class="kefu-title">
							联系在线客服
						</div>
						<div class="kefu-desc">
							点击头像添加客服QQ,及时联系客服解决问题
						</div>
					</div>
					<div class="kefu-qq">
						<a href="http://wpa.qq.com/msgrd?v=3&uin=2660866818&site=qq&menu=yes">
							<span class="ls-img">
								<img src="http://wx.1758.com/game/h5/images/qq1.png">
								<img class="ls-flag" src="http://wx.1758.com/game/h5/images/qq.png">
							</span>
						</a>
						<a href="http://wpa.qq.com/msgrd?v=3&uin=2781758974&site=qq&menu=yes">
							<span class="ls-img">
								<img src="http://images.1758.com/game/m/kf2.png">
								<img class="ls-flag" src="http://wx.1758.com/game/h5/images/qq.png">
							</span>
						</a>
					</div>
				</div>

			</section>
		</section>
		<div class="tip gone">
			<div class="tip-dialog">
				<div class="tip-wrapper">
					<div class="tip-content tip-exclusive">
						<div class="tip-body">
							<p class="ex-con"><span style="color: #71A540;">专属礼包</span>需通过1758客户端领取</p>
							<span class="ex-inner">
							<span class="ex-info">您还没有安装1758客户端？</span>
							<a class="ex-load">立即下载</a>
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="weui_dialog_confirm gone" id="weDialog">
	        <div class="weui_mask"></div>
	        <div class="weui_dialog">
				<div class="weui_dialog_hd">
					<strong class="weui_dialog_title">发表评论</strong>
					<!--<strong class="weui_add_img" id="wxAddImg"></strong>-->
				</div>
				<div class="weui_dialog_bd webd">
					<textarea id="weText" contenteditable="false" placeholder="大侠留个名吧~"></textarea>
					<div class="img-wrapper pure-g" id="wxImgWrapper">
						<div id="wxAddImg" class="pure-u-1-4">
							<div class="imgitem">
								<img src="http://images.1758.com/daren/upload.png" />
							</div>
						</div>
					</div>
				</div>
				<div class="weui_dialog_ft">
					<a id="weCancle" class="weui_btn_dialog default">取消</a>
					<a id="wePublic" class="weui_btn_dialog primary">发布</a>
				</div>
			</div>
	    </div>
	    <div id="toast" class="gone">
	        <div class="weui_mask_transparent"></div>
	        <div class="weui_toast">
	            <i class="weui_icon_toast"></i>
	            <p class="weui_toast_content">已发送</p>
	        </div>
	    </div>
	    <div id="toastTip" class="gone">
	        <div class="weui_mask_transparent"></div>
	        <div class="weui_toast weui_toast_text">
	            <p class="weui_toast_content weui_toast_content_text">最多四张图片</p>
	        </div>
	    </div>
	    <div id="loadingToast" class="weui_loading_toast gone">
	        <div class="weui_mask_transparent"></div>
	        <div class="weui_toast">
	            <div class="weui_loading">
	                <div class="weui_loading_leaf weui_loading_leaf_0"></div>
	                <div class="weui_loading_leaf weui_loading_leaf_1"></div>
	                <div class="weui_loading_leaf weui_loading_leaf_2"></div>
	                <div class="weui_loading_leaf weui_loading_leaf_3"></div>
	                <div class="weui_loading_leaf weui_loading_leaf_4"></div>
	                <div class="weui_loading_leaf weui_loading_leaf_5"></div>
	                <div class="weui_loading_leaf weui_loading_leaf_6"></div>
	                <div class="weui_loading_leaf weui_loading_leaf_7"></div>
	                <div class="weui_loading_leaf weui_loading_leaf_8"></div>
	                <div class="weui_loading_leaf weui_loading_leaf_9"></div>
	                <div class="weui_loading_leaf weui_loading_leaf_10"></div>
	                <div class="weui_loading_leaf weui_loading_leaf_11"></div>
	            </div>
	            <p class="weui_toast_content">相册打开中</p>
	        </div>
	    </div>
	</div>
</template>