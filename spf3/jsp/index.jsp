<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ page import="com.wenyan.platform.partner.*" %>
<%@ page import="com.bruce.geekway.utils.XSSDefenseUtils" %>
<%@ page import="com.wenyan.article.entity.*" %>
<%@ page import="com.wenyan.app.entity.*" %>
<%@ page import="com.wenyan.constants.*" %>
<%@ page import="com.bruce.hlmy.rpcdef.message.model.*" %>
<%@ page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="org.apache.commons.lang3.time.DateUtils"%>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>


<%
String channel = (String)session.getAttribute("chn");
channel = XSSDefenseUtils.htmlEncode(channel);
channel = channel==null?"":channel;

WxApp wxApp = (WxApp)request.getAttribute("wxApp");
String title = "1758微游戏";
if(wxApp!=null&&StringUtils.isNotBlank(wxApp.getName())){
	if("baiyuehudong".equals(channel)){//百岳互动的特别处理
		title = wxApp.getName();
	}else if("caomu-cjtzf".equals(channel)){//草木互动的特别处理
		title = wxApp.getName()+"丨粉色书城_游戏";
	}else if("guanjiancibaidu".equals(channel)){//［关键词百度］渠道的特别处理
		title = wxApp.getName()+"-A游";
	}else if("guanba".equals(channel)){//［关八］渠道的特别处理
		title = wxApp.getName()+"-关八夜总会";
	}else if("feichangdanao".equals(channel)){//［非常大脑］渠道的特别处理
		title = wxApp.getName()+"-非常大脑";
	}else if("zwhy".equals(channel)){//［非常大脑］渠道的特别处理
		title = wxApp.getName()+"-互娱盒子";
	}else if("youxiniao".equals(channel)){//［游戏鸟］渠道的特别处理
		title = wxApp.getName()+"-游戏鸟";
	}else if(channel!=null&&channel.startsWith("aiquyou")){//［爱趣游］代理渠道的特别处理
		title = wxApp.getName()+"-爱趣游";
	}else{
		title = wxApp.getName()+"-1758.com";
	}
}


String channelLogoImgUrl = null;
String channelTipImgUrl = null;
if("chiba".equals(channel)){//特定处理
	channelLogoImgUrl = "http://images.1758.com/partner/chiba/logo.png";
}else if("wanmeng".equals(channel)){//特定处理
	channelLogoImgUrl = "http://images.1758.com/article/image/2016/05/12/24911463034957464.png"; 
}else if("bahaodianying".equals(channel)){//渠道［8号电影］特定处理
	channelLogoImgUrl = "http://images.1758.com/partner/bhyy/8game.png";
}else if("ycyh".equals(channel)){//渠道［有车以后］特定处理
	channelLogoImgUrl = "http://images.1758.com/article/image/2016/05/27/21181464334782932.png";
}else if("yizi".equals(channel)){//渠道[椅子网] 特定处理
	channelLogoImgUrl = "http://images.1758.com/partner/yizi/yizi-logo.png";
}else if("baiyuehudong".equals(channel)){//渠道[百岳互动] 特定处理
	channelLogoImgUrl = "http://images.1758.com/partner/byhd/game-logo.png";
}else if("guanjiancibaidu".equals(channel)){//渠道[关键词百度] 特定处理
	channelLogoImgUrl = "http://images.1758.com/partner/byhd/game-logo.png";
	//channelLogoImgUrl = "http://images.1758.com/partner/guanjiancibaidu/logo.jpg";
}else if("guanba".equals(channel)){//渠道[关八] 特定处理
	channelLogoImgUrl = "http://images.1758.com/partner/guan/game-logo.png";
}else if("feichangdanao".equals(channel)){//渠道[非常大脑] 特定处理
	channelLogoImgUrl = "http://images.1758.com/image/20160830/open_1_d2e0596b44c6c90b060a6048360979b3.png";
}else if("zwhy".equals(channel)){//渠道[中文互娱] 特定处理
	channelLogoImgUrl = "http://images.1758.com/image/20160908/open_1_84d78ed64d4ddd2943056a8870b386d8.png";
}else if(channel!=null&&channel.startsWith("aiquyou")){//［爱趣游］代理渠道的特别处理
	channelLogoImgUrl = "http://images.1758.com/image/20160901/open_1_e7d76a05330b4a2e029982a5313b8d3b.png"; 
}else if("youxiniao".equals(channel)){//渠道[游戏鸟] 特定处理
	channelLogoImgUrl = "http://images.1758.com/image/20161018/open_1_71e01ea717952df319f88094bbeec5ee.png";
}

if("tashequ712".equals(channel)){
	channelTipImgUrl = "http://images.1758.com/partner/ta/collection-tip.png";
}else if("kunpeng".equals(channel)){
	channelTipImgUrl = "http://images.1758.com/partner/kunpeng/collection-kp-1.png";
}
%>


<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title><%=title%></title>
	
	<%if(!"meitu".equals(channel)){ %>
	<script type="text/javascript">
		if (top != this) {
			top.location.href = location.href;
		}
	</script>
	<%} %>
	
	
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="format-detection" content="telephone=no">    
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="mobile-web-app-capable" content="yes"/>
    <meta name="full-screen" content="true"/>
    <meta name="screen-orientation" content="portrait"/>
    <meta name="x5-fullscreen" content="true"/>
    <meta name="360-fullscreen" content="true"/>
    <meta http-equiv="pragma" content="no-cache"/>
    <meta http-equiv="cache-control" content="no-cache"/>
    <meta http-equiv="cache" content="no-cache"/>
    <meta http-equiv="expires" content="0"/>
    <link rel="shortcut icon "  href="http://images.1758.com/images/48.ico">
    <link rel="stylesheet" href="/spf/dist/css/game_4_6.css" type="text/css" />
    <link rel="stylesheet" href="/spf/dist/css/hweui_0_1-65bd4a8153.css" type="text/css" />
    
    <%if(StringUtils.isNotBlank(channelLogoImgUrl)){ %>
    <link rel="stylesheet" type="text/css" href="/spf/dist/css/start_0_3.css"/>
    <%} %>
    <script src="/spf/dist/scripts/metainfo_1_0.min.js"></script>
    
    <%
    String domain = "wx.1758.com";
    int payVersion = 3;//支付版本，默认为3
    if("tclyy".equals(channel)||"tashequ".equals(channel)||"tashequxnw".equals(channel)||"qipaiapp".equals(channel)||"sinagame".equals(channel)||"yuewenqq".equals(channel)||"yuewenqd".equals(channel)){
    	//部分小米手机在ifrmae下存在cookie丢失的问题，只能全部改为支付2.0版本
    	payVersion=2;
    }
    %>
    
    
    <%
    //临时统计
    if("tashequ".equals(channel)||"tashequxnw".equals(channel)){
    %>
    <script>
	var _hmt = _hmt || [];
	(function() {
	  var hm = document.createElement("script");
	  hm.src = "//hm.baidu.com/hm.js?3f628f9aa9939578047aa1e5766bbc2d";
	  var s = document.getElementsByTagName("script")[0]; 
	  s.parentNode.insertBefore(hm, s);
	})();
	</script>
	<%}%>


    <%
    int openAppId = 107686;//默认分享的公众号id
    if(wxApp.getId()==111){//特定的游戏指定分享的公众号，避免被封
    	openAppId = 107689;
    }
    
    String shareChn = "caomu-cjtzf".equals(channel)?channel:"";
    if(channel!=null&&channel.startsWith("aiquyou")){
    	shareChn="aiquyou";
    }else if("chiba".equals(channel)){
    	shareChn="chiba";
    }
    %>
    
    <script type="text/javascript">
		var HLMY_GAMEINFO = '${userinfoJson}';
		var HLMY_CHNINFO = {
			enable:false,
			chn:'<%="tashequ".equals(channel)?channel:""%>',
			shareChn:'<%=shareChn%>',
			domain: '<%=domain%>',
			payVersion: <%=payVersion%>,
			wyOpenAppId:<%=openAppId%>			
		};
		
		var HLMY_ADS_SETTINGS = {
				userAdsPlayable:'${userAdPlayable}'
		}
		<%if("kunpeng".equals(channel) || ("tashequ712").equals(channel)){%>
			//进入游戏后提示用户收藏
			HLMY_CHNINFO.enable = true;
		<%}%>
		
		<%
		//获取游戏对应的aid
		Long aid = (Long)request.getAttribute("aid"); 
		if(aid==null) aid = -1l;
		
		//判断是否是试玩用户
		Boolean isTrialUser = (Boolean)request.getAttribute("isTrialUser");
		%>
		
		/*配置信息*/
		var HLMY_CONFIG = {
			USER_INFO:{
				gid: "${userinfo_gid}",
				nickname:"${userinfo_nickname}",
				headUrl:"${userinfo_headUrl}",
				userType:"${userinfo_userType}",
				sex:"${userinfo_sex}",
				userStatus:"${userinfo_userStatus}"
			},
			
			GAME_INFO:{
				panelEnable: ${gameinfo_panelEnable},
				iconUrl:"${wxApp.iconUrl}",
				appKey:"${wxApp.appKey}",
				aid:${aid},
				name:"${wxApp.name}"
			},
			
			PAY_INFO:{
				payVersion: <%=payVersion%>,
				domain: '<%=domain%>'
			},
			
			CHN_INFO:{
				enable:false,
				chn:'<%=channel%>',
				shareChn:'<%=shareChn%>'
			},
			
			SHARE_INFO:{
				enable: true,
				wyOpenAppId:<%=openAppId%>
			},
			SUBSCRIBE_INFO:{
				enable:true
			},
			
			<%
			//如果是试玩用户
			if(isTrialUser!=null&&isTrialUser){
			
			//构造添加到桌面的配置
			boolean addDesktop = false;
			String addDesktopUrl = ConstHlmy.MOBILE_INDEX_URL+"/play/spread/desktopShortcut?aid="+aid+"&appKey="+wxApp.getAppKey()+"&chn="+channel;
			long addDesktopTimeDelay = 60*1000;//默认半分钟，单位毫秒
			//如果是闪信渠道，默认开启［添加到桌面］功能
			if("caiyinad".equals(channel)){
				addDesktop = true;
			}
			%>
			
			ADD_TO_DESKTOP:{
				enable:<%=addDesktop%>,
				time: <%=addDesktopTimeDelay%>,//ms
				url:"<%=addDesktopUrl%>"
			}
			<%}%>
		};
		
		if(typeof HLMY_GAMEINFO ==='string' ){
			HLMY_GAMEINFO = JSON.parse(HLMY_GAMEINFO);
			if(HLMY_CONFIG.ADD_TO_DESKTOP && HLMY_CONFIG.ADD_TO_DESKTOP.enable && HLMY_GAMEINFO.trialEnable){
				HLMY_GAMEINFO.trialEnable = false;
			}
		}
		
	</script>
</head>
<body>
	<div style="display: none;">
		<script type="text/javascript">var cnzz_protocol = (("https:" == document.location.protocol) ? " https://" : " http://");document.write(unescape("%3Cspan id='cnzz_stat_icon_1260623473'%3E%3C/span%3E%3Cscript src='" + cnzz_protocol + "s11.cnzz.com/z_stat.php%3Fid%3D1260623473' type='text/javascript'%3E%3C/script%3E"));</script>
	</div>
	<div class='loading-div' id='loading-div' style="position: absolute;z-index: 1000;top: 0;left: 0;">
		<%
			if (StringUtils.isNotBlank(channelLogoImgUrl)) {//特定处理
		%>
			<div class="inner-loading">
		        <div class="loading-wrapper">
		        	<%if("yizi".equals(channel)){//椅子网下方需要增加二维码图片%>
		        	<style type="text/css" media="screen">
		        		.loading-logo{
		        		padding-top: 0;
		        		}
		        	</style>
		        	<div style="margin-bottom: 40px;">
		        		<img src="http://images.1758.com/partner/yizi/yizi-ewm.jpg" style="width:90px">
		        	</div>
		        	<%}%> 
		        	
		        	
		        	<div class="loading-des">
			        	<%if("baiyuehudong".equals(channel)){//百岳互动时，不需要下方的logo%> 
			        	<%}else if("feichangdanao".equals(channel)){//非常大脑的logo替换为文字%> 
			        		<div style="color: #3c3c3c;font-size: 12px;">非常大脑</div>
			        	<%}else if("zwhy".equals(channel)){//中文互娱的logo替换为文字%> 
				        	<div style="color: #3c3c3c;font-size: 12px;">互娱盒子</div>
				        <%}else if(channel!=null&&channel.startsWith("aiquyou")){//爱趣游代理的logo替换为文字%> 
			        		<div style="color: #3c3c3c;font-size: 12px;">爱玩家  爱趣游</div>
			        	<%}else{//默认为1758的logo%> 
			        		<img src="http://images.1758.com/festival/gs9.png"/>
			        	<%} %>
		        	</div>
		        	
		        	
		        	
		        </div>
		        <div class="loading-logo">
		        	<img src="<%=channelLogoImgUrl%>" />
		        	<div class="loading-en">
		        		<img src="http://images.1758.com/festival/gs10.gif" />
		        		<p>游戏正在火速加载中</p>
		        	</div>
		        </div>
			</div>
		<%}else{%>
			<div class='inner-loading'>
				<!-- <img src='http://images.1758.com/article/image/2015/07/24/27251437716095239.png'> -->
				<img src='http://images.1758.com/game/m/game_logo_1.png'>
				<p class="p-loading">游戏载入中...</p>
				<br />
				<p class="text-loading">1758微游戏·即点即玩</p>
			</div>
		<%} %>
		
	</div>
<!--
	<div style="height: 100%;width: 100%; position: absolute;top: 0;">
	<div style="height: 100%;width: 100%; ">
-->
	<div style="height: 100%;width: 100%;">
		<iframe id='gameframe' style="display:block" src="${cpGameUrl}"  class="iframe" scrolling="no" height=100% width=100% name="cpframe" frameborder='0'></iframe>
	</div>
	
	<div id="button" class="modal-flag">
		<img src='http://images.1758.com/ranking/fubiao.png'>
		<span class="point">●</span>
	</div>
	<div id="moban" class="moban">
		<div id="modalframe" class="modalframe"></div>
		<div id="modal-left" class="modal-left">
			<img src="http://wx.1758.com/game/h5/images/modalleft.png">
		</div>
	</div>
	
	<div id="ewmCode" class="ewm-contain gone"><div class="ewm-wripper"><img src="http://images.1758.com/image/20161019/open_1_e07bcd50b8ea140797cf9f776daa1274.png"></div></div>
	
	<div id="followEwmCode" class="ewm-contain gone"><div class="ewm-wripper"><img src="http://images.1758.com/image/20161019/open_1_e07bcd50b8ea140797cf9f776daa1274.png"></div></div>
	
	<section id="deskContainer" class="hui-modal-container gone">
        <div class="innner-hui-container">
            <div id="huiWrapper" class="hui-wrapper">
                <div class="hui-img-wrapper">
                    <div class="hui-inlineblock">
                        <img class="hui-hb-2" src="http://images.1758.com/image/20161013/open_104818_caf32c4548af21f70c5ab768a138a1ed.png">
                        <img class="hui-hb-3" src="http://images.1758.com/image/20161013/open_104818_eadc2c9313187dfbcb4a2ddb449a1623.png">
                        <img id="huiGameLogo" class="hui-game-logo" src="http://images.1758.com/article/image/2016/06/28/52431467099755147.png">
                    </div>
                    <div class="hui-inlineblock">
                        <img class="hui-hb-1" src="http://images.1758.com/image/20161013/open_104818_d58375c72bd08441fb9b3248622f1487.png">
                    </div>
                </div>
                <div class="hui-res-desc">
                    添加游戏至桌面,即可得百元礼包
                </div>
                <div id='addToDesktop' class="hui-mark-btn">
                    添加到桌面
                </div>
            </div>
        </div>
    </section>
    <section id="hbiBackInfo" class="hbi-backinfo gone">
        <div id="hbiExitModal" class="hbi-back-modal">
            <div id="hbiExitDialog" class="hbi-exit-dialog">
                <div class="hbi-title">
                	<span>这些游戏更好玩！</span>
                	<img id="hbiCancelButton" class="hbi-cancel-button" src="http://h5.longdichuanshuo.com/sdk/img/cancel.png">
            	</div>
                <div id="hbiGameContainer" class="hbi-flex-wrapper hbi-back-container">
                </div>
                <div class="hbi-footer">
                    <button id="hbiFollow" class="hbi-exit-button">收藏我们</button>
                    <button id="hbiExit" class="hbi-exit-button">仍要离开</button>
                    <div class="hbi-remind-back-confirm">
                        <input type="checkbox" id="avoidBackConfirm">
                        <label for="avoidBackConfirm">今日不再提示</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="hbi-back-modal gone" id="hbiEWMWrapper">
            <div class="hbi-ewm-wrapper"><img src="http://images.1758.com/image/20161019/open_1_da18a2e3aa2aebc28210f6476b4a88a1.png"></div>
        </div>
    </section>
	
	
	<%
	//关注提示
	if("tashequ712".equals(channel)||"kunpeng".equals(channel)){
	%>
	<section id="taSheQu" class="ta-tip gone">
		<div class="ta-tip-wrapper">
			<img src="<%=channelTipImgUrl%>">
			<div class="ta-info">
				<div id="IKnow" class="ta-cover">
					<img id="selectImg" src="http://images.1758.com/partner/ta/noselect.png">
					我知道了下次不再提示
				</div>
				<div class="back-game-btn">
					<span id="taBackBtn">
						确定
					</span>
				</div>
			</div>
		</div>
	</section>
	<%}%>

	<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
	
	<!-- 线上环境压缩js -->
	<script type="text/javascript" src="/spf/dist/scripts/require-2.1.20.min.js" data-main="/spf/dist/scripts/game_5_0_3.js"></script>
	
	<script>
	
		function getQueryString(name){
	        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
	        var r = window.location.search.substr(1).match(reg);
	        return r != null ? unescape(r[2]) : '';
        }
        function handleInit(){
            var appKey = getQueryString('appKey');
            console.log(appKey);
            if(appKey == '808b7fde20cf866e18b4a95eac35f93f'){
                    document.getElementById('gameframe').setAttribute('scrolling','yes');
            }
        }
        handleInit();
	
	
		setTimeout(hide1758Mask, 3000);
		
		function hide1758Mask(){
			document.getElementById("loading-div").style.display="none";
			document.getElementById("gameframe").style.display="block";
		}
	</script>	
	
	<jsp:include page="./favorite/tipJs.jsp"></jsp:include>
</body>
</html>
