<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ page import="com.bruce.geekway.utils.XSSDefenseUtils" %>
<%@ page import="com.bruce.geekway.model.*" %>
<%@page import="com.bruce.geekway.app.model.*"%>
<%@page import="com.bruce.geekway.partner.model.*"%>
<%@ page import="com.bruce.geekway.constants.*" %>
<%@ page import="com.bruce.hlmy.rpcdef.message.model.*" %>
<%@ page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="org.apache.commons.lang3.time.DateUtils"%>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>

<%
String channel = (String)session.getAttribute("chn");
channel = XSSDefenseUtils.htmlEncode(channel);
channel = channel==null?"":channel;

SpChannel spChannel = (SpChannel)request.getAttribute("spChannel"); 
WxApp wxApp = (WxApp)request.getAttribute("wxApp");

String title = "1758微游戏";

if(wxApp!=null&&StringUtils.isNotBlank(wxApp.getName())){
	title = wxApp.getName()+"-1758.com";//默认显示的标题
	
	if(spChannel!=null && StringUtils.isNotBlank(spChannel.getTitleGamestart())){
		title = wxApp.getName() + spChannel.getTitleGamestart();//配置了渠道显示名称的情况下
	}
	//其他的特殊处理
	if(channel!=null&&channel.startsWith("mochuangdl")){//［魔窗］代理渠道的特别处理
		title = wxApp.getName()+"-魔窗";
	}else if(channel!=null&&channel.startsWith("aiquyou")){//［爱趣游］代理渠道的特别处理
		title = wxApp.getName()+"-爱趣游"; 
	}
}

String channelLogoImgUrl = null;
if(spChannel!=null){
	channelLogoImgUrl = spChannel.getLogoUrl();
}

if(channel!=null&&channel.startsWith("mochuangdl")){//［魔窗］代理渠道的特别处理
	channelLogoImgUrl = "http://images.1758.com/image/20161216/open_1_7564ccae4f9b24fbbad7e97308cc374d.png";
}else if(channel!=null&&channel.startsWith("aiquyou")){//［爱趣游］代理渠道的特别处理
	channelLogoImgUrl = "http://images.1758.com/image/20160901/open_1_e7d76a05330b4a2e029982a5313b8d3b.png";
}

String chnName = null;
if(spChannel!=null){
	chnName = spChannel.getName();
}

String shareChn = spChannel!=null && spChannel.getShareChn()!=null?spChannel.getShareChn():"";
if(channel!=null&&channel.startsWith("aiquyou")){//针对代理［爱趣游］的特殊处理
	shareChn="aiquyou";
}
%>

<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8" />
	<title>555<%=title%></title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="format-detection" content="telephone=no">    
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="mobile-web-app-capable" content="yes"/>
    <meta name="full-screen" content="true"/>
    <meta name="screen-orientation" content="portrait"/>
    <meta name="x5-fullscreen" content="true"/>
    <meta name="full-screen" content="yes"/>
    <meta name="360-fullscreen" content="true"/>
    <meta http-equiv="pragma" content="no-cache"/>
    <meta http-equiv="cache-control" content="no-cache"/>
    <meta http-equiv="cache" content="no-cache"/>
    <meta http-equiv="expires" content="0"/>
    <link rel="shortcut icon "  href="http://images.1758.com/images/48.ico">
    <link rel="stylesheet" href="/static/spf/dist/css/game_1_0_6.css" type="text/css" />
    <link rel="stylesheet" href="/static/spf/dist/css/hweui_0_1-65bd4a8153.css" type="text/css" />
    <%if(StringUtils.isNotBlank(channelLogoImgUrl)){ %>
    <link rel="stylesheet" type="text/css" href="/static/spf/dist/css/start_0_3.css"/>
    <%} %>
    <script src="/static/spf/dist/js/metainfo_1_0.min.js"></script>
    
    <%
    String domain = ConstConfig.MOBILE_DOMAIN;
    int payVersion = 3;//支付版本，默认为3
    boolean forceEnable = false;
    if("tclyy".equals(channel)
    		||"tashequ".equals(channel)
    		||"tashequxnw".equals(channel)
    		||"tashequdk".equals(channel)
    		||"qipaiapp".equals(channel)
    		||(channel!=null&&channel.startsWith("mochuangdl"))
    		||"sinagame".equals(channel)
    		||"yuewenqq".equals(channel)
    		||"yuewenqd".equals(channel)
    		||"yuewenqd".equals(channel)
    		||"fkyx".equals(channel)
    		||"qhyx".equals(channel)){
    	//部分小米手机在ifrmae下存在cookie丢失的问题，只能全部改为支付2.0版本
    	payVersion=2;
    	forceEnable=true;
    }
    %>
    
    <%
    //临时统计
    if("tashequ".equals(channel)||"tashequxnw".equals(channel)){
    %>
    <script>
	var _hmt = _hmt || [];
	(function() {
	  var hm = document.createElement("script");
	  hm.src = "//hm.baidu.com/hm.js?3f628f9aa9939578047aa1e5766bbc2d";
	  var s = document.getElementsByTagName("script")[0]; 
	  s.parentNode.insertBefore(hm, s);
	})();
	</script>
	<%}%>
    
    <%
    int openAppId = 107686;//默认分享的公众号id
    if(wxApp.getId()==107858){//特定的游戏指定分享的公众号，避免被封
    	openAppId = 107689;
    }
    %>
    
    <script type="text/javascript">
		var HLMY_GAMEINFO = '${userinfoJson}';
		var HLMY_CHNINFO = {
			enable:false,
			chn:'<%="tashequ".equals(channel)?channel:""%>',
			shareChn:'<%=shareChn%>',
			payVersion: <%=payVersion%>,
			domain: '<%=domain%>',
			wyOpenAppId:<%=openAppId%>
		};
		
		<%if(false){//"kunpeng".equals(channel) || ("tashequ712").equals(channel)){%>
			//进入游戏后提示用户收藏
			HLMY_CHNINFO.enable = true;
		<%}%>
		<%
		//获取游戏对应的aid
		Integer aid = (Integer)request.getAttribute("aid"); 
		if(aid==null) aid = -1;
		
		//判断是否是试玩用户
		Boolean isTrialUser = (Boolean)request.getAttribute("isTrialUser");
		%>
		
		/*配置信息*/
		var HLMY_CONFIG = {
			USER_INFO:{
				gid: "${userinfo_gid}",
				nickname:"${userinfo_nickname}",
				headUrl:"${userinfo_headUrl}",
				userType:"${userinfo_userType}",
				sex:"${userinfo_sex}", 
				userStatus:"${userinfo_userStatus}"
			},
				
			GAME_INFO:{
				panelEnable: ${gameinfo_panelEnable},
				iconUrl:"${wxApp.iconUrl}",
				appKey:"${wxApp.appKey}",
				aid:${aid},
				name:"${wxApp.name}",
				//trialEnable:${trialEnable},
				//trialTime:${trialTime}
			},
			
			PAY_INFO:{
				version: "${wxApp.versionPay}",
				forceEnable: <%=forceEnable%>,
				forceVersion: <%=payVersion%>
			},
			
			CHN_INFO:{
				enable:false,
				chn:'<%=channel%>'
			},
			
			<%
			boolean promptEnable = false;
			int promptDelay = 360;
			if(wxApp!=null && wxApp.getShareAbility()!=null && wxApp.getShareAbility()>0 && StringUtils.isNotBlank(wxApp.getSharePromptImgUrl())){
				promptEnable = true;
				if(wxApp.getSharePromoteDelay()!=null && wxApp.getSharePromoteDelay()>0){
					promptDelay = wxApp.getSharePromoteDelay();
				}
			}
			%>
			SHARE_INFO:{
				enable: true,
				version: '${wxApp.versionLogin}',
				shareChn:'<%=shareChn%>',
				wyOpenAppId:<%=openAppId%>,
				shareAbility:${wxApp.shareAbility},
				prompt:<%=promptEnable%>,
				promptEnable:<%=promptEnable%>,
				promptDelay:<%=promptDelay%>,
				hideMenu:['']
/* 				hideMenu:['menuItem:share:timeline'] */
			},
			SUBSCRIBE_INFO:{
				enable:true,
				//enable:${promptSubscribeEnable},
				//promptDelay:${promptSubscribeDelay}
			},
			
			<%
			//如果是试玩用户
			if(isTrialUser!=null&&isTrialUser){
			
			//构造添加到桌面的配置
			boolean addDesktop = false;
			String addDesktopUrl = ConstConfig.MOBILE_DOMAIN_INDEX +"/play/spread/desktopShortcut?aid="+aid+"&appKey="+wxApp.getAppKey()+"&chn="+channel;
			long addDesktopTimeDelay = 60*1000;//默认半分钟，单位毫秒
			//如果是闪信渠道，默认开启［添加到桌面］功能
			if("caiyinad".equals(channel)){
				addDesktop = true;
			}
			%>
			
			ADD_TO_DESKTOP:{
				enable:<%=addDesktop%>,
				time: <%=addDesktopTimeDelay%>,//ms
				url:"<%=addDesktopUrl%>" 
			}
			<%}%>
		};
		
		if(typeof HLMY_GAMEINFO ==='string' ){
			HLMY_GAMEINFO = JSON.parse(HLMY_GAMEINFO);
			if(HLMY_CONFIG.ADD_TO_DESKTOP && HLMY_CONFIG.ADD_TO_DESKTOP.enable && HLMY_GAMEINFO.trialEnable){
				HLMY_GAMEINFO.trialEnable = false;
			}
		}
		
	</script>
</head>
<body>
	
	<div class='loading-div' id='loading-div' style="position: absolute;z-index: 1000;top: 0;left: 0;">
		<%
		if(StringUtils.isNotBlank(channelLogoImgUrl)){//特定处理
			%>
			<div class="inner-loading">
		        <div class="loading-wrapper">
	        		 <div class="loading-des">
			        	<%if("baiyuehudong".equals(channel)){//百岳互动时，不需要下方的logo%> 
			        	<%}else if("feichangdanao".equals(channel)){//非常大脑的logo替换为文字%> 
			        		<div style="color: #3c3c3c;font-size: 12px;">非常大脑</div>
			        	<%}else if("zwhy".equals(channel)){//中文互娱的logo替换为文字%> 
			        		<div style="color: #3c3c3c;font-size: 12px;">互娱盒子</div>
			        	<%}else if(channel!=null&&channel.startsWith("aiquyou")){//爱趣游代理的logo替换为文字%> 
		        			<div style="color: #3c3c3c;font-size: 12px;">爱玩家 爱趣游</div>
		        		<%}else{//默认为1758的logo%>
			        		<img src="http://images.1758.com/festival/gs9.png"/>
			        	<%} %>
		        	</div>
		        </div>
		        <div class="loading-logo">
		        	<img src="<%=channelLogoImgUrl%>" />
		        	<div class="loading-en">
		        		<img src="http://images.1758.com/festival/gs10.gif" />
		        		<p>游戏正在火速加载中</p>
		        	</div>
		        </div>
			</div>
		<%}else{%>
			<div class='inner-loading'>
				<img src='//images.1758.com/game/m/game_logo_1.png'>
				<p class="p-loading">游戏载入中...</p>
				<br />
				<p class="text-loading">1758微游戏·即点即玩</p>
			</div>
		<%} %>
		
	</div>

	<div style="height: 100%;width: 100%;">
		<iframe id='gameframe' style="display:block" src="${cpGameUrl}"  class="iframe" scrolling="no" height=100% width=100% name="cpframe" frameborder='0'></iframe>
	</div>
	
	<div id="button" class="modal-flag">
		<img src='http://images.1758.com/image/20161115/open_1_469b7a6ea6374af1fe8c1b799f32a832.png'>
		<span class="point">●</span>
	</div>
	<div id="moban" class="moban">
		<div id="modalframe" class="modalframe"></div>
		<div id="modal-left" class="modal-left">
			<img src="http://wx.1758.com/game/h5/images/modalleft.png">
		</div>
	</div>
    <div id="shareTipInfo" class="share-square gone">
	    <div class="share-box">
	        <img src="http://images.1758.com/image/20161124/open_1_50b53ad143c7d193e61bb9733ceabe5c.png">
	        <span><img class="z" src="http://images.1758.com/image/20161124/open_1_8a3082c3ea9b04e38a8c76d301f5518a.png" alt="">发送微信群或朋友</span>
	    </div>
	</div>

<%
if(!StringUtils.contains(channel, "jinritoutiao")){//非今日头条的情况下，可以展示关注二维码
%>
    <div id="ewmCode" class="ewm-contain gone"><div class="ewm-wripper"><img src="http://images.1758.com/image/20161019/open_1_e07bcd50b8ea140797cf9f776daa1274.png"></div></div>
	
	<div id="followEwmCode" class="ewm-contain gone"><div class="ewm-wripper"><img src="http://images.1758.com/image/20161019/open_1_e07bcd50b8ea140797cf9f776daa1274.png"></div></div>
<%}%>
	
	<section id="deskContainer" class="hui-modal-container gone">
        <div class="innner-hui-container">
            <div id="huiWrapper" class="hui-wrapper">
                <div class="hui-img-wrapper">
                    <div class="hui-inlineblock">
                        <img class="hui-hb-2" src="http://images.1758.com/image/20161013/open_104818_caf32c4548af21f70c5ab768a138a1ed.png">
                        <img class="hui-hb-3" src="http://images.1758.com/image/20161013/open_104818_eadc2c9313187dfbcb4a2ddb449a1623.png">
                        <img id="huiGameLogo" class="hui-game-logo" src="http://images.1758.com/article/image/2016/06/28/52431467099755147.png">
                    </div>
                    <div class="hui-inlineblock">
                        <img class="hui-hb-1" src="http://images.1758.com/image/20161013/open_104818_d58375c72bd08441fb9b3248622f1487.png">
                    </div>
                </div>
                <div class="hui-res-desc">
                    添加游戏至桌面,即可得百元礼包
                </div>
                <div id='addToDesktop' class="hui-mark-btn">
                    添加到桌面
                </div>
            </div>
        </div>
    </section>
    <section id="hbiBackInfo" class="hbi-backinfo gone">
        <div id="hbiExitModal" class="hbi-back-modal">
            <div id="hbiExitDialog" class="hbi-exit-dialog">
                <div class="hbi-title">
                	<span>这些游戏更好玩！</span>
                	<img id="hbiCancelButton" class="hbi-cancel-button" src="http://h5.longdichuanshuo.com/sdk/img/cancel.png">
            	</div>
                <div id="hbiGameContainer" class="hbi-flex-wrapper hbi-back-container">
                </div>
                <div class="hbi-footer">
                    <button id="hbiFollow" class="hbi-exit-button">收藏我们</button>
                    <button id="hbiExit" class="hbi-exit-button">仍要离开</button>
                    <div class="hbi-remind-back-confirm">
                        <input type="checkbox" id="avoidBackConfirm">
                        <label for="avoidBackConfirm">今日不再提示</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="hbi-back-modal gone" id="hbiEWMWrapper">
            <div class="hbi-ewm-wrapper"><img src="http://images.1758.com/image/20161019/open_1_da18a2e3aa2aebc28210f6476b4a88a1.png"></div>
        </div>
    </section>
	<section class="onkey-login-container gone" id="loginContainer">
	    <div class="onkey-login-wrapper">
	    	<div class="onkey-head">
	    		<div class="onkey-close" id="oneKeyClose">
	        		<img src="http://images.1758.com/image/20160831/open_1_6f57ffea4e579ea726c90e8b53cc1ebb.png">
	        	</div>
				<div class="onkey-logo">
					<img src="http://wx.1758.com/game/h5/images/head-icon.png" alt="">	
				</div>
				游戏登录
			</div>
	        <div class="onkey-title">
	           为保护您的游戏数据安全，请登录后继续游戏
	        </div>
	        <div class="onkey-tip">
	        	<img src="http://images.1758.com/image/20170215/open_1_6c077b5f6e949bab2d2c46543a8b847d.png" alt="">
	        </div>
	        <div class="logo-wrapper">
	            <div id="qqLogin" class="logo-item">
	                <a href="//wx.1758.com/play/login/qqLogin?appKey=${wxApp.appKey}&chn=${chn}">
	                    <img src="http://images.1758.com/images/login_QQ1.png">
	                </a >
	            </div>
	            
	            <%
	            String weixinLoginClass = "";
	            String mobileLoginClass = "gone";
            	if("jinritoutiao".equals(channel)){
            		weixinLoginClass = "gone";
            		mobileLoginClass = "";
            	}
				%>
	            <div id="wxLogin" class="logo-item <%=weixinLoginClass%>">
	                <a href="//wx.1758.com/play/login/weixinLogin?appKey=${wxApp.appKey}&chn=${chn}">
	                    <img src="http://images.1758.com/images/login_WX1.png">
	                </a >
	            </div>
	            <div id="mobileLogin" class="logo-item <%=mobileLoginClass%>">
	                <a>
	                    <img src="http://images.1758.com/image/20170301/open_1_e5067228ad4ae21aba3989191d25d27a.png">
	                </a >
	            </div>
	            <div id="weiboLogin" class="logo-item">
	                <a href="//wx.1758.com/play/login/weiboLogin?appKey=${wxApp.appKey}&chn=${chn}">
	                    <img src="http://images.1758.com/images/login_XL1.png">
	                </a >
	            </div>
	        </div>
	    </div>
	</section>

	<!-- 手机号登录 -->
	<section id="huiMobileLogin" class="huimobile-container gone"><div class="hlui-wrapper"><div class="hlui-head"><div class="hlui-logo"><img src="http://wx.1758.com/game/h5/images/head-icon.png" alt=""></div>
			手机号登录
		</div> <div class="hlui-main"><div class="hlui-main-tip"><p>登录后保存游戏进度,账号更安全</p> <p>并有概率获得随机现金红包</p></div> <div class="hlui-input-container"><div class="hlui-flex"><div class="hlui-cell"><input id="mobileNum" type="number" data-type="phone" pattern="[0-9]{4-12}" placeholder="手机号" class="hmobile"></div></div> <div class="hlui-flex"><div class="hlui-cell"><input id="mobileImg" type="number" data-type="identifyone" pattern="[0-9]{4}" placeholder="请输入图形校验码" class="hmobile"></div> <div class="hlui-w"><img id="verifyCodeImg" src="http://wx.1758.com/play/getVerifyCode" width="100%" style="cursor: pointer;"></div></div> <div class="hlui-flex"><div class="hlui-cell"><input id="mobileMes" type="number" data-type="identifyone" pattern="[0-9]{4}" placeholder="请输入短信校验码" class="hmobile"></div> <div class="hlui-w"><div id="verifyCodeBtn" class="hlui-verf">
							获取验证码
						</div></div></div></div> <div class="hlui-btns"><div class="hlui-btn-tip">
					未注册用户手机验证后自动登录
				</div> <div id="hmobileLogin" class="hlui-btn">
					登&nbsp;&nbsp;录
				</div></div> <div class="hlui-quick-login"><div class="hlui-quick-tip"><img src="http://images.1758.com/image/20170215/open_1_6c077b5f6e949bab2d2c46543a8b847d.png" alt="快速登录"></div> <div class="hlui-flex hlui-quick-wrapper"><div class="hlui-cell" id="mobileQQLogin"><a href="//wx.1758.com/play/login/qqLogin?appKey=${wxApp.appKey}&amp;chn=${chn}"><img src="http://images.1758.com/images/login_QQ1.png" alt="QQ登录"></a></div> <div class="hlui-cell gone" id="mobileWXLogin"><a href="//wx.1758.com/play/login/weixinLogin?appKey=${wxApp.appKey}&amp;chn=${chn}"><img src="http://images.1758.com/images/login_WX1.png" alt="微信登录"></a></div> <div class="hlui-cell" id="mobileWeiBoLogin"><a href="//wx.1758.com/play/login/weiboLogin?appKey=${wxApp.appKey}&amp;chn=${chn}"><img src="http://images.1758.com/images/login_XL1.png" alt="微博登录"></a></div></div></div></div></div> <div id="hmobileModal" class="hlui-modal gone"><div class="hlui-inner-modal">
			手机号码不对
		</div></div></section>
    <section id="simulateContainer" class="simulate-container gone">
		<div class="simulate-wrapper">
			<img id="simulateShareImg" src="">
			<div id="simulateShareClose" class="simulate-close"></div>
			<img id="simulateSelected" class="simulate-selected gone" src="http://images.1758.com/image/20170116/open_1_a14a4198d13f239a65619470c89bbdf4.png">
			<div id="simulateNoTip" class="simulate-notip"></div>
			<div id="simulateShareTip" class="simulate-sharetip"></div>
		</div>
	</section>
	
	<script src="//res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
	
	<!-- 线上环境压缩js -->
	<script type="text/javascript" src="/static/spf/dist/require-2.1.20.min.js" data-main="/static/spf/dist/js/game_1_0_11.js"></script>
	<!-- <script type="text/javascript" src="/static/spf/dist/require-2.1.20.min.js" data-main="/static/spf/mweb/js/view/app_v5.js"></script> -->
	
	<script>
		setTimeout(hide1758Mask, 3000);
		
		function hide1758Mask(){
			document.getElementById("loading-div").style.display="none";
			document.getElementById("gameframe").style.display="block";
		}
	</script>
	
	<div style="display: none;">
		<script type="text/javascript">var cnzz_protocol = (("https:" == document.location.protocol) ? " https://" : " http://");document.write(unescape("%3Cspan id='cnzz_stat_icon_1260623473'%3E%3C/span%3E%3Cscript src='" + cnzz_protocol + "s11.cnzz.com/z_stat.php%3Fid%3D1260623473' type='text/javascript'%3E%3C/script%3E"));</script>
	</div>
	
	<jsp:include page="./favorite/tipJs.jsp"></jsp:include>
	
</body>
</html>
