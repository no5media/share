<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ page import="com.bruce.geekway.model.*" %>
<%@ page import="org.apache.commons.lang.math.*" %>


<%
int v1758 = NumberUtils.toInt(request.getParameter("v1758"), 1);
%>


<!DOCTYPE html>
<html>
<head>

<%if(v1758!=3){//支付3.0版本存活在iframe中，支付成功后无需跳出%>
<script>
if(top!=this){
	top.location.href = location.href;
}
</script>
<%}%>

<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<meta name="viewport" content="width=device-width initial-scale=1.0 maximum-scale=1.0 user-scalable=yes" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.js"></script>
<title>支付成功</title>

<style>

body{
	background: #f0eff5;
	padding: 0px;
	margin: 0px;
	font-size: 12px;
}

div.row{
	margin: 0px 0px 16px;
	background: #fff;
}

div.price-container{
	margin-bottom: 16px;
	padding: 5px;
	background:#fff;
	border-bottom: 1px solid #dddddd; 
}

.no-bottom{
	margin-bottom: 0px;
}

img.icon{
	width:26px;
	height:26px;
	vertical-align:middle;
	margin: 0px 6px 0px 10px;
}

.selector-group{
	margin: 4px 0px 2px; 
}

.btn{
	width:40px;
	margin: 0px 6px 6px 0px;
	padding: 8px 16px; 
	text-decoration: none;
	text-align: center;
	display:inline-block;
}

.btn-block{
	color: #fff;
	padding: 12px 20px;
	margin:20px 5px;
	text-decoration: none;
	display:block;
	text-align: center;
	font-size:14px; 
}

.btn-active{ background-color: #08ba05;color: #fff;border: 1px solid #ffffff;}
.btn-inactive{ background-color: #fff; color:#000; border: 1px solid #dddddd;border-radius: 5px;}

.btn-green{ background-color: #08ba05;color: #fff;border-radius: 5px;}
.btn-orange{ background-color: orange;color: #fff;border-radius: 5px;} 
.btn-white{ background-color: #fff;color: #000;border-radius: 5px;border: 1px solid #dddddd;}
.btn-gray{ background-color: #fff; color:#000;border-radius: 5px;}

.buy-container{margin:40px 0px}

#loadingText{ 
background:#e7e6ec;
padding: 3px 10px;
border-radius: 3px;
color:#8d8d8d;
}  

.loading-container{ 
margin:10px 0px; 
} 

.service{
text-align:center; 
color:#4584f6
}
.copyrights{
margin-top:10px;
text-align:center; 
color:#acacac
}
</style> 



 <style>
.user-level{
            	float: right;
    background-color: red;
    color: #fff;
    font-size: 10px;
    padding: 0 2px;
    border-radius: 3px;
    vertical-align: bottom;
    line-height: 17px;
    margin-top: 5px;
    margin-left: 5px;
    height: 15px;
    display: inline-block;
}
            
.vip-tips{
	color:#9c9c9c;
	font-size: 13px;
	padding: 0px 12px;
	margin-bottom:12px;
}

.vip-tips a{
	color:#18a3e3;
	text-decoration:underline;
}

.vip-tips span{
	color:red;
}

.vip-tips img{
width: 18px;
vertical-align: bottom;
}

.color-blue{
	color:#18a3e3;
}

</style>


</head>
<body> 
	
	
	
<div class="row">
	<div style="border-top: 1px solid #dddddd;border-bottom: 1px solid #dddddd; padding:10px">
		<img class="icon" src="${wxApp.iconUrl}"/>
		<span style="margin-left:9px">${orderInfo.title}</span>
	</div>
</div> 

<div class="price-container">	 
	
	<div>
		<span style="color:#8d8d8d" >${orderInfo.title}</span> 
	</div>
	 
	<div style="padding:0px 10px; text-align:center;"> 
	
		<div class="loading-container" id="payLoadingImg">
			<img src="${pageContext.request.contextPath}/images/loading.gif" width="24px"/>
		</div>
		<div class="loading-container" id="payResult">
			<strong id="payResultText" style="color: #08ba07; font-size:28px;margin-right:3px"></strong>
		</div>
		<div class="loading-container" id="payLoadingText" style="margin-bottom:8px">
			<span id="loadingText"></span>
		</div>
	</div> 
</div> 
<% Double coins=(Double)request.getAttribute("1758coins");
if (null!=coins&&coins>0) {
%>
<p style="text-align:center;margin-top:-5px;">恭喜您获得<%out.print(new java.text.DecimalFormat("0").format(coins));%>个1758游戏币</p>
<%}%>

<%
WyVipDic userVipInfo = (WyVipDic)request.getAttribute("userVipInfo");
%>
<div class="row">	
	<div style="color:#18a3e3; border-bottom: 1px solid #dddddd; padding:10px 5px; line-height:26px">
		1758游戏帐号
		<img class="icon" src="${wyUser.userinfo.head}" style="float:right"/>
		
		 <%
         if(userVipInfo!=null){%>
         	<span class="user-level">${userVipInfo.name}</span>
         <%}%>
		
		<span style="float:right;color:#8d8d8d">${wyUser.userinfo.nickname}</span>
	</div> 
</div>

<%if(userVipInfo!=null){%>
	<%if(userVipInfo.getLevel()!=null&&userVipInfo.getLevel()>=4){%>
	<p class="vip-tips">
	您是1758<span>VIP</span>用户，请加<span>VIP专属客服</span><a id="vipQQ"><img src="http://images.1758.com/game/m/qq.png">2622621758</a>，尊享专属服务。
	</p>
	<%} %>
<%}else{%>
	<p class="vip-tips">
	累计充值20元，即可成为<span>VIP</span>用户，尊享专属服务。
	</p>
<%} %>

<div id="backGame" class="buy-container">
	<div style="padding:0px 10px;">
		<a href="javascript:void(0)" id="backGameBtn" class="btn-block btn-white">返回游戏</a>
	</div>
	
	<!-- <div style="padding:0px 10px;"> 
		<span style="float:right;margin-right:10px">客服QQ:2660866818</span>
	</div>  -->
</div>

<div class="footer">
	<div class="service"><span>客服QQ:2660866818</span></div>
	<div class="copyrights">Copyright©1758微游戏 wx.1758.com</div>
</div>

</body>

<script>
getVipQQKefu();
function getVipQQKefu(){
	var ua = navigator.userAgent.toLocaleLowerCase();
	if(ua.indexOf('micromessenger') > 0 || ua.indexOf('qq/') > 0 || ua.indexOf('mqqbrowser') > 0){
		$('#vipQQ').attr('href','http://wpa.qq.com/msgrd?v=3&uin=2622621758&site=qq&menu=yes');
	}else{
		$(document).on('click','#vipQQ',function(){
			prompt('VIP专属客服QQ','2622621758');
		})
	}
}
$("#payResult").hide();

$("#loadingText").text("支付完成，正在核对订单，请稍侯");
var checkTimes = 0;
checkPayResult();
checkTimes++;
window.setInterval("checkPayResult()", 3000);

function checkPayResult(){
	if(checkTimes<=3){
		checkTimes++;
		var paramData = {'appKey':"${appKey}", 'tradeNo':"${orderInfo.outTradeNo}"};
		$.post('${pageContext.request.contextPath}/orderInfo.json', paramData, function(responseData) {
			var result = responseData.result;
			if(result==1){
				var payStatsus = responseData.data ;
				if(payStatsus==10||payStatsus==20){
					//cp无响应
					if(checkTimes>3){
						$("#payLoadingImg").hide();
						$("#payResult").show();
						$("#loadingText").text("正在发放商品，即将返回游戏");
						$("#payResultText").text("支付成功");
						//$("#backGameBtn").attr("href","${paySuccessUrl}");
						setTimeout("back2Game()",1000);
					}
				}else if(payStatsus==30){//cp响应正确&已处理发货
					$("#payResultText").text("购买成功");	
					$("#loadingText").text("购买已成功，即将返回游戏");
					//$("#backGameBtn").attr("href","${paySuccessUrl}");
					setTimeout("back2Game()",1000);
				}else if(payStatsus==1){//根本未收到微信支付通知
					$("#payResultText").text("未收到支付结果");
					$("#loadingText").text("系统将自动核对支付结果，即将返回游戏");
					//$("#backGameBtn").attr("href","${paySuccessUrl}");
					setTimeout("back2Game()",1000);
				}
			}else{
				$("#payResultText").text("查询订单...");
				$("#loadingText").text("系统将自动核对订单结果，即将返回游戏");
				//$("#backGameBtn").attr("href","${paySuccessUrl}");
				setTimeout("back2Game()",1000);
			}
		});
	}
}

$('#backGameBtn').on('click',function(){
	<%if(v1758!=3){//支付3.0版本存活在iframe中，支付成功后无需跳出%>
		location.href="${paySuccessUrl}";
	<%}else{%>
		//TODO 关闭iframe
		top.postMessage({hlmy:true,type:'pay',value:{fn:'payCancleBack',args:[]}}, '*');
	<%}%>
})
function back2Game(){
	<%if(v1758!=3){//支付3.0版本存活在iframe中，支付成功后无需跳出%>
		//location.href="${paySuccessUrl}";
	<%}else{%>
		//TODO 关闭iframe
		//top.postMessage({hlmy:true,type:'pay',value:{fn:'payCancleBack',args:[]}}, '*');
	<%}%>
}
</script>

<jsp:include page="../inc/wxOptionSettings.jsp"></jsp:include>
</html>
