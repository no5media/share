+ function($) {
	"use strict";

	var defaults;

	var showComment = function(params) {
		params = $.extend({}, defaults, params);

		var commentWrapper = $('<div></div>');

		commentWrapper = commentWrapper.append("<div class='weui_mask'></div>");

		var buttons = params.buttons;

		var buttonsHtml = buttons.map(function(d, i) {
			return '<a href="javascript:;" class="weui_btn_dialog ' + (d.className || "") + '">' + d.text + '</a>';
		}).join("");

		var tpl = '<div class="weui_dialog weui_dialog_comment">' +
			'<div class="weui_dialog_hd"><strong class="weui_dialog_title">' + params.title + '</strong></div>' +
			(params.text ? '<div class="weui_dialog_bd">' + params.text + '</div>' : '') +
			'<div class="weui_dialog_ft weui_dialog_comment_ft">' + buttonsHtml + '</div>' +
			'</div>';
		var dialog = commentWrapper.append($(tpl)).appendTo(document.body);
		
		
		dialog.find(".weui_btn_dialog").each(function(i, e) {
			var el = $(e);
			el.click(function() {
				//先关闭对话框，再调用回调函数
				$.closeComment(dialog);
				if (buttons[i].onClick) {
					var t = getCommentText(dialog)
					buttons[i].onClick(t);
				}
			});
		});

		dialog.show();
		dialog.addClass("weui_dialog_confirm");
		return dialog;
	};
	
	var getCommentText = function(dialog){
		var contentText = $(dialog).find('.weui_dialog_textarea').val();
		contentText = $.trim(contentText);
		return contentText;
	}

	$.closeComment = function(dialog) {
		dialog.remove();
	};
	$.commentBox = function(param, callbackOK, callbackCancel) {
		var defaultConfig = {
			title:'发表评论',
			isPhoto:false,
			placeholder:'大侠留个名吧~',
			successText:'发布',
			cancleText:'取消'
		};
		var textContent;
		if (arguments.length == 1) {
			callbackOk = arguments[0];
		} else {
			if (typeof param === 'object') {
				defaultConfig = $.extend({}, defaultConfig, param);
			} else {
				defaultConfig.title = param;
			}
		}
		if (defaultConfig.isPhoto) {
			textContent = '<textarea class="weui_dialog_textarea" contenteditable="false" placeholder="' + defaultConfig.placeholder + '"></textarea>' +
				'<div class="img-wrapper pure-g">' +
				'<div class="pure-u-1-4 weui_camera">' +
				'<div class="imgitem">' +
				'<img src="http://images.1758.com/daren/upload.png" />' +
				'</div>' +
				'</div>' +
				'</div>';
		} else {
			textContent = '<textarea class="weui_dialog_textarea" contenteditable="false" placeholder="' + defaultConfig.placeholder + '"></textarea>';
		}
		return showComment({
			text:textContent,
			title: defaultConfig.title,
			buttons: [{
				text: defaultConfig.cancleText,
				className: "default",
				onClick: callbackCancel
			}, {
				text: defaultConfig.successText,
				className: "primary",
				onClick: callbackOK
			}]
		});
	}

	defaults = {
		title: "发表评论",
		text: undefined,
		buttonOK: "确定",
		buttonCancel: "取消",
		buttons: [{
			text: "确定",
			className: "primary"
		}]
	};

}($);