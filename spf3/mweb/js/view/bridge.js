/**
 * 初始化浮标，以及浮标的按钮点击等
 */
define(['jquery','common/eventBase'],function($,EventBase){
	var ua = EventBase.userTerminal();
	var module = {
		//
		init:function(){
		}
	};

	/**
	 * 客户端调用方法
	 * @param  {[type]} flag [description]
	 * @return {[type]}      [description]
	 */
	function clientFn(flag){
		console.log(flag);
		switch(flag){
			case 'exit':
				if(ua == 'androidclient'){
					try{
					android_panelAssistant.quit();//点击退出
					}catch(err){}
				}
			break;
			case 'addToDesk':
				if(ua == 'androidclient'){
					try{
					android_panelAssistant.addShortcut();//点击添加到桌面
					}catch(err){}
				}
			break;
			case 'refresh':
				if(ua == 'androidclient'){
					try{
					android_panelAssistant.refresh();//点击刷新
					}catch(err){}
				}
			break;
			case 'share':
				if(ua == 'androidclient'){
					try{
					android_panelAssistant.share();//点击分享
					}catch(err){}
				}
			break;
		}
		$('.moban').fadeOut();
	}
	/**
	 * 绑定事件
	 * @param  {[type]} ){	} [description]
	 * @return {[type]}        [description]
	 */
	$(document).on('click','.client-btnwrapper img',function(){
		var index = $(this).parent('.client-btnwrapper').index();
		switch(index){
			case 0:
				//退出
				clientFn('exit');
			break;
			case 1:
				// 添加到桌面
				clientFn('addToDesk');
			break;
			case 2:
				// 刷新
				clientFn('refresh');
			break;
			case 3:
				// 分享游戏
				clientFn('share');
			break;
		}
	})
	$(document).on('click','.client-btn',function(){
		var bl = $('.clientSwitch').hasClass('gone');
		if(!bl){
			// console.log('切换账号');
			android_panelAssistant.switchAccount()
		}else{
			console.log('切换账号错误');
		}
		$('.moban').fadeOut();
	})
	return module;
});