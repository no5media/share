/**
 * 加载浮层里面的内容
 */
define(['data/detailscontroller', 'common/eventBase', 'libs/echo', 'view/bridge', 'view/chat'], function(DetailsController, EventBase, Echo, ClientBridge, KFChat) {
    window.htj_fbhot = 1;
    var module = {};
    var articleId, //游戏artivleId
        appName, //游戏appName
        iconUrl, //游戏icon
        deferArticle = $.Deferred(), //deferred 对象,等待获取当前游戏的基本信息
        htmlTem = EventBase.loadTemplate('/static/spf/dist/tpl/gameModalTem_1_9.tpl'),
        appkey = EventBase.getParameters('appKey'), //获取appkey
        replyType = '', //replayType  记录回复的类型，回复的时候用到
        replyData, //回复用到的一些数据，
        imgsData = { //评轮的时候添加的图片
            localId: [],
            serverId: []
        };
    var HLMY_MODAL_INFO = {
        tab: [{
            type: 'gift', //类别,礼包
            isLoad: false, //是否加载过
            activeIcon: 'http://images.1758.com/image/20161228/open_1_caca91653aab832fdd9fb60d7110bca1.png',
            icon: 'http://images.1758.com/image/20161228/open_1_6b9c3527e45d9dd7d07a26523a39cf45.png'
        }, {
            type: 'broadcast', //类别,公告
            isLoad: false, //是否加载过
            activeIcon: 'http://images.1758.com/image/20180323/open_104818_8c2de27542b589f00b95705663a61b01.png',
            icon: 'http://images.1758.com/image/20180323/open_104818_c77bab1319e73f580cc08f379c8225bb.png'
        }, {
            type: 'talk', //类别，聊吧
            isLoad: false, //是否加载过
            activeIcon: 'http://images.1758.com/image/20161228/open_1_6c5dcc3dcb69d2b99cacee971ffaabdb.png',
            icon: 'http://images.1758.com/image/20161228/open_1_92e53d38804abb16ae4065c24aa9c88e.png'
        }, {
            type: 'hot', //类别，热门游戏
            isLoad: false, //是否加载过
            activeIcon: 'http://images.1758.com/image/20161228/open_1_719b8d71baf112eeef0ce7cf6ee6685c.png',
            icon: 'http://images.1758.com/image/20161228/open_1_0534d0f397f5b8827b15ce5c3505b222.png'
        }, {
            type: 'chat',
            isLoad: false,
            activeIcon: 'http://images.1758.com/image/20170518/open_1_31dc3455c0eac9c7f960ea543d9686ec.png',
            icon: 'http://images.1758.com/image/20170518/open_1_7a7842b66cd387bc1c0420f1aea47563.png',
        }, {
            type: 'kefu', //类别，客服
            isLoad: false, //是否加载过
            activeIcon: 'http://images.1758.com/image/20161228/open_1_0837f2e2a8a9de4dddb6287402fc1c2a.png', //选中状态icon
            icon: 'http://images.1758.com/image/20161228/open_1_9b83f2de2a72f39adedb89a0dc19e667.png' //非选中icon
        }],
        hot: {
            pageNo: 1, //请求数据的num值
            lock: false, //点击锁，当点击的时候 锁住，等请求返回后再释放锁
        },
        talk: {
            pageNo: 1,
            lock: false
        },
        //最后选中的一个tab值，从0 开始计数，当用户打开浮标的时候，加入跟上一次关闭的时候 时间
        //间隔过长，则会从新请求最后一次选择的tab的数据。
        lastTab: '',
        kfTab: 'info', // info 或者 chat  info代表信息，chat代表聊天窗口
        userInitFlag: false, //标识是否需要初始化获取用户信息
        refreshtime: 1 * 30 * 1000, //刷新数据的间隔时间
        timeName: 'hlmyPanelT', //localstorage 存储的记录时间的名字
        initTab: HLMY_CONFIG.TAB_INFO.initTab, //初始化选择tab
    };
    var vipClubInfo = {
        "weixinNumber": "",
        "qqNumber": "",
        "realname": "",
        "idNumber": "",
        "mobile": "",
        "vipLevel": 0
    }
    module = {
        init: function(arg) {
            if (!HLMY_MODAL_INFO.userInitFlag) {
                //初始化信息
                initInfo();
            } else {
                //判断localstorage刷新时间
                var b = detectRefretime();
                //假如初始化之后，则进行tab选择
                this.selectTab(b);
            }
        },
        selectTab: function(b) {
            if (b) {
                //需要刷新
                tabClick(HLMY_MODAL_INFO.lastTab);
                //从新记录刷新的时间
                setStorageTime();
            }
        },

        setInitTab: function(index) {
            HLMY_MODAL_INFO.initTab = index;
        },
        // 指定客服tab中使用哪一个页面
        // 该函数功能主要在vip客服信息提示弹框的点击回复按钮的时候用到
        setKfTab: function(flag) {
            HLMY_MODAL_INFO.kfTab = flag;
        },
        /**
         * 自由指定某个tab
         * @return {[type]} [description]
         */
        freeSelectTab: function(index) {
            if (index < HLMY_MODAL_INFO.tab.length && index > -1) {
                if (!HLMY_MODAL_INFO.userInitFlag) {
                    // 如果没有初始化
                    HLMY_MODAL_INFO.initTab = index;
                    //初始化信息
                    initInfo();
                } else {
                    // 已经初始化了
                    HLMY_MODAL_INFO.lastTab = index;
                    tabClick(HLMY_MODAL_INFO.lastTab);
                    //从新记录刷新的时间
                    setStorageTime();
                }
            }
        }
    };

    Echo.init();
    // 获取用户俱乐部信息
    getUserClubInfo();
    // 获取用户vip信息
    getUserVIPInfo();
    //初始化内容
    function initInfo() {
        setClientHead();
        //通过appKey获取articleId
        getArticleId(appkey);
        //等待异步
        $.when(deferArticle).done(function() {
            tabClick(HLMY_MODAL_INFO.initTab);
            //设置初始化时间
            setStorageTime();
        }).fail(function() {});
        setUserInfo();
    }

    function getUrl() {
        var url = document.location.href;
        return url;
    }

    /**
     * 根据是否客户端设置header头信息
     */
    function setClientHead() {
        // 根据参数设置右侧是否显示下载app
        if (HLMY_CONFIG.MENU_INFO && HLMY_CONFIG.MENU_INFO.downloadApp === false) {
            $('#headContent .head-dowload').addClass('gone')
        }
        var ua = EventBase.userTerminal();
        if (ua == 'androidclient' || ua == 'iphoneclient') {
            $('#clientHeader').css('display', 'block');
        } else {
            $('#headContent').css('display', 'block');
        }
    }
    /**
     * @description 获取到当前在玩游戏的基本信息
     * 不再使用ajax获取，直接通过页面的配置信息获取
     * @param {Object} appkey
     */
    function getArticleId(appkey) {

        // var url = '/game/api/app/getAppInfo?appKey=' + appkey;
        // var info = '';
        // DetailsController.getDataList(url, info, function(data) {
        //     if (data.code == '0') {
        //         articleId = data.articleId;
        //         appName = data.wxApp.name;
        //         iconUrl = data.wxApp.iconUrl;
        //         HLMY_MODAL_INFO.userInitFlag = true;
        //         deferArticle.resolve();
        //     }
        // });

        articleId = HLMY_CONFIG.GAME_INFO.aid;
        appName = HLMY_CONFIG.GAME_INFO.name;
        iconUrl = HLMY_CONFIG.GAME_INFO.iconUrl;
        HLMY_MODAL_INFO.userInitFlag = true;
        deferArticle.resolve();
    }
    /**
     * 加载数据
     * @param  {[type]} num  [description]
     * @param  {[type]} type [description]
     * @return {[type]}      [description]
     */
    function getData(type) {
        var url, infos, num;
        switch (type) {
            case 'gift':
                $('#gift .more-info a').html('加载中...');
                $('#gift .more-info').removeClass('gone');
                url = '/game/api/app/getDetails?aid=' + articleId;
                infos = '';
                DetailsController.getDataList(url, infos, handleGiftData);
                DetailsController.getDataList('/play/gifts/getGameGifts.json?appKey=' + appkey, '', handleGiftTypeData)
                break;
            case 'broadcast':
                $('#broadcast .more-info a').html('加载中...');
                $('#broadcast .more-info').removeClass('gone');
                // 公告
                url = '/game/api/app/getDetails?aid=' + articleId
                infos = '';
                DetailsController.getDataList(url, infos, handleBroadCaseData);
                break;
            case 'talk':
                num = HLMY_MODAL_INFO.talk.pageNo;
                //上锁
                HLMY_MODAL_INFO.talk.lock = true;
                if (num == 3) {
                    parent.window.location.href = '/play/game/' + articleId + '?focus=1&title=' + appName + '&tp=full&ex1758=1';
                } else {
                    $('#talk .more-info a').html('加载中...');
                    url = '/play/ugc/moreGameFeeds.json';
                    infos = {
                        // type:8,
                        pageNo: num,
                        pageSize: 10,
                        aid: articleId
                    }
                    DetailsController.getGameFeeds(url, infos, handleTalkData);
                }
                break;
            case 'hot':
                num = HLMY_MODAL_INFO.hot.pageNo;
                //上锁
                HLMY_MODAL_INFO.hot.lock = true;
                url = '/game/column/listvremarkugc.html';
                info = {
                    cid: 108,
                    pageNo: HLMY_MODAL_INFO.hot.pageNo,
                    pageSize: 10,
                    reqType: 1
                };
                $('#hotGame .more-info a').html('加载中...');
                DetailsController.getDataList(url, info, handleHotData);
                break;
        }
    }
    //处理得到的礼包数据 和开服信息
    function handleGiftData(data) {
        var gifts,
            tem,
            giftdata,
            compireHtml;
        gifts = data.gifts;
        var isHaveGifts = !!gifts && gifts.length > 0;
        if (!isHaveGifts) {
            $('#gift .more-info a').text('暂无更多数据');
            $('#gift .more-info').removeClass('gone');
        } else {
            $('#gift .more-info').addClass('gone');
            //如果有礼包展示礼包
            tem = htmlTem['gift'];
            // tem = EventBase.replaceTem(tem);
            giftdata = dealData(gifts);
            compireHtml = EventBase.compileTem(tem, giftdata);
            $('#gift #giftWrapper').html(compireHtml);
            $('#giftList').removeClass('gone');
            if (gifts.length > 3) { $('#showHG').removeClass('gone') }
        }
    }
    // 处理特殊类型的礼包数据
    function handleGiftTypeData(resData) {
        var tem, compireHtml;
        if (resData.result === 1 && resData.data.giftList.length) {
            tem = htmlTem['giftType'];
            compireHtml = EventBase.compileTem(tem, resData.data.giftList);
            $('#giftType .gifttype-wrapper').html(compireHtml);
            $('#giftType').removeClass('gone');
        }
    }
    // 查看更多礼包信息
    $(document).on('click', '#showHG', function() {
            $('#showHG').addClass('gone');
            $('#giftWrapper .item').removeClass('gone');
        })
        //处理得到的公告数据
    function handleBroadCaseData(resData) {
        var
            plans = resData.plans,
            broadcast = resData.gonglue,

            // 开服计划是否有数据
            isHavePlan = !!plans && plans.length > 0,
            // 公告（攻略）是否有数据
            isHaveBroadcast = !!broadcast && broadcast.length > 0;

        // 如果没有开服计划和公告，则显示暂无跟多数据
        if (!isHavePlan && !isHaveBroadcast) {
            $('#broadcast .more-info a').text('暂无更多数据');
            $('#broadcast .more-info').removeClass('gone');
            return;
        }
        // 有开服计划
        if (isHavePlan) {
            //	处理开服信息
            handleKFInfoData(plans);
        }
        if (isHaveBroadcast) {
            //处理公告信息
            handleBCInfoData(broadcast);
        }
        $('#broadcast .more-info').addClass('gone');
    }
    //处理得到的聊吧数据
    function handleTalkData(data) {
        if (data.result == 1 && data.data.ugcData && data.data.ugcData.dataList.length > 0) {
            if (data.data.ugcData.nextPageNo < 0) {
                //没有下一页了
                HLMY_MODAL_INFO.talk.lock = true;
                $('.more-info').addClass('gone');
            } else {
                HLMY_MODAL_INFO.talk.lock = false;
            }
            var tem, compireHtml;
            tem = htmlTem['details'];
            compireHtml = EventBase.compileTem(tem, data.data.ugcData.dataList);
            $('#talk .more-info').before(compireHtml);
            HLMY_MODAL_INFO.talk.pageNo++;
            $('#talk .more-info a').html('点击加载更多');
        } else {
            $('#talk .more-info a').html('暂无更多数据');
        }
    }
    /**
     * 处理获取到的热门游戏数据
     * @return {[type]} [description]
     */
    function handleHotData(resData) {
        $('.main-container').scroll(function() {
            Echo.render();
        });
        if (resData.length == 0) {
            //没有数据
            if (HLMY_MODAL_INFO.hot.pageNo == 1) {
                $('#hotGame .more-info a').html('暂无数据');
            } else {
                $('#hotGame .more-info a').html('无更多数据');
            }
            HLMY_MODAL_INFO.hot.lock = true;
        } else {
            $('#hotGame .more-info a').html('点击加载更多');
            //展示数据
            var tem = htmlTem['hotGames'];
            var compireHtml = EventBase.compileTem(tem, resData);
            $('#hotGame .list').append(compireHtml);
            Echo.init({
                offset: 0,
                throttle: 10
            });
            HLMY_MODAL_INFO.hot.pageNo++;
            HLMY_MODAL_INFO.hot.lock = false;
        }
    }
    //处理开服信息
    function handleKFInfoData(kfinfo) {
        // var kfinfo = data.plans;
        if (!!kfinfo && kfinfo.length > 0) {
            tem = htmlTem['gameQFOpen'];
            kfInfoData = dealKFInfoDate(kfinfo);
            compireHtml = EventBase.compileTem(tem, kfInfoData);
            $('#qfWrapper').html(compireHtml);
            if (kfinfo.length > 3) {
                //大于三个 显示更多标示
                $('#showQF').removeClass('gone');
            }
            $('#QFOpenTimeList').removeClass('gone');
        }
    }
    // 点击更多开服信息
    $(document).on('click', '#showQF', function() {
            $('#showQF').addClass('gone');
            $('#qfWrapper .item').removeClass('gone');
        })
        /**
         * 处理并展示攻略（公告）信息。
         * @bcInfo Array
         * 
         * @return {[type]} [description]
         */
    function handleBCInfoData(bcInfo) {
        var tem, //公告模板
            compireHtml, //公告模板编译后的dom
            broadcastData, //公告处理后的数据
            //如果有礼包展示礼包
            tem = htmlTem['broadCast'];
        broadcastData = dealBroadCastDate(bcInfo);
        compireHtml = EventBase.compileTem(tem, broadcastData);
        //dom插入
        $('#broadList #bcWrapper').html(compireHtml);
        //显示模块
        $('#broadList').removeClass('gone');
        // 如果数字大于3个则显示更多按钮
        if (bcInfo.length > 10) { $('#showBC').removeClass('gone') }
    }
    // 点击更多开服信息
    $(document).on('click', '#showBC', function() {
            $('#showBC').addClass('gone');
            $('#bcWrapper .item').removeClass('gone');
        })
        // 公告点击打开详情页
    $(document).on('click', '#bcWrapper .item', function(evt) {
        var $this = $(this);
        var title = $this.find('.bc-name').text();
        var time = $this.find('.bc-time').text();
        var id = $this.attr('data-id');
        // 赋值title 和 time
        $('#broadDetail .bdetail-title').text(title);
        $('#broadDetail .bdetail-time').text(time);
        //隐藏内容  显示loading
        $('#broadDetail .inner-bdetail-main').addClass('gone');
        $('#broadDetail .bdetail-loading').removeClass('gone');

        $('#broadDetail').removeClass('gone');
        $('.bdetail-foot').removeClass('gone');
        $.get('/play/game/' + id + '/article.json', '', handleBroadCastItemDetailData, 'json');
        evt.stopPropagation();
    })

    function handleBroadCastItemDetailData(resData) {
        var info;
        if (resData.result == 1) {
            info = resData.data.articleInfo;
            $('#broadDetail .inner-bdetail-main').html(info.content);
            $('#broadDetail .bdetail-loading').addClass('gone')
            $('#broadDetail .inner-bdetail-main').removeClass('gone');
        }
    }
    // 公告详情页点击返回按钮
    $(document).on('click', '#bdetailBtn', function() {
            $('#broadDetail').addClass('gone');
            $('.bdetail-foot').addClass('gone');
        })
        //处理礼包时间信息，当未开放的时候显示出今日，明日，后台
    function dealData(data) {
        var datenow = new Date(); //当前时间
        var t = datenow.getTime(); //当前毫秒数
        var year = datenow.getFullYear();
        var month = datenow.getMonth();
        var day = datenow.getDate();
        var t1 = new Date(year, month, day, 0, 0, 0).getTime(); //今天凌晨毫秒数
        var t2 = t1 + (24 * 60 * 60 * 1000); //明天凌晨零点毫秒数
        var t3 = t2 + (24 * 60 * 60 * 1000); //后天凌晨零点毫秒数
        var t4 = t3 + (24 * 60 * 60 * 1000); //大后天凌晨零点毫秒数
        var bt, y, M, d, h, m;

        for (var i = 0, len = data.length; i < len; i++) {
            bt = data[i].gift.beginTime;
            dd = new Date(bt);
            y = dd.getFullYear();
            M = (dd.getMonth() + 1) > 9 ? (dd.getMonth() + 1) : '0' + (dd.getMonth() + 1);
            d = dd.getDate() > 9 ? dd.getDate() : '0' + dd.getDate();
            h = dd.getHours() > 9 ? dd.getHours() : '0' + dd.getHours();
            m = dd.getMinutes() > 9 ? dd.getMinutes() : '0' + dd.getMinutes();
            if (bt > t) {
                if (bt > t1 && bt < t2) {
                    data[i].gift.bt = '今日  ' + h + ':' + m;
                } else if (bt > t2 && bt < t3) {
                    data[i].gift.bt = '明日  ' + h + ':' + m;
                } else if (bt > t3 && bt < t4) {
                    data[i].gift.bt = '后天  ' + h + ':' + m;
                } else {
                    data[i].gift.bt = M + '-' + d + ' ' + h + ':' + m;
                }
            } else {
                data[i].gift.bt = M + '-' + d + ' ' + h + ':' + m;
            }
        }
        return data;
    }
    //处理开服时间信息，当三天之内的时候显示出今日，明日，后天
    function dealKFInfoDate(data) {
        var datenow = new Date(); //当前时间
        var t = datenow.getTime(); //当前毫秒数
        var year = datenow.getFullYear();
        var month = datenow.getMonth();
        var day = datenow.getDate();
        var t1 = new Date(year, month, day, 0, 0, 0).getTime(); //今天凌晨毫秒数
        var t2 = t1 + (24 * 60 * 60 * 1000); //明天凌晨零点毫秒数
        var t3 = t2 + (24 * 60 * 60 * 1000); //后天凌晨零点毫秒数
        var t4 = t3 + (24 * 60 * 60 * 1000); //大后天凌晨零点毫秒数
        var bt, y, M, d, h, m;

        for (var i = 0, len = data.length; i < len; i++) {
            bt = data[i].startTime;
            dd = new Date(bt);
            y = dd.getFullYear();
            M = (dd.getMonth() + 1) > 9 ? (dd.getMonth() + 1) : '0' + (dd.getMonth() + 1);
            d = dd.getDate() > 9 ? dd.getDate() : '0' + dd.getDate();
            h = dd.getHours() > 9 ? dd.getHours() : '0' + dd.getHours();
            m = dd.getMinutes() > 9 ? dd.getMinutes() : '0' + dd.getMinutes();
            if (bt > t) {
                if (bt > t1 && bt < t2) {
                    data[i].bt = '今日  ' + h + ':' + m;
                } else if (bt > t2 && bt < t3) {
                    data[i].bt = '明日  ' + h + ':' + m;
                } else if (bt > t3 && bt < t4) {
                    data[i].bt = '后天  ' + h + ':' + m;
                } else {
                    data[i].bt = M + '-' + d + ' ' + h + ':' + m;
                }
            } else {
                data[i].bt = M + '-' + d + ' ' + h + ':' + m;
            }
        }
        return data;
    }
    /**
     * 公告数据的时间处理
     * @param  {[Array]} data [返回的公告数组]
     * @return {[Array]}      [处理后的数组返回]
     */
    function dealBroadCastDate(data) {
        var len = data.length;
        var test, dd, y, M, d, h, m;
        for (var i = 0; i < len; i++) {
            test = data[i].releaseDate;
            dd = new Date(test);
            y = dd.getFullYear();
            M = (dd.getMonth() + 1) > 9 ? (dd.getMonth() + 1) : '0' + (dd.getMonth() + 1);
            d = dd.getDate() > 9 ? dd.getDate() : '0' + dd.getDate();
            h = dd.getHours() > 9 ? dd.getHours() : '0' + dd.getHours();
            m = dd.getMinutes() > 9 ? dd.getMinutes() : '0' + dd.getMinutes();
            data[i]['bctime'] = y + '.' + M + '.' + d;
        }
        return data;
    }
    //tab点击事件
    function tabClick(index) {
        var type = HLMY_MODAL_INFO.tab[index].type;
        var isLoad = HLMY_MODAL_INFO.tab[index].isLoad;
        HLMY_MODAL_INFO.lastTab = index;
        if (type === 'kefu' && HLMY_MODAL_INFO.kfTab === 'chat') {
            KFChat.wsToggleDialog(false, 'chat');
            KFChat.wsToggleDialog(true, 'kf');
        } else {
            KFChat.wsToggleDialog(false, 'kf');
        }
        if (type === 'chat') {
            KFChat.wsToggleDialog(false, 'kf');
            KFChat.wsToggleDialog(true, 'chat');
        } else {
            KFChat.wsToggleDialog(false, 'chat');
        }
        switchTabDom(type, isLoad);
    }
    // 切换tab时候，dom切换
    // 显示和隐藏等
    function switchTabDom(type, isLoad) {
        //所有tab对应的content 内容 
        $('.panelitem').addClass('gone');
        handleTabIconShown(type);
        switch (type) {
            case 'gift':
                activeGiftTab(isLoad);
                break;
            case 'broadcast':
                activeBroadcastTab(isLoad);
                break;
            case 'talk':
                activeTalkTab(isLoad);
                break;
            case 'hot':
                activeHotTab(isLoad);
                break;
            case 'chat':
                activeChatTab(isLoad);
                break;
            case 'kefu':
                activeKefuTab(isLoad);
                break;
        }
    }
    /**
     * 处理切换显示tab icon
     * @return {[type]} [description]
     */
    function handleTabIconShown(type) {
        var $t = $('.t');
        var data = HLMY_MODAL_INFO.tab;
        $t.removeClass('active-tab');
        for (var i = 0, len = data.length; i < len; i++) {
            if (data[i].type == type) {
                $t.eq(i).addClass('active-tab');
                $t.eq(i).find('img').attr('src', data[i].activeIcon);
            } else {
                $t.eq(i).find('img').attr('src', data[i].icon);
            }
        }
    }
    /**
     * 设置选项的isLoad为true
     * @param {[type]} type [description]
     */
    function setTabContentData(type) {
        var data = HLMY_MODAL_INFO.tab;
        for (var i = 0, len = data.length; i < len; i++) {
            if (data[i].type == type) {
                data[i].isLoad = true;
            }
        }
    }
    /**
     * 选中礼包tab
     * @return {[type]} [description]
     */
    function activeGiftTab(isLoad) {
        //礼包
        $('#gift').removeClass('gone');
        if (isLoad) {
            //已经加载过数据了，展示礼包tab内容，隐藏聊吧tab内容 
        } else {
            //还没有加载过数据，进行数据请求
            setTabContentData('gift');
            getData('gift');
        }
    }
    /**
     * 选中公告tab
     * @return {[type]} [description]
     */
    function activeBroadcastTab(isLoad) {
        $('#broadcast').removeClass('gone');
        if (!isLoad) {
            //第一次加载数据
            //设置isLoad 为true
            setTabContentData('broadcast');
            getData('broadcast');
        } else {
            $('#broadDetail').addClass('gone');
        }
    }
    /**
     * 选中聊吧tab
     * @return {[type]} [description]
     */
    function activeTalkTab(isLoad) {
        //聊吧
        $('#talk').removeClass('gone');
        if (isLoad) {
            //已经加载过数据了，展示聊吧tab内容，隐藏礼包tab内容
        } else {
            //还没有加载过数据，进行数据请求
            setTabContentData('talk');
            getData('talk');
        }
    }
    /**
     * 选中热门游戏tab
     * @return {[type]} [description]
     */
    function activeHotTab(isLoad) {
        //热门游戏
        $('#hotGame').removeClass('gone');
        if (isLoad) {
            //已经加载过数据了，展示聊吧tab内容，隐藏礼包tab内容
        } else {
            //还没有加载过数据，进行数据请求
            setTabContentData('hot');
            getData('hot');
        }
    }
    /**
     * 选中群聊tab
     * @return {[type]} [description]
     */
    function activeChatTab(isLoad) {
        $('#chat').removeClass('gone');
        if (!isLoad) {
            // 客服初始化
            KFChat.init(htmlTem['kf']);
            // 设置isload为true
            setTabContentData('chat');
        }
    }
    /**
     * 选中客服tab
     * 2017/12/13 客服tab对应两个页面，需要根据kfTab 进行判断显示哪一个页面
     * info显示信息页面
     * chat 显示聊天页面
     * @return {[type]} [description]
     */
    function activeKefuTab(isLoad) {
        if (HLMY_MODAL_INFO.kfTab === 'info') {
            $('#kefuInfo').removeClass('gone');
        } else {
            //客服聊天消息列表
            $('#kefu').removeClass('gone');
            if (!isLoad) {
                // 客服初始化
                KFChat.init(htmlTem['kf']);
                // 设置isload为true
                setTabContentData('kefu');
            }
            // kfTab信息使用一次后立即置成默认状态info
            HLMY_MODAL_INFO.kfTab = 'info';
        }
    }
    //点赞
    $(document).on('click', '.like-p', function() {
        var $this = $(this);
        var data = $(this).attr('data-like');
        data = data.split(',');
        like(data[0], data[1], $this);
    })

    function like(resourceId, resourceType, $this) {
        if ($this.hasClass("liked")) {
            // 禁止用户的点赞行为
            // 	$this.find('img').attr("src","http://wx.1758.com/game/h5/images/n-cxihuan-2-tw.png");
            // 	$this.removeClass("liked");
            // 	var count=$('#like-count-'+resourceId).html();
            // 	$('#like-count-'+resourceId).html(--count);
            // $.post('/game/api/unlike',{resourceId:resourceId,resourceType:resourceType}, function(data){
            // });
        } else {
            $.post('/play/ugc/likeUgcPost.json', { ugcPostId: resourceId }, function(data) {
                if (data.result == 1) {
                    $this.find('img').attr("src", "http://images.1758.com/game/n-cxihuan-1-tw.png");
                    $this.addClass("liked");
                    var count = $('#like-count-' + resourceId).html();
                    $('#like-count-' + resourceId).html(++count);
                    addLikeHead(data.data, $this);
                } else if (data.errorcode == 100) {
                    //未登录
                    location.href = '/game/user/home'
                }
            }, 'json');
        }
    }
    //留言或者回复
    $(document).on('click', '.replay', function() {
        var $this = $(this);
        var data = $this.attr('data-message');
        data = data.split(',');
        replys(data);
    })

    function replys(data) {
        replyData = data;
        showWeDialog('reply', data);
    }
    //详情页点击进入游戏
    $('#gift').on('click', '.i-info', function() {
            var url = $(this).attr('data-value');
            parent.window.location.href = url;
        })
        //领号点击
    $(document).on('click', '.linghao', function() {
        var id = $(this).attr('data-num');
        var type = $(this).attr('data-value');
        var ss;
        if (type == '1') {
            ss = EventBase.userTerminal();
            if (ss == 'weixin' || ss == 'others') {
                exclusiveTip();
            } else {
                getGiftNum(id);
            }
        } else if (type == '0') {
            getGiftNum(id);
        }
    })

    function getGiftNum(id) {
        var url = "/game/api/app/gif/apply?gifId=" + id;
        $.get(url, function(data) {
            //弹窗
            if (data.code == -1) {
                //未登录,跳转登陆页面
                location.href = "//wx.1758.com/game/platform/v1.0/user/flogin?appKey=ba0ab54c1eb63a776fa477153a0a354b";
            }
            if (data.code == 0) {
                //flag 领号0 淘号1
                //				window.prompt('领号成功,请手动复制您的礼包码',data.cdkey);
                tanChuang("领号成功", data.cdkey, id, 0);
            }
        });
    }
    //淘号
    $(document).on('click', '.taohao', function() {
        var id = $(this).attr('data-num');
        var type = $(this).attr('data-value');
        var ss;
        if (type == '1') {
            ss = EventBase.userTerminal();
            if (ss == 'weixin' || ss == 'others') {
                exclusiveTip();
            } else {
                taoHao(id);
            }
        } else if (type == '0') {
            taoHao(id);
        }

    })

    function taoHao(id) {
        var url = "/game/api/app/gif/tao?gifId=" + id;
        $.get(url, function(data) {
            if (data.code == -1) {
                //未登录,跳转登陆页面
                location.href = "//wx.1758.com/game/platform/v1.0/user/flogin?appKey=ba0ab54c1eb63a776fa477153a0a354b";
            }
            if (data.code == 0) {
                //					window.prompt('淘号成功,请手动复制您的礼包码',data.cdkey)
                tanChuang("淘号成功", data.cdkey, id, 1);
            }
        });
    }

    function tanChuang(title, cdkey, gid, flag) {
        var url = "//wx.1758.com/play/gift/" + gid + "?cdkey=" + cdkey + "&flag=" + flag;
        parent.window.location.href = url + '&tp=full&ex1758=1';
    }
    //专属礼包提示
    function exclusiveTip() {
        $('.tip').removeClass('gone');
    }

    $(document).on('click', '.tip', function(e) {
            $('.tip').addClass('gone');
        })
        //绑定点击事件
        //tab 点击事件
    $(document).on('click', '.t', function() {
            //tab选择第几个 0 开始
            var indexNum = $(this).index();
            tabClick(indexNum);
        })
        //聊吧加载更多
    $(document).on('click', '#talk .more-info', function() {
        if (!HLMY_MODAL_INFO.talk.lock) {
            getData('talk');
        }
    });
    /**
     * 热门游戏 加载更多
     * @return {[type]}          [description]
     */
    $(document).on('click', '#hotGame .more-info', function() {
        if (!HLMY_MODAL_INFO.hot.lock) {
            //聊吧
            getData('hot');
        }
    });
    //跳转到首页
    $(document).on('click', '#goHome', function() {
        parent.window.location.href = '/play/index';
    });
    //发布评论的时候添加图片
    $(document).on('click', '#wxAddImg', function() {
            if (imgsData.localId.length < 4) {
                $('#loadingToast').removeClass('gone');
                setTimeout(function() {
                    $('#loadingToast').addClass('gone');
                }, 2000);
                wx.chooseImage({
                    count: 4,
                    success: function(res) {
                        $('#loadingToast').addClass('gone');
                        var testArr = [];
                        for (var i = 0, len = res.localIds.length; i < len; i++) {
                            if (imgsData.localId.length < 4) {
                                imgsData.localId.push(res.localIds[i]);
                                testArr.push(res.localIds[i]);
                            }
                        }
                        addPreImg(testArr);
                    }
                });
            } else {
                $('#toastTip').removeClass('gone');
                setTimeout(function() {
                    $('#toastTip').addClass('gone');
                }, 2000);
            }
        })
        //添加预览文件
    function addPreImg(localIds) {
        var len = localIds.length;
        var str = '';
        if (imgsData.localId.length == 4) {
            $('#wxAddImg').addClass('gone');
        }
        for (var i = 0; i < len; i++) {
            str += '<div class="pure-u-1-4 selfimg">' +
                '<div class="imgitem">' +
                '<div style="background-image:url(' + localIds[i] + ')"></div>' +
                '<img class="gone" src="' + localIds[i] + '" />' +
                '<span class="imgdel">' +
                '<img src="http://images.1758.com/daren/delimg.png" />' +
                '</span>' +
                '</div>' +
                '</div>';
        }
        $('#wxImgWrapper').prepend(str);
    }
    //发布评论
    $(document).on('click', "#commentLogo", function() {
        showWeDialog('comment');
    });
    //显示评论框
    function showWeDialog(arg) {
        var ua = EventBase.userTerminal();
        if (arg == 'reply') {
            //回复
            replyType = 'reply';
            if (replyData[0] == 'zhanji_all') {
                $('.weui_dialog_title').html('回复' + replyData[4]);
            } else {
                $('.weui_dialog_title').html('回复评论');
            }
            $('#wxAddImg').addClass('gone');
            $('#weDialog').removeClass('gone');
        } else {
            //评论
            replyType = 'comment';
            $('.weui_dialog_title').html('发表评论');
            if (ua == 'weixin') {
                $('#wxAddImg').removeClass('gone');
            } else {
                $('#wxAddImg').addClass('gone');
            }
            $('#weDialog').removeClass('gone');
        }
    }
    //评论发布框取消点击事件
    $(document).on('click', '#weCancle', function() {
        $('#weText').val('');
        $('#weDialog').addClass('gone');
        $('#loadingToast').addClass('gone');
    })

    //评论发布框发布点击事件
    $(document).on('click', '#wePublic', function() {
        var content = $('#weText').val();
        content = $.trim(content);
        if (content.length > 0 || imgsData.localId.length > 0) {
            postPublicContent(content);
            $('#weDialog').addClass('gone');
            $('#loadingToast').addClass('gone');
            $('#weText').val('');
            $('#wxImgWrapper .selfimg').remove();
        }
    })

    //删除图片
    $(document).on('click', '.imgdel', function() {
        var delsrc = $(this).prev('img').attr('src');
        $(this).parents('.selfimg').remove();
        var len = imgsData.localId.length;
        for (var i = 0; i < len; i++) {
            if (imgsData.localId[i] == delsrc) {
                imgsData.localId.splice(i, 1);
                if (imgsData.localId.length < 4) {
                    $('#wxAddImg').removeClass('gone');
                }
                reutrn;
            };
        }
    })

    //上传图片到微信
    function pushToWeiXin(callback, content) {
        var i = 0,
            len = imgsData.localId.length;

        function upload() {
            wx.uploadImage({
                localId: imgsData.localId[i],
                success: function(res) {
                    i++;
                    imgsData.serverId.push(res.serverId);
                    if (i < len) {
                        upload();
                    } else {
                        callback(content);
                    }
                },
                fail: function(res) {
                    alert(JSON.stringify(res));
                }
            });
        }
        upload();
    }
    //发送评论内容到服务器
    function postPublicContent(content) {
        var localcookie = document.cookie;
        var lo = localcookie.indexOf('wy_user');
        var parameters;
        if (lo >= 0) {
            if (replyType == 'comment') {
                // 有图片发送评论
                if (imgsData.localId.length > 0) {
                    //上传到微信
                    pushToWeiXin(postCommentWxImg, content);
                    return;
                } else {
                    //发表评论
                    parameters = {
                        aid: articleId, //游戏aid
                        content: content, //发表的内容
                        img: '', //逗号隔开，url或者微信mediaid  可选
                        isWxImg: false, //默认false     可选
                        openAppId: '' //公众号id  默认1758微游戏   	可选的
                    }

                    // 无图片发送评论
                    $.post('/play/ugc/postFeed.json', parameters, function(response) {
                        if (response.result == 1) {
                            $('#toast').removeClass('gone');
                            setTimeout(function() {
                                $('#toast').addClass('gone');
                            }, 2000);
                        }
                    }, 'json')
                }
            } else if (replyType == 'reply') {
                //回复评论
                parameters = {
                    ugcId: replyData[1],
                    content: content,
                    referUid: ''
                }
                if (replyData[0] == 'zhanji_all') {
                    parameters.referUid = replyData[2];
                }
                $.post('/play/ugc/postReply.json', parameters, function(response) {
                    if (response.result == 1) {
                        $('#toast').removeClass('gone');
                        setTimeout(function() {
                            $('#toast').addClass('gone');
                        }, 2000);
                    }
                }, 'json')
            }
        } else {
            document.location.href = '//wx.1758.com/game/platform/v1.0/user/flogin?appKey=ba0ab54c1eb63a776fa477153a0a354b';
        }
    }
    //上传带微信图片到服务器
    function postCommentWxImg(content) {
        var ser = imgsData.serverId.join(',');
        var parameters = {
            aid: articleId,
            content: content,
            img: ser,
            isWxImg: true,
            wyOpenAppId: HLMY_CONFIG.SHARE_INFO.wyOpenAppId
        }
        $.post('/play/ugc/postFeed.json', parameters, function(response) {
            imgsData.localId = [];
            imgsData.serverId = [];
            $('#toast').removeClass('gone');
            setTimeout(function() {
                $('#toast').addClass('gone');
            }, 2000);
        })
    }
    //查询个人信息，获取个人头像
    function getUserInfo() {
        var url = '/game/platform/user/getInfo';
        DetailsController.getDataList(url, '', function(data) {
            if (data.code == 0) {
                $('#hgift img').attr('src', data.infos.head);
            }
        });
    }
    //设置个人头像
    function setUserInfo() {
        $('#modalUserHead img').attr('src', HLMY_CONFIG.USER_INFO.headUrl);
        $('.clientHeadImg').attr('src', HLMY_CONFIG.USER_INFO.headUrl);
    }
    //收藏游戏
    $(document).on('click', '#collect', function() {
        location.href = '//wx.1758.com/game/sns/followUs';
    });
    //访问个人主页
    $(document).on('click', '#hgift', function() {
            parent.window.location.href = '/play/user/home';
        })
        //访问客服
    $(document).on('click', '#goKefu', function() {
        parent.window.location.href = '//wpa.qq.com/msgrd?v=3&uin=2660866818&site=qq&menu=yes';
    });
    //下载客户端
    $(document).on('click', '.ex-load', function() {
            parent.window.location.href = "//m.1758.com/game/download.htm";
        })
        /**
         * localstorage 查询刷新time
         * 返回布尔值
         * true为超时，需要刷新
         * false为未超时，不做处理
         */
    function checkStorageTime() {
        var t = HLMY_MODAL_INFO.refreshtime;
        var nt = new Date().getTime();
        var value = window.localStorage.getItem(HLMY_MODAL_INFO.timeName);
        value = JSON.parse(value);
        if (nt - value.refti > t) {
            return true;
        } else {
            return false;
        }
    }
    /**
     * 判断刷新时间
     * 超时，对礼包和聊天进行内容重置
     */
    function detectRefretime() {
        if (checkStorageTime()) {
            //清空数据
            resetData();
            return true;
        } else {
            //没超过时间，不处理
            return false;
        }
    }
    /**
     * 设置自动刷新时间
     * 如果超过该时间，聊天选项和礼包选项进行重新加载
     * 记录每次点击的tab，聊天&礼包
     * @param {Object} panel
     */
    function setStorageTime() {
        var value = window.localStorage.getItem(HLMY_MODAL_INFO.timeName),
            name = HLMY_MODAL_INFO.timeName,
            time = new Date().getTime(),
            panel = 'refti';
        value = value ? value : {};
        if (typeof(value) == 'string') {
            value = JSON.parse(value);
        }
        value[panel] = time;
        value = JSON.stringify(value);
        window.localStorage.setItem(HLMY_MODAL_INFO.timeName, value)
    }
    /**
     * 当用户长时间间隔后则所有数据清空
     * @return {[type]} [description]
     */
    function resetData() {
        var tab = HLMY_MODAL_INFO.tab;
        var len = tab.length;
        for (var i = 0; i < len; i++) {
            tab[i].isLoad = false;
        }
        HLMY_MODAL_INFO.hot.lock = false;
        HLMY_MODAL_INFO.hot.pageNo = 1;
        HLMY_MODAL_INFO.talk.lock = false;
        HLMY_MODAL_INFO.talk.pageNo = 1;
        //清空内容，data-numn设为1
        $('#gift .list .item').remove();
        $('#talk .list .n-item').remove();
        $('#hotGame .list').html();
    }
    /**
     * 点赞的时候动态的添加用户的头像
     * @param {[type]} data [description]
     * @param {[type]} that [description]
     */
    function addLikeHead(data, that) {
        var rtime = that.parents('.r-time');
        var rcom = rtime.siblings('.r-com');
        var str = '';
        if (rcom.length == 0) {
            //不存在回复区
            str = '<div class="r-com">' +
                '<div class="mask"><div class="pladebox"></div></div>' +
                '<div class="like-com-inner">' +
                '<div class="flag-wrapper">' +
                '<img src="http://images.1758.com/game/n-cxihuan-2-tw.png">' +
                '</div>' +
                '<div class="like-com-container">' +
                '<div class="like-wrapper" data-uid="' + data.userProfile.profile.userId + '">' +
                '<img src="' + data.userProfile.profile.headUrl + '">' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>';
            rtime.after(str);
            return;
        } else {
            if (rcom.find('.like-com-inner').length == 0) {
                //不存在头像展示区
                str = '<div class="like-com-inner">' +
                    '<div class="flag-wrapper">' +
                    '<img src="http://images.1758.com/game/n-cxihuan-2-tw.png">' +
                    '</div>' +
                    '<div class="like-com-container">' +
                    '<div class="like-wrapper" data-uid="' + data.userProfile.profile.userId + '">' +
                    '<img src="' + data.userProfile.profile.headUrl + '">' +
                    '</div>' +
                    '</div>' +
                    '</div>';
                rcom.find('.mask').after(str);
                return;
            } else {
                str = '<div class="like-wrapper" data-uid="' + data.userProfile.profile.userId + '">' +
                    '<img src="' + data.userProfile.profile.headUrl + '">' +
                    '</div>';
                rcom.find('.like-com-container').append(str);
                return;
            }
        }

    }
    // 聊天折叠信息点击查看全部内容
    $(document).on('click', '.com-inner .mess-more', function() {
        $(this).addClass('gone');
        $(this).siblings('.mess').removeClass('gone');
    })

    // 客服tab项下 两个页面来回切换, 
    // 切换到信息界面
    $(document).on('click', '#kefuClose', function() {
        tabClick(5);
    });
    // 客服tab项下 两个页面来回切换，
    // 切换到聊天界面
    $(document).on('click', '#kefuOpen', function() {
        HLMY_MODAL_INFO.kfTab = 'chat';
        tabClick(5);
    });
    // 客服中vip用户领取二维码
    $(document).on('click', '#kefuInfoEwm', function() {
        if (vipClubInfo.mobile && vipClubInfo.weixinNumber) {
            if (vipClubInfo.vipLevel > 3) {
                // 用户已经填写之后，并且vip大于等于4，则弹出二维码让用户关注
                $('#highLevelEwm').removeClass('gone');
            } else {
                // 用户等级达不到，弹框提示
                $('#vipLevelNot').removeClass('gone');
            }
        } else {
            // 如果用户没有填写信息，则直接跳转到填写信息界面
            location.href = 'http://wx.1758.com/static/m/joinClub.html?tp=full&ex1758=1&title=VIP俱乐部';
        }
    });
    // 客服中vip用户点击黑色背景隐藏二维码
    $(document).on('click', '#highLevelEwm', function(evt) {
        if (evt.target.nodeName === 'IMG') {
            return;
        } else {
            $('#highLevelEwm').addClass('gone');
        }
    });

    /**
     * 获取用户的vip俱乐部信息
     */
    function getUserClubInfo() {
        var url = '/play/user/contact/loadContactInfo.json';
        $.post(url, function(response) {
            if (response.result == 1 && response.data && response.data.contactInfo) {
                vipClubInfo.mobile = response.data.contactInfo.mobile || '';
                vipClubInfo.weixinNumber = response.data.contactInfo.weixinNumber || '';
            }
        });
    }

    /**
     * 获取用户vip信息,
     * 在客服tab用户一键领取的时候用到
     */
    function getUserVIPInfo() {
        var url = '/play/user/profile.json'
        $.post(url, { mode: 4 }, function(response) {
            if (response.result == 1 && response.data && response.data.userProfile) {
                userInfo = response.data.userProfile;
                vipClubInfo.vipLevel = userInfo.vipInfo.vipLevel;
            }
        }, 'json')
    }

    // 用户vip等级不够
    // 点击隐藏
    $(document).on('click', '#vipLevelNot .viplevel-not-btn', function(evt) {
        $('#vipLevelNot').addClass('gone');
    })


    return module;
});