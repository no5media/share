/**
 * 初始化浮标，以及浮标的按钮点击等
 */
define(['jquery', 'common/eventBase', 'view/gamemodal', 'view/chat', 'libs/jquery.cookie'], function($, EventBase, GameModal, KFChat) {
    var clientHeadArr;
    var _isShowModal = false; //是否显示浮标
    var module = {
        //一些基础信息
        dataInfo: {
            userTerminal: '', //用户使用的终端情况
            appKey: ''
        },
        //初始化浮标,args 判断是否需要chatroom
        init: function(isShowModal, clientArr) {
            initInfo(clientArr);
            //判断是否需要显示浮标
            deduceModalBtn(isShowModal);
        },
        // 自由指定freetab项
        freeTab: function(index) {
            if (index == 5) {
                GameModal.setKfTab('chat');
            }
            if (_isShowModal) {
                // 有浮标
                if (!isInit) {
                    //没有初始化过
                    GameModal.setInitTab(index);
                    getshow();
                } else {
                    GameModal.freeSelectTab(index);
                    $('.moban').fadeIn();
                }
            } else {}
        }
    };
    var iX, iY;
    var isInit = false; //是否已经初始化，默认false 没有初始化
    var modalBtnHtml = EventBase.loadTemplate('/static/spf/dist/tpl/modalMainTem_1_9.tpl');
    //初始化一些info信息
    function initInfo(clientArr) {
        clientHeadArr = clientArr;
        module.dataInfo.userTerminal = EventBase.userTerminal();
        module.dataInfo.appKey = EventBase.getParameters('appKey');
    }
    //判断浮标是否需要显示
    function deduceModalBtn(isShowModal) {
        var ua = module.dataInfo.userTerminal;
        if (!!isShowModal) {
            _isShowModal = isShowModal;
            $('#vipInfoContainer .vipinfo-reply').removeClass('gone');
            // 显示浮标位置
            handleStoragePosition();
            // 绑定事件
            handleFunc();
            // 获取cookie(现在不需要了)
            // setCookie();
        } else {
            $('#moban').remove(); //去除浮层节点
            $('#button').remove(); //去除浮层图标
        }
    }
    //判断并显示最后一次浮标的位置
    function handleStoragePosition() {
        var gp = window.localStorage.gameposition;
        var x, y;
        if (!!gp) {
            gp = JSON.parse(gp);
            for (var i = 0, len = gp.length; i < len; i++) {
                if (gp[i].appkey == module.dataInfo.appKey) {
                    x = gp[i].x;
                    y = gp[i].y;
                    $("#button").css('left', x).css('top', y).css('right', 'auto').css('bottom', 'auto');
                    break;
                }
            }
        }
        $('#button').css('display', 'block')
    }

    //浮标按钮绑定拖拽事件
    function handleFunc() {
        $("#button").on('touchstart', function(e) {
                //e.preventDefault();
                $("#button").css('opacity', 1);
                if (e.type == 'mousedown') {
                    iX = e.originalEvent.clientX - $(this)[0].offsetLeft;
                    iY = e.originalEvent.clientY - $(this)[0].offsetTop;
                } else {
                    iX = e.originalEvent.targetTouches[0].clientX - $(this)[0].offsetLeft;
                    iY = e.originalEvent.targetTouches[0].clientY - $(this)[0].offsetTop;
                }
            })
            .on('touchmove', function(e) {
                e.preventDefault();
                $("#button").css('opacity', 1);
                if (e.type == 'mousemove') {
                    var x = e.originalEvent.clientX - iX;
                    var y = e.originalEvent.clientY - iY;
                } else {
                    var x = e.originalEvent.targetTouches[0].clientX - iX;
                    var y = e.originalEvent.targetTouches[0].clientY - iY;
                }
                var nHeight = document.documentElement.clientHeight - parseInt($("#button").css('height'));
                var hWidth = document.documentElement.clientWidth - parseInt($("#button").css('width'));
                y = y < 0 ? 0 : y;
                y = y > nHeight ? nHeight : y;
                x = x < 0 ? 0 : x;
                x = x > hWidth ? hWidth : x;
                $("#button").css('left', x).css('top', y).css('right', 'auto').css('bottom', 'auto');
            })
            .on('touchend', function(e) {
                if (e.type == 'mouseup') {} else {
                    var x = e.currentTarget.offsetLeft;
                    var y = e.currentTarget.offsetTop;
                }
                if (window.localStorage) {
                    var arr = {
                        'appkey': module.dataInfo.appKey,
                        'x': x,
                        'y': y
                    };
                    handleStorage(arr);
                }
                //e.preventDefault();
            });
        $('#button').on('click', function(e) {
            e.preventDefault();
            getshow();
            $('#button .point').css('display', 'none');
            try {
                _hmt.push(['_trackEvent', 'buoy', 'open', '']);
            } catch (e) {}
        });
        $('#modal-left').on('click', function() {
            getout();
            // 关闭ws 会话
            KFChat.wsToggleDialog(false);
        })
    }

    //获取localstorage
    function handleStorage(item) {
        var gpo;
        if (!!(window.localStorage.gameposition)) {
            gpo = window.localStorage.gameposition;
            gpo = JSON.parse(gpo);
        } else {
            gpo = [];
        }
        updateStorageArray(gpo, item);
    }

    //处理localstorage的数组,有该内容则替换，没有则添加
    function updateStorageArray(gpo, item) {
        var mark = false;
        if (gpo.length != 0) {
            for (var i = 0, len = gpo.length; i < len; i++) {
                if (gpo[i].appkey == item.appkey) {
                    gpo[i].x = item.x;
                    gpo[i].y = item.y;
                    mark = true;
                    break;
                }
            }
        }
        if (!mark) {
            gpo.push(item);
        }
        gpo = JSON.stringify(gpo);
        window.localStorage.setItem('gameposition', gpo);
    }

    //浮层显示
    function getshow() {
        if (isInit) {
            GameModal.init();
            $('.moban').fadeIn();
        } else {
            var str = modalBtnHtml['modalMainTem'];
            var windowHeight, kfHeight, arr, kfInfoHeight;
            $('#modalframe').html(str);
            if (clientHeadArr[0]) {
                // 退出
                $('.clientExit').removeClass('gone');
            }
            // if(clientHeadArr[1]){
            // 	// 返回
            // 	$('.clientBack').removeClass('gone');
            // }
            if (clientHeadArr[1]) {
                // 刷新
                $('.clientRefresh').removeClass('gone');
            }
            if (clientHeadArr[2]) {
                // 分享
                $('.clientShare').removeClass('gone');
            }
            if (clientHeadArr[3]) {
                // 添加到桌面
                $('.clientDeskTop').removeClass('gone');
            }
            if (clientHeadArr[4]) {
                // 切换账号
                $('.clientSwitch').removeClass('gone');
            }
            GameModal.init();
            isInit = true;
            KFChat.showItem();
            arr = HLMY_CONFIG.TAB_INFO.item;
            for (var i = 0, len = arr.length; i < len; i++) {
                $('.' + arr[i]).removeClass('gone');
            }
            $('.moban').fadeIn();

            // 设置客服height
            windowHeight = $(document.body).height();
            if (clientHeadArr.length > 0) {
                // 客户端中
                kfHeight = windowHeight - 68 - 47 - 60;
                kfInfoHeight = windowHeight - 68 - 47 - 75;
            } else {
                kfHeight = windowHeight - 46 - 47 - 60;
                kfInfoHeight = windowHeight - 46 - 47 - 75;
            }
            $('.kefuContainer').height(kfHeight + 'px');
            // 设置客服信息的height
            $('#kefuInfo .kefu-container').height(kfInfoHeight + 'px')
        }
    }
    //浮层消失
    function getout() {
        $('.moban').fadeOut();
    }
    //请求浮层里面的东西
    //动态创建frame 获取cookie
    function setCookie() {
        var url = '//wx.1758.com/game/h5/page/smallforcookie.htm?' + new Date().getTime();
        var giframe = document.createElement('iframe');
        giframe.src = url;
        giframe.style.display = 'none';
        document.body.appendChild(giframe);
    }

    window.addEventListener('message', function(evt) {
        if (evt.origin == 'http://wx.1758.com' || evt.origin == 'https://wx.1758.com') {
            var datacookie = evt.data;
            $.removeCookie('wy_user', { path: '/' });
            if (datacookie.code == 1) {
                if (!!datacookie.args) {
                    datacookie.args = decodeURIComponent(datacookie.args);
                    $.cookie('wy_user', datacookie.args, { expires: 9999, path: '/' });
                }
            }
        }
    }, false);
    return module;
});