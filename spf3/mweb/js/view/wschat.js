/**
 * 该文件暂时无用
 * @param  {Object} $){	var                  wsModule           [description]
 * @param  {[type]} checkWS:function(){			if (window.WebSocket) {				return  true;			} else {				alert("您的浏览器不支持WebSocket协议！");				return false;			}		} [description]
 * @param  {[type]} init:                     function(          [description]
 * @return {[type]}                           [description]
 */
define(['jquery'],function($){
	var wsModule = {};
	wsModule = {
		socketEmitter: function(){
			//绑定事件
			this._callbacks = {};
			this._fired = {};
			this.socketInfo = {};
		},
		checkWS:function(){
			if (window.WebSocket) {
				return true;
			} else {
				alert("您的浏览器不支持WebSocket协议！");
				return false;
			}
		},
		init: function(){
			//检测WebSocket建立链接
			//checkWS();
		},
	};
	/**
	 * @description 绑定事件，
	 * 可以输入一个name和一个callack
	 * 也可以输入一个对象
	 * @param {Object} eventname
	 * @param {Object} callback
	 */
	var socketEmitter = wsModule.socketEmitter;
	socketEmitter.prototype.on = function(eventname,callback){
		if(arguments.length == 0) return this;
		if(arguments.length == 1){
			if(typeof(eventname) == 'object'){
				for(var pro in eventname){
					if(eventname.hasOwnProperty(pro)){
						this._callbacks[pro] = this._callbacks[pro] || [];
						this._callbacks[eventname[pro]].push(eventname[pro]);
					}
				}
				return this;
			}
		}
		this._callbacks[eventname] = this._callbacks[eventname] || [];
		this._callbacks[eventname].push(callback);
		return this;
	}
	/**
	 * @description 解除事件绑定
	 * 输入一个name，则删除所有callback，
	 * 输入一个name 一个callback 则删除制定的内容
	 * @param {Object} eventname
	 * @param {Object} callback
	 */
	socketEmitter.prototype.remove = function(eventname,callback){
		var cbs = this._callbacks,
			cbList,
			cblen;
		if(!eventname) return this;
		if(!callback){
			cbs[eventname] = [];
		}else{
			cbList = cbs[eventname];
			if(!cbList) return this;
			cblen = cbList.length;
			for(var i = 0; i < cblen; i++){
				if(cbList[i] == callback){
					//待验证，获取有问题
					cbList.splice(i,1);
					break;
				}
			}
		}
	}
	
	socketEmitter.prototype.fire = function(eventname,data){
		var cbs = this._callbacks,
			cbList;
		if(!eventname) return this;
		if(!cbs[eventname]) return this;
		cbList = cbs[eventname];
		if(cbList){
			for(var i = 0,len = cbList.length; i < len; i++){
				cbList[i].apply(this,Array.prototype.slice.call(arguments,1))
			}
		}
	}
	socketEmitter.prototype.init = function(url){
		var that = this;
		//url = "http://wtest.1758.com/openapi/im/authorize.json";
		$.get(url, function(responseData){
			if(responseData.result==1){
				var imServerUrl = responseData.data.imServerUrl;
				var fromToken = responseData.data.fromToken;
				var socket = new WebSocket(imServerUrl);
				that.socketInfo.socket = socket;
				that.socketInfo.fromToken = fromToken;
				socket.onmessage = function(event){
					that.fire('onmessage',event);	
				}
				socket.onopen = function(){
					that.fire('onopen');
				} 
				socket.onclose = function(){
					that.fire('onclose');	
				}
			}
		});
	}
	//初始化通讯，告诉服务器开始聊天
	socketEmitter.prototype.initSendMessage = function(arg){
		if (this.socketInfo.socket.readyState == WebSocket.OPEN) {
			var mess;
			mess= {
				"fromToken": this.socketInfo.fromToken,
			    "type": 100,
			    "subType": '',
			    "param": {
			    	"chatroomId":"",	//[appkey]@appKey
//			    	"appKey":"",
				    "lastestMessageId":""
			    	},
			    "sendTime": new Date().getTime(),
			}
			mess = $.extend({},mess,arg);
			mess = JSON.stringify(mess);
			this.socketInfo.socket.send(mess);
		} else {
			alert("WebSocket 初始化连接没有建立成功！");
		}
	}
	socketEmitter.prototype.sendMessage = function(args,callback){
		if (this.socketInfo.socket.readyState == window.WebSocket.OPEN) {
			var mess;
			mess={
			    "id": 12,	//上行 无意义
			    "type": 600,
			    "message": {
			        "text": ''
			    },
			    "mediaType": 0,
			    "fromToken": this.socketInfo.fromToken,
			    "to": "",	//[appkey]@appKey
			    "sendTime": new Date().getTime(),
			    "clientInfo": {
			        "os": "",
			        "version": "0.0.1"
			    }
			}
			mess = $.extend({},mess,args);
			console.log(mess);
			mess = JSON.stringify(mess);
			this.socketInfo.socket.send(mess);
			callback({flag:true})
		} else {
			if(typeof(callback) =='function'){
				callback({flag:false});
				console.log("WebSocket 连接没有建立成功！");
			}
		}
	}
	return wsModule;
})