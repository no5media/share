/**
 * 游戏返回按钮监听事件
 * @param  {String} $){	var appKey        [description]
 * @return {[type]}          [description]
 */
define(['jquery'], function($) {
    var appKey = '';
    var aid = '';
    var gameArr = '';
    var chn = '';
    if (HLMY_CONFIG && HLMY_CONFIG.GAME_INFO) {
        appKey = HLMY_CONFIG.GAME_INFO.appKey;
        aid = HLMY_CONFIG.GAME_INFO.aid;
    }
    if (HLMY_CONFIG.CHN_INFO) {
        chn = HLMY_CONFIG.CHN_INFO.chn;
    }

    var pageBack = {
        init: init
    };
    /**
     * 初始化内容
     * @return {[type]} [description]
     * ?aid=113292&chn=chiba
     */
    function init() {
        var url = '/play/game/moreRecommendedContent.json';
        var info = {
            aid: aid,
            chn: chn
        }
        $.post(url, info, handleInitData, 'json');
    }
    /**
     * 处理初始化返回的数据
     * @param  {[type]} resData [description]
     * @return {[type]}         [description]
     */
    function handleInitData(resData) {
        if (resData.result == 1) {
            // 处理返回游戏的图片
            if (resData.data && resData.data.recommendedContentList) {
                gameArr = resData.data.recommendedContentList;
                if (gameArr.length > 0) {
                    pageBackInit();
                }
            }
            // 处理返回游戏的二维码
            if (resData.data && resData.data.subscribeOnback && resData.data.subscribeQrcodeUrlOnback) {
                $('#exitDialog .backinfo-ewm img').attr('src', resData.data.subscribeQrcodeUrlOnback);
                $('#exitDialog .backinfo-ewm').removeClass('gone');
            }
        }
    }
    /**
     * 返回信息初始化
     * @return {[type]} [description]
     */
    function pageBackInit() {
        //组装dom节点
        setDomInfo(gameArr);
        //初始化相关事件
        eventInit();
    }
    /**
     * state：废弃
     * 拼接返回信息的Dom节点
     * 废弃
     */
    function spliceDom(gameArr) {
        var str = '';
        for (var i = 0, len = gameArr.length; i < len; i++) {
            str += '<div class="hbi-flex-item"><a onclick="_czc.push([\'_trackEvent\',\'进入游戏\', \'' + gameArr[i].appName + '\',\'' + aid + '\']);" href="' + gameArr[i].gameUrl + '"><img src="' + gameArr[i].iconUrl + '"></a><p>' + gameArr[i].appName + '</p></div>';
        }
        $('#hbiGameContainer').html(str);
        return str;
    }
    /**
     * 设置返回按钮信息
     */
    function setDomInfo(gameArr) {
        $('#hbiGameContainer img').attr('src', gameArr[0].imgeURL);
        $('#hbiGameContainer').attr('href', gameArr[0].link);
    }
    /**
     * 监听返回事件
     * 以及相关事件的初始化
     * @return {[type]} [description]
     */
    function eventInit() {
        if (!redLocalStorageBackInfo()) {
            // 仍要离开
            $(document).on('click', '#hbiExit', handleExitEvent);
            // 点击背景关闭
            $(document).on('click', '#hbiExitModal', handleExitModalEvent);
            // 点击关闭按钮
            $(document).on('click', '#hbiCancelButton', handleCancelButtonEvent);
            // 禁止事件冒泡
            $(document).on('click', '#exitDialog', handleExitDialogEvent);
            // 点击关注我们按钮
            //$(document).on('click','#hbiFollow',handleFollowEvent);
            // 点击二维码图片关闭
            //$(document).on('click','#hbiEWMWrapper',handleEWMWrapperEvent);
            // 今日不再提示
            $(document).on('click', '#avoidBackConfirm', handleNoEvent);

            $(function() {
                //在部分游戏上pushState方法无效
                // 导致刚进去游戏弹出返回游戏提示
                // 加上ready方法后 会生效，无法确定产生问题的原因
                setTimeout(function() {
                    window.history.pushState({
                        title: document.title,
                        url: location.href
                    }, document.title, location.href);
                    window.addEventListener && window.addEventListener("popstate", function(e) {
                        if (!e.state) {
                            try {
                                _czc.push(["_trackEvent", '打开弹窗', '打开弹窗', aid]);
                            } catch (err) {}
                            openModal();
                        }
                    });
                }, 2e3);
            })
        }
    }

    function handleExitEvent(e) {
        //仍要离开
        try {
            _czc.push(["_trackEvent", '离开游戏', '离开游戏', aid]);
        } catch (err) {}

        /**
         * 有一种情况，使用index2.html 跳转进入游戏
         * document.referrer 为index2.html，
         * 但是此时的history的length为2（其中一个为上一步中手动添加的）
         * 也就是说index2没有算入history，即referrer不为空，但是没有history.back
         *
         * 所以在最后里面再加一个判断是否是微信，如果是则wx.closeWindow()
         *
         * 另window.close() 好像只能关闭open方法打开的页面
         */
        if ("" === document.referrer && getUA() === 'weixin') {
            window.wx.closeWindow();
        } else {
            var currentUrl = window.location.href;
            window.history.back();
            setTimeout(function() {
                // if location was not changed in 100 ms, then there is no history back
                if (currentUrl === window.location.href) {
                    // 判断假如是在微信中则使用微信的关闭方法
                    if (getUA() === 'weixin') {
                        window.wx.closeWindow();
                    } else {
                        // window.close() 针对一般的窗口 好像无用
                        window.close();
                    }
                }
            }, 500);
        }
        e.stopPropagation();
    };

    function handleExitModalEvent(e) {
        //点击背景取消
        try {
            _czc.push(['_trackEvent', '取消弹窗', '点击背景取消', aid]);
        } catch (err) {}
        e.stopPropagation();
        openModal('close');
        window.history.pushState({
            title: document.title,
            url: location.href
        }, document.title, location.href);
    };

    function handleCancelButtonEvent(e) {
        //点击x号关闭
        try {
            _czc.push(['_trackEvent', '取消弹窗', '点击按钮取消', aid]);
        } catch (err) {}
        e.stopPropagation();
        openModal('close');
        window.history.pushState({
            title: document.title,
            url: location.href
        }, document.title, location.href);
    };

    function handleFollowEvent(e) {
        //关注我们按钮
        try {
            _czc.push(['_trackEvent', '关注我们', '关注我们', aid]);
        } catch (err) {}
        openModal('ewm');
        e.stopPropagation();
    };

    function handleEWMWrapperEvent() {
        //二维码
        openModal('ewmClose');
    };

    function handleExitDialogEvent(e) {
        //点击游戏主体 禁止事件冒泡
        e.stopPropagation();
    };

    function handleNoEvent(e) {
        //今日不再提示点击
        var isSelect = document.getElementById('avoidBackConfirm').checked;
        if (isSelect) {
            try {
                _czc.push(['_trackEvent', '不再提示', '不再提示', aid]);
            } catch (err) {}
            //存储localstorage
            setLocalStorageBackInfo();
        }
        e.stopPropagation();
    }


    function openModal(str) {
        switch (str) {
            case 'ewm':
                // 关注我们
                break;
            case 'ewmClose':
                // 二维码关注取消
                break;
            case 'close':
                $('#hbiExitModal').addClass('gone');
                break;
            default:
                $('#hbiExitModal').removeClass('gone');
        }
    }
    //存储localstorage
    function setLocalStorageBackInfo() {
        if (!window.localStorage) {
            return false;
        }
        var backinfo = window.localStorage.backinfo;
        var mark = false;
        if (!!backinfo) {
            //backinfo存在
            backinfo = JSON.parse(backinfo);
            if (backinfo.length > 0) {
                //有内容
                for (var i = 0, len = backinfo.length; i < len; i++) {
                    if (backinfo[i].appKey == appKey) {
                        mark = true;
                        backinfo[i].back = 1;
                        backinfo[i].t = new Date().getTime();
                        break;
                    }
                }
            }
        } else {
            backinfo = [];
        }
        if (!mark) {
            var item = { 'appKey': appKey, 'back': 1 };
            item.t = new Date().getTime();
            backinfo.push(item);
        }
        backinfo = JSON.stringify(backinfo);
        window.localStorage.setItem('backinfo', backinfo);
    }
    /**
     * 读取localstorage信息  查看是否已经存储过
     * 返回弹出游戏信息
     * 如果存储过则返回true
     * 其余返回false
     * @return {[type]} [description]
     */
    function redLocalStorageBackInfo() {
        if (!window.localStorage) {
            return false;
        } else {
            var backinfo = window.localStorage.backinfo;
            if (!!backinfo) {
                backinfo = JSON.parse(backinfo);
                for (var i = 0, len = backinfo.length; i < len; i++) {
                    if (backinfo[i].appKey == appKey && backinfo[i].back == 1) {
                        if (!!backinfo[i].t) {
                            // return compareTime(backinfo[i].t);
                            return isToday(backinfo[i].t);
                        } else {
                            return false;
                        }
                        break;
                    }
                }
                return false;
            }
            return false;
        }
    }

    /**
     * 暂时无效
     * 比较local存储的时间和当前时间
     * 是否为一天
     * @param  {[type]} t [description]
     * @return {[type]}   [description]
     */
    function compareTime(t) {
        var oneDay = 1000 * 60 * 60 * 24;
        var oldTime = new Date(t);
        var nowTime = new Date();
        var cha = nowTime.getTime() - oldTime.getTime();
        if (cha > oneDay) {
            return false;
        } else if (nowTime.getDate() == oldTime.getDate()) {
            return true;
        } else {
            return false;
        }

    }
    /**
     * 获取ua
     * @return {[type]} [description]
     */
    function getUA() {
        var usa = window.navigator.userAgent.toLocaleLowerCase();
        if (usa.indexOf('micromessenger') > 0 && usa.indexOf('dwjia') < 0) {
            return 'weixin';
        } else {
            return 'other';
        }
    }

    /**
     * 检测传入的日期是否为今天
     * @param    str [毫秒数]
     * @return {Boolean}     [description]
     */
    function isToday(str) {
        var d = new Date(str);
        var todaysDate = new Date();
        if (d.setHours(0, 0, 0, 0) == todaysDate.setHours(0, 0, 0, 0)) {
            return true;
        } else {
            return false;
        }
    }
    return pageBack;
});