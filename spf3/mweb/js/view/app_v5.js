require.config({
    // baseUrl: 'http://h5.g1758.cn/static/spf/mweb/js',
    baseUrl: 'mweb/js',
    paths: {
        libs: 'libs',
        data: 'data',
        common: 'common',
        view: 'view',

        // jquery: 'jquery',
        jquery: '//res.1758.com/libs/jquery/2.2.4/jquery.min',
        // fastclick: 'libs/fastclick',
        'jquery.modal': 'plugin/weui/js/modal',
    },
    shim: {
        // fastclick: {
        //     exports: 'FastClick'
        // },
        'jquery.cookie': {
            deps: ['jquery'],
            exports: 'jQuery.fn.cookie'
        },
        'jquery.modal': {
            deps: ['jquery'],
            exports: '_'
        },
    }
});
// plugin/weui/js/modal
require(['jquery', 'common/eventBase', 'data/getInfoData', 'view/modalBtn', 'view/extraFeatures', 'view/pageBack', 'view/chat', 'view/signUp', 'view/sdkFn', 'plugin/weui/js/modal'], function($, EventBase, InfoData, ModalBtn, ExtraFeatures, PageBack, KFChat, SignUp, SDKFN) {
    // FastClick.attach(document.body);
    // 防止被嵌套劫持
    if (top != self) {
        top.location = self.location;
    }
    // if(typeof HLMY_GAMEINFO === 'string'){
    // 	HLMY_GAMEINFO = JSON.parse(HLMY_GAMEINFO);
    // }
    var dataPara = {
        verLogin: '',
        deffConfig: $.Deferred(), //配置请求
        //deffUser 		: 	$.Deferred(),	//用户gid请求
        deffLoad: $.Deferred(), //游戏页面load完
        deffIosBridge: $.Deferred(), //ios bridge注入对象
        clientHeadArr: [],
        locationUrl: encodeURIComponent(location.href),
        appkey: '', //游戏
        title: '1758微游戏', //游戏title	
        wxjsconfig: {}, //微信分享的配置信息
        trialEnable: HLMY_CONFIG.GAME_INFO.trialEnable ? true : false,
        trialTime: HLMY_CONFIG.GAME_INFO.trialTime ? HLMY_CONFIG.GAME_INFO.trialTime : '30', //（暂不启用）实现自动登录的时候用到的时间间隔
        sharePageCpurl: '', //游戏的分享地址
        shareText: [], //分享语的数组
        customizeText: [], //自定义参数使用的默认分享语数组(无效)
        gid: HLMY_CONFIG.USER_INFO.gid, //用户的gid
        userState: HLMY_CONFIG.USER_INFO.userStatus, //用户类型（0则为试玩用户）
        sid: '', //当前分享语的id
        userTerminal: '', //用户终端类型
        src: '//wx.1758.com/game/platform/v2.0/user/flogin?appKey=',
        chn: '',
        timeoutId: '',
        taTip: false, //废弃
        panelEnable: HLMY_CONFIG.GAME_INFO.panelEnable ? true : false, //是否使用浮标
        chatroomEnable: false, //是否需要聊天室  暂时无用
        isCpShareInfo: false, //控制是否有cp传递过来的信息
        messageFromCp: { //从cp传递过来的所有信息
            isCp: false, //从cp地方获取到内容后 置为true
        },
        baseState: {
            isState: false,
            state: ''
        },
        defaultText: {
            sharedesc: "1758微游戏",
            shareimg: "http://images.1758.com/images/48.ico",
            sharelink: "http://wx.1758.com",
            sharesumary: "1758微游戏",
            sharetitle: "1758微游戏，即点即玩",
            sid: ""
        },
        simulateNoTip: false, //模拟game分享弹框中不再提示的状态（默认是false）
        shareReasonType: 0, //分享的原因，默认为0正常分享，分享弹框出来后30秒内为1
        shareImageEnable: false, //分享是否能以图片的方式进行分享，ture为能 false为不能
        customizeShareEnable: false, //自定义图片分享  true 为可以  false 为不可以 
        // 游戏内用户点击关注二维码，cp通过sdk调用follow方法使用的二维码地址
        // sdk关注二维码信息
        sdkFollow: {
            "openAppId": 0,
            "subscribeImgUrl": "http://images.1758.com/image/20161019/open_1_e07bcd50b8ea140797cf9f776daa1274.png",
            "sceneId": 0,
            "sceneType": 1,
            "sceneType1758": 0
        },
        // 被动订阅二维码，根据查询结果是否显示二维码以供关注
        subscribeFollow: {
            "openAppId": 0,
            "subscribeImgUrl": "",
            "sceneId": 0,
            "sceneType": 1,
            "sceneType1758": 10
        }
    };
    /**
     * 禁止橡皮筋效果
     */
    $(document).on('touchmove', function(e) {
        e.preventDefault();
    });
    /**
     * 禁止橡皮筋效果
     * e.currentTarget.scrollTop  scroll滚动距离 距上多少
     * e.currentTarget.offsetHeight 有滚条div距文档顶部多少距离
     * e.currentTarget.scrollHeight 滚条div里面的真正高度
     *
     * touchstart的时候 scrollTop为0 说明滚动距离为0,然后置为1
     * top+offsetHeight = scrollHeight 说明已经滚动到底了
     * 
     */
    $('body').on('touchstart', '#modalMainContainer,#vipInfoContainer .vipinfo-list,.inner-kefu-container,.exch-wrapper,#warankBody,.pScroll', function(e) {
        if (e.currentTarget.scrollTop === 0) {
            e.currentTarget.scrollTop = 1;
        } else if (e.currentTarget.scrollHeight === e.currentTarget.scrollTop + e.currentTarget.offsetHeight) {
            e.currentTarget.scrollTop -= 1;
        }
    });
    /**
     * 禁止橡皮筋效果
     * scrollHeight = offsetHeight 说明滚条区域内容少 没有滚动条
     * 
     * scrollTop为0 说明滚动距离为0，禁止默认行为，防止出现橡皮筋效果
     * top+offsetHeight = scrollHeight 说明已经滚动到底了 禁止默认行为，防止出现橡皮筋效果
     *
     * 中间部分 然后阻止冒泡到document上面
     */
    $('body').on('touchmove', '#modalMainContainer,#vipInfoContainer .vipinfo-list,.inner-kefu-container,.exch-wrapper,#warankBody,.pScroll', function(e) {
        // vip消息通知用户可以滑动
        if (e.currentTarget.className == 'vipinfo-list') {
            e.stopPropagation()
            return;
        }
        if (e.currentTarget.scrollHeight !== e.currentTarget.offsetHeight) {
            if (e.currentTarget.scrollTop === 0) {
                e.preventDefault();
            } else if (e.currentTarget.scrollHeight === e.currentTarget.scrollTop + e.currentTarget.offsetHeight) {
                e.preventDefault();
            } else {
                e.stopPropagation();
            }
        }
    });

    $('body').on('touchmove', '#payWrapper', function(e) {
        e.stopPropagation();
    });
    init();
    //初始化函数
    function init() {
        dataPara.userTerminal = EventBase.userTerminal(); //得到用户使用的是客户端还是微信
        dataPara.appkey = EventBase.getParameters('appKey'); //获取微信分享config用
        dataPara.chn = EventBase.getParameters('chn');
        var ux = dataPara.userTerminal;
        //初始化一些全局对象
        initWindow(ux);
        //iframe页面绑定loadend事件
        loadEndPage();
        //检测是否一键登录过
        hasOneKeyLogin();
        //获取微信配置信息
        getWxConfigAndText();

        $.when(dataPara.deffLoad).done(function() {
            //浮标初始化信息
            ModalBtn.init(dataPara.panelEnable, dataPara.clientHeadArr);
        });
        setShareInfo();
        //接受cp传递info
        cpInfoPostInit();

        // 获取关注二维码用到的图片url地址链接
        getEwmImg();
        //页面返回事件初始化
        PageBack.init();
        //模拟游戏 弹出分享提示框 初始化
        simulateGameShareInit();
        // 页面vip弹框提示消息初始化
        setTimeout(function() {
            // 延迟执行
            ExtraFeatures.vipInfoInit();
        }, 1000 * 5);
        // websocket初始化
        KFChat.wsInit(dataPara.panelEnable);
        // 设置提醒初始化
        ExtraFeatures.setRemind();
        // 活动请求是否展示、
        if (HLMY_CONFIG.PROMOTION && HLMY_CONFIG.PROMOTION.enable) {
            setTimeout(promotionInit, 5 * 1000);
        };
        // 签到
        SignUp.init();
        // 充值活动初始化
        rechargeInit();
    }
    //config完成之后分享语注入
    function setShareInfo() {
        var ux = dataPara.userTerminal;
        $.when(dataPara.deffConfig).done(function() {
            //分享语注入信息
            if (ux == 'iphoneclient' || ux == 'androidclient') {
                if (ux == 'iphoneclient') {
                    $.when(dataPara.deffIosBridge).done(function() {
                        pareShareText();
                    })
                } else {
                    //客户端分享语的注入
                    pareShareText();
                }
            } else {
                //进行微信分享语的注入	
                changeWxShareInfo();
            }
        }).fail(function() {

        });
    }
    //获取游戏的wx配置和分享语
    function getWxConfigAndText() {
        // var url = '//wx.1758.com/game/platform/getShareInfoAndWxJsconfig';
        var url = '//wx.1758.com/play/game/getShareInfoAndWxJsconfig';
        var infos = {
            appKey: dataPara.appkey,
            pageUrl: encodeURIComponent(location.href),
            'wyOpenAppId': HLMY_CONFIG.SHARE_INFO.wyOpenAppId
        };
        InfoData.getShareInfo(url, infos, handleWxConfig);
    }
    //处理得到的微信配置信息和分享语
    function handleWxConfig(data) {
        if (data.result == 1) {
            // 是否可以进行以图片的方式进行分享
            dataPara.shareImageEnable = data.shareImageEnable;
            // 是否可以进行自定义图片分享
            dataPara.customizeShareEnable = data.customizeShareEnable;

            if (data.shareItems.length == 0) {
                dataPara.shareText.push(dataPara.defaultText);
            } else {
                handleWxShareInfoUrl(data.shareItems);
                dataPara.shareText = data.shareItems;
            }
            dataPara.sharePageCpurl = data.sharePageCpurl;
            dataPara.title = data.title;
            dataPara.wxjsconfig = data.wxJsConfig;

            //微信的分享配置信息
            try {
                window.wx.config(dataPara.wxjsconfig);
            } catch (err) {
                console.log(err);
            }
            dataPara.deffConfig.resolve();
        }
    }
    /**
     * 处理从数据库拿到的分享语信息的url
     * 由于受微信的分享机制的影响，
     * 分享的url必须和当前的url域名一致
     * @param  {[type]} data [description]
     * @return {[type]}      [description]
     */
    function handleWxShareInfoUrl(data) {
        var sharelink;
        var urlObj;
        for (var i = 0, len = data.length; i < len; i++) {
            sharelink = data[i].shareLink;
            if (sharelink.indexOf('http') == 0 || sharelink.indexOf('https') == 0) {
                urlObj = new URL(sharelink);
                data[i].shareLink = sharelink.replace(urlObj.origin, document.location.origin)
            }
        }
    }
    //获取用户的gid信息并且添加到分享link上面
    //废弃
    function getUserInfo() {
        var userInfo;
        //数据页面地址，主要是通过改地址获取gid
        var dataurl = 'http://wx.1758.com/game/platform/user/shareIframeNotify';
        //通过window.name属性进行跨域的数据读取
        var state = 0,
            giframe = document.createElement('iframe'),
            loadfn = function() {
                if (state === 1) {
                    dataPara.gid = giframe.contentWindow.name; // 读取数据
                    //changeLink(userinfo);
                    dataPara.deffUser.resolve();
                    document.body.removeChild(giframe);
                } else if (state === 0) {
                    state = 1;
                    giframe.contentWindow.location = "http://h5.g1758.cn/spf/blank.html"; // 设置的代理文件
                }
            };
        giframe.src = dataurl;
        giframe.style.display = 'none';
        if (giframe.attachEvent) {
            giframe.attachEvent('onload', loadfn);
        } else {
            giframe.onload = loadfn;
        }
        document.body.appendChild(giframe);
    }
    //微信的注入信息
    function changeWxShareInfo() {
        var info = randomGetShareContent();
        var wx = window.wx;
        if (!wx) { return; }
        wx.ready(function() {
            //关闭微信功能
            hideWxMenu();
            //分享给朋友
            wx.onMenuShareAppMessage({
                title: info.title, // 分享标题
                desc: info.desc, // 分享描述
                link: info.link + '&stype=appmessage&superlongId=' + Math.ceil(Math.random() * 10000000000),
                imgUrl: info.imgUrl,
                type: 'link',
                dataUrl: '',
                success: function() {
                    exec_iframe('appmessage', dataPara.sharePageCpurl);
                    changeWxShareInfo();
                    sendMessageServer('appmessage', 0);
                },
                cancel: function() {
                    changeWxShareInfo();
                    sendMessageServer('appmessage', 1);
                }
            });

            //分享到朋友圈
            wx.onMenuShareTimeline({
                title: info.title, // 分享标题
                link: info.link + '&stype=timeline&superlongId=' + Math.ceil(Math.random() * 10000000000),
                imgUrl: info.imgUrl,
                success: function() {
                    exec_iframe('timeline', dataPara.sharePageCpurl);
                    changeWxShareInfo();
                    sendMessageServer('timeline', 0);
                },
                cancel: function() {
                    changeWxShareInfo();
                    sendMessageServer('timeline', 1);
                }
            });
            //分享到qq好友
            wx.onMenuShareQQ({
                title: info.title, // 分享标题
                desc: info.desc, // 分享描述
                link: info.link + '&stype=qq&superlongId=' + Math.ceil(Math.random() * 10000000000), // 分享链接
                imgUrl: info.imgUrl, // 分享图标
                success: function() {
                    exec_iframe('qq', dataPara.sharePageCpurl);
                    changeWxShareInfo();
                    sendMessageServer('qq', 0);
                },
                cancel: function() {
                    changeWxShareInfo();
                    sendMessageServer('qq', 1);
                }
            });
            //分享到qq空间
            wx.onMenuShareQZone({
                title: info.title, // 分享标题
                desc: info.desc, // 分享描述
                link: info.link + '&stype=qzone&superlongId=' + Math.ceil(Math.random() * 10000000000), // 分享链接
                imgUrl: info.imgUrl, // 分享图标
                success: function() {
                    // 用户确认分享后执行的回调函数
                    exec_iframe('qzone', dataPara.sharePageCpurl);
                    changeWxShareInfo();
                    sendMessageServer('qzone', 0);
                },
                cancel: function() {
                    // 用户取消分享后执行的回调函数
                    changeWxShareInfo();
                    sendMessageServer('qzone', 1);
                }
            });
        });
    }
    //得到随机分享语
    function randomGetShareContent() {
        //先判断是否是cp传递过来的内容
        //kk 是一个对象包含title desc link sid imgUrl
        var kk;
        kk = EventBase.randomShareContent(dataPara.shareText, dataPara.gid);
        //根据特殊渠道的特殊需求，替换chn的值
        if (!!kk && !!kk.link) {
            kk.link = changeShareChn(kk.link);
        }
        // 通用state设置
        if (dataPara.baseState.isState) {
            kk.link = changeLinkState(kk.link, dataPara.baseState.state);
        }
        //如果cp自定义分享语则添加cp的分享语
        if (dataPara.isCpShareInfo) {
            var cptitle = dataPara.messageFromCp.title,
                cpdesc = dataPara.messageFromCp.desc,
                imgUrl = dataPara.messageFromCp.imgUrl,
                state = dataPara.messageFromCp.state;
            if (!!kk) {
                (!!cptitle && typeof(cptitle) == 'string') ? (kk.title = cptitle) : '';
                (!!cpdesc && typeof(cpdesc) == 'string') ? (kk.desc = cpdesc) : '';
                (!!imgUrl && typeof(imgUrl) == 'string') ? (kk.imgUrl = imgUrl) : '';
                if (!!state && typeof(state) == 'string') {
                    // if(kk.link.indexOf('?') > 0){
                    // 	kk.link += '&state='+state;
                    // }else{
                    // 	kk.link += '?state='+state;
                    // }
                    kk.link = changeLinkState(kk.link, state);
                }
                kk.sid = '';
            }
            dataPara.isCpShareInfo = false;
        }
        if (!kk) {
            dataPara.sid = '';
        } else {
            dataPara.sid = kk.sid;
        }
        return kk;
    }
    //向cp发送分享成功的请求
    function exec_iframe(shareType, cpShareUrl) {
        if (HLMY_CONFIG.SHARE_INFO.version == '2') {
            EventBase.exec_iframe(shareType, cpShareUrl);
        } else {
            //新的通知cp方法
            // 2.0以上的版本通过postmessage进行传递消息
            setInfoForSend(shareType);
        }
        //隐藏信息提示页面
        hideShareTipInfo();
    }
    //每次分享成功之后执行一次发送给服务器  0成功  1取消
    //name 为分享到哪里的字符串
    //falg 为成功或取消
    function sendMessageServer(name, flag) {
        // $.get('//wx.1758.com/game/platform/user/logShareAction',{
        // 	'gid': dataPara.gid,
        // 	'appKey': dataPara.appkey,
        // 	'type': name,
        // 	'code': flag,
        // 	'sid': dataPara.sid
        // 	},function(data){
        // 		if(data.msg == 'ok'){
        // 			return;
        // 		};
        // 	},'jsonp');

        // 	
        // 	微信好友  	1
        // 	朋友圈		2
        // 	微信收藏    4
        // 	QQ空间 		8
        // 	qq好友		16
        // 	微博分享	32
        // 	未知		-1
        var shareType = {
            'appmessage': 1,
            'timeline': 2,
            'qq': 16,
            'qzone': 8
        }[name];
        $.get('//wx.1758.com/play/share/shareResult.jsonp', {
            'appKey': dataPara.appkey,
            'gid': dataPara.gid, //用户id
            'shareType': shareType, //分享类型
            'reasonType': dataPara.shareReasonType, //分享原因  默认是0(正常分析) 提醒为1(分享弹框30秒内)
            'shareId': dataPara.sid, //分享语的id
            'code': flag //成功或者取消
        }, function(data) {

        }, 'jsonp');
    }
    //格式化json对象，注入客户端分享内容
    function pareShareText() {
        var shareAppmessageInfo, shareTimelineInfo, temp;
        var data = randomGetShareContent();
        shareTimelineInfo = JSON.stringify(data);
        // temp = data.title;
        // data.title = data.desc;
        // data.desc = temp;
        shareAppmessageInfo = JSON.stringify(data);
        if (typeof(window.weixinBridge) != 'undefined') {
            if (window.weixinBridge.invokeWx) {
                window.weixinBridge.invokeWx(shareAppmessageInfo, shareTimelineInfo);
            }
        } else {
            if (window.ios) {
                window.ios.callHandler('shareTimeline', shareTimelineInfo);
                window.ios.callHandler('shareMessage', shareAppmessageInfo);
            }
        }
    }
    //初始化一些全局对象
    function initWindow(ux) {
        var win = window;
        var clientRes;
        if (ux == 'iphoneclient' || ux == 'androidclient') {
            win.clientSuccessInfo = function(type) {
                var _type = '';
                type = type.toLowerCase();
                switch (type) {
                    case 'sendappmessage':
                        _type = 'appmessage';
                        break;
                    case 'sharetimeline':
                        _type = 'timeline';
                        break;
                    case 'shareqq':
                        _type = 'qq';
                        break;
                    case 'shareweiboapp':
                        break;
                    case 'shareqzone':
                        _type = 'qzone';
                        break;
                }
                //发送给cp，执行cp的回调函数
                exec_iframe(_type, dataPara.sharePageCpurl);
                //随机分享语
                pareShareText();
                //发送给服务器记录信息
                sendMessageServer(_type, 0);
            };
            //假如没有注册分享信息，客户端调用该方法进行注册信息
            win.clientGetShareInfo = function() {
                    pareShareText();
                }
                //ios 客户端分享对象
            win.connectWebViewJavascriptBridge = function(callback) {
                    if (window.WebViewJavascriptBridge) {
                        callback(WebViewJavascriptBridge)
                    } else {
                        document.addEventListener('WebViewJavascriptBridgeReady', function() {
                            callback(WebViewJavascriptBridge)
                        }, false)
                    }
                }
                //安卓客户端添加大桌面方法，假如没有自动执行 则客户端调用该方法
            win.clientdesktopInfo = function() {
                androidPutDesktop();
            }
            if (ux == 'iphoneclient') {
                //ios 客户端注册对象
                handleIosClient();
                dataPara.panelEnable = false;
            } else if (ux == 'androidclient') {
                dataPara.panelEnable = false;
                //Android 添加到桌面
                androidPutDesktop();
                try {
                    // 判断是否需要浮标
                    // 返回{enable:true, option:[true, true, true, true, true]}
                    clientRes = android_panelAssistant.getH5PanelOption();
                    if (typeof clientRes === 'string') {
                        clientRes = JSON.parse(clientRes)
                    }
                    if (clientRes.enable) {
                        dataPara.clientHeadArr = clientRes.option;
                        dataPara.panelEnable = true;
                    }
                } catch (e) {}
            }
        }
        $(document).on('click', '#shareTipInfo', function() {
            $(this).addClass('gone');
        })

    }
    //ios客户端注册对象
    function handleIosClient() {
        window.connectWebViewJavascriptBridge(function(bridge) {
            bridge.init(function(message, responseCallback) {
                if (responseCallback) {
                    responseCallback("")
                }
            })
            window.ios = bridge;
            ios.registerHandler('clientSuccessInfo', function(data, responseCallback) {
                window.clientSuccessInfo(data);
            });
            ios.registerHandler('clientGetShareInfo', function(data, responseCallback) {
                window.clientGetShareInfo();
            });
            dataPara.deffIosBridge.resolve();
        });
    }
    //androlid 放到桌面
    // 直接通过配置文件获取
    function androidPutDesktop() {
        var jskk = {
            'gameName': HLMY_CONFIG.GAME_INFO.name,
            'gameUrl': HLMY_CONFIG.GAME_INFO.entryUrl,
            'imgUrl': HLMY_CONFIG.GAME_INFO.iconUrl,
            'tp': ''
        };
        jskk = JSON.stringify(jskk);
        try {
            android_tw_system.addDeskIconNew(jskk);
        } catch (e) {
            //TODO handle the exception
        }

        // $.get('//wx.1758.com/game/api/app/getAppInfoJsonP', {
        //     'appKey': dataPara.appkey
        // }, function(data) {
        //     if (data.code == 0) {
        //         var jskk = {
        //             'gameName': data.wxApp.name,
        //             'gameUrl': data.wxApp.backGameUrl,
        //             'imgUrl': data.wxApp.iconUrl,
        //             'tp': ''
        //         };
        //         jskk = JSON.stringify(jskk);
        //         try {
        //             android_tw_system.addDeskIconNew(jskk);
        //         } catch (e) {
        //             //TODO handle the exception
        //         }
        //     }
        // }, 'jsonp');
    }

    /**
     * 动态jsp页面用到（iframe加载完成则消失load）
     */
    function loadEndPage() {
        var gframe = document.getElementById('gameframe');
        if (gframe.attachEvent) {
            gframe.attachEvent('onload', hideLoading);
        } else {
            gframe.onload = hideLoading;
        }
        setTimeout(function() {
            dataPara.deffLoad.resolve();
        }, 8000)
    }
    //gafarme游戏加载完成之后 
    function hideLoading() {
        $('#loading-div').css('display', 'none');
        $('#gameframe').css('display', 'block');
        dataPara.deffLoad.resolve();
    }

    /**
     * 如果是试玩状态
     * 则进行一些关于试玩的状态初始化
     * 悬浮登录状态，绑定悬浮dom点击事件
     * 
     */
    function hasOneKeyLogin() {
        if (dataPara.trialEnable) {
            //一键试玩状态登录了
            $.when(dataPara.deffLoad).done(function() {
                //添加顶部悬浮登录提示框(废弃)
                //$('body').prepend(dyCreateLoginTip());
                // 试玩悬浮框dom点击事件绑定
                // bindLogTipClick();

                // 绑定手机号登录事件
                bindLoginMobile();
                dataPara.timeoutId = setTimeout(function() {
                    //悬浮提示按钮关闭
                    // $('#logTip').remove();
                    // 弹出登录框（废弃）
                    // getWeuiModalForTrial();
                    // 弹出登录框
                    $('#loginContainer').removeClass('gone');
                }, dataPara.trialTime * 1000);
            })
        }
        // 试玩状态关闭按钮绑定
        $(document).on('click', '#oneKeyClose', function() {
            $('#loginContainer').addClass('gone');
        })
    }
    // 动态登录2
    // 试玩状态到，弹出提示登录框
    // state: 暂时废弃
    function getWeuiModalForTrial() {
        $.modal({
            wrapperCssName: 'modal-trial',
            text: '<p>本游戏由1758提供</p><p>为保存进度,请登录后在操作</p>',
            title: '<img src="http://wx.1758.com/game/h5/images/head-icon.png"/>',
            buttons: [
                //		  {
                //	        text: '退出游戏',
                //	        className: "default",
                //	        onClick: function(){
                //	        	window.opener = null;
                //	        	window.open(" ","_self");
                //	        	window.close();
                //	        }
                //	      },
                {
                    text: '前去登录',
                    className: "primary",
                    onClick: function() {
                        top.window.document.location.href = '//wx.1758.com/game/platform/v3.0/user/flogin?loginNow=true&appKey=' + dataPara.appkey + '&chn=' + dataPara.chn;
                    }
                }
            ]
        });
    }
    // 动态登录1 
    // 试玩状态到期之后，或者试玩状态中点击登录，
    // 则弹出的登录弹框
    // state: 废弃
    function dyCreateLoginHtml() {
        var str = '<div id="login-bc" class="login-bc" style="display:block">' +
            '<div id="login-main" class="login-main">' +
            '<p class="headimg">' +
            '<img src="http://images.1758.com/images/login_1758_1.png" style="width:35%;">' +
            '</p>' +
            '<hr style="border:1px rgb(247,247,247) solid;">' +
            '<p id="tip" class="tip-1">该游戏由1758.com提供，登录后即可继续操作</p>' +
            '<p class="tip-2">一键登录</p>' +
            '<div class="l-img" style="text-align:center;margin-bottom:10px;">' +
            '<span>' +
            '<a id="qqLogin">' +
            '<img src="http://images.1758.com/images/login_QQ1.png" style="width:20%;max-height:60px;margin-right:20px;border-color:#fff">' +
            '</a>' +
            '</span>' +
            '<span id="wxLogin" class="wxlogin">' +
            '<a>' +
            '<img src="http://images.1758.com/images/login_WX1.png" style="width:20%;max-height:60px;">' +
            '</a>' +
            '</span>' +
            '<span>' +
            '<a id="weiboLogin">' +
            '<img src="http://images.1758.com/images/login_XL1.png" style="width:20%;max-height:60px;margin-left:20px;">' +
            '</a>' +
            '</span>' +
            '</div>' +
            '</div>' +
            '</div>'
        return str;
    };
    /**
     * 动态登录1 dom事件处理
     * 试玩状态弹出登录框后，根据ua等显示微信按钮，并且绑定登录按钮
     *
     * 需跟着 dyCreateLoginHtml 方法一起调用
     * state: 废弃
     */
    function loginEvent() {
        var ua = dataPara.userTerminal;
        if (ua == 'weixin' || ua == 'iphoneclient' || ua == 'androidclient') {
            $('#wxLogin').css('display', 'inline');
            $(document).on('click', '#wxLogin', function() {
                try {
                    android_wxgame_auth.weixinLogin();
                    android_tw_system.toast("通过微信登录，可看到好友的得分排名哦", 1);
                } catch (exp) {
                    try {
                        window.ios.callHandler('weixinLogin', {}, function(res) {});
                    } catch (exp2) {
                        location.href = "//wx.1758.com/game/platform/v3.0/user/login?appKey=" + dataPara.appkey + "&state=&chn=onekey&share=";
                    }
                }
            })
        }
        $(document).on('click', '#qqLogin', function() {
            location.href = '//wx.1758.com/game/platform/v3.0/qq/login?appKey=' + dataPara.appkey + '&state=&chn=&share='
        })
        $(document).on('click', '#weiboLogin', function() {
            location.href = "//wx.1758.com/game/platform/v3.0/weibo/login?appKey=" + dataPara.appkey + "&state=&chn=&share=";
        })
    }
    /**
     * 悬浮在顶部的试玩提示
     * 用户可以关闭或者立即去登录
     * @return {[type]} [description]
     */
    function dyCreateLoginTip() {
        var str = '<div id="logTip" class="logtip" style="display:block">' +
            '<img src="http://wx.1758.com/game/h5/images/head-icon.png">' +
            '<div class="wrap">' +
            '<div id="tip-close" class="close"><img src="http://images.1758.com/images/closeLogin.png"></div>' +
            '<span>登录后可继续进行上次游戏进度</span>' +
            '<div id="tip-log" class="tip-log">' +
            '<span>' +
            '<span class="login-btn">登录</span>' +
            '<span id="countdown"></span>' +
            '</span>' +
            '</div>' +
            '</div>' +
            '</div>'
        return str;
    }
    /**
     * 悬浮在顶部的试玩提示
     * 用户试玩状态则绑定可删除和登录的按钮
     */
    function bindLogTipClick() {
        $(document).on('click', '#tip-close', function() {
            $('#logTip').remove();
        });
        $(document).on('click', '#tip-log', function() {
            $('#logTip').remove();
            clearTimeout(dataPara.timeoutId);
            //跳转登录链接
            top.window.document.location.href = '//wx.1758.com/game/platform/v3.0/user/flogin?loginNow=true&appKey=' + dataPara.appkey + '&chn=' + dataPara.chn;
            //登录事件
        });
    }


    //初始化与cp之间的信息交流
    function cpInfoPostInit() {
        if (typeof window.addEventListener != 'undefined') {
            window.addEventListener('message', receiveCpInfo, false);
        } else if (typeof window.attachEvent != 'undefined') {
            window.attachEvent('onmessage', receiveCpInfo);
        }
    }
    //隐藏信息提示页
    function hideShareTipInfo() {
        var shareTip = $('#shareTipInfo');
        if (shareTip) {
            shareTip.addClass('gone');
        }
    }
    //接受cp信息
    function receiveCpInfo(e) {
        var dataInfo = e.data,
            test = {},
            func;
        func = {
            // 设置分享的奖励展示 暂时废弃
            setShareReward: function(arg) {
                if (Object.prototype.toString.call(arg) === '[object Array]') {
                    var str = '';
                    for (var i = 0, len = arg.length; i < len; i++) {
                        str += '<p>' + arg[i] + '</p>'
                    }
                    $('#rewardWrapper').html(str);
                }
                $('#shareTipInfo').removeClass('gone');
            },
            showShareTipInfo: function() {
                $('#shareTipInfo').removeClass('gone');
            }
        }
        if (dataInfo.hlmy) {
            //接受cp信息
            if (dataInfo.type == 'share') {
                if (dataInfo.value.share == 'shareInfo') {
                    var cpinfo = dataInfo.value.shareInfo;
                    dataPara.messageFromCp = cpinfo;
                    dataPara.isCpShareInfo = true;
                    setShareInfo();
                    if (typeof(cpinfo.tipInfo) == 'boolean' && cpinfo.tipInfo) {
                        //true 显示信息
                        //func.setShareReward(cpinfo.reward);
                        var ua = dataPara.userTerminal;
                        if (dataPara.shareImageEnable && (ua == 'others' || ua == 'weixin') && cpinfo.type == 'img') {
                            // 服务器明确告诉可以进行图片分享，并且是
                            // 在非客户端状态下，并且type是图片分享，则进行图片弹出
                            // 其他情况进行小手提示分享
                            // 指定分享的图片id 只有在图片分享的时候该参数有用
                            var id = cpinfo.id || '';
                            showShareImg(cpinfo.state, id);
                        } else if (dataPara.customizeShareEnable && (ua == 'others' || ua == 'weixin') && cpinfo.type == 'pic') {
                            // 1，服务器允许，
                            // 2，非客户端
                            // 3，类型为pic
                            showSharePicture(cpinfo.picUrl);
                        } else {
                            //先尝试调用客户端的分享功能，弹出分享按钮
                            //其他情况下则调用h5页面的分享提示
                            try {
                                android_panelAssistant.share();
                            } catch (err) {
                                func.showShareTipInfo();
                            }
                        }

                    }
                } else if (dataInfo.value.share == 'sharePicture') {
                    var cpinfo = dataInfo.value.shareInfo;
                    showSharePicture(cpinfo.imgUrl);
                }
            } else if (dataInfo.type == 'baseState') {
                if (dataInfo.value.isState) {
                    dataPara.baseState.isState = dataInfo.value.isState;
                    dataPara.baseState.state = dataInfo.value.state;
                    setShareInfo();
                }
            } else if (dataInfo.type == 'pay' && dataInfo.value.fn == 'payInfo') {
                var para = dataInfo.value.args;
                // 日志记录
                logger('http://nlog.1758.com/log?type=pay&actionName=iframeReceive&appKey=' + dataPara.appkey + '&gid=' + dataPara.gid + '&gw=' + para.hlmy_gw + '&message=iframeReceive&openId=&orderId=' + para.paySafecode + '&time=' + new Date().getTime());

                showPayPageInfo(para);
            } else if (dataInfo.type == 'pay' && dataInfo.value.fn == 'payCancleBack') {
                // 日志记录
                logger('http://nlog.1758.com/log?type=pay&actionName=iframeCancel&appKey=' + dataPara.appkey + '&gid=' + dataPara.gid + '&gw=&message=iframeCancel&openId=&orderId=&time=' + new Date().getTime());

                createDelPayPage('del');
            } else if (dataInfo.type == 'follow') {
                followWxNum();
            } else if (dataInfo.type == 'fn') {
                SDKFN[dataInfo.value.fn](dataInfo.value.args);
            }
            //  		else if(dataInfo.type == 'fn'){
            //  			func[dataInfo.value.fn].apply(window,dataInfo.value.args);
            //  		}
        }
    }
    //根据cp的调用 显示 支付页面
    function showPayPageInfo(para) {
        var rcontentime = '&rcontentimeId=' + Math.ceil(Math.random() * 1000000);
        //支付页面有多个（需要根据不同的情况判断）
        var version = HLMY_CONFIG.PAY_INFO.version; //默认支付版本
        var urlPara = '';
        var src = '';
        if (HLMY_CONFIG.PAY_INFO.forceEnable) {
            //使用强制支付版本
            version = HLMY_CONFIG.PAY_INFO.forceVersion;
        }
        // 参数拼接
        for (var p in para) {
            if (para.hasOwnProperty(p)) {
                if (p == 'appKey') {
                    continue;
                } else {
                    urlPara += '&' + p + '=' + para[p];
                }
            }
        }

        if (version == 3) {
            src = '//wx.1758.com/pay/buy?hlmy_from=' + HLMY_CONFIG.GAME_INFO.hlmy_from + '&v1758=3&appKey=' + dataPara.appkey;
            src = src + urlPara + rcontentime;
            createDelPayPage('show', src);
        } else if (version == 2) {
            src = '//wx.1758.com/pay/buy?hlmy_from=' + HLMY_CONFIG.GAME_INFO.hlmy_from + '&appKey=' + dataPara.appkey;
            src = src + urlPara + rcontentime;
            top.window.location.href = src;
        } else {
            src = '//wx.1758.com/pay/payChoice?hlmy_from=' + HLMY_CONFIG.GAME_INFO.hlmy_from;
            src = src + urlPara + rcontentime;
            createDelPayPage('show', src);
        }
    }
    /**
     * 动态添加pay的iframe
     * @param  {[type]} flag [description]
     * @param  {[type]} src  [description]
     * @return {[type]}      [description]
     */
    function createDelPayPage(flag, src) {
        var bl = '';
        var str = '';
        flag === 'del' ? bl = false : bl = true;
        if (bl) {
            // 如果已经存在就不做处理了
            // if ($('#payWrapper').length > 0) { return; }
            //true 为添加payiframe
            // str += '<div id="payWrapper" class="pay-wrapper pScroll" style="height: 100%;width: 100%;"><iframe id="payFrame" src="' + src + '" height=100% width=100% name="payframe" scrolling="yes" frameborder="0"></iframe></div>'
            // $(document.body).append(str);
            $('#payFrame').attr('src', src);
            $('#payWrapper').removeClass('gone');
        } else {
            //false 删除payiframe
            // $('#payWrapper').remove();
            $('#payWrapper').addClass('gone');
            $('#payFrame').attr('src', '');
        }
    }
    //根据cp传回来的信息，进行注入
    //组装发送给cp的信息
    function setInfoForSend(shareType) {
        var st = '';
        if (shareType == "appmessage" || shareType == "qq") {
            // st = 'onShareFriend';
            // 为了防止微信被封，根据策略调整，
            // 诱导用户分享给朋友和朋友圈，并提供奖励，
            st = 'onShareTimeline';
        } else if (shareType == 'timeline' || shareType == 'qzone') {
            st = 'onShareTimeline';
        }
        var info = {
            hlmy: true,
            from: '1758',
            type: 'fn',
            value: {
                'fn': st,
                args: []
            }
        }
        sendCpInfo(info);
    }
    //发送给cpinfo
    function sendCpInfo(info) {
        window.frames['cpframe'].postMessage(info, '*');
    }
    //关注微信方法
    function followWxNum() {
        // var str = '<div id="ewmCode" class="ewm-contain"><div class="ewm-wripper"><img src="http://images.1758.com/game/m/ewm.png"/></div></div>';
        // $(document.body).append(str);
        $('#ewmCode').removeClass('gone');
        reportEwmShow({
            appKey: dataPara.appkey,
            chn: HLMY_CONFIG.CHN_INFO.chn,
            sceneType1758: dataPara.sdkFollow.sceneType1758,
            openAppId: dataPara.sdkFollow.openAppId,
            sceneId: dataPara.sdkFollow.sceneId,
            sceneType: dataPara.sdkFollow.sceneType
        });
    }
    /**
     * 处理她社区的localstorage信息
     * @return {[type]} [description]
     * false 表示没有显示过，
     * true 则表示已经显示过 无需再次显示
     * 
     * *****************************************************************
     * *****************          废弃           ********************
     **************************************************************/
    function taSheQuLocalStorage() {
        if (!window.localStorage) {
            return false;
        }
        var chninfo = window.localStorage.chninfo;
        if (!!chninfo) {
            chninfo = JSON.parse(chninfo);
            for (var i = 0, len = chninfo.length; i < len; i++) {
                if (chninfo[i].appkey == dataPara.appkey && chninfo[i].ta == 1) {
                    return true;
                    break;
                }
            }
            return false;
        }
        return false;
    }
    /*****************************************************************
     * 更新localstorage 把她社区的标志chninfo写入
     * @return {[type]} [description]
     * *****************************************************************
     * *****************          废弃           ********************
     **************************************************************/
    function updataTaSheQuLocalStorage() {
        if (!window.localStorage) {
            return false;
        }
        var chninfo = window.localStorage.chninfo;
        var mark = false;
        if (!!chninfo) {
            //chninfo存在
            chninfo = JSON.parse(chninfo);
            if (chninfo.length > 0) {
                //有内容
                for (var i = 0, len = chninfo.length; i < len; i++) {
                    if (chninfo[i].appkey == dataPara.appkey) {
                        mark = true;
                        chninfo[i].ta = 1;
                        break;
                    }
                }
            }
        } else {
            chninfo = [];
        }
        if (!mark) {
            var item = {
                'appkey': dataPara.appkey,
                'ta': 1
            };
            chninfo.push(item);
        }
        chninfo = JSON.stringify(chninfo);
        window.localStorage.setItem('chninfo', chninfo);
    }

    $(document).on('click', '#talk .head', function() {
        // $('#talk .list').animate({scrollTop:0},500);
        $('#talk .list').scrollTop(0);
    })
    $(document).on('click', '#gift .head', function() {
            // $('#talk .list').animate({scrollTop:0},500);
            $('#gift .gift-content-wrapper').scrollTop(0);
        })
        //关闭二维码
    $(document).on('click', '#ewmCode', function() {
            $('#ewmCode').addClass('gone');
        })
        /**
         * 根据特殊需求替换分享链接中的chn的值
         * @param  {[type]} link 分享的link地址
         * @return {[type]}      返回修改后的link地址
         */
    function changeShareChn(link) {
        link = $.trim(link);
        var shareChn = $.trim(HLMY_CONFIG.SHARE_INFO.shareChn),
            chn = '';
        if (!!shareChn) {
            //不为空的情况下，分享的chn替换为当前的chn
            chn = EventBase.getParameters(link, 'chn');
            if (!!chn) {
                link = link.replace('chn=' + chn, 'chn=' + shareChn);
            } else if (!chn && link.indexOf('chn') > 0) {
                link = link.replace('chn=', 'chn=' + shareChn);
            } else {
                if (link.indexOf('?') > 0) {
                    link = link + '&chn=' + shareChn;
                } else {
                    link = link + '?chn=' + shareChn;
                }
            }
        }
        return link;
    }
    /**
     * 设置link连接上的state 参数
     * @param  {[type]} link  [description]
     * @param  {[type]} state [description]
     * @return {[type]}       [description]
     */
    function changeLinkState(link, state) {
        state = $.trim(state);
        var oldState = EventBase.getParameters(link, 'state');
        if (state !== '') {
            if (!!oldState) {
                link = link.replace('state=' + oldState, 'state=' + state);
            } else if (!oldState && link.indexOf('state') > 0) {
                link = link.replace('state=', 'state=' + state);
            } else {
                if (link.indexOf('?') > 0) {
                    link = link + '&state=' + state;
                } else {
                    link = link + '?state=' + state;
                }
            }
        }
        return link;
    }

    /**
     * 根据HLMY_CONFIG.SUBSCRIBE_INFO提供的消息
     * 是否进行ajax 查询
     * 如果查询返回的内容为true，则弹出二维码让用户关注
     * @return {[type]} [description]
     */
    function checkFollowInfo() {
        var ua = dataPara.userTerminal;
        if (ua == 'weixin') { //微信客户端内才弹出关注图片
            if (HLMY_CONFIG.SUBSCRIBE_INFO.enable) {
                setTimeout(function() {
                    if (dataPara.subscribeFollow.subscribeImgUrl) {
                        $('#followEwmCode img').attr('src', dataPara.subscribeFollow.subscribeImgUrl);
                        $('#followEwmCode').removeClass('gone');
                        reportEwmShow({
                            appKey: dataPara.appkey,
                            chn: HLMY_CONFIG.CHN_INFO.chn,
                            sceneType1758: dataPara.subscribeFollow.sceneType1758,
                            openAppId: dataPara.subscribeFollow.openAppId,
                            sceneId: dataPara.subscribeFollow.sceneId,
                            sceneType: dataPara.subscribeFollow.sceneType
                        });
                    }
                }, HLMY_CONFIG.SUBSCRIBE_INFO.promptDelay * 1000);
            }
        }
    }
    //关闭关注的二维码
    $(document).on('click', '#followEwmCode #followEwmCodeClose', function() {
        $('#followEwmCode').addClass('gone');
    });

    /**
     * 获取二维码地址，sdk follow方法用到
     * 
     * 2018/01/16 该方法会获取主动关注和被动关注用到的二维码
     * 
     */
    function getEwmImg() {
        var url = '/play/subscribe/loadStrategy.json';
        $.post(url, {
            "appKey": dataPara.appkey,
            'chn': HLMY_CONFIG.CHN_INFO.chn
        }, function(resData) {
            if (resData.result == 1 && resData.data.dataList) {
                for (var i = 0, len = resData.data.dataList.length; i < len; i++) {
                    if (resData.data.dataList[i].sceneType1758 === 0) {
                        // 主动关注  sdk调用
                        dataPara.sdkFollow = resData.data.dataList[i];

                    } else if (resData.data.dataList[i].sceneType1758 === 10) {
                        // 被动订阅关注
                        dataPara.subscribeFollow = resData.data.dataList[i];
                    }
                }
                // 设置二维码url
                if (dataPara.sdkFollow.subscribeImgUrl) {
                    $('#ewmCode img').attr('src', dataPara.sdkFollow.subscribeImgUrl);
                }
                //检查关注信息		
                checkFollowInfo();
            }
        }, 'json');
    };
    /**
     * 获取登录版本号，区别在分享回调函数上面
     * 3.0以上 使用的是h5的postmessage 方法
     * 2.0 使用的是iframe的share文件方法
     * @return {[type]} [description]
     *
     * 废弃
     */
    function getVerLogin() {
        // dataPara.verLogin = '4';
        // console.log('vlogin:',dataPara.verLogin,'.0');
        // var verlogin = EventBase.getParameters('verLogin');
        // var vlogin = EventBase.getParameters('v');
        // if(!!verlogin){
        // 	//3.0版本
        // 	dataPara.verLogin = '3';
        // 	console.log('vlogin:3.0');
        // }else if(vlogin == '3'){
        // 	//3.0版本
        // 	dataPara.verLogin = '3';
        // 	console.log('vlogin:3_old');
        // }else{
        // 	//2.0版本
        // 	dataPara.verLogin = '2';
        // 	console.log('vlogin:2.0');
        // }
    }
    /**
     * 关闭微信部分功能
     * @return {[type]} [description]
     */
    function hideWxMenu() {
        var hideMenu = HLMY_CONFIG.SHARE_INFO.hideMenu;
        if (!!hideMenu && hideMenu.length > 0) {
            try {
                wx.hideMenuItems({
                    menuList: hideMenu
                })
            } catch (err) {}
        }
    }


    /**
     * 模拟 cp游戏中分享弹框功能 初始化
     * 
     * @return {[type]} [description]
     */
    function simulateGameShareInit() {
        var ua = dataPara.userTerminal;
        var time;
        if (ua == 'weixin' || ua == 'iphoneclient' || ua == 'androidclient') {
            if (HLMY_CONFIG.SHARE_INFO.promptEnable) {
                //经过固定时间后查询url 是否弹出模拟游戏分享内容
                time = HLMY_CONFIG.SHARE_INFO.promptDelay || 360;
                setTimeout(function() {
                    getSimulateShareInfo();
                }, 1000 * time);
                simulateGameShareEventBind();
            }
        }
    }
    /**
     * 请求查看是否需要弹分享提示框
     * @return {[type]} [description]
     */
    function getSimulateShareInfo() {
        var url = '/play/share/getStrategy.json';
        var info = {
            appKey: dataPara.appkey,
            chn: HLMY_CONFIG.CHN_INFO.chn
        }
        $.post(url, info, function(resData) {
            if (resData.result == 1 && !!resData.data) {
                //打开弹框
                if (resData.data.enable) {
                    $('#simulateShareImg').attr('src', resData.data.promptImgUrl);
                    //为了加载图片  延迟10秒弹出
                    setTimeout(function() {
                        $('#simulateContainer').removeClass('gone');
                        //分享弹框30秒内  设置为1
                        dataPara.shareReasonType = 1;
                        setTimeout(function() {
                            dataPara.shareReasonType = 0;
                        }, 1000 * 30)
                    }, 1000 * 10)
                }
            }
        }, 'json');
    }
    /**
     * 模拟游戏中分享弹框 绑定点击事件
     * 
     * @return {[type]} [description]
     */
    function simulateGameShareEventBind() {
        //关闭按钮
        $(document).on('click', '#simulateShareClose', function(evt) {
            if (dataPara.simulateNoTip) {
                //不再提示
                //服务端报错
                var url = '/play/share/ignore.json';
                var info = {
                    appKey: dataPara.appkey,
                    chn: HLMY_CONFIG.CHN_INFO.chn
                }
                $.post(url, info, function(data) {
                    console.log(data);
                })
            }
            $('#simulateContainer').addClass('gone');
        });
        // 去分享按钮
        $(document).on('click', '#simulateShareTip', function(evt) {
            $('#shareTipInfo').removeClass('gone');
        });
        // 不再提示按钮点击
        $(document).on('click', '#simulateNoTip', function(evt) {
            dataPara.simulateNoTip = !dataPara.simulateNoTip;
            if (dataPara.simulateNoTip) {
                $('#simulateSelected').removeClass('gone');
            } else {
                $('#simulateSelected').addClass('gone');
            }
        });
    }

    /**
     * 绑定手机号登录事件
     * @return {[type]} [description]
     */
    function bindLoginMobile() {
        // 试玩弹框里面的手机号按钮绑定事件
        $(document).on('click', '#mobileLogin', function() {
            // 1、重置手机登录框上面的图形验证码，因为该码两分钟有效期
            $('#verifyCodeImg').attr('src', '/play/getVerifyCode');
            // 2、初始化手机号登录对应的事件处理
            ExtraFeatures.mobileLoginInit();
            //3、试玩弹框取消
            $('#loginContainer').addClass('gone');
            // 4、弹出手机号登录弹框
            $('#huiMobileLogin').removeClass('gone');
        })
    }

    /**
     * 分享以图片展示方式进行分享
     * 弹出图片后 用户自动下载或识别
     *
     * state cp提供的state参数
     * 
     * @return {[type]} [description]
     */
    function showShareImg(state, id) {
        $.post('/play/game/getShareImgUrl.json', {
            appKey: dataPara.appkey,
            gid: dataPara.gid,
            state: state,
            shareId: id
        }, function(resData) {
            if (resData.result == 1) {
                $('#shareImgModal #shareImg').attr('src', resData.data.shareImgUrl);
                $('#shareImgModal').removeClass('gone');
            }
        })
    }
    /**
     * 分享以图片的展示方式，
     * 直接传递进来图片url
     * @param  {[type]} url [description]
     * @return {[type]}     [description]
     */
    function showSharePicture(imgUrl) {
        $('#shareImgModal #shareImg').attr('src', imgUrl);
        $('#shareImgModal').removeClass('gone');
    }
    $(document).on('click', '#shareImgModal', function(evt) {
        if (evt.target.id !== 'shareImg') {
            $('#shareImgModal').addClass('gone');
        }
    })

    /**
     * 关于活动的处理方法
     * 运营测试推广 图片展示
     * @return {[type]} [description]
     */
    function promotionInit() {
        var url = '/play/promotion/load.json';
        var infos = {
            chn: dataPara.chn
        }
        $.post(url, infos, function(resData) {
            handlePromotionData(resData);
        }, 'json');
    }
    /**
     * 处理获取到的关于活动的数据进行处理
     * @param  {[type]} resData [description]
     * @return {[type]}         [description]
     */
    function handlePromotionData(resData) {
        var data;
        if (resData.result == 1) {
            data = resData.data.promotion;
            if (!data) {
                return;
            }
            // 给活动图片进行赋值链接地址
            $('#promotionContainer img').attr('src', data.adImgUrl);
            $('#promotionContainer a').attr('href', data.link);
            $('#promotionContainer').removeClass('gone');
        }
    }
    // 关闭活动展示
    $(document).on('click', '#promotionContainer .close', function() {
        $('#promotionContainer').addClass('gone');
    })

    // 上报关注二维码显示 
    // 有sdk调用上报  和  被动订阅上报
    function reportEwmShow(params) {
        var url = '/play/subscribe/reportPrompt.json';
        $.post(url, params, function() {})
    }

    // 日志打点
    function logger(src) {
        var img = new Image();
        var rnd_id = "_hlmy_img_" + Math.random();
        window[rnd_id] = img; // 全局变量引用
        img.onload = img.onerror = function() {
            window[rnd_id] = null; // 删除全局变量引用
        }
        img.src = src;
    }

    // 充值功能
    function rechargeInit() {
        if (HLMY_CONFIG.RECHARGE_INFO && HLMY_CONFIG.RECHARGE_INFO.enable) {
            setTimeout(rechargeReq, HLMY_CONFIG.RECHARGE_INFO.promptDelay * 1000)
        }
    }

    function rechargeReq() {
        var url = '/play/activity/game/rechargePrompt.json'
        $.post(url, {
            appKey: dataPara.appkey,
            gid: dataPara.gid,
            chn: dataPara.chn
        }, function(resData) {
            if (resData.result == 1) {
                $('#rechargeContainer img').attr('src', resData.data.promptImgUrl);
                $(document).on('click', '#rechargeBtn', function() {
                    window.frames['cpframe'].postMessage({
                        hlmy: true,
                        from: '1758',
                        type: 'fn',
                        value: {
                            'fn': 'orderPayment1758',
                            args: resData.data.itemcode
                        }
                    }, '*')
                })
                $('#rechargeContainer').removeClass('gone');
            }
        }, 'json')
    }
    // 充值活动展示关闭
    $(document).on('click', '#rechargeContainer .close', function() {
        $('#rechargeContainer').addClass('gone');
    })

});