/**
 * sdk传递过来的相关内容
 */
define(['jquery', 'common/eventBase', 'view/real'], function($, EventBase, RealName) {
    var obj = {
        showRealNameCert: showRealNameCert
    };
    /**
     * 实名认证信心
     */
    function showRealNameCert(args) {
        RealName.init(args);
    }
    return obj;
})