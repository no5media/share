/*
 * 客服tab项的聊天内容
 *
 * websocket 流程如下
 * 1，进入游戏则，建立链接，并发送进入场景信息给服务器
 * 2，点击客服tab项 则进入会话，并发送进入会话信息给服务器
 * 3，点击其他tab项或者关闭浮标，则会发送离开会话消息给服务器
 * 4，如果断开链接，则重新发起链接
 * 5，尝试链接次数为5次，如果超过则停止建立链接
 */

define(['jquery','common/eventBase'],function($,EventBase){
	var obj = {
		init:init,
		wsInit:wsInit,
		wsToggleDialog:wsToggleDialog,
		showItem:showItem
	}
	var _config = {
		tpl:'',
		kfDialogKey:HLMY_CONFIG.CHAT_INFO.kfDialogKey,		//暂时无用
		ws:{
			kickoff:false,		//是否断线，true则表示是后台服务器踢下线 不需要重连
			kickoffMessage:'',			//被服务器踢掉之后的提示语
			connectNum:0,		//重连次数
			connectMax: 5,		//重连最多次数
			dialogState: false,		//会话状态，true为进入，false为离开
			kfDialogState: false,			//客服会话状态
			chatDialogState: false,			//聊天室会话状态
			state: false,			//websocket 链接状态
			fromToken:HLMY_CONFIG.USER_INFO.gid,		// 用户身份，在每次发送消息的时候带着
			// dialogKey:'S_1186493_1186630',
			kfDialogKey:'',					//	客服dialogKey
			chatDialogKey:'',				// 聊天室dialogkey
			kfEnable:false,				//是否开启客服聊天功能
			chatEnable:false,			//是否开启聊天室功能
			lastTabType:'',				//在ws聊天中最后一个tab，当用户点击关闭浮标，发送dialogclose的时候会用到
			defServerInfo: $.Deferred(),
			defKFInfo: $.Deferred(),
			defChatInfo: $.Deferred(),
			imPingInterval:60*1000  //心跳频率，单位毫秒
		},
		isPanel:false,				//是否使用浮标
	}
	var _wsConfig = {
		url:'ws://imtest.1758.com/',			//im服务器地址
        open: wsOpen,
        close: wsClose,
        message: wsMessage
	}
	function init(tpl){
		_config.tpl = tpl;
		// getChatMessage();
		// 通过ws获取历史消息
		// HLMY_WS.send({
		// 	"fromToken": _config.ws.fromToken, 
		// 	"command": "PULL_HISTORY", 
		// 	"dialogKey": _config.ws.dialogKey, 
		// 	"pageSize":10
		// });
	}
	/**
	 * 获取聊天信息
	 * @return {[type]} [description]
	 */
	function getChatMessage(){
		var url = '/play/message/chat/moreDialogMessage.json';
		var dataList;
		var infos = {
			dialogKey:_config.kfDialogKey
		}
		$.post(url,infos,function(resData){
			if(resData.result == 1 &&　!!resData.data.chatMessageData && !!resData.data.chatMessageData.dataList){
				dataList = resData.data.chatMessageData.dataList;
				handleResponeChatInfo(dataList);
			}
		})
	}
	/**
	 * 处理返回的聊天消息
	 * @return {[type]} [description]
	 */
	function handleResponeChatInfo(dataList){
		handleChatMessage(dataList);
		dataList = dataList.reverse();
		var compireHtml = EventBase.compileTem(_config.tpl,dataList);
		if(dataList[0].dialogKey == _config.ws.kfDialogKey){
			if(dataList[0].isHistory){
				$('#kefu #chatContainer .historyline').before(compireHtml);
			}else{
				$('#kefu #chatContainer').append(compireHtml);
			}
			if(!!$('#kefu #chatContainer')[0]){
				$('#kefu .inner-kefu-container').scrollTop($('#kefu #chatContainer')[0].scrollHeight);
			}
		}else if(dataList[0].dialogKey == _config.ws.chatDialogKey){
			if(dataList[0].isHistory){
				$('#chat #chatContainer .historyline').before(compireHtml);
			}else{
				$('#chat #chatContainer').append(compireHtml);
			}
			if(!!$('#chat #chatContainer')[0]){
				$('#chat .inner-kefu-container').scrollTop($('#chat #chatContainer')[0].scrollHeight);
			}
		}
	}
	/**
	 * 处理获取到的聊天数据
	 * @return {[type]} [description]
	 */
	function handleChatMessage(dataList){
		if(dataList.length == 0){return;}
		for(var i=0,len = dataList.length;i<len;i++){
			dataList[i].showTime = handleTime(dataList[i].sendTime,dataList[i].currentTime);
		}
	}
	/**
	 * 处理相关时间  进行显示
	 * @param  {[type]} time [description]
	 * @return {[type]}      [description]
	 */
	function handleTime(sendTime,currentTime){
		var minute = 1000 * 60;
		var hour = minute * 60;
		var day = hour * 24;
		var date = new Date(sendTime);
		// 今天凌晨时间
		var ling = new Date(currentTime).setHours(0,0,0,0)
		var result;
		// 时间差
		var value = currentTime - sendTime;
		if(value < 1000 * 60){
			result = '刚刚';
		}else if(value <1000 * 60 * 60){
			result = parseInt(value/minute) + '分钟前';
		}else if(value < 1000 * 60 * 60 * 24){
			result = parseInt(value/hour) + '小时前';
		}else if(sendTime > (ling-day) && sendTime < (ling-day-day)){
			result = '昨天 '+ date.getHours()+':'+date.getMinutes();
		}else if(sendTime > (ling-day-day) && sendTime < (ling-day-day-day)){
			result = '前天 '+ date.getHours()+':'+date.getMinutes();
		}else{
			result = Format(date);
		}
		return result;
	}
	/**
	 * 格式化内容
	 * @param {[type]} date [description]
	 * @param {[type]} fmt  [description]
	 */
	function Format(date,fmt){
		fmt = 'yyyy-MM-dd hh:mm:ss';
	    var o = {
	        "M+": date.getMonth() + 1, //月份 
	        "d+": date.getDate(), //日 
	        "h+": date.getHours(), //小时 
	        "m+": date.getMinutes(), //分 
	        "s+": date.getSeconds(), //秒 
	        "q+": Math.floor((date.getMonth() + 3) / 3), //季度 
	        "S": date.getMilliseconds() //毫秒 
	    };
	    if (/(y+)/.test(fmt)) {
	    	fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
	    }
	    for (var k in o){
	    	if (new RegExp("(" + k + ")").test(fmt)) {
	    		fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
	    	}
	    }
	    return fmt;
	}

	/***************************************************************************
	*********************     websocket 相关的方法          *******************
	***************************************************************************/

	/**
	 * websocket 初始化，即进入游戏后建立websocket
	 * @return {[type]} [description]
	 *
	 * 传入参数是否有浮标
	 */
	function wsInit(panelEnable){
		_config.isPanel = panelEnable || '';
		getIMServerInfo();
		$.when(_config.ws.defServerInfo).done(function(){
			if(typeof HLMY_WS === 'object'){
				HLMY_WS.init(_wsConfig);
			}
		});
		// 获取客服聊天会话id
		getIMInfo('KF');
		// 获取聊天室聊天会话id
		getIMInfo('R');
	}
	/**
	 * websocket 切换会话
	 * 进入会话和关闭会话
	 * flag 为布尔值
	 * type 为 kf 或 chat
	 * @return {[type]} [description]
	 */
	function wsToggleDialog(flag,type){
		//已经被服务器踢下线，则不处理
		if(!!type){
			_config.ws.lastTabType = type;
		}
		if(_config.ws.kickoff){
			// 防止多次提示
			if(!!type){
				showWsTipInfo(_config.ws.kickoffMessage);
			}
			return;
		}
		if(!type){type = _config.ws.lastTabType}
		var fromToken = _config.ws.fromToken,
			command,dialogKey;

		if(type === 'chat'){
			if(!_config.ws.chatEnable){return;}
		 	dialogKey = _config.ws.chatDialogKey;
			if(flag && !_config.ws.chatDialogState){
			 	command = "DIALOG_OPEN";
		        _config.ws.chatDialogState = true;
			}else if(!flag && _config.ws.chatDialogState){
			 	command = "DIALOG_CLOSE";
		        _config.ws.chatDialogState = false;
		        $('#chat #chatContainer .kfit').remove();
			}else{
			 	return;
			}
		}else if(type === 'kf'){
			if(!_config.ws.kfEnable){return;}
			dialogKey = _config.ws.kfDialogKey;
			if(flag && !_config.ws.kfDialogState){
			 	command = "DIALOG_OPEN";
		        _config.ws.kfDialogState = true;
			}else if(!flag && _config.ws.kfDialogState){
			 	command = "DIALOG_CLOSE";
		        _config.ws.kfDialogState = false;
		        $('#kefu #chatContainer .kfit').remove();
			}else{
			 	return;
			}
		}else{
			// 如果为空则表示刚进入
			return;
		}
		HLMY_WS.send({
			"fromToken": fromToken, 
        	"command": command, 
        	"dialogKey": dialogKey,
        	"scene": 'game_'+HLMY_CONFIG.GAME_INFO.appKey
		});
	}
	/**
	 * websocket open对应方法
	 * @return {[type]} [description]
	 */
	function wsOpen(evt){
		_config.ws.state = true;
		console.log('与服务器建立链接');
		enterScene();

		//间隔60s发心跳包，便于后端服务器检测用户在线
        setInterval(heartBeat, _config.ws.imPingInterval);
	}
	/**
	 * websocket 对应close方法
	 * @return {[type]} [description]
	 */
	function wsClose(evt){
		console.log('关闭链接');
		_config.ws.state = false;
		// 进行重连
		if(!_config.ws.kickoff){
			console.log('connect',_config.ws.kickoff);
			if(_config.ws.connectNum < _config.ws.connectMax){
				_config.ws.connectNum ++;
				wsInit();
			}
		}else{
			console.log('kickoff',_config.ws.kickoff);
			// 被踢下线了不处理
		}
	}
	/**
	 * websocket 对应的message方法
	 * @return {[type]} [description]
	 */
	function wsMessage(evt){
		var data,arr = [];
		if(typeof evt.data === 'string'){
			data = JSON.parse(evt.data);
		}
    	if(data.command === 'CHAT'){
    		// 如果没有command 则认为是用户发送的消息
	    	arr.push(data);
	    	handleResponeChatInfo(arr);
    	}else{
    		if(data.command === 'KICKOFF'){
    			_config.ws.kickoff = true; 
    			_config.ws.kickoffMessage = data.message;
    			showWsTipInfo(_config.ws.kickoffMessage);
    		}
    	}
	}
	/**
	 * 进入场景
	 * @return {[type]} [description]
	 */
	function enterScene(){
		HLMY_WS.send({
        	"fromToken": _config.ws.fromToken, 
        	"command": "SCENE_ENTER",
        	"scene": 'game_'+HLMY_CONFIG.GAME_INFO.appKey
        });
        showWsTipInfo('进入场景');
	}


	/**
	 * 发送心跳包
	 * @return {[type]} [description]
	 */
	function heartBeat(){
		HLMY_WS.send({
        	"fromToken": _config.ws.fromToken, 
        	"command": "PING",
        	"scene": 'game_'+HLMY_CONFIG.GAME_INFO.appKey
        });
	}


	/**
	 * 检查并连接websocket
	 * @return {[type]} [description]
	 */
	function checkAndConnect(){
		if(!_config.ws.state){
			wsInit();
		}
	}
	/**
	 * 显示tipinfo
	 * @return {[type]} [description]
	 */
	function showWsTipInfo(text){
		var str = '<div class="kfitem-message">'+text+'</div>';
			console.log(_config.ws.lastTabType,text);
		if(_config.ws.lastTabType == 'kf'){
			$('#kefu #chatContainer').append(str);
			if(!!$('#kefu #chatContainer')[0]){
				$('#kefu .inner-kefu-container').scrollTop($('#kefu #chatContainer')[0].scrollHeight);
			}
		}else if(_config.ws.lastTabType == 'chat'){
			$('#chat #chatContainer').append(str);
			if(!!$('#chat #chatContainer')[0]){
				$('#chat .inner-kefu-container').scrollTop($('#chat #chatContainer')[0].scrollHeight);
			}
		}
	}

	/**
	 * 获取用户输入的信息并且进行发送
	 * @return {[type]} [description]
	 */
	function getAndSendInfo(type){
		var value ,dialogKey;
		if(type == 'kefu'){
			dialogKey = _config.ws.kfDialogKey;
			value = $.trim($('#kefu #chatText').val());
    		$('#kefu #chatText').val('');
		}else if(type == 'chat'){
			dialogKey = _config.ws.chatDialogKey;
			value = $.trim($('#chat #chatText').val());
    		$('#chat #chatText').val('');
		}
    	if(value === ''){return;}
    	if(value.length > 300){
    		value = value.substr(0,300);
    	}
    	value = HLMY_WS.chartEscape(value);
    	var message = {
    		"fromToken": _config.ws.fromToken, 
    		"command": "CHAT", 
    		"dialogKey": dialogKey, 
    		"msgType": "TEXT", 
    		"content": value
    	};
    	HLMY_WS.send(message);
	}

	/**
	 * 获取IM聊天信息
	 * type 分为R 和KF
	 * 聊天室消息和客服消息
	 * @param  {[type]} type [description]
	 * @return {[type]}      [description]
	 */
	function getIMInfo(type){
		var url = '/play/im/getDialogSummary.json',
			infos={
				dialogType: type,
				appKey:HLMY_CONFIG.GAME_INFO.appKey
			},
			dialogSummary = '';
		$.post(url,infos,function(resData){
			if(resData.result == 1 && !!resData.data && !!resData.data.dialogSummary){
				if(type === 'R'){
					_config.ws.chatDialogKey = resData.data.dialogSummary.dialogKey;
					_config.ws.chatEnable = resData.data.dialogSummary.enable;
					
					_config.ws.defChatInfo.resolve();
				}else if(type === 'KF'){
					_config.ws.kfDialogKey = resData.data.dialogSummary.dialogKey;
					_config.ws.kfEnable = resData.data.dialogSummary.enable;
						
					_config.ws.defKFInfo.resolve();
				}
			}
		},'json');
	}

	/**
	 * 获取IM的链接地址，以及token
	 * 
	 * @return {[type]} [description]
	 */
	function getIMServerInfo(){
		var url = '/play/im/config.json';
		$.post(url,function(resData){
			if(resData.result == 1 && !!resData.data){
				_wsConfig.url = resData.data.imServer,
				_config.ws.imPingInterval = resData.data.imPingInterval,
				_config.ws.fromToken = resData.data.fromToken;
				_config.ws.defServerInfo.resolve();
			}
		},'json');
	}

	/**
	 * 根据ajax请求回来的参数进行设置是否显示item
	 * @return {[type]} [description]
	 */
	function showItem(){
		$.when(_config.ws.defChatInfo).done(function(){
			if(_config.ws.chatEnable){
				$('.tabChat').removeClass('gone');
			}
		})
		$.when(_config.ws.defKFInfo).done(function(){
			if(_config.ws.kfEnable){
				$('.tabKeFu').removeClass('gone');
			}
		})
	}
	/**
	 * 绑定发送事件
	 * @param  {[type]} ){                 	var value [description]
	 * @return {[type]}     [description]
	 */
	$(document).on('click','#kefu #chatBtn',function(){
    	getAndSendInfo('kefu');
    })
    $(document).on('click','#chat #chatBtn',function(){
    	getAndSendInfo('chat');
    })

    // $(document).on('keydown','#chatText',function(evt){
    // 	console.log(evt);
    // 	if(evt.keyCode === 13){
    // 		getAndSendInfo();
    // 	}
    // })
	return obj;
})