/**
 * 该js主要是为了处理付费游戏里面
 * 除了弹框主要功能外的额外功能（辅助功能）
 * 包括但不限于：第三方渠道的各种临时需求、弹框提示功能
 * 设置提醒功能
 */
define(['jquery', 'common/eventBase', 'view/modalBtn'], function($, EventBase, ModalBtn) {
    var exportModal = {
        // 手机号登录弹框初始化
        mobileLoginInit: mobileLoginInit,
        // 如果有vip客服消息则弹框显示
        vipInfoInit: vipInfoInit,
        // 设置提醒功能初始化
        setRemind: remindInit
    };
    var UA = window.navigator.userAgent.toLocaleLowerCase();
    /**
     * 内部对象，里面是各种内部需要的方法
     * @type {Object}
     */
    var _Tool = {
        isAndroid: UA.indexOf('android') > 0,
        isIos: /(iphone|ipad|ipod|ios)/i.test(UA),
        isClient: UA.indexOf('dwjia') > 0,
        isWeChat: (UA.indexOf('micromessenger') > 0) && (UA.indexOf('dwjia') < 0)
    };

    /**
     * 手机号登录事件的初始化
     * @return {[type]} [description]
     */
    function mobileLoginInit() {
        var chn = HLMY_CONFIG.CHN_INFO.chn;
        // 获取验证码按钮
        var verifyCodeLock = false; //false 为无锁  true为上锁状态
        // 登录按钮
        var phoneBtnLock = false;
        // 用户填写的手机号存储
        var userPhone;
        var ua = navigator.userAgent.toLocaleLowerCase();

        //今日头条渠道 隐藏，其他打开
        if (chn != 'jinritoutiao') {
            $('#mobileWXLogin').removeClass('gone');
        }
        // 设置提示信息
        function setTipInfo(text, time) {
            time = time || 1;
            $('#hmobileModal .hlui-inner-modal').text(text);
            $('#hmobileModal').removeClass('gone');
            setTimeout(function() {
                $('#hmobileModal').addClass('gone')
            }, time * 1000)
        }
        // 获取短信验证码信息
        function getMessInfo(phoneNum, imgNum) {
            // 上锁
            verifyCodeLock = true;
            var second = 60;
            var intervalId = setInterval(function() {
                if (second == 0) {
                    verifyBtnInit(intervalId);
                } else {
                    $('#verifyCodeBtn').text(second + 's');
                    second--;
                }
            }, 1000);
            // 获取短信验证码信息
            $.post('/play/login/fetchSmsCode.json', {
                'mobile': phoneNum,
                'rnum': imgNum
            }, function(resData) {
                handleSmsCodeData(resData, intervalId);
            }, 'json');
            $('#verifyCodeBtn').addClass('hmobile-disable');
        }
        // 恢复验证码按钮为初始化状态
        function verifyBtnInit(intervalId) {
            clearInterval(intervalId);
            $('#verifyCodeBtn').text('获取验证码');
            $('#verifyCodeBtn').removeClass('hmobile-disable');
            // 开锁
            verifyCodeLock = false;
        }
        // 恢复手机号码登录按钮为初始状态
        function phoneLoginBtnInit() {
            phoneBtnLock = false;
            $('#hmobileLogin').html('登&nbsp;&nbsp;录');
            $('#hmobileLogin').removeClass('hmobile-btn-disable');
        }
        // 处理获取验证码返回的数据
        function handleSmsCodeData(resData, intervalId) {
            if (resData.result == 0) {
                //出错了  重置获取验证码按钮
                setTipInfo(resData.message);
                verifyBtnInit(intervalId);
            }
        }
        /**
         * 手机号登录
         * @return {[type]} [description]
         */
        function mobileLogin(message) {
            $.post('/play/login/verifySmsCode.json', {
                mobile: userPhone,
                smsVerifyCode: message,
                appKey: HLMY_CONFIG.GAME_INFO.appKey,
                chn: HLMY_CONFIG.CHN_INFO.chn
            }, function(resData) {
                handleMobileLoginData(resData)
            }, 'json')
        }
        /**
         * 处理手机号码登录返回的数据
         * @param  {[type]} resData [description]
         * @return {[type]}         [description]
         */
        function handleMobileLoginData(resData) {
            phoneLoginBtnInit();
            if (resData.result == 1) {
                top.location.href = resData.data.authorizeUrl;
            } else {
                setTipInfo(resData.message);
            }
        }


        /**************************
         ********   绑定事件  ******
         **************************/
        // 获取验证码按钮点击事件
        $(document).on('click', '#verifyCodeBtn', function() {
            if (!verifyCodeLock) {
                //开锁上状态
                var rep = /^1(3|4|5|7|8)\d{9}$/ig
                    // 获取填写的手机号码
                var num = $.trim($('#mobileNum').val());
                // 获取填写的图形验证码
                var imgNum = $.trim($('#mobileImg').val());
                if (num.length == 0) {
                    setTipInfo('手机号码不能为空');
                } else if (!rep.test(num)) {
                    setTipInfo('手机号码格式不对');
                } else if (imgNum.length == 0) {
                    setTipInfo('图形码不能为空');
                } else {
                    userPhone = num;
                    getMessInfo(num, imgNum);
                }
            }
        });

        // 刷新图像校验码
        $(document).on('click', '#verifyCodeImg', function() {
            $(this).attr('src', '/play/getVerifyCode');
        });

        // 手机号登录点击事件
        $(document).on('click', '#hmobileLogin', function() {
            var message = $.trim($('#mobileMes').val());
            var phoneNum = $.trim($('#mobileNum').val());
            // 优先以获取短信验证码的时候的手机号为准，
            // 假如没有 则再获取当前填写的值
            userPhone = userPhone || phoneNum;
            if (userPhone === undefined || userPhone == '') {
                setTipInfo('手机号不能为空')
            } else if (message.length == 0) {
                // 校验短信验证码不能为空
                setTipInfo('短信验证码不能为空')
            } else {
                $('#hmobileLogin').addClass('hmobile-btn-disable')
                $(this).text('正在登录...');
                phoneBtnLock = true;
                mobileLogin(message);
            }
        })

        //点击隐藏消失(暂时废弃)
        // $(document).on('click','#huiMobileLogin',function(){
        // 	$(this).addClass('gone');
        // })
        // 阻止冒泡
        // $(document).on('click','.hmobile-wrapper',function(evt){
        // 	evt.stopPropagation();
        // })
        // $(document).on('click','#hmobileModal',function(evt){
        // 	evt.stopPropagation();
        // })	
    }
    // 拉去vip信息显示内容
    function vipInfoInit() {
        var url = '/play/message/chat/getPromoteInfo.json';
        $.post(url, function(response) {
            if (response.result === 1 && !!response.data && !!response.data.promoteMessageList && response.data.promoteMessageList.length > 0) {
                handleVipData(response.data.promoteMessageList);
            } else {
                //暂无数据
            }
        }, 'json');

        // 关闭弹框
        $(document).on('click', '#vipInfoContainer .vipinfo-close', function() {
                $("#vipInfoContainer").addClass("gone");
            })
            // 回复点击
        $(document).on('click', '#vipInfoContainer #vipInfoReply', function() {
            $('#vipInfoContainer').addClass('gone');
            ModalBtn.freeTab(5);
        })
    }
    /**
     * 处理vip拉去到的数据
     * @param  {[type]} chatDataList [description]
     * @return {[type]}              [description]
     */
    function handleVipData(promoteMessageList) {
        for (var i = 0, len = promoteMessageList.length; i < len; i++) {
            promoteMessageList[i].showTime = EventBase.handleVipInfoDate(promoteMessageList[i].sendTime, promoteMessageList[i].currentTime);
        }
        promoteMessageList = promoteMessageList.reverse();
        var dom = EventBase.compileTem(document.getElementById('vipInfoTpl').text, promoteMessageList);
        // var dom = doT.template(document.getElementById('vipInfoTpl').text)(promoteMessageList);
        $('#vipInfoContainer .vipinfo-list').append(dom);
        $('#vipInfoContainer').removeClass('gone');
    }

    /**
     * 设置提醒初始化功能
     * 根据页面上参数是否显示该弹框
     * 若显示，则ajax获取提醒设置参数，拿到后显示
     * 若不显示，则不请求参数
     * 
     * @return {[type]} [description]
     */
    function remindInit() {
        var deffPer, deffUser;
        if (HLMY_CONFIG.GAME_INFO.initial === 'perference') {
            deffPer = $.Deferred();
            deffUser = $.Deferred();
            // 绑定关闭事件
            $(document).on('click', '#remindClose', function() {
                $('#remindContainer').addClass('gone');
            })

            $(document).on('click', '#remindDay', function(evt) {
                var key = 'day';
                var value = $('#remindDay input').prop('checked');
                $('#remindDay input').prop('checked', !value)
                updatePerference(key, !value)
                evt.stopPropagation();
            })
            $(document).on('click', '#remindNight', function(evt) {
                var key = 'night';
                var value = $('#remindNight input').prop('checked');
                $('#remindNight input').prop('checked', !value)
                updatePerference(key, !value)
                evt.stopPropagation();
            })

            // 设置用户姓名
            $('#remindUserName').text(HLMY_CONFIG.USER_INFO.nickname);
            // 获取用户vip等级和经验等级
            getUserVipExp(deffUser);

            $.post('/play/user/perference.json', function(resData) {
                if (resData.result == 1 && !!resData.data && !!resData.data.userPerference) {
                    if (resData.data.userPerference.pushDayEnable) {
                        $('#remindDay input').prop('checked', true);
                    }
                    if (resData.data.userPerference.pushNightEnable) {
                        $('#remindNight input').prop('checked', true);
                    }
                    deffPer.resolve();

                } else {
                    deffPer.reject();
                }
            }, 'json')

            $.when(deffUser, deffPer).done(function() {
                $('#remindContainer').removeClass('gone');
            })
        }
    }
    // 更新设置提醒状态
    function updatePerference(key, value) {
        var url = '/play/user/updatePerference.json';
        var info = {};
        if (key == 'day') {
            info.pushDayEnable = value;
        } else if (key == 'night') {
            info.pushNightEnable = value;
        }
        $.post(url, info, function(resData) {

        }, 'json')
    }
    /**
     * 获取用户vip等级和经验等级
     * @return {[type]} [description]
     */
    function getUserVipExp(deffUser) {
        $.post('/play/user/profile.json', { mode: 12 }, function(resData) {
            var userInfo, vipLevel, expLevel, iconHtml;
            if (resData.result == 1 && resData.data && resData.data.userProfile) {
                userInfo = resData.data.userProfile;
                vipLevel = userInfo.vipInfo.vipLevel;
                expLevel = userInfo.expInfo.expLevel;
                if (vipLevel > 0) {
                    $('#remindUserVip').text('VIP' + vipLevel);
                    $('#remindUserVip').removeClass('gone');
                }
                iconHtml = expLevelToIcon(expLevel);
                $('#remindLevel').html(iconHtml);
            }
            deffUser.resolve();
        }, 'json')
    }
    /**
     * 根据expLevel换算成icon图标
     * 在设置提醒的时候用到
     * @return {[type]} [description]
     */
    function expLevelToIcon(level) {
        var levToFour, levArr,
            i, len,
            imgurl, testHtml, strHtml = '';
        level = parseInt(level);
        if (!level) { return ''; }
        levToFour = level.toString(4);
        levArr = levToFour.split('').reverse();
        for (i = 0, len = levArr.length; i < len; i++) {
            switch (i) {
                case 0:
                    //星星
                    imgurl = '<img src="http://images.1758.com/game/m/hdt.png" alt="星星">';
                    break;
                case 1:
                    //月亮
                    imgurl = '<img src="http://images.1758.com/game/m/hds.png" alt="月亮">';
                    break;
                case 2:
                    //太阳
                    imgurl = '<img src="http://images.1758.com/game/m/hdu.png" alt="太阳">';
                    break;
                case 3:
                    //皇冠
                    imgurl = '<img src="http://images.1758.com/game/m/hdr.png" alt="皇冠">';
                    break;
            }
            testHtml = '';
            n = parseInt(levArr[i]);
            while (n > 0) {
                testHtml += imgurl;
                n--;
            }
            if (strHtml == '') {
                strHtml = testHtml;
            } else {
                strHtml = testHtml + strHtml;
            }
        }
        return strHtml;
    }
    return exportModal;
});