/**
 * 抓娃娃机的排行榜活动
 * 疯狂动物雨排行榜活动，借用抓娃娃
 */

define(['jquery', 'common/eventBase'], function($, EventBase) {
    var outObj = {
        init: init,
        openModal: reload,
        // 疯狂动物雨
        openFkdwyModal: reloadFkdwy
    }
    var pageNo = 1;
    var pageSize = 10;
    // ajax请求锁
    var lock = false;
    // 结束时间
    var endTime = '2018/06/24 23:59:59';
    // 填写信息提交的时候用到
    var itemId = 0;
    var intervalId = null;
    // 活动标识
    var activityFlag = 'wawa';
    // 活动状态   -1未开始  0进行时   1结束
    var activityStatus = -1;
    // 活动结束后中奖状态  -1 未中奖不可领取  0 未领取  1 已领取
    var rewardStatus = -1;

    // 活动结束后我的排行名词
    var rankIndex = -1;

    // 是否使用localstorage进行存储
    var isLocalStorage = false;


    function init() {}
    //  从头开始 重新加载
    function reload() {
        pageNo = 1;
        activityFlag = 'wawa';
        getRankData();
        localStorage.removeItem('hlmy618')
    }

    function reloadFkdwy() {
        pageNo = 1;
        activityFlag = 'fkdwy'
        getRankData();
    }
    // 重置状态
    function reset() {
        $('.warank-item').remove();
        clearInterval(intervalId);
    }
    // 获取排行榜信息
    function getRankData() {
        if (lock == true) {
            return;
        }
        lock = true;
        var url;
        $('#warankBody .warank-more').text('加载中...');
        if (activityFlag == 'wawa') {
            url = "/play/activity/zjzww/getTopList.json";
        } else {
            url = '/play/activity/zjzww/getDiamondTopList.json'
        }
        var infos = {
            pageNo: pageNo,
            pageSize: pageSize
        }
        $.post(url, infos, function(resData) {
            if (resData.result == 1 && resData.data.rankingResult) {
                var rankingResult = resData.data.rankingResult;
                activityStatus = rankingResult.activityStatus;
                // 第一次
                if (pageNo == 1) {
                    dateTimeInit();
                }
                // 处理排行榜数据
                if (rankingResult.pagingFallloadResult) {
                    handleRankData(rankingResult.pagingFallloadResult)
                }
                // 处理最后一名数据
                if (rankingResult.lastRankInfo) {
                    handleLastRankData(rankingResult.lastRankInfo)
                }
                // 处理自己消息数据
                if (rankingResult.myRankInfo) {
                    handleMyRankData(rankingResult.myRankInfo)
                }
            }
            lock = false;
            $('#warankContainer').removeClass('gone');
            $('#warankBody .warank-more').text('加载更多排行')
        }, 'json')
    }
    // 处理排行数据
    function handleRankData(pagingFallloadResult) {
        var dataList = pagingFallloadResult.dataList;
        // 有数据
        if (dataList.length) {
            var dom = getDom(dataList)
            $('#warankBody .warank-more').before(dom);
        }
        // 处理更多排行按钮
        if (pagingFallloadResult.nextPageNo > 0) {
            pageNo = pagingFallloadResult.nextPageNo;
            $('#warankBody .warank-more').removeClass('gone')
        } else {
            $('#warankBody .warank-more').addClass('gone')
        }
    }
    // 处理最后一个上榜内容
    function handleLastRankData(lastRankInfo) {
        var arr = [];
        arr.push(lastRankInfo);
        var dom = getDom(arr);
        $('#warankLast').html(dom);
    }
    // 处理我的上榜内容
    function handleMyRankData(myRankInfo) {
        myRankInfo.isMine = true;
        myRankInfo.userProfile.profile.nickname = "我";
        itemId = myRankInfo.itemId;
        var arr = [];
        arr.push(myRankInfo);
        var dom = getDom(arr);
        $('#warankMy').html(dom);
        // 设置我的排行榜 相关信息
        setHeadImg();
        setBtnStatus(myRankInfo);
    }

    function getDom(dataList) {
        if (activityFlag == 'wawa') {
            var tpl = document.getElementById('waRankingTpl').text;
        } else {
            var tpl = document.getElementById('fkdwyRankingTpl').text;
        }
        var dom = EventBase.compileTem(tpl, dataList);
        return dom;
    }

    function setHeight() {
        // 设置body 高度
        var h = window.screen.height;
        h = h - 136 - 145 - 160;
        h = h > 300 ? 300 : h;
        h = h < 150 ? 150 : h;
        document.getElementById('warankBody').style.height = h + 'px';
    }
    // 设置活动倒计时
    function dateTimeInit() {
        // 活动进行时
        if (activityStatus == 0) {
            intervalId = setInterval(function() {
                handleTime();
            }, 1000);
        } else if (activityStatus == 1) {
            // 活动已结束
            $('.warank-djs span').text('活动已结束');
            // $('#warankContainer .warank-end span').text('活动已结束');
        }
        $('.warank-djs').removeClass('gone');
    }
    // 设置 按钮状态相关信息
    function setBtnStatus(myRankInfo) {
        var url = "";
        rewardStatus = myRankInfo.rewardStatus;
        rankIndex = myRankInfo.rankIndex;
        if (activityStatus == 0) {
            // 去抓取  或者 去消耗
            if (activityFlag == 'wawa') {
                // 抓娃娃  如果第一次
                if (isLocalStorage && localStorage && !localStorage.getItem('hlmy618')) {
                    // 点击领取 618福利
                    url = 'http://images.1758.com/image/20180613/open_104818_e69fa03abd40e1d0ead4817aa4a92ab6.png'
                } else {
                    // 去抓取
                    url = "http://images.1758.com/image/20180620/open_104818_9932fabd765542ac0f5f308558d09d9b.png"
                }
            }
            setBottomImg(url);
        } else if (activityStatus == 1) {
            // 结束
            if (rewardStatus == -1) {
                // 确定
                url = "http://images.1758.com/image/20180620/open_104818_97aa8100d9b420d54e4c886d6f2ecd60.png"
            } else if (myRankInfo.rankIndex < 10 && myRankInfo.rankIndex >= 0) {
                // 领取领奖
                url = "http://images.1758.com/image/20180620/open_104818_c1e10069f9511f6b003a1aca8bea3366.png"
            } else {
                // 确定
                url = "http://images.1758.com/image/20180620/open_104818_97aa8100d9b420d54e4c886d6f2ecd60.png"
            }
            setBottomImg(url);
        }
    }

    // 设置头部header按钮
    function setHeadImg() {
        if (activityFlag == 'fkdwy') {
            // 头部
            $('#warankContainer .warank-head img').attr('src', 'http://images.1758.com/image/20180510/open_104818_8b4487fbc2be6326a3d3d57da67df343.png');
            $('#avoidWawaWrapper').removeClass('gone');
        } else {
            // 头部
            $('#warankContainer .warank-head img').attr('src', 'http://images.1758.com/image/20180620/open_104818_e959134eeaa43f7ac9e6dbf352d97b17.png');
            // 规则图标
            $('.warank-r img').attr('src', 'http://images.1758.com/image/20180620/open_104818_ae7de47d3fac3c60b4e1950737e416dc.png')
        }
    }
    // 设置底部按钮
    function setBottomImg(url) {
        $('#warankFoot .warank-btn > img').attr('src', url);
    }

    // 加载更多内容
    $(document).on('click', '#warankBody .warank-more', function() {
        getRankData();
    });

    // 去抓取 和 去领取 按钮
    $(document).on('click', '#warankFoot .warank-btn>img', function() {
        var key;
        var url;
        activityFlag == 'wawa' ? key = 'h517wawa' : key = 'h517fkdwy';
        if (activityStatus == 1 && rankIndex > -1 && rankIndex < 10) {
            // 活动结束并且已中奖
            location.href = 'http://wpa.qq.com/msgrd?v=3&uin=2660866818&site=qq&menu=yes';
        } else if (activityStatus == 0 && isLocalStorage && localStorage && !localStorage.getItem('hlmy618')) {
            // 点击领取  61福利
            $('#61Container').removeClass('gone');
            localStorage.setItem('hlmy618', 1);
            $('#warankContainer').addClass('gone');
            reset();
        } else {
            // 活动中  或者 活动结束后但是未中奖
            $('#warankContainer').addClass('gone');
            reset();
        }
    });
    // 计算活动倒计时
    function handleTime() {
        var time = new Date().getTime();
        var end = new Date(endTime).getTime();
        var total = end - time;

        var hh = Math.floor(total / 1000 / 60 / 60);
        var mm = Math.floor(total / 1000 / 60 % 60);
        var ss = Math.floor(total / 1000 % 60);
        hh = hh < 10 ? '0' + hh : hh;
        mm = mm < 10 ? '0' + mm : mm;
        ss = ss < 10 ? '0' + ss : ss;
        // $('#warankContainer .warank-end span').text('活动倒计时 ' + hh + ':' + mm + ':' + ss);
        $('.warank-djs span').text(hh + ':' + mm + ':' + ss);
    }
    // 查看规则
    $(document).on('click', '#warankContainer .warank-r', function() {
        if (activityFlag == 'wawa') {
            $('#warankRulesContainer').removeClass('gone');
        } else {
            $('#fkdwyrankRulesContainer').removeClass('gone');
        }
    });
    // 知道了
    $(document).on('click', '#warankRulesContainer .warank-btn img', function() {
        $('#warankRulesContainer').addClass('gone');
    });
    $(document).on('click', '#fkdwyrankRulesContainer .warank-btn img', function() {
        $('#fkdwyrankRulesContainer').addClass('gone');
    });
    // 排行榜关闭
    $(document).on('click', '#warankContainer', function(evt) {
        if (evt.target.id == 'warankContainer') {
            $(this).addClass('gone');
            reset();
        }
    });
    // 隐藏浮标选项点击事件
    $(document).on('click', '#avoidWawa', function(e) {
        var isSelect = document.getElementById('avoidWawa').checked;
        if (isSelect) {
            try {
                _hmt.push(['_trackEvent', 'rankBtnHide', 'hide', HLMY_CONFIG.GAME_INFO.appKey]);
            } catch (error) {}
            $('#waCoins').addClass('gone');
        } else {
            $('#waCoins').removeClass('gone');
        }
        e.stopPropagation();
    });
    // 抵用券隐藏浮标选项点击事件
    $(document).on('click', '#avoidDyq', function(e) {
        var isSelect = document.getElementById('avoidDyq').checked;
        if (isSelect) {
            $('#waCoins').addClass('gone');
        } else {
            $('#waCoins').removeClass('gone');
        }
        e.stopPropagation();
    });

    // 填写邮寄地址  点击弹框
    $(document).on('click', '#warankPostInfo', function() {
        $('#duijContainer').removeClass('gone');
    });
    // 填写邮寄地址弹框  关闭
    // $(document).on('click', '#duijContainer #duijCancel', function() {
    //     $('#duijContainer').addClass('gone');
    // });
    $(document).on('click', '#duijContainer a', function() {
        $('#duijContainer').addClass('gone');
    });


    return outObj;
})