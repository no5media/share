/**
 * 实名认证 信息
 */
define(['jquery', 'common/eventBase'], function($, EventBase) {
    var obj = {
        init: init
    };


    /**
     * 初始化实名认证内容
     * @param {*} args 
     */
    function init(args) {
        $('#rzContainer').removeClass('gone');
    }

    function IdentityCodeValid(code) {
        var city = {
            11: "北京",
            12: "天津",
            13: "河北",
            14: "山西",
            15: "内蒙古",
            21: "辽宁",
            22: "吉林",
            23: "黑龙江 ",
            31: "上海",
            32: "江苏",
            33: "浙江",
            34: "安徽",
            35: "福建",
            36: "江西",
            37: "山东",
            41: "河南",
            42: "湖北 ",
            43: "湖南",
            44: "广东",
            45: "广西",
            46: "海南",
            50: "重庆",
            51: "四川",
            52: "贵州",
            53: "云南",
            54: "西藏 ",
            61: "陕西",
            62: "甘肃",
            63: "青海",
            64: "宁夏",
            65: "新疆",
            71: "台湾",
            81: "香港",
            82: "澳门",
            91: "国外 "
        };
        var tip = "";
        var pass = true;

        if (code.length !== 15 && code.length !== 18) {
            pass = false;
            return pass;
        }
        if (!code || !/^\d{6}(18|19|20)?\d{2}(0[1-9]|1[012])(0[1-9]|[12]\d|3[01])\d{3}(\d|X)?$/i.test(code)) {
            tip = "身份证号格式错误";
            pass = false;
        } else if (!city[code.substr(0, 2)]) {
            tip = "地址编码错误";
            pass = false;
        } else {
            //18位身份证需要验证最后一位校验位
            if (code.length == 18) {
                code = code.split('');
                //∑(ai×Wi)(mod 11)
                //加权因子
                var factor = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2];
                //校验位
                var parity = [1, 0, 'X', 9, 8, 7, 6, 5, 4, 3, 2];
                var sum = 0;
                var ai = 0;
                var wi = 0;
                for (var i = 0; i < 17; i++) {
                    ai = code[i];
                    wi = factor[i];
                    sum += ai * wi;
                }
                var last = parity[sum % 11];
                if (parity[sum % 11] != code[17]) {
                    tip = "校验位错误";
                    pass = false;
                }
            }
        }
        if (!pass) console.log(tip);
        return pass;
    }

    /**
     * 提交信息
     */
    function submitInfo(realName, id) {
        var url = '/play/user/contact/fillRealname.json';
        $.post(url, {
            realname: realName,
            idNumber: id
        }, function(resData) {
            if (resData.result == 1) {
                $('#rzTip > div').text('认证成功');
                setTimeout(function() {
                    $('#rzTip').addClass('gone');
                    $('#rzContainer').addClass('gone');
                }, 1500);
                window.frames['cpframe'].postMessage({
                    hlmy: true,
                    from: '1758',
                    type: 'fnCb',
                    value: {
                        'fn': 'realNameCallback',
                        args: resData
                    }
                }, '*');
            } else {
                $('#rzTip > div').text(resData.message);
                setTimeout(function() {
                    $('#rzTip').addClass('gone');
                }, 1500);

            }
        });
    }

    // 点击背景关闭
    $(document).on('click', '#rzContainer', function(evt) {
        if (evt.target.id === 'rzContainer') {
            $(this).addClass('gone');
        }
    });
    // 关闭
    $(document).on('click', '#rzClose', function() {
        $('#rzContainer').addClass('gone');
    });
    // 提交认证
    $(document).on('click', '#rzBtn', function() {
        var name = $('#rzName').val().trim();
        var ids = $('#rzID').val().trim();
        var text = "";
        var bl = false;
        if (name == '') {
            // 不能为空
            text = "请填写真实姓名";
        } else if (ids == '') {
            text = '身份证号不能为空'
        } else if (!IdentityCodeValid(ids)) {
            text = '身份证号格式不正确'
        } else {
            bl = true;
            text = '认证中...';
        }
        $('#rzTip > div').text(text);
        $('#rzTip').removeClass('gone');
        // 错误提示
        if (!bl) {
            setTimeout(function() {
                $('#rzTip').addClass('gone');
            }, 1500)
        } else {
            submitInfo(name, ids)
        }
    });

    return obj;
})