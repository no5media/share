/**
 * 游戏邀请活动功能
 * 暂时开放在 雷霆战神
 */
define(['jquery', 'common/eventBase'], function($, EventBase) {

    var obj = {
        init: init,
        upLoadTime: upLoadTime
    }

    // 被邀请人数
    var inviteUserProfileList = [];
    // 任务列表
    var taskList = [];
    var playSeconds = 0;
    // 礼包码
    var keyCode;
    var endTime = '2018/06/11 23:59:59';
    // 当前正在执行的第几个邀请任务
    var executingIndex = -1;

    // 没有邀请成功弹框的使用状态
    // false  表示头像点击在使用弹框
    // true  表示领取按钮在使用
    var notInviteStatus = true;
    // 积分不够的情况下的弹框的按钮  是否是联系客服跳转
    var notBtnKf = false;

    // 轮播的个数
    var loopLen = 0;
    var loopNum = 1;
    // 每个的高度
    var loopHeight = 35;



    function init() {
        getInfo();
        var time = new Date().getTime();
        var end = new Date(endTime).getTime();
        if (time > end) {
            $('.invite-djs span').text('活动已结束');
            // 隐藏邀请按钮
            $('#invite522Container .i522-foot>img').css('display', 'none');
            // 调整规则按钮 位置
            $('#invite522Container .i522-rules').css('bottom', '0')
        } else {
            setInterval(handleTime, 1000)
        }
    }
    /**
     * 上报时间
     */
    function upLoadTime(time) {
        playSeconds = time;
        if (playSeconds > 0) {
            setTimeout(upUserPlayGameTime, playSeconds * 1000);
        }
    }

    function getInfo() {
        var url = '/play/activity/ltzs/loadActivityData.json';
        $.post(url, {
            appKey: HLMY_CONFIG.GAME_INFO.appKey,
            chn: HLMY_CONFIG.CHN_INFO.chn
        }, function(resData) {
            if (resData.result == 1) {
                inviteUserProfileList = resData.data.inviteUserProfileList;
                taskList = resData.data.taskList;
                handleTaskList();
                handleInviteUserList();
                // 处理轮播
                if (resData.data.lottoryList && resData.data.lottoryList.length > 0) {
                    handleLoopItem(resData.data.lottoryList);
                }
                $('#invite522Container').removeClass('gone');
                // 处理分享弹框 里面显示的奖项图片
                executingIndex = resData.data.executingIndex;
                var arr = ['http://images.1758.com/image/20180608/open_104818_1cca85552c0499db79911b186a5d3baa.png',
                    'http://images.1758.com/image/20180608/open_104818_3e3a6d161b60abce46136dc848f7fcb6.png',
                    'http://images.1758.com/image/20180608/open_104818_0341631b4c844913824def7197119b66.png',
                    'http://images.1758.com/image/20180608/open_104818_bf83edf443637c36424b0dd5ab55ffe8.png'
                ]
                if (executingIndex > -1) {
                    $('.is522-con-ani').attr('src', arr[executingIndex]);
                    $('.is522-con').removeClass('gone');
                }
            }
        })
    }

    // 上报用户玩游戏 到达五分钟
    function upUserPlayGameTime() {
        var url = '/play/activity/ltzs/submitPlaySeconds.json';
        $.post(url, {
            appKey: HLMY_CONFIG.GAME_INFO.appKey,
            chn: HLMY_CONFIG.CHN_INFO.chn,
            seconds: playSeconds
        }, function(resData) {})
    }

    // 处理任务列表数据
    function handleTaskList() {
        var tpl = document.getElementById('inviteTaskTpl').text;
        var dom1 = EventBase.compileTem(tpl, taskList);
        // 删除已存在节点
        $('.i522-item').remove();
        $('#invite522Container #i522More').before(dom1);

        // 任务列表大于1  则去掉隐藏
        // if (taskList.length > 1) {
        //     $('#i522More').removeClass('gone')
        // }
    }

    // 处理邀请人列表数据
    function handleInviteUserList() {
        var len = inviteUserProfileList.length;
        var str = "";
        for (var i = 0; i < len; i++) {
            str += '<img src="' + inviteUserProfileList[i].userProfile.profile.headUrl;
            if (inviteUserProfileList[i].status == 0) {
                str += '" class="i522-user-none'
            }
            str += '">'
        }
        $('.i522-users-inner').html(str);
    }

    /**
     * 处理轮播图
     * @param {*} dataList 
     */
    function handleLoopItem(dataList) {
        var len = dataList.length;
        loopLen = len;
        var str = "";
        for (var i = 0; i < len; i++) {
            if (dataList[i].userProfile) {
                str += '<div class="i68loop-item"><img src="' + dataList[i].userProfile.profile.headUrl + '">' + dataList[i].title + '</div>'
            } else {
                str += '<div class="i68loop-item"><img src="http://images.1758.com/image/20180608/open_104818_d2d538b50cdb02c5a5a06e40a2c56b75.png">' + dataList[i].title + '</div>'
            }
        }
        $('#i68loopContent').html(str);
        // setInterval(handleTimeOut, 4000);

    }
    // 进行轮播
    function handleTimeOut() {
        var len = loopNum * loopHeight;
        var $this = $('#i68loopContent');
        $this.css('transition-duration', '1000ms')
        $this.css('transform', 'translateY(-' + len + 'px)');
        if (loopNum >= loopLen) {
            loopNum = 0;
        }
        loopNum += 1;
    }

    /**
     * 领取任务 抵用券
     * @param {任务id} id 
     */
    function doReward(id, type, callback) {
        callback = callback || function() {};
        var url = '/play/activity/ltzs/doReward.json'
        $.post(url, {
            appKey: HLMY_CONFIG.GAME_INFO.appKey,
            chn: HLMY_CONFIG.CHN_INFO.chn,
            sourceId: id,
            sourceType: type
        }, callback)
    }

    // 由于用户未完成弹框提示  和 邀请头像点击弹框合用一个 
    // 所以需要进行dom的配置初始化
    // 暂时无用
    function notInviteModalInit() {
        $('.inot522-bo').text('大哥，邀请人数还不够领奖');
        $('.inot522-btn').text('点我去邀请');
        notInviteStatus = true;
        notBtnKf = false;
        $('.inot522-container').removeClass('gone');
    }
    // 设置文字
    function setInviteModalInit(title, btn, bl) {
        bl === true ? bl = true : bl = false;
        notBtnKf = bl;
        btn = btn || '确定';
        $('.inot522-bo').text(title);
        $('.inot522-btn').text(btn);
        notInviteStatus = false;
        $('.inot522-container').removeClass('gone');
    }

    function notInviteModalFull() {
        $('.inot522-bo').text('已邀请成功');
        $('.inot522-btn').text('知道了');
        notInviteStatus = false;
        notBtnKf = false;
        $('.inot522-container').removeClass('gone');
    }

    function notInviteModalFail() {
        $('.inot522-bo').text('好友游戏时长未达到3min');
        $('.inot522-btn').text('知道了');
        notInviteStatus = false;
        notBtnKf = false;
        $('.inot522-container').removeClass('gone');
    }

    // 计算活动倒计时
    function handleTime() {
        var time = new Date().getTime();
        var end = new Date(endTime).getTime();
        var total = end - time;
        var dd = Math.floor(total / 1000 / 60 / 60 / 24)
        var hh = Math.floor(total / 1000 / 60 / 60 % 24);
        var mm = Math.floor(total / 1000 / 60 % 60);
        var ss = Math.floor(total / 1000 % 60);
        hh = hh < 10 ? '0' + hh : hh;
        mm = mm < 10 ? '0' + mm : mm;
        ss = ss < 10 ? '0' + ss : ss;
        $('.invite-djs span').text(dd + '天' + hh + '时' + mm + '分' + ss + '秒');
    }

    // 复制内容
    function copyText(s) {
        if (typeof document.execCommand === 'function') {
            var oInput = document.createElement('input');
            oInput.value = s;
            oInput.style.position = 'fixed';
            oInput.style.zIndex = -222;
            oInput.className = 'oInput';
            document.body.appendChild(oInput);
            oInput.select(); // 选择对象
            var bl = document.execCommand("Copy"); // 执行浏览器复制命令
            oInput.remove();
            return bl;
        } else {
            return false;
        }
    }


    // 更多任务折叠打开
    $(document).on('click', '#i522More img', function() {
        $('#i522More').addClass('gone');
        $('.i522-item').removeClass('gone');
    });

    // 领取按钮 查看按钮 各种操作
    $(document).on('click', '.i522-btn', function() {
        var status = $(this).attr('data-status');
        var type = $(this).attr('data-type');
        var index = $(this).attr('data-index');
        switch (status) {
            case '0':
                // 未完成未领取
                break;
            case '1':
                // 未完成进行中
                break;
            case '10':
                // 已完成未领取
                if (index == 0) {
                    // 30金币
                    setInviteModalInit('领取成功，24小时内工作人员将发送到您游戏中')
                } else if (index == 1) {
                    // 8 抵用券
                    // 抵用券领取
                    doReward($(this).attr('data-id'), type);
                    setInviteModalInit('领取成功，已发到您账户中，可在支付页面中使用')
                } else if (index == 2) {
                    // 1个随机娃娃
                    setInviteModalInit('请您联系在线客服QQ：2660866818领取。（另需支付运费15元）', '联系客服领取', true)
                } else if (index == 3) {
                    // 2个随机娃娃
                    setInviteModalInit('请您联系在线客服QQ：2660866818领取。（另需支付运费15元）', '联系客服领取', true)
                }
                break;
            case '20':
                // 已完成已领取
                if (index == 0) {
                    // 30金币
                    setInviteModalInit('领取成功，稍后工作员发送到您游戏中')
                } else if (index == 1) {
                    // 8 抵用券
                    // 抵用券领取
                    setInviteModalInit('领取成功，已发到您账户中，可在支付页面中使用')
                } else if (index == 2) {
                    // 1个随机娃娃
                    setInviteModalInit('请您联系在线客服QQ：2660866818领取。（另需支付运费15元）', '联系客服领取', true)
                } else if (index == 3) {
                    // 2个随机娃娃
                    setInviteModalInit('请您联系在线客服QQ：2660866818领取。（另需支付运费15元）', '联系客服领取', true)
                }
                break;
        }
    });
    // 邀请用户头像点击操作
    $(document).on('click', '.i522-users-inner img', function(evt) {
        if ($(this).hasClass('i522-user-none')) {
            notInviteModalFail();
        } else {
            notInviteModalFull();
        }
    });
    // 隐藏浮标选项点击事件
    $(document).on('click', '#avoidIn522', function(evt) {
        var isSelect = document.getElementById('avoidIn522').checked;
        if (isSelect) {
            $('#waCoins').addClass('gone');
        } else {
            $('#waCoins').removeClass('gone');
        }
        evt.stopPropagation();
    });
    // 主弹框关闭
    $(document).on('click', '#invite522Container', function(evt) {
        if (evt.target.id === 'invite522Container') {
            $(this).addClass('gone');
        }
    });
    // 分享弹框 领取成功弹框 点击关闭
    $(document).on('click', '.is522-container,.isuc522-container', function(evt) {
        if (evt.target.nodeName.toLocaleLowerCase() === 'div') {
            $(this).addClass('gone');
        }
    });
    // 邀请人数不够  关闭弹框
    $(document).on('click', '.inot522-container', function(evt) {
        if (evt.target.className.toLocaleLowerCase().indexOf('inot522-container') >= 0) {
            $(this).addClass('gone');
        }
    });
    // 邀请人数不够 去分享按钮
    $(document).on('click', '.inot522-btn', function(evt) {
        // 联系客服 暂时处理
        if (notBtnKf) {
            location.href = "http://wpa.qq.com/msgrd?v=3&uin=2660866818&site=qq&menu=yes"
            return;
        }
        if (notInviteStatus) {
            // 点击领取按钮弹框
            $('.inot522-container').addClass('gone');
            try {
                _hmt.push(['_trackEvent', '邀请活动522', '点我去邀请', HLMY_CONFIG.GAME_INFO.appKey]);
            } catch (e) {}
            $('.is522-container').removeClass('gone');
            evt.stopPropagation();
        } else {
            // 点击头像弹框
            $('.inot522-container').addClass('gone');
        }
    });
    // 去分享按钮
    $(document).on('click', '.i522-foot>img', function(evt) {
        $('.is522-container').removeClass('gone');
        try {
            _hmt.push(['_trackEvent', '邀请活动522', '邀请好友', HLMY_CONFIG.GAME_INFO.appKey]);
        } catch (e) {}
    })

    // 规则 我知道了
    $(document).on('click', '.ir522-btn', function(evt) {
        $('.ir522-container').addClass('gone');
    });
    // 展示规则
    $(document).on('click', '.i522-rules', function(evt) {
        $('.ir522-container').removeClass('gone');
    });

    // 复制功能
    // 如果复制成功  则直接显示提示  然后定时消失
    // 如果复制失败 弹框 不消失
    $(document).on('click', '#ig61Container .ig61-copy', function(evt) {
        var text = "";
        var bl = false;
        if (keyCode && copyText(keyCode)) {
            text = "复制成功"
            bl = true;
        } else {
            text = "请手动长按复制礼包码"
        }
        $('#ig61Tip .ig61-tip').text(text);
        if (bl) {
            $('#ig61Body').addClass('gone');
        }
        $('#ig61Tip').removeClass('gone');
        setTimeout(function() {
            if (bl) {
                $('#ig61Container').addClass('gone');
            }
            $('#ig61Tip').addClass('gone');
        }, 1000)
    });

    // 复制功能旁边的我知道了点击功能
    $(document).on('click', '#ig61Container .ig61-know', function(evt) {
        $('#ig61Container').addClass('gone');
    })

    // 奖励点击图片 进行弹框说明提示
    $(document).on('click', '#invite522Container .i522-re img', function(evt) {
        var index = $(this).attr('id');
        showInstructionModal(index);
    })

    function showInstructionModal(index) {
        $('#i68InsContent > div').addClass('gone');
        var str = '#i68InsContent .i68-' + index;
        var arr = ['30金币', '8元抵用券', '史迪奇', '两个随机娃娃', '趴趴熊'];
        $('.i68-title').text(arr[index]);
        $(str).removeClass('gone');
        $('#i68InsContainer').removeClass('gone');
    }
    $(document).on('click', '#i68InsContainer .i68-btn', function() {
        $('#i68InsContainer').addClass('gone');
    })
    return obj;
})