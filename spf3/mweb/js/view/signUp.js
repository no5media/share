/**
 * 1、目前抓娃娃游戏下的浮标弹框按钮
 * 2、签到功能
 */
define(['jquery', 'common/eventBase', 'view/wawaRank', 'view/invite'], function($, EventBase, WaRank, Invite) {
    var outObj = {
        init: init
    };

    // 活动用到的类型
    var gameType = 0;
    // 活动状态   -1未开始  0进行时   1结束
    var activityStatus = -1;
    // 活动入口按钮状态  0 不开启 1 一级入口   2 二级入口
    var activityEntryStatus = 0;
    // 邀请活动的上报玩游戏时间的延迟时间秒数
    var playSeconds = 0;
    // 签到情况
    var signUpTemList = [{
        // 第一轮
        head: 'http://images.1758.com/image/20180320/open_104818_e3993af6d7eae085b15c01b4094b5289.jpg',
        dayList: ['http://images.1758.com/image/20180320/open_104818_5652b755cca9c00c2ff4d460e7603d28.png', 'http://images.1758.com/image/20180320/open_104818_5652b755cca9c00c2ff4d460e7603d28.png', 'http://images.1758.com/image/20180320/open_104818_5652b755cca9c00c2ff4d460e7603d28.png', 'http://images.1758.com/image/20180320/open_104818_5652b755cca9c00c2ff4d460e7603d28.png', 'http://images.1758.com/image/20180320/open_104818_5652b755cca9c00c2ff4d460e7603d28.png', 'http://images.1758.com/image/20180320/open_104818_5652b755cca9c00c2ff4d460e7603d28.png', 'http://images.1758.com/image/20180320/open_104818_5652b755cca9c00c2ff4d460e7603d28.png'],
        active: 'http://images.1758.com/image/20180320/open_104818_7542d973dd4889f54a77342f3678629f.png',
        // 当前第几轮模板
        temp: 0,
        text: ['一天', '二天', '三天', '四天', '五天', '六天', '七天']
    }, {
        // 第二轮
        head: 'http://images.1758.com/image/20180320/open_104818_f2e6edb1a77bd67adea6ea31f0f151cd.jpg',
        dayList: ['http://images.1758.com/image/20180320/open_104818_5652b755cca9c00c2ff4d460e7603d28.png', 'http://images.1758.com/image/20180320/open_104818_5652b755cca9c00c2ff4d460e7603d28.png', 'http://images.1758.com/image/20180320/open_104818_5652b755cca9c00c2ff4d460e7603d28.png', 'http://images.1758.com/image/20180320/open_104818_5652b755cca9c00c2ff4d460e7603d28.png', 'http://images.1758.com/image/20180320/open_104818_5652b755cca9c00c2ff4d460e7603d28.png', 'http://images.1758.com/image/20180320/open_104818_5652b755cca9c00c2ff4d460e7603d28.png', 'http://images.1758.com/image/20180320/open_104818_5652b755cca9c00c2ff4d460e7603d28.png'],
        active: 'http://images.1758.com/image/20180320/open_104818_7542d973dd4889f54a77342f3678629f.png',
        activeMoney: "http://images.1758.com/image/20180320/open_104818_7542d973dd4889f54a77342f3678629f.png",
        // 当前第几轮模板
        temp: 1,
        text: ['一天', '二天', '三天', '四天', '五天', '六天', '七天']
    }, {
        // 第三轮
        head: 'http://images.1758.com/image/20180320/open_104818_f2e6edb1a77bd67adea6ea31f0f151cd.jpg',
        dayList: ['http://images.1758.com/image/20180320/open_104818_95c291a2e67f9fde8ccc031defe7829a.png', 'http://images.1758.com/image/20180320/open_104818_95c291a2e67f9fde8ccc031defe7829a.png', 'http://images.1758.com/image/20180320/open_104818_95c291a2e67f9fde8ccc031defe7829a.png', 'http://images.1758.com/image/20180320/open_104818_95c291a2e67f9fde8ccc031defe7829a.png', 'http://images.1758.com/image/20180320/open_104818_95c291a2e67f9fde8ccc031defe7829a.png', 'http://images.1758.com/image/20180320/open_104818_95c291a2e67f9fde8ccc031defe7829a.png', 'http://images.1758.com/image/20180320/open_104818_271a58d8a4d372483cef6f9f2d085655.png'],
        active: 'http://images.1758.com/image/20180320/open_104818_8a97d02d7c236f754e89396346861b9b.png',
        activeMoney: "http://images.1758.com/image/20180320/open_104818_9ac68628aede4240f70cecffa2f0a478.png",
        // 当前第几轮模板
        temp: 2,
        text: ['一天', '二天', '三天', '四天', '五天', '六天', '七天']
    }];

    // 签到基本信息
    var signUpInfo = {
        "rounds_times": 1, // 轮次 1-8
        "rounds_type": 1, // 类型 1-3
        "total_coins": 0, // 钱数 单位是分
        "consecutive_sign_in_days": 1, //累计
        "sign_in_days": 1, //当前签到天数 1-7
        "sign_in_remind": true, //是否提醒
        "follow": false, // 是否关注过公众号
        "user_points": 0, //积分
        "exchange_info": "已连续签到x天！连签XX天领红包"
    };
    // 已签到信息
    // var userSignUpInfoList = [{
    //     "sign_in_days": 1, //当前第几天签到
    //     "reward_type": 1, // 类型 1红包  2积分
    //     "reward_amount": 90, // 得到的奖励
    //     "sign_in_status": 1 // 签到类型 暂时无用 
    // }]
    var userSignUpInfoList = [];



    function init() {
        // 五四活动
        wuSiActive();
    };
    // 五四活动
    function wuSiActive() {
        var url = '/play/activity/loadActivity20180520.json';
        $.post(url, {
            appKey: HLMY_CONFIG.GAME_INFO.appKey,
            chn: HLMY_CONFIG.CHN_INFO.chn
        }, function(resData) {
            if (resData.result == 1 && resData.data) {
                // 活动类型
                gameType = resData.data.gameType;
                // 活动状态
                activityStatus = resData.data.activityStatus;
                activityEntryStatus = resData.data.activityEntryStatus;
                switch (gameType) {
                    case -1:
                        // 无活动
                        break;
                    case 3:
                        // 邀请活动
                        playSeconds = resData.data.playSeconds;
                        if (resData.data.promptActive == 1) {
                            // 弹屏显示
                            Invite.init();
                        }
                        // 活动进行时  上报时间
                        if (activityStatus == 0) {
                            Invite.upLoadTime(playSeconds);
                        }
                        // 一级浮标入口
                        if (activityEntryStatus == 1) {
                            $('#waCoins').css('width', '70px');
                            $('#waCoins img').attr('src', 'http://images.1758.com/image/20180428/open_104818_493b771602bc371282d9c2e982cd887f.gif');
                            $('#waCoins').removeClass('gone');
                        } else if (activityEntryStatus == 2) {
                            // 二级入口
                            $('#waCoins').removeClass('gone');
                        }

                        break;
                    case 2:
                        // 抵用券活动
                        $('#dyqContainer img').attr('src', resData.data.promoteImgUrl);
                        if (resData.data.promptActive == 1) {
                            // 弹屏显示
                            $('#dyqContainer').removeClass('gone');
                        }
                        // 一级入口
                        if (activityEntryStatus == 1) {
                            $('#waCoins').css('width', '70px');
                            $('#waCoins img').attr('src', 'http://images.1758.com/article/image/2018/06/13/2161528886644456.gif');
                            $('#waCoins').removeClass('gone');
                        }
                        break;
                    case 1:
                        // 疯狂动物雨   按钮显示正常
                        if (activityEntryStatus == 1) {
                            $('#waCoins').css('width', '70px');
                            $('#waCoins img').attr('src', 'http://images.1758.com/image/20180504/open_104818_a1250319d8214c30930d38bf0d64fc43.gif');
                            $('#waCoins').removeClass('gone');
                            return;
                        }
                        // 是否自动弹屏
                        if (resData.data.promptActive == 1) {
                            WaRank.openFkdwyModal(activityStatus);
                        }
                        break;
                    case 0:
                        // 娃娃机
                        // 自动弹框
                        if (resData.data.promptActive == 1) {
                            WaRank.openModal(activityStatus);
                        }
                        // 设置福利抵用券
                        $('#61Container img').attr('src', resData.data.promoteImgUrl);
                        if (activityEntryStatus == 0) {
                            // 活动按钮关闭 不显示
                            $('#waBtnRank').addClass('gone');
                        } else if (activityEntryStatus == 1) {
                            // 1级入口
                            $('#waCoins img').attr('src', 'http://images.1758.com/image/20180620/open_104818_7a57d729d66955f9bebbcaec6fdeaaa5.gif')
                            $('#waCoins').css('width', '70px');
                        }
                        // 特殊处理  特殊渠道 不展示 改按钮
                        if (HLMY_CONFIG.CHN_INFO.chn == '' || voidCoinBtn.indexOf(HLMY_CONFIG.CHN_INFO.chn) < 0) {
                            $('#waCoins').removeClass('gone');
                        }
                        break;
                }
            }
        })

        // 抵用券点击取消事件绑定
        $(document).on('click', '#dyqContainer', function(evt) {
            if (evt.target.id == 'dyqContainer') {
                $(this).addClass('gone');
            }
        });

        // 61福利 展示 gif
        $(document).on('click', '#61Container', function(evt) {
            if (evt.target.id == '61Container') {
                $(this).addClass('gone');
            }
        })
    }

    /**
     * 显示关注和邀请按钮
     */
    function showFollowBtn() {
        if (HLMY_CONFIG.GAME_INFO.appKey == '20fb85e1670f4124536dd5d2e6752f1a') {
            $('#waCoins').removeClass('gone');
        }
    }
    // 点击显示浮层弹框
    $(document).on('click', '#waCoins', function() {
        try {
            _hmt.push(['_trackEvent', 'waBtn', 'open', HLMY_CONFIG.GAME_INFO.appKey]);
        } catch (error) {}
        // 活动
        switch (gameType) {
            case -1:
                // 无活动
                break;
            case 3:
                // 邀请活动
                if (activityEntryStatus == 1) {
                    Invite.init();
                } else {
                    $('#waCoinsA').removeClass('gone');
                }
                break;
            case 2:
                // 抵用券活动
                $('#dyqContainer').removeClass('gone');
                break;
            case 1:
                // 疯狂动物雨
                WaRank.openFkdwyModal(activityStatus);
                break;
            case 0:
                // 则弹出二级页面
                if (activityEntryStatus == 1) {
                    WaRank.openModal(activityStatus);
                } else {
                    $('#waCoinsA').removeClass('gone');
                }
                break;
        }
    });
    // 浮层弹框点击取消
    $(document).on('click', '#waCoinsA', function(evt) {
        $(this).addClass('gone');
    });
    // 邀请按钮点击事件
    $(document).on('click', '#waBtnInvite', function(evt) {
        $('#waCoinsA').addClass('gone');
        $('#shareTipInfo').removeClass('gone');
        try {
            _hmt.push(['_trackEvent', 'waInvite', 'click', HLMY_CONFIG.GAME_INFO.appKey]);
        } catch (error) {}
        evt.stopPropagation();
    });
    // 关注按钮点击事件
    $(document).on('click', '#waBtnFollow', function(evt) {
        $('#waCoinsA').addClass('gone');
        $('#waCoinsF').removeClass('gone');
        try {
            _hmt.push(['_trackEvent', 'waFollow', 'click', HLMY_CONFIG.GAME_INFO.appKey]);
        } catch (error) {}
        evt.stopPropagation();
    });
    // 关注提示关闭事件
    $(document).on('click', '#waCoinsF .wa-close', function() {
        $('#waCoinsF').addClass('gone');
    });

    // 签到按钮点击事件
    $(document).on('click', '#waBtnSignin', function(evt) {
        $('#waCoinsA').addClass('gone');
        $('#waCoinsSign').removeClass('gone');
        try {
            _hmt.push(['_trackEvent', 'waSignin', 'click', HLMY_CONFIG.GAME_INFO.appKey]);
        } catch (error) {}
        evt.stopPropagation();
    });
    // 签到提示关闭事件
    $(document).on('click', '#waCoinsSign .wa-close', function() {
        $('#waCoinsSign').addClass('gone');
    });
    // 娃娃机排行榜事件点击
    $(document).on('click', '#waBtnRank', function() {
        if (gameType == 3) {
            // 邀请活动
            Invite.init();
        } else if (gameType == 0) {
            // 排行榜活动
            WaRank.openModal(activityStatus);
        }
    });


    /******************************************* 
     *********** 签到功能 **************
     *******************************************/
    function signUpInit() {
        var url = "/openapi/activity/signin/loadSignInInfo.json";
        $.get(url, handleSignUpInfo, 'json');
    }

    /**
     * 处理签到信息
     * 
     * @param {any} resData 
     */
    function handleSignUpInfo(resData) {
        // 组装要渲染模板的数据
        var signData;
        // 头部图片
        var headImg = "";
        if (resData.result == 1) {
            var rootDom = $('#checkInContainer');
            signUpInfo = resData.data.user_sign_info;
            // 设置提示信息
            if (signUpInfo.rounds_type == 1) {
                signUpInfo.exchange_info = '已连续签到' + signUpInfo.sign_in_days + '天！连签7天最高可领10元';
            } else {
                signUpInfo.exchange_info = '已连续签到' + signUpInfo.sign_in_days + '天！第7天红包最大';
            }
            userSignUpInfoList = resData.data.user_sign_info_list;
            // 第几轮 返回是 1 2 3  需要是 0 1 2
            signData = signUpTemList[signUpInfo.rounds_type - 1];
            // 添加当前签到第几天
            signData.nowDay = signUpInfo.sign_in_days - 1;
            // 已经签到的数组列表
            signData.checkinList = [];
            // 获取显示的已签到信息
            getSignUpMoneyInfo(userSignUpInfoList, signData.checkinList);
            signData.row = 1;
            var tpl = document.getElementById('signUpTpl').text;
            var dom1 = EventBase.compileTem(tpl, signData);
            $('#checkInContainer .check1').html(dom1);
            signData.row = 2;
            var dom2 = EventBase.compileTem(tpl, signData);
            $('#checkInContainer .check2').html(dom2);
            // 设置头部图片
            if (signUpInfo.rounds_type == 1) {
                // 新人
                headImg = "http://images.1758.com/image/20180320/open_104818_e3993af6d7eae085b15c01b4094b5289.jpg";
            } else {
                headImg = "http://images.1758.com/image/20180320/open_104818_f2e6edb1a77bd67adea6ea31f0f151cd.jpg"
                    // 设置积分
                rootDom.find('.chec-score').html(signUpInfo.user_points + '积分');
                rootDom.find('.chec-score').removeClass('gone');
            }
            rootDom.find('.chec-head img').attr('src', headImg);
            // 设置提示语
            rootDom.find('.chec-tips div').html(signUpInfo.exchange_info);
            // 设置金钱树
            var domMoney = 0;
            // 如果后台数据告诉金币有变化了 则进行滚动显示   其余是默认展示
            if (signUpInfo.coinChange) {
                var intervalId = setInterval(function() {
                    if (domMoney >= signUpInfo.total_coins) {
                        clearInterval(intervalId);
                    } else {
                        if (signUpInfo.total_coins - domMoney >= 10) {
                            domMoney += 10;
                        } else {
                            domMoney = signUpInfo.total_coins;
                        }
                        rootDom.find('.chec-money').html(domMoney / 100);
                    }
                }, 100);
            } else {
                domMoney = signUpInfo.total_coins;
                rootDom.find('.chec-money').html(domMoney / 100);
            }
            // 设置提醒
            var isCheck = signUpInfo.follow && signUpInfo.sign_in_remind;
            if (isCheck) {
                rootDom.find('.checset-switch input').attr('checked', isCheck);
            }
            // 设置动画显示成功签到几天
            var successImgArr = ['http://images.1758.com/image/20180327/open_104818_10e59f5492bdc393610272267b29d3e4.png',
                'http://images.1758.com/image/20180327/open_104818_074626de619c170abd608d59d652774a.png',
                'http://images.1758.com/image/20180327/open_104818_f4f60785bcb05470bc49d8c99ce92f08.png',
                'http://images.1758.com/image/20180327/open_104818_453d710936855bdd7842939cc4bfc2ae.png',
                'http://images.1758.com/image/20180327/open_104818_50b8c36d29af4bba1d9721415a51d387.png',
                'http://images.1758.com/image/20180327/open_104818_8a6769b8564cd7666e8643258e9b8deb.png',
                'http://images.1758.com/image/20180327/open_104818_65336ed9f0947817d2ffd2417af480fc.png'
            ];
            $('.chec-te2').attr('src', successImgArr[signData.nowDay])
            $('#checkInContainer').removeClass('gone');
            // 去掉特效 几秒后
            setTimeout(function() {
                $('.chec-tx').addClass('gone');
            }, 2500);
        }

        // 获取兑换列表信息
        exchangeListInit();
        changeFollowText();
    }

    /**
     * 显示 积分30  或者 ¥ 3.4 
     */
    function getSignUpMoneyInfo(arr, checkinList) {
        arr.forEach(function(item) {
            if (item.reward_type == 1) {
                checkinList.push('¥' + item.reward_amount / 100);
            } else if (item.reward_type == 2) {
                checkinList.push('积分' + item.reward_amount);
            }
        })
    }
    /**
     * 获取兑换列表信息
     */
    function exchangeListInit() {
        var url = "/openapi/activity/signin/loadExchanges.json";
        $.get(url, handleExchangeInfo, 'json');
    }
    /**
     * 处理兑换信息
     * 
     * @param {any} resData 
     */
    function handleExchangeInfo(resData) {
        if (resData.result == 1) {
            var tpl = document.getElementById('signUpExchangeTpl').text;
            var dom = EventBase.compileTem(tpl, resData.data.exchange_infos);
            $('#exchItemWrapper').html(dom);
            $('#exchMoney').html(signUpInfo.total_coins / 100);
            handleEndTime(resData.data.invalidDate);
        }
    }

    /**
     * 处理到期时间
     * @param {time} 时间 
     */
    function handleEndTime(time) {
        time = time.toString();
        var year = time.substring(0, 4);
        var month = parseInt(time.substring(4, 6));
        var day = time.substring(6, 8);
        var now = new Date().getTime();
        var endTimeDate = year + '年' + month + '月' + day + '日到期'
        var endTime = new Date(year, month - 1, day).getTime();
        if (endTime - now <= 20 * 24 * 60 * 60 * 1000) {
            $('#endTime').text(endTimeDate);
            $('#endTime').removeClass('gone');
        }
    }

    // 显示兑换页面
    $(document).on('click', '.chec-btn', function() {
        $('#exchangeContainer').removeClass('gone');
    });
    // 签到页面消失
    $(document).on('click', '#checkInContainer', function(evt) {
        var $this = $(this);
        if (evt.target.id == 'checkInContainer') {
            $this.addClass('gone');
            // 如果是新用户 则提示
            if (signUpInfo.rounds_type == 1) {
                $('#checkInTip .exfo-img').attr('src', 'http://images.1758.com/image/20180404/open_104818_eef52efa84f4b77d29866b9423d612cc.png');
                $('#checkInTip').removeClass('gone');
            }
        }
    });
    // 切换开关
    $(document).on('click', '#checkInContainer .checset-switch', function(evt) {
        var value = $('#checkInContainer input').prop('checked');
        if (value) {
            // 打开状态 用户点击要关闭
            $('#checkInContainer input').prop('checked', !value);
            $.get('/openapi/activity/signin/changeSignInRemind.json?signInRemind=false', function(resData) {
                console.log(resData);
            }, 'json');
            // 提示二维码  在哪里可以签到
            $('#checkInTip .exfo-img').attr('src', 'http://images.1758.com/image/20180404/open_104818_eef52efa84f4b77d29866b9423d612cc.png');
            $('#checkInTip').removeClass('gone');
        } else {
            // 关闭状态要打开  提示去关注
            if (signUpInfo.follow) {
                // 用户已经关注过了  无需再谈提示
                $.get('/openapi/activity/signin/changeSignInRemind.json?signInRemind=true', function(resData) {
                    $('#checkInContainer input').prop('checked', !value);
                    changeFollowText();
                }, 'json');
            } else {
                $('#checkInTip .exfo-img').attr('src', 'http://images.1758.com/image/20180321/open_104818_e086811b574d322e4fb4ecf5b5f15e16.png');
                $('#checkInTip').removeClass('gone');
            }
        }
        changeFollowText();
        evt.stopPropagation();
    })
    $(document).on('click', '#checkInTip .exfo-close', function() {
        $('#checkInTip').addClass('gone');
    })

    // 体现按钮点击
    $(document).on('click', '.chec-btn', function() {
        $('#exchangeContainer').removeClass('gone');
    });
    // 兑换按钮点击
    $(document).on('click', '.chec-btn', function() {
        $('#exchangeContainer').removeClass('gone');
    });
    // 提现页面消失
    $(document).on('click', '#exchangeContainer', function(evt) {
        var $this = $(this);
        if (evt.target.id == 'exchangeContainer') {
            $this.addClass('gone');
        }
    });
    // 提现页面 提现按钮点击
    $(document).on('click', '.tx', function() {
        if (signUpInfo.total_coins >= 10 * 100) {
            alert('请联系官方客服QQ：2660866818 提现领奖')
        } else {
            alert('金额不足10元');
        }
    });
    // 提现页面 兑换按钮点击
    $(document).on('click', '.dui', function() {
        var $this = $(this);
        $.post('/openapi/activity/signin/doExchanges.json', {
            exchangeId: $(this).attr('id')
        }, function(resData) {
            if (resData.result == 1) {
                $this.addClass('exch-has');
                $this.html('已兑换');
                $this.parents('.exch-item').find('.exch-num').html('1/1')
            } else {
                alert(resData.message);
            }
        }, 'json')
    });
    // 签到功能按钮
    $(document).on('click', '#headContent .head-dowload', function() {
        try {
            _hmt.push(['_trackEvent', 'signin', 'click', '签到']);
        } catch (error) {}
        signUpInit();
    })

    /**
     * 判断是否是新用户 如果是新用户 则会弹框
     * 暂时废弃
     * 
     */
    function judgeIsNew() {
        var url = "/openapi/activity/signin/lodadUserInfo.json";
        $.get(url, function(resData) {
            if (resData.result == 1 && resData.data.is_new) {
                try {
                    _hmt.push(['_trackEvent', 'signinAuto', 'click', '弹框签到']);
                } catch (error) {}
                signUpInit();
            }
        }, 'json')
    }

    /**
     * 根据关注订阅状态修改显示文字
     */
    function changeFollowText() {
        // 当前订阅状态
        var value = $('#checkInContainer input').prop('checked');
        if (value) {
            // 已开启
            $('.checset-swtext').addClass('gone');
            $('.checset-text').text('已开启签到提醒')
        } else if (signUpInfo.rounds_type == 1) {
            // 未开启，并且是新用户
            $('.checset-swtext').removeClass('gone');
            $('.checset-text').text('断签后新人福利将消失')
        } else {
            // 未开启，老用户
            $('.checset-swtext').removeClass('gone');
            $('.checset-text').text('断签后签到记录退至起点')
        }
    }

    return outObj;
})