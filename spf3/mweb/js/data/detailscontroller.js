define(['data/getInfoData','common/eventBase'],function(DetailsData,EventBase){
	var dataInfo = {};
	dataInfo = {
		/**
		 * 
		 * @param {Object} url
		 * @param {Object} infos
		 * @param {Object} callback
		 */
		getDataList:function(url,infos,callback){
			DetailsData.getData(url,infos,function(data){
				// data = dateFormat(data);
				callback(data);
			});
		},
		/**
		 * 获取有戏的聊吧ugc信息
		 * @param  {[type]}   url      [description]
		 * @param  {[type]}   infos    [description]
		 * @param  {Function} callback [description]
		 * @return {[type]}            [description]
		 */
		getGameFeeds:function(url,infos,callback){
			DetailsData.getData(url,infos,function(data){
				if (data.result == 1 && !!data.data.ugcData) {
					data.data.ugcData.dataList = dateFormat(data.data.ugcData.dataList);
				}
				callback(data);
			});
		}
	}
	function dateFormat(dataArray){
		var testLevel;
		for(var i=0,len=dataArray.length;i<len;i++){
			testLevel = dataArray[i].userProfile.expInfo;
			
			dataArray[i].createTime = EventBase.dateFormat(dataArray[i].createTime,'yyyy.MM.dd hh:mm');
			if(!dataArray[i].img){
				dataArray[i].img = [];
			}else{
				dataArray[i].img = dataArray[i].img.split(',');
			}
			
			testLevel['expShowing'] = userExpLevelFormat(testLevel.expLevel);
		}
		return dataArray;
	}
	/*用户等级计算*/
	function userExpLevelFormat(level){
		var levToFour,levArr,
			i,len,
			imgurl,testHtml,strHtml='';
		level = parseInt(level);
		if(!level){return '';}
		levToFour = level.toString(4);
		levArr = levToFour.split('').reverse();
		for(i =0,len = levArr.length;i<len;i++){
			switch(i){
				case 0:
					//星星
					imgurl = '<img src="http://images.1758.com/game/m/hdt.png" alt="星星">';
				break;
				case 1:
					//月亮
					imgurl = '<img src="http://images.1758.com/game/m/hds.png" alt="月亮">';
				break;
				case 2:
					//太阳
					imgurl = '<img src="http://images.1758.com/game/m/hdu.png" alt="太阳">';
				break;
				case 3:
					//皇冠
					imgurl = '<img src="http://images.1758.com/game/m/hdr.png" alt="皇冠">';
				break;
			}
			testHtml = '';	
			n = parseInt(levArr[i]);
			while(n > 0){
				testHtml += imgurl;
				n--;
			}
			if(strHtml == ''){
				strHtml = testHtml;
			}else{
				strHtml = testHtml + strHtml;
			}
		}
		return strHtml;
	}
	return dataInfo;
})