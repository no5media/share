function connectWebViewJavascriptBridge(callback) {
    if (window.WebViewJavascriptBridge) {
        callback(WebViewJavascriptBridge)
    } else {
        document.addEventListener('WebViewJavascriptBridgeReady', function() {
            callback(WebViewJavascriptBridge)
        }, false)
    }
}
var G_intervalTimer={
	//setInterval Id
	intervalCountDown:'',
	//setTimeoutId
	timeoutId:'',
	countdownTime: 60,
	appkey:'',
	qqlogin:'',
	weibologin:'',
	weixinlogin:'',
	trialTime: 2,
	gameFrameLoad:false,
	receiveMessage:false,
	state:'',
	chn:'',
	share:'',
	//初始化操作
	init:function(){
		//判断设备，是否显示微信登录
		this.isClient();
		//确定登录的url
		this.loginUrl();
		//绑定微信登录
		this.weixinLogin();
		//绑定tip的关闭和登录按钮事件
		this.tipHandler();
		//添加接受登录页面消息事件
		this.bindMesage();
	},
	//倒计时
	setCountDown:function(){
		if(G_intervalTimer.countdownTime == 0){
			$('.logtip').css('display','none');
			window.clearInterval(G_intervalTimer.intervalCountDown);
		}else{
			G_intervalTimer.countdownTime = G_intervalTimer.countdownTime-1;
			$('#countdown').html(G_intervalTimer.countdownTime);
		}
	},
	countDown:function (){
		this.intervalCountDown = setInterval(function(){
			G_intervalTimer.setCountDown();
		},1000);
	},
	//设定时间 弹出登录窗口
	showLogin:function(){
		$('#login-bc').css('display','block');
	},
	//接受登录页面传递的消息
	receiveMesage:function(e){
		var time = e.data;
		var origin = e.origin;
		G_intervalTimer.receiveMessage = true;
		if(G_intervalTimer.gameFrameLoad){
			$('.logtip').css('display','block');
			//G_intervalTimer.countDown();
		}
		this.timeoutId = setTimeout(function(){
			$('.logtip').css('display','none');
			G_intervalTimer.showLogin();
		},G_intervalTimer.trialTime*60*1000)
	},
	//判断ua
	isClient:function(){
		var ua = navigator.userAgent.toLowerCase();
		if((ua.indexOf('micromessenger') >= 0) || (ua.indexOf('dwjia') >= 0)){
			$('#wxLogin').css('display','inline');
		}
	},
	//微信登录
	weixinLogin:function(){
		$('#wxLogin').on('click',function(){
			try{
		      	android_wxgame_auth.weixinLogin();
				android_tw_system.toast("通过微信登录，可看到好友的得分排名哦",1);
		    }catch (exp){
		        try{
		  			ios.callHandler('weixinLogin',{},function(res){});
		  		}catch (exp2){
		   		 	location.href=G_intervalTimer.weixinlogin;
		  		}
		    }
		});
	},
	loginUrl:function(){
		this.qqlogin="http://wx.1758.com/game/platform/v1.0/qq/login?appKey="+this.appkey+"&state="+this.state+"&chn="+this.chn+"&share="+this.share;
		this.weibologin="http://wx.1758.com/game/platform/v1.0/weibo/login?appKey="+this.appkey+"&state="+this.state+"&chn="+this.chn+"&share="+this.share;
		this.weixinlogin="http://wx.1758.com/game/platform/v1.0/user/login?appKey="+this.appkey+"&state="+this.state+"&chn="+this.chn+"&share="+this.share;
		$('#qqLogin').attr('href',this.qqlogin);
		$('#weiboLogin').attr('href',this.weibologin);
	},
	//绑定message事件接受从login传递过来的参数
	bindMesage:function(){
		if(typeof window.addEventListener != 'undefined') {
		    window.addEventListener('message', this.receiveMesage, false);
		} else if (typeof window.attachEvent != 'undefined') {
		    window.attachEvent('onmessage', this.receiveMesage);
		}
	},
	tipHandler:function(){
		$('#tip-close').on('click',function(){
			$('.logtip').css('display','none');
		});
		$('#tip-log').on('click',function(){
			$('.logtip').css('display','none');
			$('#login-bc').css('display','block');
			clearTimeout(G_intervalTimer.timeoutId);
		});
	}

}
var G_ShareFunc = {shareText:''};
var G_ShareClient = {
	shareAppmessageInfo: {
        'info':{
            'title':'1758微游戏·即点即玩',
            'desc':'还在刷朋友圈吗？快来跟我一起玩游戏吧~ ~ ~ ~',
            'link':'wx.1758.com',
            'imgUrl':'http://images.1758.com/images/1758_icon.png'
        },
        'sid':''},
    shareTimelineInfo: {
        'info':{
            'title':'还在刷朋友圈吗？快来跟我一起玩游戏吧~ ~ ~ ~',
            'desc':'1758微游戏·即点即玩',
            'link':'wx.1758.com',
            'imgUrl':'http://images.1758.com/images/1758_icon.png' 
        },
        'sid':''},
    //分享成功之后发送给服务器name为appmessage timeline qq flag为0成功，1失败
    sendMessageServer: function (name,flag){
        var appkey = this.getParameters(location.href,'appKey');
        var sid = '';
        if (name == 'appmessage') {sid = this.shareAppmessageInfo.sid};
        if (name == 'timeline'){ sid = this.shareTimelineInfo.sid};
        $.get('http://wx.1758.com/game/platform/user/logShareAction',{
            'gid': G_ShareFunc.wxShareData.gid,
            'appKey': appkey,
            'type': name,
            'code': flag,
            'sid': sid
            },function(data){
                if(data.msg == 'ok'){
                    return;
                };
            },'jsonp');
    },
    getRandomNun : function (Min,Max){
        var range = Max-Min;
        var rand = Math.random();
        return (Min+Math.floor(rand * range))
    },
    //该方法调用方法通知cp
    exec_iframe: function (shareType,cpShareUrl){  
        if(typeof(exec_obj)=='undefined'){  
            exec_obj = document.createElement('iframe');  
            exec_obj.name = 'tmp_frame';  
            exec_obj.src = cpShareUrl+'?shareType='+shareType;  
            exec_obj.style.display = 'none';  
            document.body.appendChild(exec_obj);  
        }else{  
            exec_obj.src = cpShareUrl+'?shareType='+shareType+'&' + Math.random();  
        }  
    },
    //客户端的随机分享语
	randomShareClient: function (){
		var textarray,len,num;
		textarray = G_ShareFunc.shareText;
		len = textarray.length;
		var conApp = {};
		var conTim = {};
		if(len > 0){
			num = this.getRandomNun(0,len);
			conApp.desc = textarray[num].sharetitle;
			conApp.title = textarray[num].sharedesc;
			conApp.imgUrl = textarray[num].shareimg;
			conApp.link = textarray[num].sharelink + '&stype=appmessage&superlongId='+Math.ceil(Math.random()*10000000000);
			this.shareAppmessageInfo.info = conApp;
			this.shareAppmessageInfo.sid = textarray[num].sid;

			conTim.title = textarray[num].sharetitle;
			conTim.desc = textarray[num].sharedesc;
			conTim.imgUrl = textarray[num].shareimg;
			conTim.link = textarray[num].sharelink + '&stype=timeline&superlongId='+Math.ceil(Math.random()*10000000000);
			this.shareTimelineInfo.info = conTim;
			this.shareTimelineInfo.sid = textarray[num].sid;
		}
	},
	addParameter:function(){
		//给link地址添加share=
		var sharegid = this.getParameters(location.href,'share');
		if(!sharegid){
			if(G_ShareFunc.wxShareData.gid){
				this.shareAppmessageInfo.info.link += '&share=' + G_ShareFunc.wxShareData.gid;
				this.shareTimelineInfo.info.link += '&share=' + G_ShareFunc.wxShareData.gid;
			}
		}
	},
	getParameters: function (url,name){
        //正则验证
         var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
         //var r = window.location.search.substr(1).match(reg);
         var r = url.substr(url.indexOf('?')).substr(1).match(reg);
         if(r!=null){
            //return  unescape(r[2]);
            return  decodeURIComponent(r[2]);
         } 
         return '';
    },
    //格式化json对象，并且调用native对象注入
    pareShareText: function(){
    	this.shareAppmessageInfo.info = JSON.stringify(this.shareAppmessageInfo.info);
		this.shareTimelineInfo.info = JSON.stringify(this.shareTimelineInfo.info);
		if(typeof(weixinBridge) != 'undefined'){
			if(weixinBridge.invokeWx){
				weixinBridge.invokeWx(this.shareAppmessageInfo.info, this.shareTimelineInfo.info);
	        }
		}else{
			if(window.ios){
				ios.callHandler('shareTimeline', G_ShareClient.shareTimelineInfo.info);
				ios.callHandler('shareMessage', G_ShareClient.shareAppmessageInfo.info);
			}
		}
       	
    },
    //分享成功之后调用随机分享语
    successRandomText: function(){
    	//分享成功之后id和分享语肯定已经存在，故直接执行下面方法即可。
    	this.randomShareClient();
    	this.addParameter();
    },
    //得到分享语之后进行初始化
	init: function(){
		this.randomShareClient();
		if(!!G_ShareFunc.wxShareData.gid){
			this.addParameter();
			//if(typeof(weixinBridge) != 'undefined'){
				//if(weixinBridge.invokeWx){
					this.pareShareText();
		       //}
			//}
		}
		
	},
	//得到gid的时候进行初始化。
	initForGid: function(){
		if(G_ShareFunc.wxShareData.flag){
            this.shareAppmessageInfo.info.link += '&share=' + G_ShareFunc.wxShareData.gid;
			this.shareTimelineInfo.info.link += '&share=' + G_ShareFunc.wxShareData.gid;
			//if(typeof(weixinBridge) != 'undefined'){
				//if(weixinBridge.invokeWx){
					this.pareShareText();
		        //}
			//}
		}
		
	}

};
(function(){
	//引用fashclick.js文件，消除click事件在移动设备上的300ms的延迟
	FastClick.attach(document.body);
	//微信的单条分享语
	var wxShareData = {
			sid: '',	//分享语的id
			flag: false,	//服务器返回微信配置信息后为true
			cpShareUrl: '',
			webSite:'1758微游戏',
			src:'http://wx.1758.com/game/platform/v2.0/user/flogin?appKey=',
			gid: '',
			appid: '',
			title: '1758微游戏·即点即玩',
			desc: '还在刷朋友圈吗？快来跟我一起玩游戏吧~ ~ ~ ~',
			link: 'http://wx.1758.com',
			imgUrl: 'http://images.1758.com/images/1758new.png',
			type: 'link',
			dataUrl: '',
			success: function(){},
			cancel: function() {}
		};
	//微信的分享语集合
	var shareText = '';
	//微信的配置信息集合
	var configContent = '';

	var locationUrl = encodeURIComponent(location.href);

	var appkey = getParameters(location.href,'appKey');
	var trialTime = getParameters(location.href,'trialTime');
	//添加appkey
	wxShareData.src += appkey;
	//添加试玩参数
	if(!!trialTime){
		if(!isNaN(parseInt(trialTime))){
			G_intervalTimer.trialTime = trialTime;
		}
		wxShareData.src = wxShareData.src+'&trialTime='+trialTime;
	}
	loadPage(wxShareData.src);
	//自动一键试玩
	G_intervalTimer.appkey = appkey;
	G_intervalTimer.state = getParameters(location.href,'state');
	G_intervalTimer.share = getParameters(location.href,'share');
	G_intervalTimer.chn = getParameters(location.href,'chn');
	G_intervalTimer.init();	
	if(is_ios_client()){
		connectWebViewJavascriptBridge(function(bridge) {
			bridge.init(function(message, responseCallback) {
				if (responseCallback) {
					responseCallback("")
				}
			})
			window.ios = bridge;
			wxConfigContent(locationUrl,appkey);
			ios.registerHandler('clientSuccessInfo', function(data, responseCallback) {
			    clientSuccessInfo(data);
			});
			ios.registerHandler('clientGetShareInfo', function(data, responseCallback) {
			    clientGetShareInfo();
			});
		});
	}else{
		wxConfigContent(locationUrl,appkey);
	}
	getUserInfo();
	//添加到桌面
	getGameInfo();

	//请求服务器拿到数据，包括微信的分享配置和分享语
	function wxConfigContent(url,appkey){
		//url为当前页面地址，用于获取微信配置信息。
		$.get('http://wx.1758.com/game/platform/getShareInfoAndWxJsconfig',{
			'appKey': appkey,
			'pageUrl': url
			},function(data){
				if(data.code == 0){
					shareText = data.appshares;
					configContent = data.wxjsconfig;
					wxShareData.cpShareUrl = data.sharePageCpurl;
					if(!!data.title){
						wxShareData.webSite = data.title;
					}
					if(data.trialTime){
						G_intervalTimer.trialTime = data.trialTime;
					}
					wxShareData.flag = true;
					//赋值给G_ShareFunc对象，以便客户端调用分享的时候使用。
					G_ShareFunc.shareText = shareText;
					//分享语随机选择一个
					randomShare();
					//配置微信信息
					wxConfig(configContent);
					//获取gid信息
					setTitle(wxShareData.webSite);
				}
			},'jsonp');
	}
	function getGameInfo(){
		$.get('http://wtest.1758.com/game/api/app/getAppInfoJsonP',{
			'appKey': appkey
		},function(data){
			if(data.code == 0){
				var jskk = {
					'gameName':data.wxApp.name,
					'gameUrl':data.wxApp.backGameUrl,
					'imgUrl':data.wxApp.iconUrl,
					'tp':''
				};
				jskk = JSON.stringify(jskk);
				try{
					android_tw_system.addDeskIconNew(jskk);
				}catch(e){
					//TODO handle the exception
				}
			}
		},'jsonp');
	}
	//微信的分享配置信息
	function wxConfig(option){
		wx.config(option);
		//先执行一遍预防gid没有获取
		if(!!wxShareData.gid){

			url = wxShareData.link;
			var shareGid = getParameters(url,'share');
			if(!shareGid){
				if(url.indexOf('?') > -1){
					wxShareData.link = wxShareData.link +'&share='+ wxShareData.gid;
				}else{
					wxShareData.link = wxShareData.link + '?share=' + wxShareData.gid;
				}
			}

			changeWxShareInfo();
			//exec_iframe('appmessage',wxShareData.cpShareUrl);
        	//exec_iframe('timeline',wxShareData.cpShareUrl);
		}
		
		wx.error(function(res){ });
	}
	
	//根据不同的shareType进行给cp的方法进行回调
	function exec_iframe(shareType,cpShareUrl){  
	    if(typeof(exec_obj)=='undefined'){  
	        exec_obj = document.createElement('iframe');  
	        exec_obj.name = 'tmp_frame';  
	        exec_obj.src = cpShareUrl+'?shareType='+shareType;  
	        exec_obj.style.display = 'none';  
	        document.body.appendChild(exec_obj);  
	    }else{  
	        exec_obj.src = cpShareUrl+'?shareType='+shareType+'&' + Math.random();  
    	}  
	}
	//获取url中的参数
	function getParameters(url,name){
		//正则验证
		 var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
	     //var r = window.location.search.substr(1).match(reg);
	     var r = url.substr(url.indexOf('?')).substr(1).match(reg);
	     if(r!=null){
	     	//return  unescape(r[2]);
	     	return  decodeURIComponent(r[2]);
	     } 
	     return '';
	}
	
	//加载iframe框架中的src地址
	function loadPage(gameurl){
		/*
		if(typeof(cpgame) == 'undefined'){
			var cpgame = document.createElement('iframe');
			cpgame.name='cpframe';
			cpgame.className = 'gameframe';
			cpgame.src = gameurl;
			document.body.appendChild(cpgame);
		}else{
			cpgame.src = gameurl;
		}
		if (cpgame.attachEvent) {
	        cpgame.attachEvent('onload', hideLoading);
	    } else {
	        cpgame.onload  = hideLoading;
	    }*/
		

		var gframe = document.getElementById('gameframe');
		gframe.src= gameurl;
		if (gframe.attachEvent) {
	        gframe.attachEvent('onload', hideLoading);
	    } else {
	        gframe.onload  = hideLoading;
	    }
	}

	
	//获取iframe里面的游戏地址的appkey
	/*
	function getIframeUrlAppkey(){
		var url = document.getElementById('cpgame').src;
		var appkey = getParameters(url,'appKey');
		return appkey;
		
	}*/
	//设置title
	function setTitle(htmlTitle){
		document.title= htmlTitle;
	}
	//取随机数参数为起始数和终止数
	function getRandomNun(Min,Max){
		var range = Max-Min;
		var rand = Math.random();
		return (Min+Math.floor(rand * range))
	}
	

	//获取用户的uid信息并且添加到分享link上面
	function getUserInfo(){
		var userInfo;
		//数据页面地址，主要是通过改地址获取gid
		var dataurl = 'http://wx.1758.com/game/platform/user/shareIframeNotify';
		//通过window.name属性进行跨域的数据读取
		var state = 0, 
	    giframe = document.createElement('iframe'),
	    loadfn = function() {
	        if (state === 1) {
	            userinfo = giframe.contentWindow.name;    // 读取数据
	            changeLink(userinfo);
	            document.body.removeChild(giframe);
	        } else if (state === 0) {
	            state = 1;
	            giframe.contentWindow.location = "http://h5.g1758.cn/spf/blank.html";    // 设置的代理文件
	        }  
	    };
	    giframe.src = dataurl;
	    giframe.style.display = 'none';
	    if (giframe.attachEvent) {
	        giframe.attachEvent('onload', loadfn);
	    } else {
	        giframe.onload  = loadfn;
	    }
	    document.body.appendChild(giframe);

	}
	//微信的注入信息
	function changeWxShareInfo(){
		randomGetShareContent();
		wx.ready(function(){
        	//分享给朋友
	        wx.onMenuShareAppMessage({
	            title: wxShareData.desc, // 分享标题
	            desc: wxShareData.title, // 分享描述
	            link:  wxShareData.link + '&stype=appmessage&superlongId='+Math.ceil(Math.random()*10000000000),
	            imgUrl: wxShareData.imgUrl,
	            type: wxShareData.type,
	            dataUrl: wxShareData.dataUrl,
	            success: function(){
	            	exec_iframe('appmessage',wxShareData.cpShareUrl);
	            	changeWxShareInfo();
	            	sendMessageServer('appmessage',0);
	            },
	            cancel: function(){
	            	changeWxShareInfo();
	            	sendMessageServer('appmessage',1);
	            }
	        });

	        //分享到朋友圈
	        wx.onMenuShareTimeline({
	            title: wxShareData.title, // 分享标题
	            link:  wxShareData.link + '&stype=timeline&superlongId='+Math.ceil(Math.random()*10000000000),
	            imgUrl: wxShareData.imgUrl,
	            success: function(){
	            	exec_iframe('timeline',wxShareData.cpShareUrl);
	            	changeWxShareInfo();
	            	sendMessageServer('timeline',0);
	            },
	            cancel: function(){
	            	changeWxShareInfo();
	            	sendMessageServer('timeline',1);
	            }
	        });
	        //分享到qq好友
	        wx.onMenuShareQQ({
			    title: wxShareData.desc, // 分享标题
			    desc: wxShareData.title, // 分享描述
			    link: wxShareData.link + '&stype=qq&superlongId='+Math.ceil(Math.random()*10000000000), // 分享链接
			    imgUrl: wxShareData.imgUrl, // 分享图标
			    success: function(){
			    	exec_iframe('qq',wxShareData.cpShareUrl);
			    	changeWxShareInfo();
			    	sendMessageServer('qq',0);
			    },
			    cancel: function(){
			    	changeWxShareInfo();
			    	sendMessageServer('qq',1);
			    }
			});
			//分享到qq空间
			/*
			wx.onMenuShareQZone({
				title: wxShareData.desc, // 分享标题
			    desc: wxShareData.title, // 分享描述
			    link: wxShareData.link + '&stype=qqzone', // 分享链接
			    imgUrl: wxShareData.imgUrl, // 分享图标
			    success: function () { 
			       // 用户确认分享后执行的回调函数
			       	exec_iframe('qzone',wxShareData.cpShareUrl);
			    	changeWxShareInfo();
			    	sendMessageServer('qzone',0);
			    },
			    cancel: function () { 
			        // 用户取消分享后执行的回调函数
			        changeWxShareInfo();
			    	sendMessageServer('qzone',1);
			    }
			});*/

        });
	}

	//随机分享语录
	function randomGetShareContent(){
		var textarray,len,num;
		textarray = shareText;
		len = textarray.length;
		if(len > 0){
			num = getRandomNun(0,len);
			wxShareData.title = textarray[num].sharetitle;
			wxShareData.desc = textarray[num].sharedesc;
			wxShareData.imgUrl = textarray[num].shareimg;
			wxShareData.sid = textarray[num].sid;
			if(!!wxShareData.gid){
				wxShareData.link = textarray[num].sharelink + '&share='+wxShareData.gid;
			}else{
				wxShareData.link = textarray[num].sharelink;	
			}
		}
	}
	//随机选择一条分享语录，填充wxShareData信息
	//该方法在获取到配置信息之后仅执行一次
	function randomShare(){
		var text,len,num;
		text = shareText;
		len = text.length;
		if(len > 0){
			num = getRandomNun(0,len);
			wxShareData.appid= text[num].appid;
			wxShareData.title = text[num].sharetitle;
			wxShareData.desc = text[num].sharedesc;
			wxShareData.link = text[num].sharelink;
			wxShareData.imgUrl = text[num].shareimg;
			wxShareData.sid = text[num].sid;
		}
		G_ShareFunc.wxShareData = wxShareData;
		//执行客户端的分享
		G_ShareClient.init();
	}

	//每次分享成功之后执行一次发送给服务器  0成功  1取消
	function sendMessageServer(name,flag){
		//var appkey = getIframeUrlAppkey();
		$.get('http://wx.1758.com/game/platform/user/logShareAction',{
			'gid': wxShareData.gid,
			'appKey': appkey,
			'type': name,
			'code': flag,
			'sid': wxShareData.sid
			},function(data){
				if(data.msg == 'ok'){
					return;
				};
			},'jsonp')
	}

	//获取gid后调用,判断微信config信息是否返回
	//如果没有返回则，赋值wxShareData.gid信息
	//如果返回，则修改wxShareData.link加上gid信息
	
	function changeLink(gid){
		wxShareData.gid = gid;
		G_ShareFunc.wxShareData = wxShareData;
		G_ShareClient.initForGid();
		if(wxShareData.flag){
			var url = wxShareData.link;
			if(url.indexOf("?")>-1){
            	wxShareData.link = wxShareData.link + '&share='+ gid;
            }else{
            	wxShareData.link = wxShareData.link + '?share='+ gid;
            }
        	changeWxShareInfo();
        	//exec_iframe('appmessage',wxShareData.cpShareUrl);
        	//exec_iframe('timeline',wxShareData.cpShareUrl);
		}
	}

	//gafarme游戏加载完成之后 
	function hideLoading(){
		$('#loading-div').css('display','none');
		$('#gameframe').css('display','block');
		G_intervalTimer.gameFrameLoad = true;
		if(G_intervalTimer.receiveMessage){
			$('.logtip').css('display','block');
			//G_intervalTimer.countDown();
		}
	}

	function is_ios_client(){
		var ua = navigator.userAgent.toLowerCase();
		if(ua.match(/ios/i) == 'ios' && (ua.match(/dwjia/i) == "dwjia")){
			return true;
		}else{
			return false;
		}
	}

	
}());

//type的定义
//sendAppMessage    分享到微信好友
//shareTimeline     分享到微信朋友圈
//shareQQ       分享到qq好友
//shareWeiboApp     分享到微博
//shareQZone    分享到qq空间
//客户端分享成功之后native调用的方法。
function clientSuccessInfo(type){
    var _type='';
    type = type.toLowerCase();
    switch(type){
        case 'sendappmessage':
            _type = 'appmessage';
        break;
        case 'sharetimeline':
            _type = 'timeline';
        break;
        case 'shareqq':
            _type = 'qq';
        break;
        case 'shareweiboapp':
        break;
        case 'shareqzone':
        	_type = 'qzone';
        break;
    }
    //发送给cp，执行cp的回调函数
    G_ShareClient.exec_iframe(_type,G_ShareFunc.wxShareData.cpShareUrl);
    //随机分享语
    G_ShareClient.successRandomText();
    //发送给服务器记录信息
    G_ShareClient.sendMessageServer(_type,0);
    //向native注入信息，改变分享语内容。
    G_ShareClient.pareShareText();
}
function clientGetShareInfo(all){
    //该方法为native方法，两个参数 第一个为appmessage信息，第二个位timeline信息
    //两个都为json对象
    G_ShareClient.pareShareText();
}
//ios客户端调用成功后执行的方法
/*
bridge.registerHandler("clientSuccessInfo", function(data, responseCallback) {
     	clientSuccessInfo(data);
});
*/



