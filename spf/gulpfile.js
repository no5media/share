var gulp = require('gulp');
var concat = require('gulp-concat');                            //- 多个文件合并为一个；
var minifyCss = require('gulp-minify-css');                     //- 压缩CSS为一行；
var rev = require('gulp-rev');                                  //- 对文件名加MD5后缀
var revCollector = require('gulp-rev-collector');    			//路径替换
var rjs = require("gulp-rjs");
var del = require("del");			//删除文件
var runSequence = require('run-sequence');
var rename = require('gulp-rename');

var taskName = {
	concatcss:'concatcss',		//
	revcss:'revcss',			//
	rejs:'rejs',				//
	htmlmd5:'htmlmd5',
	replacejs:'replacejs',
	replacehtml:'replacehtml',
	
	outCssName:'game_2_3.css',
	outJsName:'game_2_3.js',
	htmTemVersion:'_2_3'
}

//合并*压缩*md5
gulp.task(taskName.concatcss, function() {                                //- 创建一个名为 concat 的 task
   return gulp.src(['./mweb/style/external/pure-min.css', 
    		'./mweb/style/common.css',
    		'./mweb/style/modal/baseinfo.css',
    		'./mweb/style/share_index.css',
    		'./mweb/style/modal/gamemodal.css',
   			'./mweb/style/modal/weself.css'])    				   	//- 需要处理的css文件，放到一个字符串数组里
        .pipe(concat(taskName.outCssName))        //- 合并后的文件名
        .pipe(minifyCss())                                      //- 压缩处理成一行
        .pipe(rev())                                            //- 文件名加MD5后缀
        .pipe(gulp.dest('./dist/css'))                               //- 输出文件本地
        .pipe(rev.manifest())                                   //- 生成一个rev-manifest.json
        .pipe(gulp.dest('./dist/rev'));                              //- 将 rev-manifest.json 保存到 rev 目录内
});

//html替换md5时间戳
gulp.task(taskName.revcss,['concatcss'], function() {
    gulp.src(['./dist/rev/*.json', './dist/game_v3.html'])   //- 读取 rev-manifest.json 文件以及需要进行css名替换的文件
        .pipe(revCollector())                                   //- 执行文件内css名的替换
        .pipe(gulp.dest('./dist/html/'));                     //- 替换后的文件输出的目录
});

/*=========================================================*/
//利用r.js 压缩文件
gulp.task(taskName.rejs,function(){
	return gulp.src('mweb/js/view/app.js')
		.pipe(rjs({baseUrl:'mweb/js',name:'view/app',out:'dist/smin/'+taskName.outJsName}))
		.pipe(gulp.dest('dist/scripts/'));
});
//html模板添加md5时间戳
gulp.task(taskName.htmlmd5,function(){
	return gulp.src(['mweb/html/gameModalTem.html','mweb/html/modalMainTem.html'])
		.pipe(rename(function(path){
			path.basename += taskName.htmTemVersion;
		}))
		.pipe(rev())
		.pipe(gulp.dest('dist/html/'))
		.pipe(rev.manifest())
		.pipe(gulp.dest('dist/rev/'));
});
//js替换时间戳html模板*js文件加时间戳
gulp.task(taskName.replacejs,[taskName.htmlmd5,taskName.rejs],function(){
	return gulp.src(['./dist/rev/*.json', './dist/smin/*.js'])
	.pipe(revCollector())
	.pipe(rev())
	.pipe(gulp.dest('dist/scripts/'))
	.pipe(rev.manifest())
	.pipe(gulp.dest('dist/rev/'));
})

//替换html里面的js文件
gulp.task(taskName.replacehtml, [taskName.replacejs],function() {
	return gulp.src(['./dist/rev/*.json', './src/game_v3.html'])
	.pipe(revCollector())
	.pipe(gulp.dest('./dist/'));                  //- 替换后的文件输出的目录
});

gulp.task('clean',function(){
	//del(['./dist/rev','./dist/smin']);
});

//压缩js	模板md5
//js替换模板md5
//js md5
//html 替换jsmd5

gulp.task('style', [taskName.concatcss,taskName.revcss]);
gulp.task('scripts',[taskName.rejs,taskName.htmlmd5,taskName.replacejs,taskName.replacehtml]);
gulp.task('gamev2',function(cb){
	taskName.outCssName='game_2_3.css';
	taskName.outJsName='game_2_3.js';
	runSequence(
		['style','scripts'],
		'clean',
		cb
	);
});
gulp.task('gamev3',function(cb){
	taskName.outCssName='game_3_0.css';
	taskName.outJsName='game_3_0.js';
	runSequence(
		['style','scripts'],
		'clean',
		cb
	);
});
