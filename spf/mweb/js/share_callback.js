(function(){
	var shareType = getParameters('shareType');
	var win = parent.window;
	var iframobj = win.cpframe || win[0];
	if(shareType == 'timeline' || shareType == 'qzone'){
		if(typeof iframobj.onShareTimeline === 'function'){
			iframobj.onShareTimeline();
		}
	}else if(shareType == 'appmessage'){
		if(typeof iframobj.onShareFriend === 'function'){
			iframobj.onShareFriend();	
		}
	}else if(shareType == 'qq'){
		if(typeof iframobj.onShareFriend === 'function'){
			iframobj.onShareFriend();	
		}
	}
	//获取url里面的参数
	function getParameters(name){
		//正则验证
		 var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
	     var r = window.location.search.substr(1).match(reg);
	     if(r!=null){
	     	//return  unescape(r[2]);
	     	return  decodeURIComponent(r[2]);
	     } 
	     return '';
	}
}())