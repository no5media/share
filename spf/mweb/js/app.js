require.config({
	baseUrl:'mweb/js',
	paths:{
		libs:'libs',
		data:'data',
		common:'common',
		view:'view',
		
		jquery:'jquery',
		fastclick:'libs/fastclick'
	},
	shim:{
		fastclick:{
			exports: 'FastClick'
		},
		'jquery.cookie': {
        	deps: ['jquery'],
        	exports: 'jQuery.fn.cookie'
        }
	}
});
require(['libs/fastclick','jquery','common/eventBase','data/getInfoData','view/modalBtn'],function(FastClick,$,EventBase,InfoData,ModalBtn){
	FastClick.attach(document.body);
	var dataPara = {
		deffConfig : $.Deferred(),	//配置请求
		deffUser : $.Deferred(),	//用户gid请求
		deffLoad: $.Deferred(),		//游戏页面load完
		locationUrl : encodeURIComponent(location.href),
		appkey:'',	//游戏
		title:'1758微游戏',	//游戏title	
		wxjsconfig:{},		//微信分享的配置信息
		trialTime:'3',		//（暂不启用）实现自动登录的时候用到的时间间隔
		sharePageCpurl:'',		//游戏的分享地址
		shareText:[],		//分享语的数组
		customizeText:[],		//自定义参数使用的默认分享语数组
		gid:'',		//用户的gid
		sid:'',		//当前分享语的id
		userTerminal:'',
		src:'http://wx.1758.com/game/platform/v2.0/user/flogin?appKey=',
		timeoutId: '',
		chatroomEnable:false,		//是否需要聊天室
		messageFromCp:{				//是否是从cp传递过来的内容
			isCp:false,				//从cp地方获取到内容后 置为true
			urlPara:'',
			title:'',
			desc:'',
			img:''
		},
		defaultText:{
			sharedesc:"1758微游戏",
			shareimg:"http://images.1758.com/images/48.ico",
			sharelink:"http://wx.1758.com",
			sharesumary:"1758微游戏",
			sharetitle:"1758微游戏，即点即玩",
			sid:""
		}
	}
	$(document).on('touchmove',function(e){
		e.preventDefault();
	});
	$('body').on('touchmove', '#moban', function(e) {
		e.stopPropagation();
	});
	init();
	//初始化函数
	function init(){
		dataPara.userTerminal = EventBase.userTerminal();
		dataPara.appkey = EventBase.getParameters('appKey');
		var chn = EventBase.getParameters('chn')
		var ux = dataPara.userTerminal;
		//初始化一些全局对象
		initWindow(ux);
		loadPage((dataPara.src+dataPara.appkey+'&chn='+chn));
		//获取微信配置信息
		getWxConfigAndText();
		//获取gid信息
		getUserInfo();
		//显示浮标
		//modalBtn();
		$.when(dataPara.deffLoad).done(function(){
			ModalBtn.init();
			//在第二个版本中暂时不开启websocket通讯
//			ModalBtn.initWschat();
		});
		$.when(dataPara.deffConfig,dataPara.deffUser).done(function(){
			if(ux == 'iphoneclient' || ux == 'androidclient'){
				//客户端分享语的注入
				pareShareText();
			}else{
				//进行微信分享语的注入	
				changeWxShareInfo();
			}
		}).fail(function(){
			
		});
	}
	//获取游戏的wx配置和分享语
	function getWxConfigAndText(){
		var url = 'http://wx.1758.com/game/platform/getShareInfoAndWxJsconfig';
		var infos = {
			appKey:dataPara.appkey,
			pageUrl:encodeURIComponent(location.href)
		};
		InfoData.getShareInfo(url,infos,handleWxConfig);
	}
	//处理得到的微信配置信息和分享语
	function handleWxConfig(data){
		if(data.code == 0){
			if(data.appshares.length == 0){
				dataPara.shareText.push(dataPara.defaultText);
			}else{
				dataPara.shareText = data.appshares;
			}
			dataPara.sharePageCpurl = data.sharePageCpurl;
			dataPara.title = data.title;
			dataPara.wxjsconfig = data.wxjsconfig;
			if(data.trialTime == 0){
			}else{
				dataPara.trialTime = data.trialTime;
			}
			//设置title名字
			if(!!data.title){
				document.title = data.title;
			}
			//设置是否有聊天室
			if(data.chatroomEnable != undefined){
				dataPara.chatroomEnable = data.chatroomEnable;
				ModalBtn.chatroomEnable = data.chatroomEnable;
			}
			//微信的分享配置信息
			window.wx.config(data.wxjsconfig);
			dataPara.deffConfig.resolve();
		}
	}
	//获取用户的gid信息并且添加到分享link上面
	function getUserInfo(){
		var userInfo;
		//数据页面地址，主要是通过改地址获取gid
		var dataurl = 'http://wx.1758.com/game/platform/user/shareIframeNotify';
		//通过window.name属性进行跨域的数据读取
		var state = 0, 
	    giframe = document.createElement('iframe'),
	    loadfn = function() {
	        if (state === 1) {
	            dataPara.gid = giframe.contentWindow.name;    // 读取数据
	            //changeLink(userinfo);
	            dataPara.deffUser.resolve();
	            document.body.removeChild(giframe);
	        } else if (state === 0) {
	            state = 1;
	            giframe.contentWindow.location = "http://h5.g1758.cn/spf/blank.html";    // 设置的代理文件
	        }  
	    };
	    giframe.src = dataurl;
	    giframe.style.display = 'none';
	    if (giframe.attachEvent) {
	        giframe.attachEvent('onload', loadfn);
	    } else {
	        giframe.onload  = loadfn;
	    }
	    document.body.appendChild(giframe);
	}
	//微信的注入信息
	function changeWxShareInfo(){
		var info = randomGetShareContent();
		var wx = window.wx;
		wx.ready(function(){
        	//分享给朋友
	        wx.onMenuShareAppMessage({
	            title: info.desc, // 分享标题
	            desc: info.title, // 分享描述
	            link:  info.link + '&stype=appmessage&superlongId='+Math.ceil(Math.random()*10000000000),
	            imgUrl: info.imgUrl,
	            type: 'link',
	            dataUrl: '',
	            success: function(){
	            	exec_iframe('appmessage',dataPara.sharePageCpurl);
	            	changeWxShareInfo();
	            	sendMessageServer('appmessage',0);
	            },
	            cancel: function(){
	            	changeWxShareInfo();
	            	sendMessageServer('appmessage',1);
	            }
	        });

	        //分享到朋友圈
	        wx.onMenuShareTimeline({
	            title: info.title, // 分享标题
	            link:  info.link + '&stype=timeline&superlongId='+Math.ceil(Math.random()*10000000000),
	            imgUrl: info.imgUrl,
	            success: function(){
	            	exec_iframe('timeline',dataPara.sharePageCpurl);
	            	changeWxShareInfo();
	            	sendMessageServer('timeline',0);
	            },
	            cancel: function(){
	            	changeWxShareInfo();
	            	sendMessageServer('timeline',1);
	            }
	        });
	        //分享到qq好友
	        wx.onMenuShareQQ({
			    title: info.desc, // 分享标题
			    desc: info.title, // 分享描述
			    link: info.link + '&stype=qq&superlongId='+Math.ceil(Math.random()*10000000000), // 分享链接
			    imgUrl: info.imgUrl, // 分享图标
			    success: function(){
			    	exec_iframe('qq',dataPara.sharePageCpurl);
			    	changeWxShareInfo();
			    	sendMessageServer('qq',0);
			    },
			    cancel: function(){
			    	changeWxShareInfo();
			    	sendMessageServer('qq',1);
			    }
			});
			//分享到qq空间
			wx.onMenuShareQZone({
			    title: info.desc, // 分享标题
			    desc: info.title, // 分享描述
			    link: info.link + '&stype=qzone&superlongId='+Math.ceil(Math.random()*10000000000), // 分享链接
			    imgUrl: info.imgUrl, // 分享图标
			    success: function () { 
			       // 用户确认分享后执行的回调函数
			    	exec_iframe('qzone',dataPara.sharePageCpurl);
			    	changeWxShareInfo();
			    	sendMessageServer('qzone',0);
			    },
			    cancel: function () { 
			        // 用户取消分享后执行的回调函数
			        changeWxShareInfo();
			    	sendMessageServer('qzone',1);
			    }
			});
        });
	}
	//得到随机分享语
	function randomGetShareContent(){
		//先判断是否是cp传递过来的内容
		var kk;
		if(dataPara.messageFromCp.isCp && dataPara.customizeText.length != 0){
			kk = EventBase.randomShareContent(dataPara.customizeText,dataPara.gid);
			dataPara.messageFromCp.isCp = false;
			kk.link += '&state='+dataPara.messageFromCp.urlPara; 
		}else{
			kk = EventBase.randomShareContent(dataPara.shareText,dataPara.gid);
		}
		if(!kk){
			dataPara.sid = '';			
		}else{
			dataPara.sid = kk.sid;
		}
		return kk;
	}
	//向cp发送分享成功的请求
	function exec_iframe(shareType,cpShareUrl){
		EventBase.exec_iframe(shareType,cpShareUrl);
		//新的通知cp方法
//		setInfoForSend(shareType);
	}
	//每次分享成功之后执行一次发送给服务器  0成功  1取消
	function sendMessageServer(name,flag){
		$.get('http://wx.1758.com/game/platform/user/logShareAction',{
			'gid': dataPara.gid,
			'appKey': dataPara.appkey,
			'type': name,
			'code': flag,
			'sid': dataPara.sid
			},function(data){
				if(data.msg == 'ok'){
					return;
				};
			},'jsonp');
	}
	//格式化json对象，注入客户端分享内容
	function pareShareText(){
		var shareAppmessageInfo,shareTimelineInfo,temp;
		var data = randomGetShareContent();
		shareTimelineInfo = JSON.stringify(data);
		temp = data.title;
		data.title = data.desc;
		data.desc = temp;
		shareAppmessageInfo = JSON.stringify(data);
		if(typeof(window.weixinBridge) != 'undefined'){
			if(window.weixinBridge.invokeWx){
				window.weixinBridge.invokeWx(shareAppmessageInfo, shareTimelineInfo);
	        }
		}else{
			if(window.ios){
				window.ios.callHandler('shareTimeline', shareTimelineInfo);
				window.ios.callHandler('shareMessage', shareAppmessageInfo);
			}
		}
	}
	//初始化一些全局对象
	function initWindow(ux){
		var win = window;
		if(ux == 'iphoneclient' || ux == 'androidclient'){
			win.clientSuccessInfo = function(type){
				var _type='';
			    type = type.toLowerCase();
			    switch(type){
			        case 'sendappmessage':
			            _type = 'appmessage';
			        break;
			        case 'sharetimeline':
			            _type = 'timeline';
			        break;
			        case 'shareqq':
			            _type = 'qq';
			        break;
			        case 'shareweiboapp':
			        break;
			        case 'shareqzone':
			        	_type = 'qzone';
			        break;
			    }
			    //发送给cp，执行cp的回调函数
			    exec_iframe(_type,dataPara.sharePageCpurl);
			    //随机分享语
			    pareShareText();
			    //发送给服务器记录信息
			    sendMessageServer(_type,0);
			};
			//假如没有注册分享信息，客户端调用该方法进行注册信息
			win.clientGetShareInfo = function(){
				pareShareText();
			}
			//ios 客户端分享对象
			win.connectWebViewJavascriptBridge = function(callback) {
			    if (window.WebViewJavascriptBridge) {
			        callback(WebViewJavascriptBridge)
			    } else {
			        document.addEventListener('WebViewJavascriptBridgeReady', function() {
			            callback(WebViewJavascriptBridge)
			        }, false)
			    }
			}
			//安卓客户端添加大桌面方法，假如没有自动执行 则客户端调用该方法
			win.clientdesktopInfo = function(){
				androidPutDesktop();
			}
			if(ux == 'iphoneclient'){
				//ios 客户端注册对象
				handleIosClient();
			}else if(ux == 'androidclient'){
				//Android 添加到桌面
				androidPutDesktop();
			}
		}
		var tri = EventBase.getParameters('trialTime');
		if(!!tri){
			//一键登录初始化
			oneKeyInit();
		}
	}
	//ios客户端注册对象
	function handleIosClient(){
		window.connectWebViewJavascriptBridge(function(bridge) {
			bridge.init(function(message, responseCallback) {
				if (responseCallback) {
					responseCallback("")
				}
			})
			window.ios = bridge;
			ios.registerHandler('clientSuccessInfo', function(data, responseCallback) {
			    window.clientSuccessInfo(data);
			});
			ios.registerHandler('clientGetShareInfo', function(data, responseCallback) {
			    window.clientGetShareInfo();
			});
		});
	}
	//androlid 放到桌面
	function androidPutDesktop(){
		$.get('http://wx.1758.com/game/api/app/getAppInfoJsonP',{
			'appKey': dataPara.appkey
		},function(data){
			if(data.code == 0){
				var jskk = {
					'gameName':data.wxApp.name,
					'gameUrl':data.wxApp.backGameUrl,
					'imgUrl':data.wxApp.iconUrl,
					'tp':''
				};
				jskk = JSON.stringify(jskk);
				try{
					android_tw_system.addDeskIconNew(jskk);
				}catch(e){
					//TODO handle the exception
				}
			}
		},'jsonp');
	}
	//加载游戏地址
	function loadPage(gameurl){
		gameurl =  detachTrialTime(gameurl);
		var gframe = document.getElementById('gameframe');
		gframe.src= gameurl;
		if (gframe.attachEvent) {
	        gframe.attachEvent('onload', hideLoading);
	    } else {
	        gframe.onload  = hideLoading;
	    }
	}
	//gafarme游戏加载完成之后 
	function hideLoading(){
		$('#loading-div').css('display','none');
		$('#gameframe').css('display','block');
		dataPara.deffLoad.resolve();
	}
	
    //一键登录初始化
    function oneKeyInit(){
    	//接受登录页面信息
    	bindMessage();
    	bindLogTipClick();
    }
    //postmessage 绑定事件
    function bindMessage(){
    	if(typeof window.addEventListener != 'undefined') {
		    window.addEventListener('message', receiveMessage, false);
		} else if (typeof window.attachEvent != 'undefined') {
		    window.attachEvent('onmessage', receiveMessage);
		}
    }
	//接受登录页面传递的消息
    function receiveMessage(e){
		var time = e.data;
		var origin = e.origin;
		if(origin == 'http://wx.1758.com'){
			$.when(dataPara.deffLoad).done(function(){
				//添加登录提示框
				$('body').prepend(dyCreateLoginTip());
				dataPara.timeoutId = setTimeout(function(){
					$('#logTip').remove();
					$('body').append(dyCreateLoginHtml());
					loginEvent();
				},dataPara.trialTime*60*1000)
			})
		}
    }
    //动态生成登录页面
    function dyCreateLoginHtml(){
    	var str = '<div id="login-bc" class="login-bc" style="display:block">'+
			'<div id="login-main" class="login-main">'+
				'<p class="headimg">'+
					'<img src="http://images.1758.com/images/login_1758_1.png" style="width:35%;">'+
				'</p>'+
				'<hr style="border:1px rgb(247,247,247) solid;">'+
				'<p id="tip" class="tip-1">该游戏由1758.com提供，登录后即可继续操作</p>'+
				'<p class="tip-2">一键登录</p>'+
				'<div class="l-img" style="text-align:center;margin-bottom:10px;">'+
					'<span>'+
						'<a id="qqLogin">'+
						'<img src="http://images.1758.com/images/login_QQ1.png" style="width:20%;max-height:60px;margin-right:20px;border-color:#fff">'+
						'</a>'+
					'</span>'+
					'<span id="wxLogin" class="wxlogin">'+
						'<a>'+
						'<img src="http://images.1758.com/images/login_WX1.png" style="width:20%;max-height:60px;">'+
						'</a>'+
					'</span>'+
					'<span>'+
						'<a id="weiboLogin">'+
						'<img src="http://images.1758.com/images/login_XL1.png" style="width:20%;max-height:60px;margin-left:20px;">'+
						'</a>'+
					'</span>'+
				'</div>'+
			'</div>'+
		'</div>'
		return str;
    };
    function dyCreateLoginTip(){
    	var str = '<div id="logTip" class="logtip" style="display:block">'+
				'<img src="http://wx.1758.com/game/h5/images/head-icon.png">'+
				'<div class="wrap">'+
					'<div id="tip-close" class="close"><img src="http://images.1758.com/images/closeLogin.png"></div>'+
					'<span>登录后可继续进行上次游戏进度</span>'+
					'<div id="tip-log" class="tip-log">'+
						'<span>'+
							'<span class="login-btn">登录</span>'+
							'<span id="countdown"></span>'+
						'</span>'+
					'</div>'+
				'</div>'+
			'</div>'
		return str;
    }
    function bindLogTipClick(){
    	$(document).on('click','#tip-close',function(){
			$('#logTip').remove();
		});
		$(document).on('click','#tip-log',function(){
			$('#logTip').remove();
			$('body').append(dyCreateLoginHtml());
			clearTimeout(dataPara.timeoutId);
			//登录事件
    		loginEvent();
		});
    }
    //登录按钮点击事件
    function loginEvent(){
    	var ua = dataPara.userTerminal;
    	if(ua == 'weixin' || ua == 'iphoneclient' || ua == 'androidclient'){
    		$('#wxLogin').css('display','inline');
    		$(document).on('click','#wxLogin',function(){
    			try{
			      	android_wxgame_auth.weixinLogin();
					android_tw_system.toast("通过微信登录，可看到好友的得分排名哦",1);
			    }catch (exp){
			        try{
			  			window.ios.callHandler('weixinLogin',{},function(res){});
			  		}catch (exp2){
			   		 	location.href="http://wx.1758.com/game/platform/v1.0/user/login?appKey="+dataPara.appkey+"&state=&chn=onekey&share=";
			  		}
			    }
    		})
    	}
    	$(document).on('click','#qqLogin',function(){
    		location.href = 'http://wx.1758.com/game/platform/v1.0/qq/login?appKey='+dataPara.appkey+'&state=&chn=&share='
    	})
    	$(document).on('click','#weiboLogin',function(){
    		location.href="http://wx.1758.com/game/platform/v1.0/weibo/login?appKey="+dataPara.appkey+"&state=&chn=&share=";
    	})
    }
    //根据traltime 重写url
    function detachTrialTime(gameurl){
    	var localurl = location.href;
    	if(localurl.indexOf('trialTime') > -1){
    		//有参数
			var triltime = EventBase.getParameters('trialTime');
			triltime = parseInt(triltime);
			if(isNaN(triltime)){
			}else{
				dataPara.trialTime = triltime;
			}
			gameurl += '&trialTime='+dataPara.trialTime;
			return gameurl;
    	}else{
    		return gameurl;
    	}
    }
    //初始化与cp之间的信息交流
    function cpInfoPostInit(){
    	if(typeof window.addEventListener != 'undefined') {
		    window.addEventListener('message', receiveCpInfo, false);
		} else if (typeof window.attachEvent != 'undefined') {
		    window.attachEvent('onmessage', receiveCpInfo);
		}
    }
    //接受cp信息
    function receiveCpInfo(e){
    	var dataInfo = e.data;
    	var state='';
    	if(dataInfo.hlmy){
    		//接受cp信息
    		if(dataInfo.type == 'share'){
    			state = dataInfo.value.state;
    			dataPara.messageFromCp.isCp = true;
    			dataPara.messageFromCp.urlPara = state;
    		}
    	}
    }
    //根据cp传回来的信息，进行注入
    //组装发送给cp的信息
    function setInfoForSend(shareType){
    	var st = '';
		if(shareType == "appmessage" || shareType == "qq"){
			st = 'onShareTimeline';
		}else if(shareType == 'timeline' || shareType == 'qzone'){
			st = 'onShareFriend';
		}
		var info = {
			hlmy:true,
			from:'1758',
			type:'fn',
			value:{
				'fn':st,
				args:[]
			}
		}
		sendCpInfo(info);
    }
    //发送给cpinfo
    function sendCpInfo(info){
    	window.frames['cpframe'].postMessage(info,'*');
    }
});