$(function(){
	var userId,	//用户Id 
		appId,	//游戏Id
		cpId,	//cp的标示符
		txId,	//订单号
		state,	//cp传递过来的参数
		shareFlag,	//分享部分显示标识。
		itemCode,
		sharelink,	//分享出去的连接
		chn,		//渠道号
		postUrlMain = 'http://wx.1758.com/pay/iceapi/share/',	//请求连接地址
		postShare = postUrlMain + 'tapp',		//获取分享情况的请求
		postFriends = postUrlMain + 'myfriends',		//获取好友信息的请求
		postReward = postUrlMain + 'getreward',		//领取奖励的请求。
		shareTitle,			//分享出去的title
		shareDesc,			//分享出去的描述
		shareImgUrl,		//分享出去的图片地址
		shareSummary;		//分享出去的摘要（微博用到）

	userId = getParameters('gid');
	appId = getParameters('appKey');
	cpId = getParameters('cpid');
	txId = getParameters('txId');
	state = getParameters('state');
	chn = getParameters('chn');

	shareContent();		//初始化奖励内容部分
	getAllFriends();	//初始化好友关系部分
	

	shareFlag = judgeDevice();	//初始化判断用户的使用情况，微信、客户端、浏览器


	//得到微信内的分享的参数配置
	function getWxOptions(){
		var wxNativeOption;
		wxNativeOption = {
			title: shareTitle,
			desc: shareDesc,
			link: sharelink,
			imgUrl: shareImgUrl,
			type: 'link',
			dataUrl: '',
			success: function(){},
			cancel: function() {}
		}
		return wxNativeOption;
	}

	//引用fashclick.js文件，消除click事件在移动设备上的300ms的延迟
	FastClick.attach(document.body);


	//初始化分享按钮，根据不同的终端情况显示不同的分享按钮
	shareShowInfo();

	function shareShowInfo(){
		switch(shareFlag){
			case 'wx':
				//微信 朋友圈 qq好友 qq空间 新浪微博
				$('.s1,.s2,.s3,.s4,.s5').css('display','block');
			break;
			case 'client':
				//微信 朋友圈 qq空间 新浪微博
				$('.s4,.s5').css('display','block');
			break;
			case 'browser':
				//qq空间 新浪微博
				$('.s4,.s5').css('display','block');
			break;
		}
	}
	/**
	 * 进入页面之后调用该方法获取奖励的图片、名字、描述等内容
	 */
	function shareContent(){
		$.post(postShare,{
			'gid': userId,
			'appKey': appId
		},function(data){
			if(!data.code){
				initializeReward(data);
			}else{
				alert('初始化失败！')
			}
		},'json');
	}
	function initializeReward(data){
		$('#reward-img').attr('src', data.svo_imgs);
		$('#reward-detail').html(data.svo_details);
		$('#reward-name').html(data.svo_name);
		itemCode = data.svo_itemcode;
		
		sharelink = data.svo_sharelink;
		shareTitle = data.svo_sharetitle;
		shareDesc = data.svo_sharedesc;
		shareImgUrl =data.svo_shareimg;
		shareSummary = data.svo_sharesumary;
		if(sharelink.length >0){
			sharelink += '&share='+userId;
			var _wxOptions = getWxOptions();
			$.wxShare(_wxOptions);
		}
	}
	
	function getAllFriends(){
		$.post(postFriends, {
			'gid':userId,
			'appKey': appId
		}, function(data) {
			/*optional stuff to do after success */
			var len, contents ;
			if(!data.code){
				if(data.list.length > 0){
					len = data.list.length;
					contents = data.list;
					friendsContent(contents);
				}else{
					noFriends();
				}
			}else{
				alert('获取好友信息失败！')
			}
		});
	}
	/**
	 * 循环动态构建好友信息
	 * @param  {[数组]} contents [该参数是包含一系列对象的数组]
	 * @return {[type]}          [description]
	 */
	function friendsContent(contents){
		var len = contents.length;
		var friendsHtml = ''
		for (var i = 0; i < len; i++) {
			friendsHtml += '<li>'+
					'<figure class="head-user"><img src="'+contents[i].uhead+'" alt="'+contents[i].unickname+'"></figure>'+
					'<div class="user-info">'+
						'<div class="name-info">'+contents[i].unickname;
						/*
						if (contents[i].usex == 0) {
							friendsHtml += '<img src="http://images.1758.com/images/share/share_man.png"></div>';
						}else{
							friendsHtml += '<img src="http://images.1758.com/images/share/share_woman.png"></div>';
						}*/
						friendsHtml += '</div><div class="more-info"></div>'+
					'</div>'+
					'<div class="receive-info">'+
						'<div class="money-info">'+contents[i].udesc+'</div>';
						switch(contents[i].uprogress){
							case 0: 	//有奖励未领取状态
								friendsHtml += '<div class="btn-info btn-none" id="btn-info" data-cid="'+contents[i].uid+'">领取</div>';
								friendsHtml += '<div class="btn-info btn-img"><img src="http://images.1758.com/images/share/loading-32.gif"></div>'
							break;
							case 1: 	//已领取状态
								friendsHtml += '<div class="btn-info btn-have" >已领取</div>';
							break;
							case 3: 	//可以邀请状态
								friendsHtml += '<div class="btn-info btn-invite">邀请</div>';
							break;
						};
						friendsHtml += '</div></li>';
		}
		$('#friends-main').find('ul').html(friendsHtml);
		$('.main').show();
	}
	function noFriends(){
		var winHeight = $(document).height();
		var headHeight = $('#head').height();
		var mainHeight = winHeight - headHeight - 52;
		$('#nobody-text').css({
			'margin-top':mainHeight/2 + 'px',
			'display': 'block'
		});
	}
	/**
	 * [getParameters description]
	 * 根据url获取参数，&符合后面的。
	 * @return {[string]} [description]
	 */
	function getParameters(name){
		//正则验证
		 var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
	     var r = window.location.search.substr(1).match(reg);
	     if(r!=null){
	     	//return  unescape(r[2]);
	     	return  decodeURIComponent(r[2]);
	     } 
	     return '';
	}
	
	/**
	 * 判断用户的使用情况，客户端 微信 浏览器
	 * 根据不同的终端情况判断分享按钮的使用情况
	 */
	function judgeDevice(){
		var ua = window.navigator.userAgent.toLocaleLowerCase();
		if(ua.match(/MicroMessenger/i) == 'micromessenger' && (!(ua.match(/dwjia/i)=="dwjia")) ){
			//微信
			return 'wx';
		}else if(ua.match(/dwjia/i) == 'dwjia' && (ua.match(/MicroMessenger/i) == 'micromessenger')){
			//客户端
			return 'client';
		}else{
			//浏览器
			return 'browser';
		}
	}
	 
	/**
	 * 领取奖励
	 * 改变页面样式
	 */
	$('#friends-main').on('click','.btn-none',function(){
		$this = $(this);
		$this.css('display','none');
		$this.next('.btn-img').css('display','block');
		var clickUid = $this.attr('data-cid');
		$.post(postReward, {
				'gid': userId,
				'appKey': appId,
				'clickuid': clickUid,
				'cpid': cpId,
				'txId': txId,
				'itemcode': itemCode,
				'state':state
			}, function(data){
				if(data == 0){
					//领取奖励成功后使用的回调函数
					$this.removeClass('btn-none').addClass('btn-have');
					$this.next('.btn-img').css('display','none');
					$this.css('display','block');
					$this.html('已领取');
				}else{
					//领取奖励失败提示信息
					$this.next('.btn-img').css('display','none');
					$this.css('display','block');
					alert('领取失败！')
				}
			},'json');
	});
	
	//$('.qq-zone').qqZone(zoneOptions);
	
	//给分享的图标绑定事件
	$(document).on('click','.wx-circle img,.wx-friends img,.qq-friends img,.qq-zone img,.sina img',function(){
		
		var $this = $(this);
		var dataFlag = $this.parent('a').attr('data-share');
		var weiboOptions = {
			'url':sharelink,
			'title': shareTitle,
			'pic': shareImgUrl
		}
		var zoneOptions = {
				url: sharelink,
				/*分享的游戏地址*/
				loginpage: 'loginindex.html',
				logintype: 'qzone',
				page: 'qzshare.html',

				desc: shareSummary,		//分享理由，用户可修改内容
				/*默认分享理由(可选)*/
				
				summary: shareDesc,	//qqzone的分享摘要实际效果和其他的描述一样，所以换为描述
				/*分享摘要(可选)*/
				
				title: shareTitle,
				/*分享标题(可选)*/
				
				site: 'http://wx.1758.com',
				/*分享来源 如：腾讯网(可选)*/
				
				sid: '',
				
				imageUrl: shareImgUrl,
				/*分享图片的路径(可选)*/

				pics: shareImgUrl

		}
		if(shareFlag == 'wx'){
			switch(dataFlag){
				case 'wxFriends':
				case 'wxCircle':
				case 'qqFriends':
					$('.step-guide').show();
					//$.wxShare(wxNativeOption);
				break;
				case 'qqZone':
					$('.qq-zone').qqZone(zoneOptions);
				break;
				case 'sina':
					$('.sina').sinaWeibo(weiboOptions);
				break;
			}	
		}else if(shareFlag == 'client'){
			//shareClientWx(wxNativeOption);
			switch(dataFlag){
				case 'wxFriends':
					//WeixinJSBridge.fire('menu:share:appmessage');
					break;
				case 'wxCircle':
					//WeixinJSBridge.fire('menu:share:timeline');
					break;
				case 'qqFriends':
					break;
				case 'qqZone':
					$('.qq-zone').qqZone(zoneOptions);
				break;
				case 'sina':
					$('.sina').sinaWeibo(weiboOptions);
				break;
			}
		}else{

			switch(dataFlag){
				case 'wxFriends':
				case 'wxCircle':
				case 'qqFriends':
					//$('.step-guide').show();
					//$.wxShare(wxNativeOption);
				break;
				case 'qqZone':
					$('.qq-zone').qqZone(zoneOptions);
				break;
				case 'sina':
					$('.sina').sinaWeibo(weiboOptions);
				break;
			}
		}
		
	})
	
	//微信里面的指引线点击事件
	$('.step-guide img').on('click',function(){
		$('.step-guide').css('display','none');
	})

	/*功能不可用
	//客户端模拟微信的功能分享到朋友圈和微信好友
	function shareClientWx(shareData){
		document.addEventListener('WeixinJSBridgeReady', function onBridgeReady() {
		    WeixinJSBridge.on('menu:share:appmessage', function(argv) {
		        WeixinJSBridge.invoke('sendAppMessage', {
		            "img_url": window.shareData.imgUrl,
		            "link": window.shareData.link,
		            "desc": window.shareData.desc,
		            "title": window.shareData.title
		        }, onShareComplete);
		    });

		    WeixinJSBridge.on('menu:share:timeline', function(argv) {
		        WeixinJSBridge.invoke('shareTimeline', {
		            "img_url": window.shareData.imgUrl,
		            "img_width": "640",
		            "img_height": "640",
		            "link": window.shareData.link,
		            "desc": window.shareData.desc,
		            "title": window.shareData.title
		        }, onShareComplete);
		    });
		}, false);
	}
	function onShareComplete(){}
	*/
})