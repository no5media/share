define(['data/getInfoData','common/eventBase'],function(DetailsData,EventBase){
	var dataInfo = {};
	dataInfo = {
		/**
		 * 
		 * @param {Object} url
		 * @param {Object} infos
		 * @param {Object} callback
		 */
		getDataList:function(url,infos,callback){
			DetailsData.getData(url,infos,function(data){
				data = dateFormat(data);
				callback(data);
			});
		}
	}
	function dateFormat(dataArray){
		for(var i=0,len=dataArray.length;i<len;i++){
			if(dataArray[i].comment){
				dataArray[i].comment.createTime = EventBase.dateFormat(dataArray[i].comment.createTime,'yyyy.MM.dd hh:mm');
				if(!dataArray[i].comment.img){
					dataArray[i].comment.img = [];
				}else{
					dataArray[i].comment.img = dataArray[i].comment.img.split(',');
				}
			}
		}
		return dataArray;
	}
	return dataInfo;
})