$(function(){
	//立即执行函数，执行微信的分享功能，此功能需要先引入wx的js文件
	//http://res.wx.qq.com/open/js/jweixin-1.0.0.js
	function registerWxShare(wxShareData){
		var defaults = {
			title: '怪兽来了',
			desc: '还在刷朋友圈吗？快来跟我一起玩怪兽吧~ ~ ~ ~',
			link: 'http://wx.1758.com',
			imgUrl: 'http://images.1758.com/images/1758new.png',
			type: 'link',
			dataUrl: '',
			success: function(){},
			cancel: function() {}

		}
		var wxShareData = $.extend({},defaults,wxShareData);
		var hr = location.href;
		hr = encodeURIComponent(hr);
		var postUrl = 'http://wx.1758.com/game/platform/v1.0/weixin/js/config?pageUrl='+hr;
		$.get(postUrl, function(data){
			var conf=data;
	        wx.config({
	            debug: false,
	            appId: conf.appId,
	            timestamp: conf.timestamp,
	            nonceStr: conf.nonceStr,
	            signature: conf.signature,
	            jsApiList: conf.jsApiList
	        });

	        wx.ready(function(){ 
	        	//分享给朋友
		        wx.onMenuShareAppMessage({
		            title: wxShareData.title, // 分享标题
		            desc: wxShareData.desc, // 分享描述
		            link:  wxShareData.link,
		            imgUrl: wxShareData.imgUrl,
		            //imgUrl: "http://" + shareId + "." + domain + "/icon.png",
		            type: wxShareData.type,
		            dataUrl: wxShareData.dataUrl,
		            success: wxShareData.success,
		            cancel: wxShareData.cancel
		        });

		        //分享到朋友圈
		        wx.onMenuShareTimeline({
		            title: wxShareData.title, // 分享标题
		            link:  wxShareData.link,
		            imgUrl: wxShareData.imgUrl,
		            //imgUrl: "http://" + shareId + "." + domain + "/icon.png",
		            //success: function () { window["vutimes_foo"]["share_cb"](); },
		            success: wxShareData.success,
		            cancel: wxShareData.cancel
		        });
		        //分享到qq好友
		        wx.onMenuShareQQ({
				    title: wxShareData.title, // 分享标题
				    desc: wxShareData.desc, // 分享描述
				    link: wxShareData.link, // 分享链接
				    imgUrl: wxShareData.imgUrl, // 分享图标
				    success: wxShareData.success,
				    cancel: wxShareData.cancel
				});
	        });

	        wx.error(function(res){ });
	        
		});
	};
	/**
	 * 分享到qq空间
	 * @return {[type]} [description]
	 */
	$.fn.extend({
		//分享到qq空间
		qqZone: function(options){
			var $this = $(this),
				shareSrc,
				systemFlag;

			systemFlag = judgeSystem();
			if(systemFlag == 'mobile'){
				shareSrc = qqShareMobile(options);
			}else{
				shareSrc = qqSharePC(options);
			}
			window.open(shareSrc,'target:_blank');
		},
		
		sinaWeibo : function(options){
			var $this = $(this),
				shareUrl ;
			shareUrl = sinaShare(options);
			window.open(shareUrl);
		} 
	});
	
	/**
	 * 判断操作系统是pc还是mobile
	 * @return {[string]} [description]
	 * 'mobile' 手机端
	 * 'pc' 电脑端
	 */
	$.extend({
		system: (function(){
			return judgeSystem;
		}()),

		wxShare: function(data){
			registerWxShare(data);
		}
	})
	function judgeSystem(){
		var system = {
            win: false,
            mac: false,
            xll: false
        };
        //检测平台
        var p = navigator.platform;
        system.win = p.indexOf("Win") == 0;
        system.mac = p.indexOf("Mac") == 0;
        system.x11 = (p == "X11") || (p.indexOf("Linux") == 0);
        if (system.win || system.mac || system.xll) {//电脑页面
            return 'pc';
        } else {
            return 'mobile';//手机
        }
	}
	/**
	 * [手机端qq空间分享功能]
	 * @param  {object} options [配置的参数]
	 * @return {string}         [分享超链接地址]
	 * 
	 */
	function qqShareMobile(option){
		var defaults = {
			url: 'http://wx.1758.com',
			/*分享的游戏地址*/
			
			loginpage: 'loginindex.html',
			logintype: 'qzone',
			page: 'qzshare.html',

			desc: '',
			/*默认分享理由(可选)*/
			
			summary: '神在创造我的时候。。。竟然加了这些东西。',
			/*分享摘要(可选)*/
			
			title: '1758.com分享',
			/*分享标题(可选)*/
			
			site: 'http://www.wx.1758.com',
			/*分享来源 如：腾讯网(可选)*/
			
			sid: '',
			
			imageUrl:'http://images.1758.com/images/1758new.png'
			/*分享图片的路径(可选)*/
		};
		var options = $.extend({}, defaults, option);
		var sTest = [];
		for (var i in options) {
			sTest.push(i + '=' + encodeURIComponent( options[i] || ''));
		}
		var sHref = ['http://openmobile.qq.com/oauth2.0/m_jump?', sTest.join('&')].join('');
		return sHref;
	}
	/**
	 * [pc端分享到qq空间功能]
	 * @param  {[对象]} options [设置参数]
	 * @return {[string]}         [分享qq空间跳转的href地址]
	 */
	function qqSharePC(option){
		var defaults = {
				url:'http://wx.1758.com',
				showcount:'0',/*是否显示分享总数,显示：'1'，不显示：'0' */
				desc:'',/*默认分享理由(可选)*/
				summary:'',/*分享摘要(可选)*/
				title:'',/*分享标题(可选)*/
				site:'www.wx.1758.com',/*分享来源 如：腾讯网(可选)*/
				pics:'', /*分享图片的路径(可选)*/
				loginpage: 'loginindex.html',
				logintype: 'qzone',
				page: 'qzshare.html'
		}
		var options = $.extend({}, defaults, option);
		var sTest = [];
		for(var i in options){
			sTest.push(i + '=' + encodeURIComponent( options[i] || ''));
		}
		var sHref = ['http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?',sTest.join('&')].join('');
		return sHref;
	}


	function sinaShare(option){
		var defaults = {
			url: '',
			title: '1758.com，即点即玩，快喊朋友一块来玩！',
			appkey: '582335886',
			pic: ''
		};
		var options,	//参数选项 
			sTest = [],		//定义的临时变量
			systemFlag,		//判断用户使用的是pc还是mobile的标示
			sUrl	//用户分享组装出来的url
		options = $.extend({}, defaults, option);
		for(var i in options){
			sTest.push(i + '=' + encodeURIComponent( options[i] || ''));
		}
		systemFlag = judgeSystem();
		if(systemFlag == 'mobile'){
			sUrl = ['http://service.weibo.com/share/mobile.php?',sTest.join('&')].join('');
		}else{
			sUrl = ['http://service.weibo.com/share/share.php?', sTest.join('&')].join('');
		}
		return sUrl;
	};



})