/**
 * 加载浮层里面的内容
 */
define(['data/detailscontroller','common/eventBase','libs/echo','view/wschat'],function(DetailsController,EventBase,Echo,WSC){
	var module = {};
	var articleId,
		appName,
		iconUrl,
		deferArticle = $.Deferred(),	//deferred 对象,等待获取当前游戏的基本信息
		htmlTem = EventBase.loadTem('/spf/mweb/html/gameModalTem.html'),
		appkey = EventBase.getParameters('appKey'),
		replyType = '',
		replyData ,
		imgsData = {
			localId: [],
		    serverId: []
		};
	module = {
		init:function(arg){
			this.arginfo = $.extend({},this.arginfo,arg);
			if(!this.arginfo.userInitFlag){
				//初始化信息
				initInfo();
			}else{
				//判断localstorage刷新时间
				var b = detectRefretime();
				//假如初始化之后，则进行tab选择
				this.selectTab(b);
			}
		},
		selectTab:function(b){
			if(b){
				//需要刷新
				tabClick(module.arginfo.lastTab,'un');
				//从新记录刷新的时间
				setStorageTime();
			}else{
				//tabClick('gift','un')
			}
		},
		arginfo:{
			userInitFlag:false,			//标识是否需要初始化获取用户信息
			refreshtime:1*30*1000,
			timeName:'hlmyPanelT',		//localStorage 存储的名字
			lastTab:'',				//每次切换tab，记录tab项
			backTab:'',				//记录除聊天外其余的tab项，保证退出聊天项后可以切换到最后一次的非聊天tab
			needMessage:false,		//是否需要信息，初始化的链接的时候用到,通知用户是否需要马上发送消息
			lastestMessageId:'',
			needLastId:false			//判断进入游戏的时候  需不需要告诉服务器lastmessageid
		},
		//初始化websocket
		initWschat:function(){
			initWebSocket();
		},
		//通知服务器是否需要发送消息
		noticeServer:function(args){
			if(args){
				this.arginfo.needMessage = true;
				initSocketInfo({
					"subType":"102",
					"param":{
						"chatroomId":appkey+"@appKey"
					}
				});
//				module.arginfo.needMessage = false;
				console.log('%c 通知服务器获取信息','color:#129FEA')
			}else if(!args){
				this.arginfo.needMessage = false;
				this.arginfo.needLastId = false;
				initSocketInfo({
					"subType":"152",
					"param":{
						"chatroomId":appkey+"@appKey"
					}
				});
				console.log('%c 通知服务器断开信息','color:#129FEA');
				$('#panelChatContainer').html('');
			}
			
		}
	}
//	$('body').on('touchmove', function (event) {
//	    event.preventDefault();
//	});
	//初始化内容
	function initInfo(){
		//var appkey = EventBase.getParameters('appKey');
		//通过appKey获取articleId
		getArticleId(appkey);
		//等待异步
		$.when(deferArticle).done(function(){
			tabClick('gift','un');
			$('#talk').scroll(function(){
				Echo.init();
			})
			//设置初始化时间
			setStorageTime();
		}).fail(function(){});
		getUserInfo();
	}
	function getUrl(){
		var url = document.location.href;
		return url; 
	}
	/**
	 * @description 获取到当前在玩游戏的基本信息
	 * @param {Object} appkey
	 */
	function getArticleId(appkey){
		var url = '/game/api/app/getAppInfo?appKey='+appkey;
		var info = '';
		DetailsController.getDataList(url,info,function(data){
			if(data.code == '0'){
				articleId = data.articleId;
				appName = data.wxApp.name;
				iconUrl = data.wxApp.iconUrl;
				$('#game-icon').attr('src',iconUrl);
				deferArticle.resolve();
				module.arginfo.userInitFlag = true;
			}
		});
	}
	function getData(num,type){
		var url,
			infos;
		if(type == 'gift'){
			url = '/game/api/app/getDetails?aid='+articleId;
			infos='';
			DetailsController.getDataList(url,infos,handleGiftData);
		}else{
			if(num == 3){
				parent.window.location.href = 'http://wx.1758.com/game/h5/game.htm?focus=1&aid='+articleId+'&title='+appName+'&tp=full&ex1758=1';
			}else{
				url = '/game/api/commentsWithReplies';
				infos = {
					type:8,
					pageNo:num,
					pageSize:10,
					resourceId:articleId
				}
				DetailsController.getDataList(url,infos,handleTalkData);	
			}
		}
	}
	//处理得到的礼包数据
	function handleGiftData(data){
		var gifts,
			tem,
			giftdata,
			compireHtml;
		gifts = data.gifts;
		//如果有礼包展示礼包
		if(!!gifts && gifts.length > 0){
			tem =  htmlTem.find('#gift').html();
			tem = EventBase.replaceTem(tem);
			giftdata = dealData(gifts);
			compireHtml = EventBase.compileTem(tem,giftdata);
			$('#gift .list').html(compireHtml);
		}
	}
	//处理得到的聊吧数据
	function handleTalkData(data){
		if($('.more-info').attr('data-num') == 2){
			$('.more-info a').html('点击加载更多');
		}else{
			$('.more-info a').html('点击加载更多');
		}
		
		if(data.length >0 ){
			var tem,compireHtml;
			var datanum = parseInt($('.more-info').attr('data-num'));
			tem = htmlTem.find('#details').html();
			compireHtml = EventBase.compileTem(tem,data);
			$('.more-info').before(compireHtml);
			$('.more-info').attr('data-num',datanum+1);
			Echo.init();
		}else{
			$('.more-info').addClass('gone');
		}
	}
	//处理礼包时间信息，当未开放的时候显示出今日，明日，后台
	function dealData(data){
		var datenow = new Date();	//当前时间
		var t = datenow.getTime();		//当前毫秒数
		var year = datenow.getFullYear();
		var month = datenow.getMonth();
		var day = datenow.getDate();
		var t1 = new Date(year,month,day,0,0,0).getTime();	//今天凌晨毫秒数
		var t2 = t1+(24*60*60*1000); //明天凌晨零点毫秒数
		var t3 = t2+(24*60*60*1000);	//后天凌晨零点毫秒数
		var t4 = t3+(24*60*60*1000);	//大后天凌晨零点毫秒数
		var bt,y,M,d,h,m;
		
		for(var i = 0,len = data.length;i<len;i++){
			bt = data[i].gift.beginTime;
			dd = new Date(bt);
			y = dd.getFullYear();
			M = (dd.getMonth()+1) > 9 ? (dd.getMonth()+1): '0'+(dd.getMonth()+1);
			d = dd.getDate() > 9 ? dd.getDate():'0'+dd.getDate();
			h = dd.getHours() > 9 ? dd.getHours():'0'+dd.getHours();
			m = dd.getMinutes() > 9 ? dd.getMinutes():'0'+dd.getMinutes();
			if(bt > t){
				if(bt>t1 && bt<t2){
					data[i].gift.bt = '今日  '+h+':'+m;
				}else if(bt>t2 && bt<t3){
					data[i].gift.bt = '明日  '+h+':'+m;
				}else if(bt>t3 && bt<t4){
					data[i].gift.bt = '后天  '+h+':'+m;
				}else{
					data[i].gift.bt = M+'-'+d+' '+h+':'+m;
				}
			}else{
				data[i].gift.bt = M+'-'+d+' '+h+':'+m;
			}
		}
		return data;
	}
	//tab点击事件
	function tabClick(type,dataLoad){
		//type = 'chat';
		//dataLoad == 'un';
		module.arginfo.lastTab = type;
		if(module.arginfo.lastTab != 'chat'){
			module.arginfo.backTab = type;
		}
		if(type == 'gift'){
			//礼包
			$('#base-footer-game').removeClass('gone')
			$('#gift').removeClass('gone');
			$('#gg img').attr('src','http://images.1758.com/ranking/smallgifty.png');
			$('#tt img').attr('src','http://images.1758.com/images/talkw.png');
			if(dataLoad == 'ac'){
				//已经加载过数据了，展示礼包tab内容，隐藏聊吧tab内容 
			}else{
				//还没有加载过数据，进行数据请求
				$('#t-g').attr('data-value','ac');
				getData(1,'gift');
			}
		}else if(type == 'talk'){
			//聊吧
			$('#base-footer-game').removeClass('gone')
			$('#talk').removeClass('gone');
			$('#gg img').attr('src','http://images.1758.com/images/giftw.png');
			$('#tt img').attr('src','http://images.1758.com/ranking/gametalky.png');
			if(dataLoad == 'ac'){
				//已经加载过数据了，展示聊吧tab内容，隐藏礼包tab内容
			}else{
				//还没有加载过数据，进行数据请求
				$('.more-info a').html('加载中...');
				$('#t-t').attr('data-value','ac');
				getData(1,'talk');
			}
		}else if(type == 'chat'){
			//向服务器获取历史信息
//			module.noticeServer(true);
			//聊天室
			$('#base-footer-game').addClass('gone');
			$('#chat').removeClass('gone');
//			$('#cc img').attr('src','http://images.1758.com/images/giftw.png');
//			$('#cc img').attr('src','http://images.1758.com/images/talky.png');
			if(dataLoad == 'ac'){
				//已经加载过数据了，展示聊吧tab内容，隐藏礼包tab内容
			}else{
				//还没有加载过数据，进行数据请求
				//getData(1,'talk');
			}
		}
	}
	//点赞
	$(document).on('click','.like-p',function(){
		var $this = $(this);
		var data = $(this).attr('data-like');
		data = data.split(',');
		like(data[0],data[1],$this);
	})
	function like(resourceId,resourceType,$this){
		if ($this.hasClass("liked")){
		      	$this.find('img').attr("src","http://wx.1758.com/game/h5/images/n-cxihuan-2-tw.png");
		      	$this.removeClass("liked");
		      	var count=$('#like-count-'+resourceId).html();
		      	$('#like-count-'+resourceId).html(--count);
		      $.post('/game/api/unlike',{resourceId:resourceId,resourceType:resourceType}, function(data){
		      });
	    }else{
	      	$this.find('img').attr("src","http://wx.1758.com/game/h5/images/n-cxihuan-1-tw.png");
	      	$this.addClass("liked");
	      	var count=$('#like-count-'+resourceId).html();
	      	$('#like-count-'+resourceId).html(++count);
			$.post('/game/api/like',{resourceId:resourceId,resourceType:resourceType}, function(data){
			});
	    }
	}
	//留言或者回复
	$(document).on('click','.replay',function(){
		var $this = $(this);
		var data = $this.attr('data-message');
		data = data.split(',');
//		reply(data[0],data[1],data[2],data[3],data[4]);
		replys(data);
	})
	function reply(prefix,id,appId,receiverId,receiverName){
//		  if ( receiverId && receiverName ){
//			  var json='{"resourceId":'+appId+',"parentId":'+id+',"receiverId":"'+receiverId+'"}';
//			  var tags='[{"id":3,"name":"回复'+receiverName+'"}]';
//	  	  }else{ 
//			  var json='{"resourceId":'+appId+',"parentId":'+id+'}';
//			  var tags='[{"id":3,"name":"回复"}]';
//		  }
//		  var state='{"id":"'+id+'","prefix":"'+prefix+'"}';
//		  try{
//		  	android_tw_ugc.ugc(json,4,tags,'updateReplies',state);//callback,state
//		  }catch (exp){
//	      	location.href="http://wx.1758.com/game/comment/send.html?atype=3&aid="+appId+"&pid="+id;
//	      }
	}
	function replys(data){
		replyData = data;
		showWeDialog('reply',data);
	}
	//详情页点击进入游戏
	$('#gift').on('click','.i-info',function(){
		var url = $(this).attr('data-value');
		parent.window.location.href = url;
	})
	//领号点击
	$(document).on('click','.linghao',function(){
		var id = $(this).attr('data-num');
		var type = $(this).attr('data-value');
		var ss;
		if(type == '1'){
			ss = EventBase.userTerminal();
			if(ss =='weixin' || ss=='others'){
				exclusiveTip();
			}else{
				getGiftNum(id);
			}
		}else if(type == '0'){
			getGiftNum(id);
		}
	})
	function getGiftNum(id){
		var url="/game/api/app/gif/apply?gifId="+id;
		$.get(url,function(data){
			//弹窗
			if(data.code == -1){
				//未登录,跳转登陆页面
				location.href="http://wx.1758.com/game/platform/v1.0/user/flogin?appKey=ba0ab54c1eb63a776fa477153a0a354b";
			}
			if(data.code == 0 ){
				//flag 领号0 淘号1
//				window.prompt('领号成功,请手动复制您的礼包码',data.cdkey);
				tanChuang("领号成功",data.cdkey,id,0);
			}
		});
	}
	//淘号
	$(document).on('click','.taohao',function(){
		var id = $(this).attr('data-num');
		var type = $(this).attr('data-value');
		var ss;
		if(type == '1'){
			ss = EventBase.userTerminal();
			if(ss =='weixin' || ss=='others'){
				exclusiveTip();
			}else{
				taoHao(id);
			}
		}else if(type == '0'){
			taoHao(id);
		}
		
	})
	function taoHao(id){
		var url="/game/api/app/gif/tao?gifId="+id;
			$.get(url,function(data){
				if(data.code == -1){
					//未登录,跳转登陆页面
					location.href="http://wx.1758.com/game/platform/v1.0/user/flogin?appKey=ba0ab54c1eb63a776fa477153a0a354b";
				}
				if(data.code == 0){
//					window.prompt('淘号成功,请手动复制您的礼包码',data.cdkey)
					tanChuang("淘号成功",data.cdkey,id,1);
				}
			});
	}
	function tanChuang(title,cdkey,gid,flag){
		var url="http://wx.1758.com/game/h5/giftinfo.htm?cdkey="+cdkey+"&aid="+gid+"&flag="+flag;
		parent.window.location.href=url+'&tp=outer&client=1&tp=full&ex1758=1';
	}
	//专属礼包提示
	function exclusiveTip(){
		$('.tip').removeClass('gone');
	}
	$(document).on('click','.tip',function(e){
		$('.tip').addClass('gone');
	})
	//绑定点击事件
	//tab 点击事件
	$(document).on('click','.t',function(){
		var type,	//具体指哪个tab
			dataLoad;	//是否加载过数据
		type = $(this).attr('data-type');
		dataLoad = $(this).attr('data-value');
		$('#gift,#talk,#chat').addClass('gone');
		tabClick(type,dataLoad);
	})
	//加载更多点击事件
	$(document).on('click','.more-info',function(){
		var type, //类型，判断是礼包tab还是聊吧tab；
			dataLoad,	//判断是否已经加载数据了 un 为未加载 ac为已经加载
			num;	//第几页
		type = $(this).attr('data-track');
		dataLoad = $(this).attr('data-value');
		num = $(this).attr('data-num');
		$('.more-info a').html('加载中...');
		if(type == 'talk'){
			//聊吧
			getData(num,'talk');
		}else{
		}
	});
	//跳转到首页
	$(document).on('click','#goHome',function(){
		parent.window.location.href = 'http://wx.1758.com/game/h5/index.htm';
	});
	//发布评论的时候添加图片
	$(document).on('click','#wxAddImg',function(){
		if(imgsData.localId.length <4){
			$('#loadingToast').removeClass('gone');
			setTimeout(function () {
	            $('#loadingToast').addClass('gone');
	        }, 2000);
			wx.chooseImage({
			  count:4,
		      success: function (res) {
		      	$('#loadingToast').addClass('gone');
		      	var testArr = [];
		      	for(var i=0,len = res.localIds.length;i<len;i++){
		      		if(imgsData.localId.length < 4){
				        imgsData.localId.push(res.localIds[i]);
				        testArr.push(res.localIds[i]);
		      		}
		      	}
		        addPreImg(testArr);
		      }
		    });
		}else{
			$('#toastTip').removeClass('gone');
	        setTimeout(function () {
	            $('#toastTip').addClass('gone');
	        }, 2000);
		}
	})
	//添加预览文件
	function addPreImg(localIds){
		var len = localIds.length;
		var str = '';
		if(imgsData.localId.length == 4){
			$('#wxAddImg').addClass('gone');
		}
		for(var i=0;i<len;i++){
			str += '<div class="pure-u-1-4 selfimg">'+
				'<div class="imgitem">'+
					'<div style="background-image:url('+localIds[i]+')"></div>'+
					'<img class="gone" src="'+localIds[i]+'" />'+
					'<span class="imgdel">'+
						'<img src="http://images.1758.com/daren/delimg.png" />'+
					'</span>'+
				'</div>'+
			'</div>';
		}
		$('#wxImgWrapper').prepend(str);
	}
	//发布评论
	$(document).on('click',"#commentLogo",function(){
//		var localcookie = document.cookie;
//		var lo = localcookie.indexOf('wy_user');
//		if(lo >= 0){
//			//评论部分,老版本没这个功能,该版本userAgent加了“dwjia”,以此来区分老版本提示更新
//			var json='{"resourceId":'+articleId+'}';
//			//json = json.toString(json);
//			var tags='[{"id":8,"name":"评论"}]';
//			try{
//				android_tw_ugc.ugc(json,0,tags,'updateComments','');
//			}catch (exp){
//				location.href="http://wx.1758.com/game/comment/send.html?aid="+articleId+"&atype=8&ex1758=1&tp=full";
//			}
//		}else{
//			document.location.href = 'http://wx.1758.com/game/platform/v1.0/user/flogin?appKey=ba0ab54c1eb63a776fa477153a0a354b';
//		}
		showWeDialog('comment');
	});
	//显示评论框
	function showWeDialog(arg){
		var ua = EventBase.userTerminal();
		if(arg == 'reply'){
			//回复
			replyType = 'reply';
			if(replyData[0] == 'zhanji_all'){
				$('.weui_dialog_title').html('回复'+replyData[4]);
			}else{
				$('.weui_dialog_title').html('回复评论');
			}
			$('#wxAddImg').addClass('gone');
			$('#weDialog').removeClass('gone');
		}else{
			//评论
			replyType = 'comment';
			$('.weui_dialog_title').html('发表评论');
			if(ua == 'weixin'){
				$('#wxAddImg').removeClass('gone');
			}else{
				$('#wxAddImg').addClass('gone');
			}
			$('#weDialog').removeClass('gone');
		}
	}
	//评论发布框取消点击事件
	$(document).on('click','#weCancle',function(){
		$('#weText').val('');
		$('#weDialog').addClass('gone');
		$('#loadingToast').addClass('gone');
	})
	//评论发布框发布点击事件
	$(document).on('click','#wePublic',function(){
		var content = $('#weText').val();
		content = $.trim(content);
		if(content.length > 0 || imgsData.localId.length > 0){
			postPublicContent(content);
			$('#weDialog').addClass('gone');
			$('#loadingToast').addClass('gone');
			$('#weText').val('');
			$('#wxImgWrapper .selfimg').remove();
		}
	})
	//删除图片
	$(document).on('click','.imgdel',function(){
		var delsrc = $(this).prev('img').attr('src');
		$(this).parents('.selfimg').remove();
		var len = imgsData.localId.length;
		for(var i = 0;i<len;i++){
			if(imgsData.localId[i] == delsrc){
				imgsData.localId.splice(i,1);
				if(imgsData.localId.length < 4){
					$('#wxAddImg').removeClass('gone');
				}
				reutrn ;
			};
		}
	})
	//上传图片到微信
	function pushToWeiXin(callback,content){
		var i=0,len=imgsData.localId.length;
		function upload(){
			wx.uploadImage({
		        localId: imgsData.localId[i],
		        success: function (res) {
		          i++;
		          imgsData.serverId.push(res.serverId);
		          if (i < len) {
		            upload();
		          }else{
		          	callback(content);
		          }
		        },
		        fail: function (res) {
		          alert(JSON.stringify(res));
		        }
		      });
		}
		upload();
	}
	//发送评论内容到服务器
	function postPublicContent(content){
		var localcookie = document.cookie;
		var lo = localcookie.indexOf('wy_user');
		var parameters;
		if(lo >= 0){
			if(replyType == 'comment'){
				//发表评论
				parameters = {
				 	aid:articleId,
				 	content:content,
				 	img:'',
				 	channelType:'8'
				 }
				if(imgsData.localId.length >0){
					//上传到微信
					pushToWeiXin(postCommentWxImg,content);
					return ;
				}
			}else if(replyType == 'reply'){
				//回复评论
				parameters = {
				 	aid:replyData[2],
				 	content:content,
				 	rid:replyData[1],
				 	channelType:'3'
				 }
				if(replyData[0] == 'zhanji_all'){
					parameters.channelType = '10';
				}
			}
			$.post('/game/api/comments',parameters,function(response){
				$('#toast').removeClass('gone');
                setTimeout(function () {
                    $('#toast').addClass('gone');
                }, 2000);
			})
		}else{
			document.location.href = 'http://wx.1758.com/game/platform/v1.0/user/flogin?appKey=ba0ab54c1eb63a776fa477153a0a354b';
		}
	}
	//上传带微信图片到服务器
	function postCommentWxImg(content){
		var ser = imgsData.serverId.join(',');
		var parameters = {
			 	aid:articleId,
			 	content:content,
			 	img:ser,
			 	channelType:'8',
			 	isWxImg:true
			 }
		$.post('/game/api/comments',parameters,function(response){
			imgsData.localId = [];
			imgsData.serverId = [];
			$('#toast').removeClass('gone');
            setTimeout(function () {
                $('#toast').addClass('gone');
            }, 2000);
		})
	}
	//查询个人信息，获取个人头像
	function getUserInfo(){
		var url = '/game/platform/user/getInfo';
		DetailsController.getDataList(url,'',function(data){
			if(data.code == 0){
				$('#hgift img').attr('src',data.infos.head);
			}
		});
	}
	/**
	 * 初始化websocket聊天信息
	 * hlmySocket结构
	 * {
	 *	_callbacks
	 * 	_fired
	 * 	socketInfo{
	 * 		socket
	 * 		fromToken
	 * 	}
	 * }
	 */
	function initWebSocket(){
		var url = "/openapi/im/authorize.json";
		//检测boolean是否支持websocket
		var bl = WSC.checkWS();
		if(!window.hlmySocket){
			if(bl == true){
				var socketEmitterObj = new WSC.socketEmitter();
				window.hlmySocket = socketEmitterObj;
				handleWSEvent(socketEmitterObj);
				socketEmitterObj.init(url);
			}
		}else if(window.hlmySocket.socketInfo.socket.readyState != WebSocket.OPEN){
			window.hlmySocket.init(url);
		}else{
		}
	}
	//初始化socketinfo 场景通知
	function initSocketInfo(args){
		hlmySocket.initSendMessage(args);
	}
	//注册事件
	function handleWSEvent(socketEmitterObj){
		socketEmitterObj.on('onopen',wsOpen);
		socketEmitterObj.on('onmessage',wsMessage);
		socketEmitterObj.on('onclose',wsClose);
	}
	function wsOpen(){
		var time = new Date();
		var value = time.getHours()+':'+time.getMinutes()+':'+time.getSeconds();
		console.log('%c 打开socket连接'+value,'color:green;');
		var socketEmitterObj = window.hlmySocket;
		//建立连接下 初始化信息
		if(module.arginfo.needMessage){
			if(module.arginfo.needLastId){
				socketEmitterObj.initSendMessage({
					"subType":"102",
					"param":{
						"chatroomId":appkey+"@appKey",
						"lastestMessageId":module.arginfo.lastestMessageId
					}
				});
			}else{
				socketEmitterObj.initSendMessage({
						"subType":"102",
						"param":{
							"chatroomId":appkey+"@appKey"
						}
					});
			}
		}
		clientTipInfo('连接服务器成功');
	}
	function wsClose(){
		var time = new Date();
		var value = time.getHours()+':'+time.getMinutes()+':'+time.getSeconds();
		console.log('%c 关闭socket连接'+value,'color:red;');
		clientTipInfo('服务器断开连接');
		//设置为true，则发送信息的时候带lastid
		module.arginfo.needLastId = true;
		if(hlmySocket.socketInfo.close){
			clientTipInfo('<span id="reconnect" class="sys-1">您已经在另一个设备登录了！点击登录</span>');
			hlmySocket.socketInfo.close = false;
		}else{
			clientTipInfo('正在连接服务器...');
			initWebSocket();
		}
	}
	function wsMessage(event){
		console.log('%c 接收到的数据信息：\n','color:#FCA218');
		var str = wsHtml(event.data);
		$('#panelChatContainer').append(str);
	}
	//收藏游戏
	$(document).on('click','#collect',function(){
		var usa=navigator.userAgent;
		var isWeiXin = /\bMicroMessenger\/[\d\.]+/.test(usa);
		var isMQQ = /.*QQ\/([\d\.]+)/.test(usa);
		var p;
		if(usa.indexOf("Android")>-1){
            p = "android";
        }else if(usa.indexOf("iPhone")>-1 || usa.indexOf("iPad")>-1 || usa.indexOf("iPod")>-1){
            p = "ios";
        }else{
            p = "pc";
        }
         if(isMQQ){
        	location.href = 'mqqapi://card/show_pslcard?uin=2177540690&account_flag=33626421&card_type=public_account';
        }else{
        	location.href = 'http://mp.weixin.qq.com/s?__biz=MjM5MjQyOTg3MA==&mid=207867528&idx=1&sn=19c7b9fb2c23871ab7a189dfe86834ef#rd';
        }
//      if(isWeiXin){
//			parent.window.location.href = 'http://mp.weixin.qq.com/s?__biz=MjM5MjQyOTg3MA==&mid=207867528&idx=1&sn=19c7b9fb2c23871ab7a189dfe86834ef#rd';
//		}else if(isMQQ){
//			location.href = 'mqqapi://card/show_pslcard?uin=2177540690&account_flag=33626421&card_type=public_account';
//		}else if(p == 'android' || p == "ios"){
//			location.href = 'mqqapi://card/show_pslcard?uin=2177540690&account_flag=33626421&card_type=public_account';
//		}else{
//			location.href = "http://s.p.qq.com/mpmobile/card/index.html?puin=2177540690";
//		}
	});
	//访问个人主页
	$(document).on('click','#hgift',function(){
		parent.window.location.href = 'http://wx.1758.com/game/h5/user.htm';
	})
	//访问客服
	$(document).on('click','#goKefu',function(){
		parent.window.location.href = 'http://wpa.qq.com/msgrd?v=3&uin=2660866818&site=qq&menu=yes';
	});
	//下载客户端
	$(document).on('click','.ex-load',function(){
		parent.window.location.href = "http://m.1758.com/game/download.htm";
	})
	//聊天发送信息
	$(document).on('click','#sendMessage',function(){
		var value = $('#chatTextarea #chatCon').val();
		value = $.trim(value);
		if(!!value){
			window.hlmySocket.sendMessage({
			    "message": {
			        "text": value
			    },
			    "to": appkey+'@appKey'
			},sendInfoCallback);
		}
	})
	//聊天重连消息
	$(document).on('click','#reconnect',function(){
		clientTipInfo('正在连接服务器...');
		//没有连接成功，重新连接
		initWebSocket();
	})
	/**
	 * 该函数在没有连接成功的情况下触发
	 */
	function sendInfoCallback(data){
		if(data.flag){
			//发送成功
			$('#chatTextarea #chatCon').val('');
		}else{
			//提示信息
			clientTipInfo('正在连接服务器...');
			//没有连接成功，重新连接
			initWebSocket();
		}
	}
	//关闭聊天
	$(document).on('click','#chatClose',function(){
		var backTab = module.arginfo.backTab;
		var flag = '';
		$('#base-menu .t').each(function(){
			if($(this).attr('data-type') == backTab){
				flag = $(this).attr('data-value');
				return flag;
			};
			
		})
		$('#gift,#talk,#chat').addClass('gone');
		tabClick(module.arginfo.backTab,flag)
	})
	//组装消息样式
	function wsHtml(data){
		data = JSON.parse(data);
		if(data.type){
			switch(data.type){
				case 0:		//应答，暂不处理
					
				break;
				case 100:	//场景切换，断开连接
					serverCreen(data);
				break;
				case 600:		//群聊消息
					return showGroupChat(data);
				break;
			}
		}
	}
	//切换场景的时候 记录断开连接原因
	function serverCreen(data){
		hlmySocket.socketInfo.close = true;
	}
	//群聊渲染
	function showGroupChat(data){
		var info = data.message,
			user = data.fromProfile,
			flag = 'buddy',
			classname,
			str;
		module.arginfo.lastestMessageId = data.id;
		if(user.isHost){
			flag = 'self';
		}
		if(flag == 'buddy'|| flag == 'self'){
			classname = flag;
			str = '<div class="chat-content-group '+classname+'" _sender_uin="3684649976">'+
			    '<img class="chat-content-avatar" src="'+user.headUrl+'" width="40px" height="40px">'+
		        '<p class="chat-nick">'+user.nickname+'</p>'+
			    '<p class="chat-content ">'+info.text+'</p>'+
			'</div>';
		}else if(flag == 'system'){
			str = '<div class="chat-time">'+
					'<span>14:26</span>'+
				'</div>';
		}
		return str;
	}
	/**
	 * 客户端系统提示信息
	 * 包括建立连接
	 */
	function clientTipInfo(info){
		var str = '<div class="chat-time">'+info+'</div>';
		$('#panelChatContainer').append(str)
	}
	/**
	 * localstorage 查询刷新time
	 * 返回布尔值
	 * true为超时，需要刷新
	 * false为未超时，不做处理
	 */
	function checkStorageTime(){
		var t = module.arginfo.refreshtime;
		var nt = new Date().getTime();
		var value = window.localStorage.getItem(module.arginfo.timeName);
		value = JSON.parse(value);
		if(nt - value.refti > t){
			return true;
		}else{
			return false;
		}
	}
	/**
	 * 判断刷新时间
	 * 超时，对礼包和聊天进行内容重置
	 */
	function detectRefretime(){
		if(checkStorageTime()){
			//超过时间,设置底部tab标识为un
			$('#t-g,#t-t').attr('data-value','un');
			//清空内容，data-numn设为1
			$('#gift .list .item').remove();
			$('#talk .list .n-item').remove();
			$('#talk .list .more-info').attr('data-num','1');
			return true;
		}else{
			//没超过时间，不处理
			return false;
		}
	}
	/**
	 * 设置自动刷新时间
	 * 如果超过该时间，聊天选项和礼包选项进行重新加载
	 * 记录每次点击的tab，聊天&礼包
	 * @param {Object} panel
	 */
	function setStorageTime(){
		var value = window.localStorage.getItem(module.arginfo.timeName),
			name = module.arginfo.timeName,
			time = new Date().getTime(),
			panel = 'refti';
		value = value?value:{};
		if(typeof(value) == 'string'){
			value = JSON.parse(value);
		}
		value[panel] = time;
		value = JSON.stringify(value);
		window.localStorage.setItem(module.arginfo.timeName,value)
	}
	return module;
});