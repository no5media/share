/**
 * 初始化浮标，以及浮标的按钮点击等
 */
define(['jquery','common/eventBase','view/gamemodal','libs/jquery.cookie'],function($,EventBase,GameModal){
	var module = {
		//一些基础信息
		dataInfo:{
			userTerminal:'',				//用户使用的终端情况
			appKey:''
		},
		//初始化浮标,args 判断是否需要chatroom
		init:function(){
			initInfo();
			//判断是否需要显示浮标
			deduceModalBtn();
		},
		//初始化websocket，仅仅初始化连接，不进行聊天通讯
		initWschat:function(){
			GameModal.initWschat();
		},
		chatroomEnable:false
	};
	var modalBtnHtml = EventBase.loadTem('/spf/mweb/html/modalMainTem.html');
	//初始化一些info信息
	function initInfo(){
		module.dataInfo.userTerminal = EventBase.userTerminal();
		module.dataInfo.appKey = EventBase.getParameters('appKey');
	}
	//判断浮标是否需要显示
	function deduceModalBtn(){
		var ua = module.dataInfo.userTerminal;
		var flag = EventBase.getParameters('mk');
		if(ua == 'androidclient' || ua == 'iphoneclient'){
			$('#moban').remove();	//去除浮层节点
			$('#button').remove();	//去除浮层图标
		}else{
			if(flag == 'mk'){
				handleStoragePosition();
				handleFunc();
				setCookie();
			}else{
				$('#moban').remove();	//去除浮层节点
				$('#button').remove();	//去除浮层图标
			}
		}
	}
	//判断并显示最后一次浮标的位置
	function handleStoragePosition(){
		var gp = window.localStorage.gameposition;
		var x , y;
		if(!!gp){
			gp = JSON.parse(gp);
			for(var i = 0,len = gp.length; i<len;i++ ){
				if(gp[i].appkey == module.dataInfo.appKey){
					x = gp[i].x;
					y = gp[i].y;
					$("#button").css('left', x).css('top', y).css('right','auto').css('bottom','auto');
					break;
				}
			}
		}
		$('#button').css('display','block')
	}
	
	//浮标按钮绑定拖拽事件
	function handleFunc(){
		$("#button").on('touchstart', function(e){
            	//e.preventDefault();
                $("#button").css('opacity',1);
                if(e.type == 'mousedown'){
                    iX = e.originalEvent.clientX - $(this)[0].offsetLeft;
                    iY = e.originalEvent.clientY - $(this)[0].offsetTop;
                }else{
                    iX = e.originalEvent.targetTouches[0].clientX - $(this)[0].offsetLeft;
                    iY = e.originalEvent.targetTouches[0].clientY - $(this)[0].offsetTop;
                }
         })
        .on('touchmove', function(e){
                e.preventDefault();
                $("#button").css('opacity',1);
                if(e.type == 'mousemove'){
                    var x = e.originalEvent.clientX - iX;
                    var y = e.originalEvent.clientY - iY;
                }else{
                    var x = e.originalEvent.targetTouches[0].clientX - iX;
                    var y = e.originalEvent.targetTouches[0].clientY - iY;
                }
                var nHeight = document.documentElement.clientHeight - parseInt($("#button").css('height'));
                var hWidth = document.documentElement.clientWidth - parseInt($("#button").css('width'));
                y = y < 0 ? 0 : y;
                y = y > nHeight ?  nHeight : y;
                x = x < 0 ? 0 : x;
                x = x > hWidth ? hWidth : x;
                $("#button").css('left', x).css('top', y).css('right','auto').css('bottom','auto');
        })
        .on('touchend', function(e){
        	if(e.type == 'mouseup'){
            }else{
                var x = e.currentTarget.offsetLeft;
                var y = e.currentTarget.offsetTop;
            }
            if(window.localStorage){
            	var arr = {
            		'appkey':module.dataInfo.appKey,
            		'x':x,
            		'y':y
            	};
            	handleStorage(arr);
            }
            //e.preventDefault();
        });
	    $('#button').on('click',function(e){
	        e.preventDefault();
	        getshow();
	        $('#button .point').css('display','none');
	    });
	    $('#modal-left').on('click',function(){
	    	getout();
	    })
	}
	
	//获取localstorage
	function handleStorage(item){
		var gpo;
		if(!!(window.localStorage.gameposition)){
        	gpo = window.localStorage.gameposition;
        	gpo = JSON.parse(gpo);
    	}else{
    		gpo = [];
    	}
    	updateStorageArray(gpo,item);
	}
	
	//处理localstorage的数组,有该内容则替换，没有则添加
	function updateStorageArray(gpo,item){
		var mark = false;
		if(gpo.length != 0){
			for(var i = 0,len = gpo.length; i < len; i++){
				if(gpo[i].appkey == item.appkey){
					gpo[i].x = item.x;
					gpo[i].y = item.y;
					mark = true;
					break;
				}
			}
		}
		if(!mark){
			gpo.push(item);
		}
		gpo = JSON.stringify(gpo);
    	window.localStorage.setItem('gameposition',gpo);
	}
	
	//浮层显示
	function getshow(){
		if(GameModal.arginfo.userInitFlag){
			GameModal.init();
			$('.moban').fadeIn();
		}else{
			var str = modalBtnHtml.find('#modalMainTem').html();
			$('#modalframe').html(str);
			GameModal.init();
	        $('.moban').fadeIn();
		}
		//chatroomEnable 不需要聊天室
		$('#t-c').remove();
		$('#base-menu .t').addClass('pure-u-1-2');
//		if(!module.chatroomEnable){
//			$('#t-c').remove();
//			$('#base-menu .t').addClass('pure-u-1-2');
//		}else{
//			GameModal.noticeServer(true);
//		}
    }
	//浮层消失
    function getout(){
    	$('.moban').fadeOut();
    	//判断是否有聊天室&浮层关闭，不在接受消息
//  	if(!!module.chatroomEnable){
//	    	GameModal.noticeServer(false);
//		}
    }
    //请求浮层里面的东西
	//动态创建frame 获取cookie
	function setCookie(){
		var url = 'http://wx.1758.com/game/h5/page/smallforcookie.htm?'+new Date().getTime();
	    var giframe = document.createElement('iframe');
	    giframe.src = url;
	    giframe.style.display = 'none';
	    document.body.appendChild(giframe);
	}
	
	window.addEventListener('message',function(evt){
		if(evt.origin == 'http://wx.1758.com'){
			var datacookie = evt.data;
			$.removeCookie('wy_user', { path: '/' });
			if(datacookie.code == 1){
				if(!!datacookie.args){
					datacookie.args = decodeURIComponent(datacookie.args);
					$.cookie('wy_user',datacookie.args,{ expires: 9999, path: '/' });
				}
			}
		}
	},false);
	return module;
});