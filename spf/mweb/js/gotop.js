$(function(){
	//实现回到顶部的功能
	FastClick.attach(document.body);
	$('#test').on('click',function(event) {
		/* Act on the event */
		$.post('http://42.96.193.47:9110/game/tapi/share/tapp',{
			'uid':20135,
			'appid':210541
		},function(data){
			console.log(data);
		},'jsonp');
	});

	//实现
	$.fn.extend({
		goTop: function(){
			var $this = $(this);
			$this.css({
				'position':'fixed',
				'bottom':'10px',
				'right':'3px',
				'display':'none'
			})
			$(window).scroll(function(e){
				if($(window).scrollTop() > 100 ){
					$this.fadeIn(1000);
				}else{
					$this.fadeOut(1000);
				}
			});
			$this.on({
				"click": function()	{
					$('body,html').animate({scrollTop: 0},1000);
				}
			}); 
		}
	});

	$('#totop').goTop();
	
})