/**
 * 定义常用事件
 */
define(['jquery'],function($){
	var obj = {
		getParameters:function(url,name){
			if(arguments.length == 1){
				name = url;
				url = window.location.href;
			}
			//正则验证
			 var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
		     var r = url.substr(url.indexOf('?')).substr(1).match(reg);
		     if(r!=null){
		     	return  decodeURIComponent(r[2]);
		     } 
		     return '';
		},
		//在最大最小值之间获取个随机数
		getRandomNun : function (Min,Max){
	        var range = Max-Min;
	        var rand = Math.random();
	        return (Min+Math.floor(rand * range))
    	},
    	//加载游戏地址
		loadPage:function (gameurl){
			var gframe = window.document.getElementById('gameframe');
			gframe.src= gameurl;
			if (gframe.attachEvent) {
		        gframe.attachEvent('onload', hideLoading);
		    } else {
		        gframe.onload  = hideLoading;
		    }
		},
		//设置title
		setTitle:function (htmlTitle){
			document.title= htmlTitle;
		},
		//得到随机分享语
		randomShareContent: function (shareText,gid){
			var textarray,
				len,
				num;
			textarray = shareText;
			len = textarray.length;
			var conApp = {};
			if(len > 0){
				num = this.getRandomNun(0,len);
				conApp.desc = textarray[num].sharedesc;
				conApp.title = textarray[num].sharetitle;
				conApp.imgUrl = textarray[num].shareimg;
				if(textarray[num].sharelink.indexOf('?') >-1){
					conApp.link = textarray[num].sharelink +'&share='+gid;
				}else{
					conApp.link = textarray[num].sharelink+'?share'+gid;
				}
				conApp.sid = textarray[num].sid;
				return conApp;
			}else{
				return;
			}
		},
		getRandomNun:function(Min,Max){
			var k = Max-Min;
			var rand = Math.random();
			return (Min+Math.floor(rand * k))
		},
		//分享成功向cp发送请求
		//该方法调用方法通知cp
	    exec_iframe: function (shareType,cpShareUrl){  
	        if(typeof(exec_obj)=='undefined'){  
	            exec_obj = document.createElement('iframe');  
	            exec_obj.name = 'tmp_frame';  
	            exec_obj.src = cpShareUrl+'?shareType='+shareType;  
	            exec_obj.style.display = 'none';  
	            document.body.appendChild(exec_obj);  
	        }else{  
	            exec_obj.src = cpShareUrl+'?shareType='+shareType+'&' + Math.random();  
	        }  
	    },
	    //判断用户客户端信息
	    userTerminal:function(){
	    	var ua = window.navigator.userAgent.toLowerCase();
			if(ua.match(/MicroMessenger/i) == 'micromessenger' && !(ua.match(/dwjia/i) == 'dwjia')){
				return 'weixin';
			}else if(ua.match(/dwjia/i) == 'dwjia'){
				if(ua.match(/iPhone/i) == 'iphone' || ua.match(/ios/i) == 'ios'){
					return 'iphoneclient';
				}else if(ua.match(/Android/i) == 'android'){
					return 'androidclient';
				}
			}else{
				var fork = this.getParameters('fork');
				if(fork == 4){
					return 'androidclient';
				}else if(fork == 5){
					return 'iphoneclient';
				}
				return 'others';
			}
	    }
	};
	return obj;
})