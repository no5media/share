function connectWebViewJavascriptBridge(callback) {
    if (window.WebViewJavascriptBridge) {
        callback(WebViewJavascriptBridge)
    } else {
        document.addEventListener('WebViewJavascriptBridgeReady', function() {
            callback(WebViewJavascriptBridge)
        }, false)
    }
}
var G_ShareFunc = {shareText:''};
(function(){
	//引用fashclick.js文件，消除click事件在移动设备上的300ms的延迟
	FastClick.attach(document.body);
	var option = GC.option;
	var GameList = {
		dmyx:{'key':'dmyx','url':'http://wx.topyouxi.cn/dmyx','appkey':''}
	};
	var shareUrl = GC.share_url;
	var len = shareUrl.length;
	var num = getRandomNun(0,len);
	option.link = shareUrl[num];
	var cpShareUrl = GC.cp_url;
	var wxShareData = {
		flagc: false,
		flagt: false,
		cpShareUrl: cpShareUrl
	};
	G_ShareFunc.wxShareData = wxShareData;
	var USERINFO= {
		gid:''
	}
	var shareText = {
		'dmyx_text':[
		{
			img:'http://images.1758.com/article/image/2015/07/31/51381438341483960.png',
			title:'加入我的战队吧~包吃住，还有妹子！嗯？【盗墓英雄·即点即玩】',
			desc:'盗墓英雄-1758.com',
			link:'http://game.g1758.cn/dmyx?chn=dmyx_share_43',
			sid:'43'
		},{
			img:'http://images.1758.com/article/image/2015/07/31/12681438341548348.png',
			title:'在这个草泥马的时代里，对凡事都要抱着去你妹的心态。【盗墓英雄·即点即玩】',
			desc:'盗墓英雄-1758.com',
			link:'http://game.g1758.cn/dmyx?chn=dmyx_share_39',
			sid:'39'
		},{
			img:'http://images.1758.com/article/image/2015/07/31/8991438341622034.png',
			title:'城池我丢得起，女人不能丢。【盗墓英雄·即点即玩】',
			desc:'盗墓英雄-1758.com',
			link:'http://game.g1758.cn/dmyx?chn=dmyx_share_40',
			sid:'40'
		},{
			img:'http://images.1758.com/article/image/2015/07/31/7281438341668095.png',
			title:'老板，给我来5斤黄瓜使！【盗墓英雄·即点即玩】',
			desc:'盗墓英雄-1758.com',
			link:'http://game.g1758.cn/dmyx?chn=dmyx_share_41',
			sid:'41'
		},{
			img:'http://images.1758.com/article/image/2015/07/31/75961438341720358.png',
			title:'过儿！你的右臂呢0.0？【盗墓英雄·即点即玩】',
			desc:'盗墓英雄-1758.com',
			link:'http://game.g1758.cn/dmyx?chn=dmyx_share_42',
			sid:'42'
		}],
		'jswc_text':[{
			img:'http://images.1758.com/article/image/2015/07/17/51321437131909148.png',
			title:'不能同生，只愿同死，千秋万世，至死不渝！【僵尸围城·即点即玩】',
			desc:'僵尸围城-1758.com',
			link:'http://game.g1758.cn/jswc?chn=jswc_share_9',
			sid:'9'
		},{
			img:'http://images.1758.com/article/image/2015/07/17/34431437131923506.png',
			title:'从现在开始，没有我的允许你不许死！【僵尸围城·即点即玩】',
			desc:'僵尸围城-1758.com',
			link:'http://game.g1758.cn/jswc?chn=jswc_share_10',
			sid:'10'
		},{
			img:'http://images.1758.com/article/image/2015/07/17/90741437132020983.png',
			title:'不是你开的太快，而是你飞得太低！【僵尸围城·即点即玩】',
			desc:'僵尸围城-1758.com',
			link:'http://game.g1758.cn/jswc?chn=jswc_share_11',
			sid:'11'
		},{
			img:'http://images.1758.com/article/image/2015/07/17/3551437132032978.png',
			title:'别跟我谈兴趣，谈钱！【僵尸围城·即点即玩】',
			desc:'僵尸围城-1758.com',
			link:'http://game.g1758.cn/jswc?chn=jswc_share_12',
			sid:'12'
		},{
			img:'http://images.1758.com/article/image/2015/07/17/2121437132044262.png',
			title:'绿巨人、鹰眼、娜塔莎...幻觉，绝对是幻觉  【僵尸围城·即点即玩】',
			desc:'僵尸围城-1758.com',
			link:'http://game.g1758.cn/jswc?chn=jswc_share_13',
			sid:'13'
		},{
			img:'http://images.1758.com/article/image/2015/07/17/49261437132059449.png',
			title:'你敢划破我的脸，我就跟你拼命。【僵尸围城·即点即玩】',
			desc:'僵尸围城-1758.com',
			link:'http://game.g1758.cn/jswc?chn=jswc_share_14',
			sid:'14'
		}],
		'ygys_text':[{
			img:'http://images.1758.com/article/image/2015/08/14/61731439545266128.png',
			title:'放开那苍井峰，让我来。【愚公移山·即点即玩】',
			desc:'愚公移山-1758.com',
			link:'http://www.yadian.xyz/ygys2?chn=ygys_share_15',
			sid:'15'
		},{
			img:'http://images.1758.com/article/image/2015/08/14/15111439545285091.png',
			title:'草丛轮你知道吗？人类文明从此开始！【愚公移山·即点即玩】',
			desc:'愚公移山-1758.com',
			link:'http://www.yadian.xyz/ygys2?chn=ygys_share_16',
			sid:'16'
		},{
			img:'http://images.1758.com/article/image/2015/08/14/43061439545320583.png',
			title:'愚公的繁殖速度岂是人类能理解的了？【愚公移山·即点即玩】',
			desc:'愚公移山-1758.com',
			link:'http://www.yadian.xyz/ygys2?chn=ygys_share_17',
			sid:'17'
		}],
		'ygys':[{
			img:'http://images.1758.com/article/image/2015/08/14/27311439545359360.png',
			title:'和你造过人之后，其他人都成了将就，而我不愿将就！[愚公移山.即点即玩]',
			desc:'我要和你生猴子-1758.com',
			link:'http://i1758.cn/ygys2?chn=ygys_share_47',
			sid:'47'
		},{
			img:'http://images.1758.com/article/image/2015/08/14/25441439545800716.png',
			title:'人与人之间最基本的信任，就是一直和TA生孩子！[愚公移山.即点即玩]',
			desc:'我要和你生猴子-1758.com',
			link:'http://i1758.cn/ygys2?chn=ygys_share_48',
			sid:'48'
		},{
			img:'http://images.1758.com/article/image/2015/08/14/71141439545818463.png',
			title:'繁衍后代，人人有责。[愚公移山.即点即玩]',
			desc:'我要和你生猴子-1758.com',
			link:'http://i1758.cn/ygys2?chn=ygys_share_49',
			sid:'49'
		},{
			img:'http://images.1758.com/article/image/2015/08/14/13051439545534619.png',
			title:'基友一生一起走，一发不够，再来一发！[愚公移山.即点即玩]',
			desc:'我要和你生猴子-1758.com',
			link:'http://i1758.cn/ygys2?chn=ygys_share_50',
			sid:'50'
		}],
		'glqx_text':[{
			img:'http://images.1758.com/article/image/2015/08/25/81991440503994075.png',
			title:'我的麒麟臂早已饥渴难耐',
			desc:'古龙群侠传',
			link:'http://game.g1758.cn/glqx/index.htm?chn=glqx_share_60',
			sid:'60'
		},{
			img:'http://images.1758.com/article/image/2015/08/25/57841440504006341.png',
			title:'秃驴，敢跟贫道抢师太',
			desc:'古龙群侠传',
			link:'http://game.g1758.cn/glqx/index.htm?chn=glqx_share_61',
			sid:'61'
		}]
	};
	randomGetShareContent();
	if(is_ios_client()){
		connectWebViewJavascriptBridge(function(bridge) {
			bridge.init(function(message, responseCallback) {
				if (responseCallback) {
					responseCallback("")
				}
			})
			window.ios = bridge;
			wxConfig(option);
			ios.registerHandler('clientSuccessInfo', function(data, responseCallback) {
			    clientSuccessInfo(data);
			});
			ios.registerHandler('clientGetShareInfo', function(data, responseCallback) {
			    clientGetShareInfo();
			});
		});
	}else{
		wxConfig(option);
	}
	getUserInfo();
	//微信的分享功能
	function wxConfig(option){
		//微信情况
		var defaults = {
			title: '1758微游戏，即点即玩',
			desc: '还在刷朋友圈吗？快来跟我一起玩游戏吧~ ~ ~ ~',
			link: 'http://wx.1758.com',
			imgUrl: 'http://images.1758.com/images/1758new.png',
			type: 'link',
			dataUrl: '',
			success: function(){},
			cancel: function() {}
		};
		wxShareData = $.extend({},defaults,option,wxShareData);
		var hr = location.href;
		hr = encodeURIComponent(hr);
		var postUrl = 'http://h5.g1758.cn/game/platform/v1.0/weixin/js/config?pageUrl='+hr;
		$.get(postUrl, function(data){
			var conf=data;
	        wx.config({
	            debug: false,
	            appId: conf.appId,
	            timestamp: conf.timestamp,
	            nonceStr: conf.nonceStr,
	            signature: conf.signature,
	            jsApiList: conf.jsApiList
	        });
	        wxShareData.flagc = true;
	        
	        G_ShareClient.init();
	        //执行该函数确保gid无法获取的情况下 照常执行。
	        changeWxShareInfo();
			//安卓添加到桌面
			if(is_android_client()){
				G_ShareClient.androidPutDesktop();
			}
	        wx.error(function(res){ });
	        
		});
	}
	
	//根据不同的shareType进行提示
	function exec_iframe(shareType,cpShareUrl){
	    if(typeof(exec_obj)=='undefined'){  
	        exec_obj = document.createElement('iframe');  
	        exec_obj.name = 'tmp_frame';  
	        exec_obj.src = cpShareUrl+'?shareType='+shareType;  
	        exec_obj.style.display = 'none';  
	        document.body.appendChild(exec_obj);
	    }else{  
	        exec_obj.src = cpShareUrl+'?shareType='+shareType+'&' + Math.random();
    	}  
	}
	//获取url中的参数
	function getParameters(url,name){
		//正则验证
		 var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
	     //var r = window.location.search.substr(1).match(reg);
	     var r = url.substr(url.indexOf('?')).substr(1).match(reg);
	     if(r!=null){
	     	//return  unescape(r[2]);
	     	return  decodeURIComponent(r[2]);
	     } 
	     return '';
	}
	
	//加载iframe框架中的src地址
	function loadPage(gameurl){
		var iframes = document.getElementsByTagName('iframe');
		iframes['cpframe'].src= gameurl;
	}

	//以下内容临时用到
	
	//根据key信息获取game的所有需要信息
	function gameInfo(key){
		var data = GameList[key];
		return data;
	}
	//根据appKey信息后台请求
	function getInfo(){
		$.post(url,function(data){

		},'json');	
	}
	//获取iframe里面的游戏地址的appkey
	function getIframeUrlAppkey(){
		var url = document.getElementById('cpgame').src;
		var appkey = getParameters(url,'appKey');
		return appkey;
		
	}
	//设置title
	function setTitle(htmlTitle){
		document.title= htmlTitle;
	}
	//取随机数参数为起始数和终止数
	function getRandomNun(Min,Max){
		var range = Max-Min;
		var rand = Math.random();
		return (Min+Math.floor(rand * range))
	}
	//给服务器发送分享成功后的用户id
	/*
	function sendServerShareUserInfo(){
		$.ajax('',{
			'gid': 'fb0d05f36b12c8738457670c6fb735e7'
		},function(){

		},'jsonp');
	}*/

	//获取用户的uid信息
	function getUserInfo(){
		var userInfo;
		//数据页面地址，主要是通过改地址获取gid
		var dataurl = 'http://wx.1758.com/game/platform/user/shareIframeNotify';
		//通过window.name属性进行跨域的数据读取
		var state = 0, 
	    giframe = document.createElement('iframe'),
	    loadfn = function() {
	        if (state === 1) {
	            var data = giframe.contentWindow.name;    // 读取数据
	            userinfo = data;
	            USERINFO.gid = data;
	            //为客户端分享添加数据
	            G_ShareFunc.wxShareData.gid = data;
	            G_ShareClient.initForGid(); 
	            var linkurl = option.link;
	            if(linkurl.indexOf("?")>-1){
	            	wxShareData.link = wxShareData.link + '&gid='+ userinfo;
	            	option.link = option.link + '&gid='+ userinfo;
	            }else{
	            	wxShareData.link = wxShareData.link + '?gid='+ userinfo;
	            	option.link = option.link + '?gid='+ userinfo;
	            }
	            changeWxShareInfo();
	            document.body.removeChild(giframe);

	        } else if (state === 0) {
	            state = 1;
	            giframe.contentWindow.location = "http://h5.g1758.cn/spf/blank.html";    // 设置的代理文件
	        }  
	    };
	    giframe.src = dataurl;
	    giframe.style.display = 'none';
	    if (giframe.attachEvent) {
	        giframe.attachEvent('onload', loadfn);
	    } else {
	        giframe.onload  = loadfn;
	    }
	    document.body.appendChild(giframe);

	}
	//微信的注入信息
	function changeWxShareInfo(){
		randomGetShareContent();
		wx.ready(function(){
        	//分享给朋友
	        wx.onMenuShareAppMessage({
	            title: wxShareData.ydesc, // 分享标题
	            desc: wxShareData.ytitle, // 分享描述
	            link:  wxShareData.ylink + '&type=appmessage',
	            imgUrl: wxShareData.yimgUrl,
	            type: wxShareData.type,
	            dataUrl: wxShareData.dataUrl,
	            success: function(){
	            	exec_iframe('appmessage',cpShareUrl);
	            	changeWxShareInfo();
	            	sendMessageServer('appmessage',0,wxShareData.ysid);
	            },
	            cancel: function(){
	            	changeWxShareInfo();
	            	sendMessageServer('appmessage',1,wxShareData.ysid);
	            }
	        });

	        //分享到朋友圈
	        wx.onMenuShareTimeline({
	            title: wxShareData.title, // 分享标题
	            link:  wxShareData.link + '&type=timeline',
	            imgUrl: wxShareData.imgUrl,
	            success: function(){
	            	exec_iframe('timeline',cpShareUrl);
	            	changeWxShareInfo();
	            	sendMessageServer('timeline',0,wxShareData.sid);
	            },
	            cancel: function(){
	            	changeWxShareInfo();
	            	sendMessageServer('timeline',1,wxShareData.sid);
	            }
	        });
	        //分享到qq好友
	        wx.onMenuShareQQ({
			    title: wxShareData.ydesc, // 分享标题
			    desc: wxShareData.ytitle, // 分享描述
			    link: wxShareData.ylink + '&type=qq', // 分享链接
			    imgUrl: wxShareData.yimgUrl, // 分享图标
			    success: function(){
			    	exec_iframe('qq',cpShareUrl);
			    	changeWxShareInfo();
			    	sendMessageServer('qq',0,wxShareData.ysid);
			    },
			    cancel: function(){
			    	changeWxShareInfo();
			    	sendMessageServer('qq',1,wxShareData.ysid);
			    }
			});
        });
	}

	//随机分享语录
	function randomGetShareContent(){
		var flag = GC.random_text;
		var textarray,len,num;
		if(!!flag){
			textarray = shareText[flag];
			G_ShareFunc.shareText = textarray;
			if(flag == 'ygys_text'){
				G_ShareFunc.shareTextFriend = shareText['ygys'];
				G_ShareFunc.wxShareData.flagc = true;
			}
			len = textarray.length;
			num = getRandomNun(0,len);
			option.title = shareText[flag][num].title;
			option.imgUrl = shareText[flag][num].img;
			option.desc = shareText[flag][num].desc;
			//option.link = shareText[flag][num].link;

			wxShareData.title = shareText[flag][num].title;
			wxShareData.imgUrl = shareText[flag][num].img;
			wxShareData.desc = shareText[flag][num].desc;
			

			if(!!USERINFO.gid){
				wxShareData.link = shareText[flag][num].link + '&gid=' + USERINFO.gid;
				option.link = shareText[flag][num].link + '&gid=' + USERINFO.gid;
			}else{
				wxShareData.link = shareText[flag][num].link;
				option.link = shareText[flag][num].link;
			}
			wxShareData.sid = shareText[flag][num].sid;
			randomForFriend(flag);
		}
		
	}

	//每次分享成功之后执行一次发送给服务器  0成功  1取消
	function sendMessageServer(name,flag,sid){
		var appkey = getIframeUrlAppkey();
		$.get('http://wx.1758.com/game/platform/user/logShareAction',{
			'gid': USERINFO.gid,
			'appKey': appkey,
			'type': name,
			'code': flag,
			'sid': sid
			},function(data){
				if(data.msg == 'ok'){
					return;
				};
			},'jsonp')
	}


	function randomForFriend(flag){
		if(flag == 'ygys_text'){
			var ftext = shareText['ygys'];
			var flen = ftext.length;
			var fnum = getRandomNun(0,flen);

			wxShareData.ytitle = ftext[fnum].title;
			wxShareData.yimgUrl = ftext[fnum].img;
			wxShareData.ydesc = ftext[fnum].desc;
			wxShareData.ysid = ftext[fnum].sid;

			if(!!USERINFO.gid){
				wxShareData.ylink = ftext[fnum].link + '&gid=' + USERINFO.gid;
			}else{
				wxShareData.ylink = ftext[fnum].link;
			}
		}else{
			wxShareData.ytitle = wxShareData.title;
			wxShareData.yimgUrl = wxShareData.imgUrl;
			wxShareData.ydesc = wxShareData.desc;
			wxShareData.ysid = wxShareData.sid;
			wxShareData.ylink = wxShareData.link;
		}
	}

	function is_ios_client(){
		var ua = navigator.userAgent.toLowerCase();
		if(ua.match(/ios/i) == 'ios' && (ua.match(/dwjia/i) == "dwjia")){
			return true;
		}else{
			return false;
		}
	}
	function is_android_client(){
		var ua = navigator.userAgent.toLowerCase();
		if(ua.match(/Android/i) == 'android' && (ua.match(/dwjia/i) == "dwjia")){
			return true;
		}else{
			return false;
		}
	}
}());
var G_ShareClient = {
	shareAppmessageInfo: {
        'info':{
            'title':'1758微游戏·即点即玩',
            'desc':'还在刷朋友圈吗？快来跟我一起玩游戏吧~ ~ ~ ~',
            'link':'wx.1758.com',
            'imgUrl':'http://images.1758.com/images/1758_icon.png'
        },
        'sid':''},
    shareTimelineInfo: {
        'info':{
            'title':'还在刷朋友圈吗？快来跟我一起玩游戏吧~ ~ ~ ~',
            'desc':'1758微游戏·即点即玩',
            'link':'wx.1758.com',
            'imgUrl':'http://images.1758.com/images/1758_icon.png' 
        },
        'sid':''},
    //分享成功之后发送给服务器name为appmessage timeline qq flag为0成功，1失败
    sendMessageServer: function (name,flag){
        var appkey = this.getParameters(location.href,'appKey');
        var sid = '';
        if (name == 'appmessage') {sid = this.shareAppmessageInfo.sid};
        if (name == 'timeline'){ sid = this.shareTimelineInfo.sid};
        $.get('http://wx.1758.com/game/platform/user/logShareAction',{
            'gid': G_ShareFunc.wxShareData.gid,
            'appKey': appkey,
            'type': name,
            'code': flag,
            'sid': sid
            },function(data){
                if(data.msg == 'ok'){
                    return;
                };
            },'jsonp');
    },
    getRandomNun : function (Min,Max){
        var range = Max-Min;
        var rand = Math.random();
        return (Min+Math.floor(rand * range))
    },
    //该方法调用方法通知cp
    exec_iframe: function (shareType,cpShareUrl){  
        if(typeof(exec_obj)=='undefined'){  
            exec_obj = document.createElement('iframe');  
            exec_obj.name = 'tmp_frame';  
            exec_obj.src = cpShareUrl+'?shareType='+shareType;  
            exec_obj.style.display = 'none';  
            document.body.appendChild(exec_obj);  
        }else{  
            exec_obj.src = cpShareUrl+'?shareType='+shareType+'&' + Math.random();  
        }  
    },
    //客户端的随机分享语
	randomShareClient: function (){
		var textarray,
			len,
			lenf,
			num,
			textFriend;
		textarray = G_ShareFunc.shareText;
		if(GC.random_text != 'ygys_text'){
			G_ShareFunc.shareTextFriend = G_ShareFunc.shareText;
		}
		textFriend = G_ShareFunc.shareTextFriend;
		len = textarray.length;
		lenf = textFriend.length;
		var conApp = {};
		var conTim = {};
		if(len > 0){
			num = this.getRandomNun(0,len);
			conApp.desc = textFriend[num].title;
			conApp.title = textFriend[num].desc;
			conApp.imgUrl = textFriend[num].img;
			conApp.link = textFriend[num].link + '&stype=appmessage';
			this.shareAppmessageInfo.info = conApp;
			this.shareAppmessageInfo.sid = textFriend[num].sid;

			conTim.title = textarray[num].title;
			conTim.desc = textarray[num].desc;
			conTim.imgUrl = textarray[num].img;
			conTim.link = textarray[num].link + '&stype=timeline';
			this.shareTimelineInfo.info = conTim;
			this.shareTimelineInfo.sid = textarray[num].sid;
		}
	},
	addParameter:function(){
		//给link地址添加share=
		var sharegid = this.getParameters(location.href,'share');
		if(!sharegid){
			if(G_ShareFunc.wxShareData.gid){
				this.shareAppmessageInfo.info.link += '&share=' + G_ShareFunc.wxShareData.gid;
				this.shareTimelineInfo.info.link += '&share=' + G_ShareFunc.wxShareData.gid;
			}
		}
	},
	getParameters: function (url,name){
        //正则验证
         var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
         //var r = window.location.search.substr(1).match(reg);
         var r = url.substr(url.indexOf('?')).substr(1).match(reg);
         if(r!=null){
            //return  unescape(r[2]);
            return  decodeURIComponent(r[2]);
         } 
         return '';
    },
    //格式化json对象，并且调用native对象注入
    pareShareText: function(){
    	this.shareAppmessageInfo.info = JSON.stringify(this.shareAppmessageInfo.info);
		this.shareTimelineInfo.info = JSON.stringify(this.shareTimelineInfo.info);
		if(typeof(weixinBridge) != 'undefined'){
			if(weixinBridge.invokeWx){
				weixinBridge.invokeWx(this.shareAppmessageInfo.info, this.shareTimelineInfo.info);
			}
		}else{
			if(window.ios){
				ios.callHandler('shareTimeline', G_ShareClient.shareTimelineInfo.info);
				ios.callHandler('shareMessage', G_ShareClient.shareAppmessageInfo.info);	
			}
		}
       	
    },
    //分享成功之后调用随机分享语
    successRandomText: function(){
    	//分享成功之后id和分享语肯定已经存在，故直接执行下面方法即可。
    	this.randomShareClient();
    	this.addParameter();
    },
    //得到分享语之后进行初始化
	init: function(){
		this.randomShareClient();
		if(!!G_ShareFunc.wxShareData.gid){
			this.addParameter();
			//if(typeof(weixinBridge) != 'undefined'){
				//if(weixinBridge.invokeWx){
					this.pareShareText();
		        //}
			//}
		}
		
	},
	//得到gid的时候进行初始化。
	initForGid: function(){
		if(G_ShareFunc.wxShareData.flagc){
            this.shareAppmessageInfo.info.link += '&share=' + G_ShareFunc.wxShareData.gid;
			this.shareTimelineInfo.info.link += '&share=' + G_ShareFunc.wxShareData.gid;
			//if(typeof(weixinBridge) != 'undefined'){
				//if(weixinBridge.invokeWx){
					this.pareShareText();
		        //}
			//}
		}
		
	},
	//androlid 放到桌面
	androidPutDesktop:function(){
		$.get('http://wx.1758.com/game/api/app/getAppInfoJsonP',{
			'appKey': '34ab9f1f07f2731f4c3586e282a551ba'
		},function(data){
			if(data.code == 0){
				var jskk = {
					'gameName':data.wxApp.name,
					'gameUrl':data.wxApp.backGameUrl,
					'imgUrl':data.wxApp.iconUrl,
					'tp':''
				};
				jskk = JSON.stringify(jskk);
				try{
					android_tw_system.addDeskIconNew(jskk);
				}catch(e){
					//TODO handle the exception
				}
			}
		},'jsonp');
	}
};
//type的定义
//sendAppMessage    分享到微信好友
//shareTimeline     分享到微信朋友圈
//shareQQ       分享到qq好友
//shareWeiboApp     分享到微博
//shareQZone    分享到qq空间
//客户端分享成功之后native调用的方法。
function clientSuccessInfo(type){
    var _type='';
    type = type.toLowerCase();
    switch(type){
        case 'sendappmessage':
            _type = 'appmessage';
        break;
        case 'sharetimeline':
            _type = 'timeline';
        break;
        case 'shareqq':
            _type = 'qq';
        break;
        case 'shareweiboapp':
        	_type = 'weibo';
        break;
        case 'shareqzone':
        	_type = 'qzone';
        break;
    }
    //发送给cp，执行cp的回调函数
    G_ShareClient.exec_iframe(_type,G_ShareFunc.wxShareData.cpShareUrl);
    //随机分享语
    G_ShareClient.successRandomText();
    //发送给服务器记录信息
    G_ShareClient.sendMessageServer(_type,0);
    //向native注入信息，改变分享语内容。
    G_ShareClient.pareShareText();
}
function clientGetShareInfo(all){
    //该方法为native方法，两个参数 第一个为appmessage信息，第二个位timeline信息
    //两个都为json对象
    G_ShareClient.pareShareText();
}
//添加到桌面，Android回调方法
function clientdesktopInfo(){
	G_ShareClient.androidPutDesktop();
}
